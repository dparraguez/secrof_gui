<?php
//Funciones
function conexionPostgres()
{
    $conexion=pg_connect("host=localhost port=5432 dbname=APPMOVIL user=postgres password=postgres...") 
               or die ("Error de conexion. ".pg_last_error());
    return $conexion;
}

function agregarInspector($conexion, $nombres, $apellidos, $rut, $fono)
{
    $inspector = "INSERT INTO app_inspector (in_nombres, in_apellidos, in_rut, in_fono) 
                 VALUES ('".$nombres."','".$apellidos."','".$rut."','".$fono."') returning in_id";
    
    $resultado = pg_query($conexion, $inspector) or die("Error al agregar inspector".pg_last_error());

    $in_id = pg_fetch_row($resultado);
  
    return $in_id[0];
}

function agregarReporte($conexion,$descripcion,$fecha,$hora,$foto,$latitud,$longitud,$in_id)
{
    $fecha_hoy = date("Y-m-d H:i:s");

    $reporte = "INSERT INTO app_reporte (rp_descripcion,rp_fecha_app,rp_hora_app,
                 rp_foto,rp_latitud,rp_longitud,rp_estado,rp_fecha_hora_web,in_id ) 
                 VALUES ('".$descripcion."','".$fecha."','".$hora."','".$foto."','".$latitud."','".$longitud."','Recepcionado',
                         '".$fecha_hoy."','".$in_id."') returning rp_id";
    
    $resultado =pg_query($conexion, $reporte) or die("Error al agregar reporte".pg_last_error());

    $rp_id = pg_fetch_row($resultado);

    return $rp_id[0];
}

function consultarInspector($conexion, $rut)
{
    $inspector = "SELECT in_id FROM app_inspector WHERE in_rut ='".$rut."'";
    
    $resultado = pg_query($conexion, $inspector) or die("Error al consultar inspector".pg_last_error());  

    $in_id = pg_fetch_row($resultado);

    return $in_id[0];
}

function agregarFoto($foto,$nombre){

    $fecha_hoy= new DateTime(date("Y-m-d H:i"));
    $anio=$fecha_hoy->format('Y');
    $mes=$fecha_hoy->format('m');

    $data = file_get_contents($foto);
    $imageFile = imagecreatefromstring($data);

    $rutaAnio = "../backend/web/images/inspectores/".$anio;
    $rutaMes = $rutaAnio."/".$mes;
    
    if (!file_exists($rutaAnio)) {
        mkdir($rutaMes, 0777, true);   
    }
    else{
       if (!file_exists($rutaMes)) 
           mkdir($rutaMes, 0777, true);
    }
    $imageRuta2 = "../web/images/inspectores/".$anio."/".$mes."/".basename($nombre);
    $imageRuta = $rutaMes."/".basename($nombre); 
    $imageSave = imagepng($imageFile,$imageRuta,9);
    imagedestroy($imageFile);
    
    return $imageRuta2;
}

?>