<?php
//Funciones
function conexionPostgresGui()
{
    $conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...") 
               or die ("Error de conexion. ".pg_last_error());
    return $conexion;
}
function obtenerHorario($conexion, $latitud, $longitud){
    $horario = "SELECT * from zona_servicio where ST_Covers(geography::geometry, ST_GeomFromText('POINT($latitud $longitud)',4326)::geometry) limit 1";
    $resultado =pg_query($conexion, $horario) or die("Error en la consulta ".pg_last_error());
    $zona_servicio = pg_fetch_array($resultado);
    return $zona_servicio;
}

?>