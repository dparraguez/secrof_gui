<?php
//Funciones 
function conexionPostgres()
{
    $conexion=pg_connect("host=localhost port=5432 dbname=APPMOVIL user=postgres password=postgres...") 
               or die ("Error de conexion. ".pg_last_error());
    return $conexion;
}

function agregarUsurio($conexion, $nombres, $apellidos, $rut, $fono)
{
    $usuario = "INSERT INTO app_usuario (us_nombres, us_apellidos, us_rut, us_fono) 
                 VALUES ('".$nombres."','".$apellidos."','".$rut."','".$fono."') returning us_id";
    
    $resultado = pg_query($conexion, $usuario) or die("Error al agregar usuario".pg_last_error());

    $us_id = pg_fetch_row($resultado);
  
    return $us_id[0];
}

function agregarReclamo($conexion,$ti_id,$descripcion,$fecha,$hora,$foto,$latitud,$longitud,$prioridad,$us_id)
{
    $reclamo = "INSERT INTO app_reclamo (ti_id,re_descripcion,re_fecha_app,re_hora_app,
                 re_foto,re_latitud,re_longitud,re_prioridad,us_id ) 
                 VALUES (".$ti_id.",'".$descripcion."','".$fecha."','".$hora."','".$foto."','".$latitud."','".$longitud."',
                         '".$prioridad."','".$us_id."') returning re_id";
    
    $resultado =pg_query($conexion, $reclamo) or die("Error al agregar reclamo".pg_last_error());

    $re_id = pg_fetch_row($resultado);

    $es_id = consultarEstado($conexion);

    agregarReclamoEstado($conexion,$re_id[0],$es_id);

    return $re_id[0];
}


function agregarReclamoEstado($conexion,$re_id,$es_id)
{
    $fecha_hoy = date("Y-m-d H:i:s");
    $reclamo_estado = "INSERT INTO reclamo_estado (re_id,es_id,re_es_fecha) 
                      VALUES (".$re_id.",".$es_id.",'".$fecha_hoy."')";
    $resultado =pg_query($conexion, $reclamo_estado) or die("Error al agregar reclamo estado".pg_last_error());
}

function agregarSugerencia($conexion,$tipo, $descripcion,$fecha,$hora,$latitud,$longitud,$us_id)
{
    $fecha_hoy = date("Y-m-d H:i:s");

    $sugerencia = "INSERT INTO app_sugerencia (su_tipo,su_descripcion,su_fecha_app,su_hora_app,su_latitud,su_longitud,
                 su_estado,su_fecha_hora_web,us_id) 
                 VALUES ('".$tipo."','".$descripcion."','".$fecha."','".$hora."','".$latitud."','".$longitud."','Recepcionado',
                         '".$fecha_hoy."','".$us_id."') returning su_id";
    
    $resultado =pg_query($conexion, $sugerencia) or die("Error al agregar sugerencia".pg_last_error());
 
    $su_id = pg_fetch_row($resultado);

    return $su_id[0];
}

function consultarUsuario($conexion, $rut)
{
    $usuario = "SELECT us_id FROM app_usuario WHERE us_rut ='".$rut."'";
    
    $resultado = pg_query($conexion, $usuario) or die("Error al consultar usuario".pg_last_error());  

    $us_id = pg_fetch_row($resultado);

    return $us_id[0];
}

function consultarTipoReclamo($conexion, $tipo)
{
    $tipo_reclamo = "SELECT ti_id FROM web_tipo_reclamo WHERE ti_nombre ='".$tipo."'";
    
    $resultado = pg_query($conexion, $tipo_reclamo) or die("Error al consultar tipo reclamo".pg_last_error());  

    $ti_id = pg_fetch_row($resultado);

    return $ti_id[0];
}

function consultarEstado($conexion)
{
    $estado = "SELECT es_id FROM web_estado WHERE es_nombre ='Recepcionado'";
    
    $resultado = pg_query($conexion, $estado) or die("Error al consultar estado".pg_last_error());  

    $es_id = pg_fetch_row($resultado);

    if($es_id[0]!=0){

         return $es_id[0];
    }
    else{
         $estado = "INSERT INTO web_estado (es_nombre, es_descripcion) 
                    VALUES ('Recepcionado','Estado inicial del reclamo recibido') returning es_id";
    
         $resultado =pg_query($conexion, $estado) or die("Error al agregar estado".pg_last_error());

         $es_id = pg_fetch_row($resultado);

         return $es_id[0];
    }
}


function agregarFoto($foto,$nombre){

    $fecha_hoy= new DateTime(date("Y-m-d H:i"));
    $anio=$fecha_hoy->format('Y');
    $mes=$fecha_hoy->format('m');

    $data = file_get_contents($foto);
    $imageFile = imagecreatefromstring($data);

    $rutaAnio = "../backend/web/images/ciudadanos/".$anio;
    $rutaMes = $rutaAnio."/".$mes;
    
    if (!file_exists($rutaAnio)) {
        mkdir($rutaMes, 0777, true);   
    }
    else{
       if (!file_exists($rutaMes)) 
           mkdir($rutaMes, 0777, true);
    }
    $imageRuta2 = "../web/images/ciudadanos/".$anio."/".$mes."/".basename($nombre);
    $imageRuta = $rutaMes."/".basename($nombre); 
    $imageSave = imagepng($imageFile,$imageRuta,9);
    imagedestroy($imageFile);
    
    return $imageRuta2;
}

?>

