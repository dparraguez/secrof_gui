
<?php

/*Función que consultando en la tabla web_tipo_reclamo obtiene la prioridad asignada a un tipo de reclamo
  esta prioridad es de 1 a 5 siendo 5 máxima prioridad, estos valores fueron asignados según  el valor de la 
  multa que se aplica según contrato*/

  function prioridadReclamo($conexion,$ti_id,$tipo,$fecha,$hora)
  {
    if(strcmp ($tipo,'Escombros') != 0){
             
             //Obtiene la prioridad que tiene el reclamo segun el tipo en la bd
             $prioridad = "SELECT ti_prioridad FROM web_tipo_reclamo WHERE ti_id ='".$ti_id."'";
             $resultado = pg_query($conexion, $prioridad) or die("Error al consultar prioridad reclamo".pg_last_error());  
             $ti_prioridad = pg_fetch_row($resultado);
             $prioridad = $ti_prioridad[0];

             //Junta fecha y hora de creación del reclamo, le da formato para compararla a la de hoy y obtener la prioridad
             $fecha=$fecha." ".$hora;
             $fecha_reclamo= date("Y-m-d H:i",strtotime($fecha));
             $fecha_reclamo= new DateTime($fecha_reclamo);
             //Obtiene fecha de hoy
             $fecha_hoy = date("Y-m-d H:i");
             $fecha_hoy= new DateTime($fecha_hoy);
             //Calcula la diferencia entre las fechas
             $dif_fecha = $fecha_reclamo->diff($fecha_hoy);
             //Comparación por fecha
             if($dif_fecha->y==0){
                 if($dif_fecha->m==0){
                    if($dif_fecha->d==0){
                      $prioridad=$prioridad+2;
                      //Comparación por hora
                      if($dif_fecha->h>2){
                         if($dif_fecha->h>3){
                           $prioridad=$prioridad+1;
                         }
                         else $prioridad=$prioridad+2;
                      }
                      else $prioridad=$prioridad+3;
                      //Fin comparación por hora
                     }
                     else $prioridad=$prioridad+1;
                 }
                 else$prioridad=$prioridad+1;
             }
             else $prioridad=$prioridad+1;
             //Fin comparación por fecha 
             //Aumenta dos puntos si el reclamo es tipo sumidero y esta entre los meses de mayo a septiembre
             if(strcmp ($tipo,'Sumideros') == 0){
              $mes_hoy=$fecha_hoy->format('m');
              if(($mes_hoy>=5)&&($mes_hoy<=9))
                   $prioridad=$prioridad+2;
             }
             //Obtiene el nivel de prioridad
             if($prioridad>4){
                  if($prioridad>7) return "Alta";
                  else return "Media";
             }
             else return "Baja"; 
    }
    else  return "Derivar"; 
  }

?>