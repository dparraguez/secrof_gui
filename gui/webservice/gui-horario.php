<?php

include('funciones-gui-horario.php');

$conexion = conexionPostgresGui();

//Datos enviados de la app
$app_datos = $_POST['data'];
//print $app_datos."<br>";
$datos = json_decode($app_datos,true);

$latitud = $datos['latitud'];
$longitud = $datos['longitud'];

$zona_servicio =  obtenerHorario($conexion, $latitud, $longitud);

if(empty($zona_servicio)){
	$horario = array('estado'=> 1);
}else{
	$horario = array(
		'estado'=> 2,
		'nombre' => $zona_servicio['nombre'],
		'dia_lunes' => $zona_servicio['dia_lunes'],
		'dia_martes' => $zona_servicio['dia_martes'],
		'dia_miercoles' => $zona_servicio['dia_miercoles'],
		'dia_jueves' => $zona_servicio['dia_jueves'],
		'dia_viernes' => $zona_servicio['dia_viernes'],
		'dia_sabado' => $zona_servicio['dia_sabado'],
		'dia_domingo' => $zona_servicio['dia_domingo'],
		'horario' => $zona_servicio['horario'],
		'hora_inicio' => $zona_servicio['hora_inicio'],
		'hora_fin' => $zona_servicio['hora_fin'],
	);
}

pg_close($conexion);
$resultado = json_encode($horario);
echo $resultado;