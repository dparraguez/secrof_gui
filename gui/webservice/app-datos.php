<?php

include('funciones-app-datos.php');
include('funcion-prioridad-reclamo.php');

$app_datos = $_POST['data'];

$datos = json_decode($app_datos,true);

$control = $datos['control'];

$conexion = conexionPostgres();


switch ($control) {
	//Primera operacion datos(Sugerencia+Usuario)
    case 1:
        //consulta con  el rut si el usuario esta registrado en la base de datos y obtiene su us_id
        $us_id = consultarUsuario($conexion,$datos['rut']);
        if($us_id!=null){
            $su_id = agregarSugerencia($conexion,$datos['tipo'],$datos['descripcion'],$datos['fecha'],$datos['hora'],
                     $datos['latitud'],$datos['longitud'],$us_id);
        }
        else{
            $us_id = agregarUsurio($conexion,$datos['nombres'], $datos['apellidos'],$datos['rut'],$datos['fono']);
            $su_id = agregarSugerencia($conexion,$datos['tipo'],$datos['descripcion'],$datos['fecha'],$datos['hora'],
                     $datos['latitud'],$datos['longitud'],$us_id);
        }
        $enviar= array('control'=> 1,'us_id'=> $us_id, 'su_id'=> $su_id);
        $resultado = json_encode($enviar);
        break;
    //Sugerencia + Id Usuario Server
    case 2:
        $su_id = agregarSugerencia($conexion,$datos['tipo'],$datos['descripcion'],$datos['fecha'],$datos['hora'],
                 $datos['latitud'],$datos['longitud'],$datos['usuario']);
        $enviar = array('control'=> 2, 'su_id'=> $su_id);
        $resultado = json_encode($enviar);
        break;
    //Primera operacion datos(Reclamo+Usuario)
    case 3:
        $us_id = consultarUsuario($conexion,$datos['rut']);
        $ti_id = consultarTipoReclamo($conexion, $datos['tipo']);
        $prioridad= prioridadReclamo($conexion,$ti_id,$datos['tipo'],$datos['fecha'],$datos['hora']);
        if($us_id!=null){
            //consulta si el arreglo existe una clave 'foto', si es asi se almacena la foto en una carpeta y
            //se guarda en la bd el nombre de la imagen.
            if (array_key_exists('foto', $datos)) {
                    
                     $foto = agregarFoto($_FILES['imageToUpload']['tmp_name'],$datos['foto']);

                     $re_id = agregarReclamo($conexion,$ti_id,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                     $foto,$datos['latitud'],$datos['longitud'],$prioridad,$us_id);
            }
            else{
                    $re_id = agregarReclamo($conexion,$ti_id,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                    null,$datos['latitud'],$datos['longitud'],$prioridad,$us_id);
            }     
        }
        else{
            $us_id = agregarUsurio($conexion,$datos['nombres'], $datos['apellidos'],$datos['rut'],$datos['fono']);
            if (array_key_exists('foto', $datos)) {

                $foto = agregarFoto($_FILES['imageToUpload']['tmp_name'],$datos['foto']);
                
                $re_id = agregarReclamo($conexion,$ti_id,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                     $foto,$datos['latitud'],$datos['longitud'],$prioridad,$us_id);
            }
            else{
                $re_id = agregarReclamo($conexion,$ti_id,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                     null,$datos['latitud'],$datos['longitud'],$prioridad,$us_id);
            }
        }

        $enviar = array('control'=> 3, 'us_id'=> $us_id, 're_id'=> $re_id);
        $resultado = json_encode($enviar);
        break;
    //Reclamo + Id Usuario Server
    case 4:
        $ti_id = consultarTipoReclamo($conexion, $datos['tipo']);
        $prioridad= prioridadReclamo($conexion,$ti_id,$datos['tipo'],$datos['fecha'],$datos['hora']);
        if (array_key_exists('foto', $datos)){
            $foto = agregarFoto($_FILES['imageToUpload']['tmp_name'],$datos['foto']);

            $re_id =  agregarReclamo($conexion,$ti_id,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                      $foto,$datos['latitud'],$datos['longitud'],$prioridad,$datos['usuario']);

        }
        else{
            $re_id =  agregarReclamo($conexion,$ti_id,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                      null,$datos['latitud'],$datos['longitud'],$prioridad,$datos['usuario']);
        }
        $enviar= array('control'=> 4, 're_id'=> $re_id);
        $resultado = json_encode($enviar);
        break;
}

pg_close($conexion);

echo $resultado;

?>