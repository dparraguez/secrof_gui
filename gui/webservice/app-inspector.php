<?php

include('funciones-app-inspector.php');

$app_datos = $_POST['data'];

$datos = json_decode($app_datos,true);

$control = $datos['control'];

$conexion = conexionPostgres();


switch ($control) {
    //Reporte + Datos del Inspector (Primer envio datos app)
    case 1:
        $in_id = consultarInspector($conexion,$datos['rut']);
        if($in_id!=null){
            //consulta si el arreglo existe una clave 'foto', si es asi se almacena la foto en una carpeta y
            //se guarda en la bd el nombre de la imagen.
            if (array_key_exists('foto', $datos)) {
                    
                     $foto = agregarFoto($_FILES['imageToUpload']['tmp_name'],$datos['foto']);

                     $rp_id = agregarReporte($conexion,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                     $foto,$datos['latitud'],$datos['longitud'],$in_id);
            }
            else{
                     $rp_id = agregarReporte($conexion,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                     null,$datos['latitud'],$datos['longitud'],$in_id);
            }     
        }
        else{
            $in_id = agregarInspector($conexion,$datos['nombres'], $datos['apellidos'],$datos['rut'],$datos['fono']);
            if (array_key_exists('foto', $datos)) {

                $foto = agregarFoto($_FILES['imageToUpload']['tmp_name'],$datos['foto']);

                $rp_id = agregarReporte($conexion,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                     $foto,$datos['latitud'],$datos['longitud'],$in_id);
            }
            else{
                $rp_id = agregarReporte($conexion,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                     null,$datos['latitud'],$datos['longitud'],$in_id);
            }
        }
        $enviar = array('control'=> 1, 'in_id'=> $in_id, 'rp_id'=> $rp_id);
        $resultado = json_encode($enviar);
        break;
    //Reporte + Id Inspector Server
    case 2:
        if (array_key_exists('foto', $datos)){
            $foto = agregarFoto($_FILES['imageToUpload']['tmp_name'],$datos['foto']);

            $rp_id =  agregarReporte($conexion,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                      $foto,$datos['latitud'],$datos['longitud'],$datos['usuario']);

        }
        else{
            $rp_id =  agregarReporte($conexion,$datos['descripcion'],$datos['fecha'],$datos['hora'],
                      null,$datos['latitud'],$datos['longitud'],$datos['usuario']);
        }
        $enviar= array('control'=> 2, 'rp_id'=> $rp_id);
        $resultado = json_encode($enviar);
        break;
}

pg_close($conexion);

echo $resultado;

?>