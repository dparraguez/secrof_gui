<?php
// ***DEBUG*** Quitar etiquetas html anteriores    

/**
*
* Base de datos
*
**/

$host = "localhost";
$user = "postgres";
$pass = "postgres...";
$db = "secrof_gui";
$port = "5432";
$id_conn = pg_connect("host=$host port=$port dbname=$db user=$user password=$pass");

//2015-05-01T00:00:01+00:00
const TIMESTAMP_BASE = 1420070400;

$q_id_vehiculo = "SELECT id from vehiculo";
$rs_id_vehiculo=pg_query($id_conn,$q_id_vehiculo) or die($q_id_vehiculo);

/**
*
* Login
*
**/

// Se abre sesion
$ch = curl_init();

// Se define al url
curl_setopt($ch, CURLOPT_URL,"http://app.ultragps.com/Session/Login.castle");
curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');

// Se definen metodo y cantidad de parametros
curl_setopt($ch, CURLOPT_POST, 3);

// Se definen los parametros
curl_setopt($ch, CURLOPT_POSTFIELDS, "userName=dtorres&password=LF5klaqXwvjvjacCK-L7ZvXn3_bTWPOK_hVqz1yStp0&remember=false");
curl_setopt($ch, CURLOPT_COOKIESESSION, true);
curl_setopt($ch, CURLOPT_COOKIEJAR, "/tmp/cookieFileName");

// Se define el tipo de respuesta
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

// Se ejecuta la peticion y se almacena la respuesta
$remote_server_output = curl_exec ($ch);

// Se cierra la sesión cURL
curl_close ($ch);

/**
*
* Listado de vehiculos
*
**/

$ch = curl_init();
curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
curl_setopt($ch, CURLOPT_URL,"http://app.ultragps.com/Carriers/ListByUserType.castle?_dc=1429790353168");
curl_setopt($ch, CURLOPT_COOKIEFILE, "/tmp/cookieFileName");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$remote_server_output = curl_exec ($ch);
curl_close ($ch);

// Almacenar resultado
$vehiculos = json_decode($remote_server_output);

$cantidad_vehiculos = count($vehiculos);

// Almacena el listado de vehiculos
$id_vehiculos = array();

foreach ($vehiculos as $key => $value) {
	// Verificar si el vehiculo ya esta ingresado
	$q_id_vehiculo = "SELECT id from vehiculo where patente = '$value->Code'";
	$rs_id_vehiculo=pg_query($id_conn,$q_id_vehiculo) or die($q_id_vehiculo);
	$row_id_vehiculo=pg_fetch_array($rs_id_vehiculo);
	$id_vehiculo=$row_id_vehiculo['id'];
	if(empty($id_vehiculo)){
		// Si es nuevo almacena los datos
		$q_nuevo_id_vehiculo="SELECT nextval('vehiculo_id_seq') as id";
		$rs_nuevo_id_vehiculo=pg_query($id_conn,$q_nuevo_id_vehiculo) or die($q_nuevo_id_vehiculo);
		$row_nuevo_id_vehiculo=pg_fetch_array($rs_nuevo_id_vehiculo);
		$id_vehiculo=$row_nuevo_id_vehiculo['id'];
		$q_vehiculo = "INSERT INTO vehiculo (id, patente, id_externo, flota, tipo, tipo_descripcion, label, conductor) VALUES ($id_vehiculo,'".$value->Code."', ".$value->Id.", '".$value->Fleet."', ".$value->TypeOf.",'".$value->TypeOfCarrier."','".$value->Label."','".$value->DriverFullname."')";
		pg_query($id_conn, $q_vehiculo);
	}
	$id_vehiculos[$value->Code] = $id_vehiculo;
}

// ***DEBUG*** Mostrar resultado    
//print_r($id_vehiculos);
// ***FIN DEBUG***

/**
*
* Rango de hora
*
**/

$q_ultima_consulta = "SELECT timestamp from log_obtencion_datos where id>3429 order by timestamp desc limit 1";
$rs_ultima_consulta = pg_query($id_conn, $q_ultima_consulta);

//rango de tiempo 3600 segundos = 1 hora

$rango = 3600;

if( pg_num_rows($rs_ultima_consulta)==0){
	$timestamp_inicio = TIMESTAMP_BASE;
} else {
	$row_ultima_consulta = pg_fetch_array($rs_ultima_consulta);
	$timestamp_inicio = $row_ultima_consulta['timestamp'];
}

$timestamp_fin = $timestamp_inicio + $rango;


//Formato 2015-05-12T23:00:00Z
$fecha_hora_inicio = date("Y-m-d H:i:s", $timestamp_inicio);
$fecha_hora_fin = date("Y-m-d H:i:s", $timestamp_fin);

$fecha_hora_inicio = str_replace(" ", "T", $fecha_hora_inicio)."Z";
$fecha_hora_fin = str_replace(" ", "T", $fecha_hora_fin)."Z";

// ***DEBUG*** Mostrar resultado    
echo "Inicio: ".$timestamp_inicio."  Fin: ".$timestamp_fin."\n";
echo "Inicio: ".$fecha_hora_inicio."  Fin: ".$fecha_hora_fin."\n";
// ***FIN DEBUG***

/**
*
* Listado de posiciones por cada vehiculo
*
**/

// Almacena el tiempo que se demora en hacer todas las peticiones
$tiempo_total = 0;
$cantidad_posiciones_total=0;
$tiempo_total_ejecucion = 0;

foreach ($vehiculos as $vehiculo) {

	$tiempo_iteracion_inicio = time();

	$posiciones = "";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/32.0.1700.107 Chrome/32.0.1700.107 Safari/537.36');
	curl_setopt($ch, CURLOPT_URL,"http://app.ultragps.com/TableTrace/List.castle");
	curl_setopt($ch, CURLOPT_COOKIEFILE, "/tmp/cookieFileName");
	curl_setopt($ch, CURLOPT_POST, 12);

	$parametros = 'start=0';
	$parametros .= '&limit=300';
	$parametros .= '&carrierIds='.$vehiculo->Id;
	$parametros .= '&criteria={type:""}';

	//Intervalo de tiempo
	$parametros .= '&criteria={"positions":"track","includePeripherals":"on","includeAlwaysLocation":"on","intervalPositions":"00:00:01","is-range":"on","fromDateTime":"'.$fecha_hora_inicio.'","toDateTime":"'.$fecha_hora_fin.'","type":"View"}';
	$parametros .= '&mapState={"0":{"Srid":900913}}';
	$parametros .= '&eventsOnly=false';
	$parametros .= '&isLastPosition=false';
	$parametros .= '&includeAlwaysLocation=false';
	$parametros .= '&includePeripherals=true';

	curl_setopt($ch, CURLOPT_POSTFIELDS, $parametros);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$remote_server_output1 = curl_exec ($ch);
	curl_close ($ch);


	// Fix de variable datetime que incluye un new date() que da error si no se elimina
	$remote_server_output1 = str_replace("new Date(", "", $remote_server_output1);
	$remote_server_output1 = str_replace('),"TimeDiference"', ',"TimeDiference"', $remote_server_output1);

	//Almacenar posiciones

	$posiciones = json_decode($remote_server_output1);

	$cantidad_posiciones = count($posiciones->Rows);

	$cantidad_posiciones_total += $cantidad_posiciones;

	foreach ($posiciones->Rows as $key => $posicion) {
		//Quitar milisegundos, eran siempre los mismos
		$posicion->Timestamp = substr($posicion->Timestamp, 0, -3);

		// ***DEBUG*** Para evitar repetir posiciones
		//$q_id_posicion = "SELECT id from posicion where fecha = $posicion->Timestamp and id_vehiculo=".$id_vehiculos[$posicion->Code];
		//$rs_id_posicion=pg_query($id_conn,$q_id_posicion) or die($q_id_posicion);
		//$row_id_posicion=pg_fetch_array($rs_id_posicion);
		//$id_posicion=$row_id_posicion['id'];

		//if(empty($id_posicion)){
			
			$q_posicion = "INSERT INTO posicion (latitud, longitud, velocidad, fecha, orientacion, id_vehiculo) 
							VALUES ($posicion->Latitude, ".$posicion->Longitude.",$posicion->Speed,$posicion->Timestamp,$posicion->Course,".$id_vehiculos[$posicion->Code].")";
			
			// ***DEBUG*** Mostrar consulta
			//echo $q_posicion."<br>";
			// ***FIN DEBUG***
			pg_query($id_conn, $q_posicion);
		//}
	}

	$tiempo_iteracion_fin = time();

	$tiempo_iteracion_total = $tiempo_iteracion_fin-$tiempo_iteracion_inicio;

	//echo "Tiempo consulta: ".$tiempo_iteracion_total."  Cantidad posiciones: ".$cantidad_posiciones."<br>";

	$tiempo_total += $tiempo_iteracion_total;

	// se detiene 1 segundo antes de la siguiente consulta
	sleep(1);

	$tiempo_total_ejecucion += $tiempo_total+5;
}

echo " Tiempo consulta: ".$tiempo_total."  Cantidad posiciones: ".$cantidad_posiciones_total." Tiempo ejecucion: ".$tiempo_total_ejecucion;

$q_insert_log = "INSERT into log_obtencion_datos(timestamp, cantidad_vehiculos, cantidad_posiciones, tiempo) Values (".$timestamp_fin.",".$cantidad_vehiculos.",".$cantidad_posiciones_total.",".$tiempo_total.")";
pg_query($id_conn, $q_insert_log);