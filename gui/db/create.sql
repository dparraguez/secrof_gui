CREATE DATABASE SECROF;

\connect SECROF



CREATE TABLE log_obtencion_datos
(
  id serial,
  "timestamp" integer,
  cantidad_vehiculos integer,
  cantidad_posiciones integer,
  tiempo integer,
  CONSTRAINT log_obtencion_datos_pk PRIMARY KEY (id)
)

CREATE TABLE vehiculo
(
  id serial,
  patente character varying(20),
  observacion character varying(200),
  estado integer,
  id_externo bigint,
  flota character varying(200),
  label character varying(200),
  tipo integer,
  tipo_descripcion character varying(200),
  conductor character varying(200),
  CONSTRAINT vehiculo_pk PRIMARY KEY (id)
)

CREATE TABLE posicion
(
  id serial,
  latitud numeric(9,6),
  longitud numeric(9,6),
  velocidad integer,
  fecha integer,
  orientacion integer,
  id_vehiculo integer,
  CONSTRAINT posicion_pk PRIMARY KEY (id),
  CONSTRAINT posicion_fk1 FOREIGN KEY (id_vehiculo)
      REFERENCES public.vehiculo (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)

CREATE TABLE public.user
(
  id serial,
  username character varying(255) NOT NULL,
  auth_key character varying(32) NOT NULL,
  password_hash character varying(255) NOT NULL,
  password_reset_token character varying(255),
  email character varying(255) NOT NULL,
  status smallint NOT NULL DEFAULT 10,
  created_at integer NOT NULL,
  updated_at integer NOT NULL,
  CONSTRAINT user_pkey PRIMARY KEY (id)
)

CREATE TABLE public.zona_servicio
(
  id serial,
  observacion character varying(200),
  nombre character varying(50),
  zona geometry,
  puntos text,
  horario integer,
  dia_lunes integer,
  dia_martes integer,
  dia_miercoles integer,
  dia_jueves integer,
  dia_viernes integer,
  dia_sabado integer,
  dia_domingo integer,
  hora_inicio character varying(10),
  hora_fin character varying(10),
  geography geography,
  CONSTRAINT rutas_pk PRIMARY KEY (id)
)

CREATE TABLE public.cuadra
(
  id serial,
  id_calle integer,
  geography geography,
  latitud_inicio numeric(9,6),
  longitud_inicio numeric(9,6),
  latitud_fin numeric(9,6),
  longitud_fin numeric(9,6),
  CONSTRAINT cuadra_pk PRIMARY KEY (id),
  CONSTRAINT cuadra_fk_calle FOREIGN KEY (id_calle)
      REFERENCES public.calle (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)

CREATE TABLE public.reporte_indice
(
  id serial NOT NULL,
  tipo integer,
  id_posicion integer,
  CONSTRAINT reporte_indice_pk PRIMARY KEY (id),
  CONSTRAINT reporte_indice_fk_posicion FOREIGN KEY (id_posicion)
      REFERENCES public.posicion (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)