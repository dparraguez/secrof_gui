--Importante: Los insert que se encuentran al final deben ser realizados INMEDIATAMENTE 
--después de la creación de la base de datos, ya que son necesarios para el correcto 
--funcionamiento tanto de las aplicaciones como de la página web.
CREATE DATABASE APPMOVIL;

CREATE TABLE APP_USUARIO(
	us_id SERIAL NOT NULL,
	us_nombres CHARACTER varying(25),
	us_apellidos CHARACTER varying(25),
	us_rut CHARACTER varying(15),
	us_fono CHARACTER varying(15),
	CONSTRAINT USUARIO_PK PRIMARY KEY(us_id)
);

CREATE TYPE TIPO AS ENUM('Sugerencia','Felicitación');

CREATE TABLE APP_SUGERENCIA (
	su_id SERIAL NOT NULL,
	su_tipo TIPO,
	su_descripcion CHARACTER varying(250),
	su_fecha_app DATE,
	su_hora_app CHARACTER varying(5),
	su_latitud  NUMERIC(9,6),
	su_longitud NUMERIC(9,6),
	su_estado CHARACTER varying(15),
	su_fecha_hora_web TIMESTAMP,
	us_id INTEGER,
	CONSTRAINT SUGERENCIA_PK PRIMARY KEY(su_id),
	CONSTRAINT SUGERENCIA_FK1 FOREIGN KEY (us_id) REFERENCES APP_USUARIO
);

CREATE TABLE WEB_TIPO_RECLAMO(
	ti_id SERIAL NOT NULL,
	ti_nombre CHARACTER varying(20),
	ti_prioridad INTEGER,
	CONSTRAINT WEB_TIPO_RECLAMO_PK PRIMARY KEY(ti_id)
);

CREATE TYPE PRIORIDAD AS ENUM('Alta','Media','Baja','Derivar');

CREATE TABLE APP_RECLAMO (
	re_id SERIAL NOT NULL,
    re_descripcion CHARACTER varying(250),
	re_fecha_app DATE,
	re_estadoactual CHARACTER varying(15) default 'Recepcionado',
	re_hora_app CHARACTER varying(5),
	re_foto CHARACTER varying(100),
	re_latitud  NUMERIC(9,6),
	re_longitud NUMERIC(9,6),
	re_prioridad PRIORIDAD,
	us_id INTEGER,
	ti_id INTEGER,
	CONSTRAINT RECLAMO_PK PRIMARY KEY(re_id),
	CONSTRAINT RECLAMO_FK1 FOREIGN KEY(us_id) REFERENCES APP_USUARIO,
	CONSTRAINT RECLAMO_FK2 FOREIGN KEY(ti_id) REFERENCES WEB_TIPO_RECLAMO
);

CREATE TABLE APP_INSPECTOR(
	in_id SERIAL NOT NULL,
	in_nombres CHARACTER varying(25),
	in_apellidos CHARACTER varying(25),
	in_rut CHARACTER varying(15),
	in_fono CHARACTER varying(15),
	in_clave character varying(25),
	CONSTRAINT INSPECTOR_PK PRIMARY KEY(in_id)
);

CREATE TABLE APP_REPORTE (
	rp_id SERIAL NOT NULL,
    rp_descripcion CHARACTER varying(250),
	rp_fecha_app DATE,
	rp_hora_app CHARACTER varying(5),
	rp_foto CHARACTER varying(100),
	rp_latitud  NUMERIC(9,6),
	rp_longitud NUMERIC(9,6),
	rp_estado CHARACTER varying(15),
	rp_fecha_hora_web TIMESTAMP,
	in_id INTEGER,
	CONSTRAINT REPORTE_PK PRIMARY KEY(rp_id),
	CONSTRAINT REPORTE_FK1 FOREIGN KEY(in_id) REFERENCES APP_INSPECTOR
);

CREATE TABLE WEB_ESTADO(
	es_id SERIAL NOT NULL,
	es_nombre CHARACTER varying(25),
	es_descripcion CHARACTER varying(150),
	CONSTRAINT WEB_ESTADO_PK PRIMARY KEY(es_id)
);


CREATE TABLE RECLAMO_ESTADO(
	re_id INTEGER,
	es_id INTEGER,
	re_es_fecha TIMESTAMP,
	CONSTRAINT RECLAMO_ESTADO_PK PRIMARY KEY(re_id,es_id),
	CONSTRAINT RECLAMO_ESTADO_FK1 FOREIGN KEY(re_id) REFERENCES APP_RECLAMO,
	CONSTRAINT RECLAMO_ESTADO_FK2 FOREIGN KEY(es_id) REFERENCES WEB_ESTADO
);

CREATE TABLE TIENE_SUCESOR(
	es_id INTEGER,
	web_es_id INTEGER,
	CONSTRAINT TIENE_SUCESOR_PK PRIMARY KEY(es_id,web_es_id),
	CONSTRAINT TIENE_SUCESOR_FK1 FOREIGN KEY(es_id) REFERENCES WEB_ESTADO,
	CONSTRAINT TIENE_SUCESOR_FK2 FOREIGN KEY(web_es_id) REFERENCES WEB_ESTADO
);

INSERT INTO web_tipo_reclamo(ti_nombre,ti_prioridad) 
VALUES('Vehículo abandonado',4),('Vehículo filtración',3),
	  ('Vehículo otros',1),('Microbasural',4),
      ('Trabajadores',2),('Calles',3), ('Residuos',5),
      ('Escombros',0),('Sumideros',2);

INSERT INTO web_estado (es_nombre,es_descripcion) VALUES ('Recepcionado','Estado inicial del reclamo recibido');
INSERT INTO web_estado (es_nombre,es_descripcion) VALUES ('Leído','El reclamo ya ha sido visto por el fiscalizador');
INSERT INTO web_estado (es_nombre,es_descripcion) VALUES ('Cerrado','Al reclamo ya se le dio una solución');
INSERT INTO web_estado (es_nombre,es_descripcion) VALUES ('Derivado','El reclamo es tipo Escombros por lo que se deriva');