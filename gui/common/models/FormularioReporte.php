<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Formulario Reporte
 */
class FormularioReporte extends Model
{
    public $fecha_inicio;
    public $fecha_fin;
    public $hora_inicio;
    public $hora_fin;
    public $id_zona_servicio;
    public $id_cuadra;
    public $id_vehiculo;

    public $posiciones;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // fecha_inicio and fecha_fin are both required
            [['fecha_inicio', 'fecha_fin'], 'required', 'on' => ['comportamiento', 'detencion', 'inicioruta', 'iniciodescargado', 'historico']],
            [['hora_inicio', 'id_cuadra'], 'required', 'on' => ['probabilidad']],
            [['id_zona_servicio'], 'required', 'on' => ['histograma', 'inicioruta']],
            [['id_vehiculo'], 'required', 'on' => ['historico']],
        ];
    }
}
