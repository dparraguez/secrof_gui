<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppReporte;

/**
 * AppReporteSearch represents the model behind the search form about `app\models\AppReporte`.
 */
class AppReporteSearch extends AppReporte
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rp_id', 'in_id'], 'integer'],
            [['rp_descripcion', 'rp_fecha_app', 'rp_hora_app', 'rp_foto', 'rp_estado', 'rp_fecha_hora_web'], 'safe'],
            [['rp_latitud', 'rp_longitud'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppReporte::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rp_id' => $this->rp_id,
            'rp_fecha_app' => $this->rp_fecha_app,
            'rp_latitud' => $this->rp_latitud,
            'rp_longitud' => $this->rp_longitud,
            'rp_fecha_hora_web' => $this->rp_fecha_hora_web,
            'in_id' => $this->in_id,
        ]);

        $query->andFilterWhere(['like', 'rp_descripcion', $this->rp_descripcion])
            ->andFilterWhere(['like', 'rp_hora_app', $this->rp_hora_app])
            ->andFilterWhere(['like', 'rp_foto', $this->rp_foto])
            ->andFilterWhere(['like', 'rp_estado', $this->rp_estado]);

        return $dataProvider;
    }
}
