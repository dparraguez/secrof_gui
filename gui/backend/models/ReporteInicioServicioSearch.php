<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReporteInicioDescargado;

/**
 * ReporteInicioDescargadoSearch represents the model behind the search form about `app\models\ReporteInicioDescargado`.
 */
class ReporteInicioServicioSearch extends ReporteInicioServicio
{
    public $nombreZonaServicio;
    public $fechaInicio;
    public $fechaFin;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'fecha_inicio_real', 'fecha_inicio_programada'], 'integer'],
            [['nombreZonaServicio', 'fechaInicio', 'fechaFin'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReporteInicioServicio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'fecha_inicio_real',
                'fecha_inicio_programada',
                'nombreZonaServicio' => [
                    'asc' => ['zona_servicio.nombre' => SORT_ASC],
                    'desc' => ['zona_servicio.nombre' => SORT_DESC],
                    'label' => 'Zona Servicio'
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            /**
             * The following line will allow eager loading with country data
             * to enable sorting by country on initial loading of the grid.
             */
            $query->joinWith(['zonaServicio']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_inicio_real' => $this->fecha_inicio_real,
            'fecha_inicio_programada' => $this->fecha_inicio_programada,
            'id_zona_servicio' => $this->id_zona_servicio,
        ]);

        if (!empty($this->fechaInicio)) {
            $query->andFilterWhere([
                '>=', 'fecha_inicio_programada', strtotime($this->fechaInicio)+3600*3
            ]);
        }
        if (!empty($this->fechaFin)) {
            $query->andFilterWhere([
                '<=', 'fecha_inicio_programada', (strtotime($this->fechaFin)+3600*27)
            ]);
        }
        if (!empty($this->nombreZonaServicio)) {
            $query->joinWith(['zonaServicio' => function ($q) {
                $q->where('zona_servicio.id ='.$this->nombreZonaServicio);
            }]);
        }
        return $dataProvider;
    }
}
