<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "histograma".
 *
 * @property integer $id_histograma
 * @property string $hora_inicio
 * @property string $hora_fin
 * @property integer $frecuencia
 * @property integer $id_cuadra
 * @property string $dia
 * @property double $probabilidad
 */
class Histograma extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'histograma';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hora_inicio', 'hora_fin'], 'safe'],
            [['frecuencia', 'id_cuadra'], 'integer'],
            [['probabilidad'], 'number'],
            [['dia'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_histograma' => 'Id Histograma',
            'hora_inicio' => 'Hora Inicio',
            'hora_fin' => 'Hora Fin',
            'frecuencia' => 'Frecuencia',
            'id_cuadra' => 'Id Cuadra',
            'dia' => 'Dia',
            'probabilidad' => 'Probabilidad',
        ];
    }
}
