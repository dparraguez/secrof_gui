<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppUsuario;

/**
 * AppUsuarioSearch represents the model behind the search form about `app\models\AppUsuario`.
 */
class AppUsuarioSearch extends AppUsuario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['us_id'], 'integer'],
            [['us_nombres', 'us_apellidos', 'us_rut', 'us_fono'], 'safe'],
            ['us_rut','validaRutCaracter'],
            ['us_rut','validateRut'],
        ];
    }

    public function validaRutCaracter($attribute, $params) {
        $pattern = '/^([0-9.]+\-+[0-9kK]{1}+)$/';
        $pattern2 = '/^([0-9.]{1}+\-+[0-9kK]{1}+)$/';
        $pattern3 = '/^([0.]+\-+[0-9kK]{1}+)$/';

        if (!preg_match($pattern, $this->$attribute) OR preg_match($pattern2, $this->$attribute) OR preg_match($pattern3, $this->$attribute))
            $this->addError($attribute, 'El Rut no es válido');
    }


    public function validateRut($attribute, $params) {
        $data = explode('-', $this->us_rut);
        $evaluate = strrev($data[0]);
        $multiply = 2;
        $store = 0;
        for ($i = 0; $i < strlen($evaluate); $i++) {
            $store += $evaluate[$i] * $multiply;
            $multiply++;
            if ($multiply > 7)
                $multiply = 2;
        }
        isset($data[1]) ? $verifyCode = strtolower($data[1]) : $verifyCode = '';
        $result = 11 - ($store % 11);
        if ($result == 10)
            $result = 'k';
        if ($result == 11)
            $result = 0;
        if ($verifyCode != $result)
            $this->addError('us_rut', 'El Rut no es válido');
    }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppUsuario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'us_id' => $this->us_id,
        ]);

        $query->andFilterWhere(['like', 'us_nombres', $this->us_nombres])
            ->andFilterWhere(['like', 'us_apellidos', $this->us_apellidos])
            ->andFilterWhere(['like', 'us_rut', $this->us_rut])
            ->andFilterWhere(['like', 'us_fono', $this->us_fono]);

        return $dataProvider;
    }
}
