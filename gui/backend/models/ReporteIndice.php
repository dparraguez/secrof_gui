<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reporte_indice".
 *
 * @property integer $id
 * @property integer $tipo
 *
 * @property Posicion $idPosicion
 */
class ReporteIndice extends \yii\db\ActiveRecord
{

    /**
    * Identificadores
    */
    const TIPO_EVENTO_ZONA=1;
    const TIPO_CALCULO_INICIO_DESCARGADO=2;
    const TIPO_CALCULO_INICIO_RUTA=3;
    const TIPO_CALCULO_COMPORTAMIENTO=4;
    const TIPO_CALCULO_DETENCION=5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reporte_indice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tipo'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo' => 'Tipo',
        ];
    }
}
