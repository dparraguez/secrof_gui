<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "zona".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $poligono
 * @property integer $tipo
 *
 * @property PosicionZona[] $posicionZonas
 */
class Zona extends \yii\db\ActiveRecord
{
    /**
    * Tipos de zona
    */
    const TIPO_VERTEDERO=1; 
    const TIPO_ESTACIONAMIENTO=2; 
    const TIPO_OTRAS=3; 

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zona';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['poligono'], 'string'],
            [['tipo'], 'integer'],
            [['nombre'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'poligono' => 'Poligono',
            'tipo' => 'Tipo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosicionZonas()
    {
        return $this->hasMany(PosicionZona::className(), ['id_zona' => 'id']);
    }
}
