<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_reporte".
 *
 * @property integer $rp_id
 * @property string $rp_descripcion
 * @property string $rp_fecha_app
 * @property string $rp_hora_app
 * @property string $rp_foto
 * @property string $rp_latitud
 * @property string $rp_longitud
 * @property string $rp_estado
 * @property string $rp_fecha_hora_web
 * @property integer $in_id
 *
 * @property AppInspector $in
 */
class AppReporte extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_reporte';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_movil');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rp_fecha_app', 'rp_fecha_hora_web'], 'safe'],
            [['rp_latitud', 'rp_longitud'], 'number'],
            [['in_id'], 'integer'],
            [['rp_descripcion'], 'string', 'max' => 250],
            [['rp_hora_app'], 'string', 'max' => 5],
            [['rp_foto'], 'string', 'max' => 100],
            [['rp_estado'], 'string', 'max' => 15],
            [['in_id'], 'exist', 'skipOnError' => true, 'targetClass' => AppInspector::className(), 'targetAttribute' => ['in_id' => 'in_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rp_id' => 'ID',
            'rp_descripcion' => 'Descripción',
            'rp_fecha_app' => 'Fecha App',
            'rp_hora_app' => 'Hora App',
            'rp_foto' => 'Foto',
            'rp_latitud' => 'Latitud',
            'rp_longitud' => 'Longitud',
            'rp_estado' => 'Estado',
            'rp_fecha_hora_web' => 'Fecha Recepción',
            'in_id' => 'ID Inspector',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIn()
    {
        return $this->hasOne(AppInspector::className(), ['in_id' => 'in_id']);
    }
}
