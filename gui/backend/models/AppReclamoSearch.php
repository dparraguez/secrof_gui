<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppReclamo;

/**
 * AppReclamoSearch represents the model behind the search form about `app\models\AppReclamo`.
 */
class AppReclamoSearch extends AppReclamo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['re_id', 'us_id', 'ti_id'], 'integer'],
            [['re_descripcion', 're_fecha_app', 're_estadoactual', 're_hora_app', 're_foto', 're_prioridad'], 'safe'],
            [['re_latitud', 're_longitud'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppReclamo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            're_id' => $this->re_id,
            're_fecha_app' => $this->re_fecha_app,
            're_latitud' => $this->re_latitud,
            're_longitud' => $this->re_longitud,
            'us_id' => $this->us_id,
            'ti_id' => $this->ti_id,
        ]);

        //muestra los reclamos que tienen re_estadoactual Recepcionado o Leído
        $query->andFilterWhere(['like', 're_descripcion', $this->re_descripcion])
            ->andFilterWhere(['like', 're_estadoactual', $this->re_estadoactual='Recepcionado'])  
            ->orFilterWhere(['like', 're_estadoactual', $this->re_estadoactual='Leído'])
            ->andFilterWhere(['like', 're_hora_app', $this->re_hora_app])
            ->andFilterWhere(['like', 're_foto', $this->re_foto])
            ->andFilterWhere(['like', 're_prioridad', $this->re_prioridad]);

        return $dataProvider;
    }
}
