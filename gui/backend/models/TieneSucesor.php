<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tiene_sucesor".
 *
 * @property integer $es_id
 * @property integer $web_es_id
 *
 * @property WebEstado $es
 * @property WebEstado $webEs
 */
class TieneSucesor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tiene_sucesor';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_movil');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['es_id', 'web_es_id'], 'required'],
            [['es_id', 'web_es_id'], 'integer'],
            [['es_id'], 'exist', 'skipOnError' => true, 'targetClass' => WebEstado::className(), 'targetAttribute' => ['es_id' => 'es_id']],
            [['web_es_id'], 'exist', 'skipOnError' => true, 'targetClass' => WebEstado::className(), 'targetAttribute' => ['web_es_id' => 'es_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'es_id' => 'Es ID',
            'web_es_id' => 'Web Es ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEs()
    {
        return $this->hasOne(WebEstado::className(), ['es_id' => 'es_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWebEs()
    {
        return $this->hasOne(WebEstado::className(), ['es_id' => 'web_es_id']);
    }
}
