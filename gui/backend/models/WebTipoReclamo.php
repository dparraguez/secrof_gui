<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "web_tipo_reclamo".
 *
 * @property integer $ti_id
 * @property string $ti_nombre
 * @property integer $ti_prioridad
 *
 * @property AppReclamo[] $appReclamos
 */
class WebTipoReclamo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'web_tipo_reclamo';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_movil');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ti_prioridad'], 'integer'],
            [['ti_nombre'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ti_id' => 'ID',
            'ti_nombre' => 'Nombre',
            'ti_prioridad' => 'Prioridad',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppReclamos()
    {
        return $this->hasMany(AppReclamo::className(), ['ti_id' => 'ti_id']);
    }
}
