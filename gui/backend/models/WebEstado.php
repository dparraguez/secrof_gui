<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "web_estado".
 *
 * @property integer $es_id
 * @property string $es_nombre
 * @property string $es_descripcion
 *
 * @property ReclamoEstado[] $reclamoEstados
 * @property AppReclamo[] $res
 * @property TieneSucesor[] $tieneSucesors
 * @property TieneSucesor[] $tieneSucesors0
 */
class WebEstado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'web_estado';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_movil');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['es_nombre'], 'string', 'max' => 25],
            [['es_descripcion'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'es_id' => 'Es ID',
            'es_nombre' => 'Es Nombre',
            'es_descripcion' => 'Es Descripcion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReclamoEstados()
    {
        return $this->hasMany(ReclamoEstado::className(), ['es_id' => 'es_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRes()
    {
        return $this->hasMany(AppReclamo::className(), ['re_id' => 're_id'])->viaTable('reclamo_estado', ['es_id' => 'es_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTieneSucesors()
    {
        return $this->hasMany(TieneSucesor::className(), ['es_id' => 'es_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTieneSucesors0()
    {
        return $this->hasMany(TieneSucesor::className(), ['web_es_id' => 'es_id']);
    }
}
