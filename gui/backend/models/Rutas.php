<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rutas".
 *
 * @property integer $id
 * @property string $observacion
 * @property string $nombre
 * @property string $ruta
 * @property string $puntos
 */
class Rutas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rutas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dia_lunes', 'dia_martes', 'dia_miercoles', 'dia_jueves', 'dia_viernes', 'dia_sabado', 'dia_domingo', 'horario'], 'number'],
            [['puntos', 'ruta', 'hora_inicio', 'hora_fin'], 'string'],
            [['observacion'], 'string', 'max' => 200],
            [['nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'observacion' => 'Observacion',
            'nombre' => 'Nombre',
            'ruta' => 'Ruta',
            'dia_lunes' => 'Lunes',
            'dia_martes' => 'Martes',
            'dia_miercoles' => 'Miercoles',
            'dia_jueves' => 'Jueves',
            'dia_viernes' => 'Viernes',
            'dia_sabado' => 'Sabado',
            'dia_domingo' => 'Domingo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTramos()
    {
        return $this->hasMany(Tramo::className(), ['id_rutas' => 'id']);
    }
}
