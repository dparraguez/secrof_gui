<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reporte_comportamiento".
 *
 * @property integer $id
 * @property string $fecha
 * @property integer $comportamiento
 * @property integer $id_vehiculo
 *
 * @property Vehiculo $idVehiculo
 */
class ReporteComportamiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reporte_comportamiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['comportamiento', 'id_vehiculo'], 'integer'],
            [['id_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['id_vehiculo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'comportamiento' => 'Comportamiento',
            'id_vehiculo' => 'Id Vehiculo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }
}
