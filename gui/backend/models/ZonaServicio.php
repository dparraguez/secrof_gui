<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "zona_servicio".
 *
 * @property integer $id
 * @property string $observacion
 * @property string $nombre
 * @property string $zona
 * @property string $puntos
 * @property integer $horario
 * @property integer $dia_lunes
 * @property integer $dia_martes
 * @property integer $dia_miercoles
 * @property integer $dia_jueves
 * @property integer $dia_viernes
 * @property integer $dia_sabado
 * @property integer $dia_domingo
 * @property string $hora_inicio
 * @property string $hora_fin
 *
 * @property Tramo[] $tramos
 */
class ZonaServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'zona_servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['horario', 'hora_inicio', 'hora_fin'], 'required'],
            [['zona', 'puntos'], 'string'],
            [['horario', 'dia_lunes', 'dia_martes', 'dia_miercoles', 'dia_jueves', 'dia_viernes', 'dia_sabado', 'dia_domingo'], 'integer'],
            [['observacion'], 'string', 'max' => 200],
            [['nombre'], 'string', 'max' => 50],
            [['hora_inicio', 'hora_fin'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'observacion' => 'Observacion',
            'nombre' => 'Nombre',
            'zona' => 'Zona',
            'puntos' => 'Puntos',
            'horario' => 'Horario',
            'dia_lunes' => 'Dia Lunes',
            'dia_martes' => 'Dia Martes',
            'dia_miercoles' => 'Dia Miercoles',
            'dia_jueves' => 'Dia Jueves',
            'dia_viernes' => 'Dia Viernes',
            'dia_sabado' => 'Dia Sabado',
            'dia_domingo' => 'Dia Domingo',
            'hora_inicio' => 'Hora Inicio',
            'hora_fin' => 'Hora Fin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTramos()
    {
        return $this->hasMany(Tramo::className(), ['id_rutas' => 'id']);
    }

    static $horarios =['1' => 'Diurno', '2' => 'Nocturno'];
}
