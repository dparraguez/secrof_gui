<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posicion".
 *
 * @property integer $id
 * @property string $latitud
 * @property string $longitud
 * @property integer $velocidad
 * @property integer $fecha
 * @property integer $orientacion
 * @property integer $id_vehiculo
 *
 * @property Vehiculo $idVehiculo
 */
class Posicion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posicion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['latitud', 'longitud'], 'number'],
            [['velocidad', 'fecha', 'orientacion', 'id_vehiculo'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'velocidad' => 'Velocidad',
            'fecha' => 'Fecha',
            'orientacion' => 'Orientacion',
            'id_vehiculo' => 'Id Vehiculo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }

    /* Getter for country name */
    public function getPatenteVehiculo()
    {
        return $this->vehiculo->patente;
    }
}
