<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_usuario".
 *
 * @property integer $us_id
 * @property string $us_nombres
 * @property string $us_apellidos
 * @property string $us_rut
 * @property string $us_fono
 *
 * @property AppReclamo[] $appReclamos
 * @property AppSugerencia[] $appSugerencias
 */
class AppUsuario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_usuario';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_movil');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['us_nombres', 'us_apellidos'],'required', 'message' => 'Campo requerido'],
            [['us_rut', 'us_fono'], 'required', 'message' => 'Campo requerido'],
            ['us_rut','unique'],
            ['us_fono','validaTelefono'],
            ['us_rut', 'validaRutCaracter'],
            ['us_rut', 'validateRut'],
          //  ['us_rut', 'validaRutUnico'],
            [['us_nombres'], 'string','min' => 3, 'max' => 25, 'message' => 'Mínimo 3 y máximo 25 caracteres'],
            [['us_apellidos'],'string', 'min' => 2, 'max' => 25, 'message' => 'Mínimo 2 y máximo 25 caracteres'],
            ['us_nombres','validatextoNombres'],
            ['us_apellidos','validatextoApellidos'],
            [['us_nombres','us_apellidos'],'validatexto2'],
            [['us_nombres','us_apellidos'],'validatexto3'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'us_id' => 'ID',
            'us_nombres' => 'Nombres',
            'us_apellidos' => 'Apellidos',
            'us_rut' => 'Rut',
            'us_fono' => 'Teléfono',
        ];
    }


    public function validaRutCaracter($attribute, $params) {
        $pattern = '/^([0-9.]+\-+[0-9kK]{1}+)$/';
        $pattern2 = '/^([0-9.]{1}+\-+[0-9kK]{1}+)$/';
        $pattern3 = '/^([0.]+\-+[0-9kK]{1}+)$/';

        if (!preg_match($pattern, $this->$attribute) OR preg_match($pattern2, $this->$attribute) OR preg_match($pattern3, $this->$attribute))
            $this->addError($attribute, 'El Rut no es válido');
    }


    public function validateRut($attribute, $params) {
        $data = explode('-', $this->us_rut);
        $evaluate = strrev($data[0]);
        $multiply = 2;
        $store = 0;
        for ($i = 0; $i < strlen($evaluate); $i++) {
            $store += $evaluate[$i] * $multiply;
            $multiply++;
            if ($multiply > 7)
                $multiply = 2;
        }
        isset($data[1]) ? $verifyCode = strtolower($data[1]) : $verifyCode = '';
        $result = 11 - ($store % 11);
        if ($result == 10)
            $result = 'k';
        if ($result == 11)
            $result = 0;
        if ($verifyCode != $result)
            $this->addError('us_rut', 'El Rut no es válido');
    }


    public function validaRutUnico($attribute, $params) {
        if (Yii::app()->user->us_rut->loadUser($this->$attribute))
            $this->addError($attribute, 'Rut ya existe y esta siendo ocupado');
    }

    public function validatextoNombres($attribute, $params) {
        $pattern = '/^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,3}[a-zA-ZñÑÁÉÍÓÚáéíóú]+)*)$/';
        
        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'Error sólo letras o verifique que no tenga espacios al final');
    }

    public function validatextoApellidos($attribute, $params) {
        $pattern = '/^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,2}[a-zA-ZñÑÁÉÍÓÚáéíóú]+)*)$/';
        
        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'Error sólo letras o verifique que no tenga espacios al final');
    }

    public function validatexto($attribute, $params) {
        $pattern = '/^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,3}[a-zA-ZñÑÁÉÍÓÚáéíóú]+)*)$/';
        
        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'Error sólo letras o verifique que no tenga espacios al final');
    }

  public function validatexto2($attribute, $params) {
        $pattern = '/^([a-zA-ZñÑÁÉÍÓÚáéíóú0-9º°\.\,\'\"\)\(\-\@\:\/\+]+([[:space:]]{0,2}[a-zA-ZñÑÁÉÍÓÚáéíóú0-9º°\.\,\'\"\)\(\-\@\:\/\+]+)*)$/';
        $pattern2 = '/^([0-9º°\.\,\'\"\)\(\-\@\:\/\+]+)$/';
        
        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'Error sólo letras o letras y número, verifique que no tenga espacios al final o muchos en medio.');
        if (preg_match($pattern2, $this->$attribute))
            $this->addError($attribute, 'Error No puede ser solo números o caracteres especiales');
    }

    public function validatexto3($attribute, $params) {
        $pattern2 = '/(a{3}|e{3}|i{4}|o{3}|u{3}|b{3}|c{3}|d{3}|f{3}|g{3}|h{3}|j{3}|k{3}|l{4}|m{3}|n{3}|ñ{3}|p{3}|q{3}|r{3}|s{3}|t{3}|v{3}|w{4}|x{3}|y{3}|z{3}|º{2}|°{2}|\.{2}|\'{2}|\"{2}|\){2}|\({2}|\,{2}|\-{2}|\@{2}|\:{2}|\/{3}|\+{2})/i';
        $pattern3 = '/(A{3}|E{3}|I{4}|O{3}|U{3}|B{3}|C{3}|D{3}|F{3}|G{3}|H{3}|J{3}|K{3}|L{4}|M{3}|N{3}|Ñ{3}|P{3}|Q{3}|R{3}|S{3}|T{3}|V{3}|W{4}|X{3}|Y{3}|Z{3})/i';
        $pattern4 = '/(á{3}|Á{3}|é{3}|É{3}|í{3}|Í{3}|ó{3}|Ó{3}|ú{3}|Ú{3})/i';
        $pattern5 = '/([0-9]{13})/i';
        

        if (preg_match($pattern2, $this->$attribute) OR preg_match($pattern3, $this->$attribute) OR preg_match($pattern4, $this->$attribute))
            $this->addError($attribute, 'Error, verifique que no este repetidos continuamente los caracteres');
        if (preg_match($pattern5, $this->$attribute))
            $this->addError($attribute, 'Error, no puede haber un número superior a 9999999999999');
    }
    public function validaTelefono($attribute, $params) {
        $pattern = '/^([\+]*[0-9]+)$/';
        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'No es válido, la forma es Ej; +5698142785 o 98142785');
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppReclamos()
    {
        return $this->hasMany(AppReclamo::className(), ['us_id' => 'us_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppSugerencias()
    {
        return $this->hasMany(AppSugerencia::className(), ['us_id' => 'us_id']);
    }
}
