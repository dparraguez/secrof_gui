<?php 
namespace app\models;
use Yii;
//use yii\base\model;
use yii\base\Model;


class ValidarFormularioAjax extends model
{

	public $fecha_inicio;
	public $fecha_termino;
	public $valorcombustible;

	public function rules()
	{
return [
['fecha_inicio','required','message'=> 'Campo obligatorio'],
['fecha_inicio','match','pattern'=> "/^.{3,50}$/", 'message'=> 'Minimo 3 y maximo 50 caracteres'],

[['fecha_inicio'], 'validateFechainicio'],

['fecha_termino','required','message'=> 'Campo obligatorio'],
['fecha_termino','match','pattern'=> "/^.{3,50}$/", 'message'=> 'Minimo 3 y maximo 50 caracteres'],

[['fecha_termino'], 'validateFechatermino'],

['valorcombustible','required','message'=> 'Campo obligatorio'],
 ['valorcombustible', 'match', 'pattern' => "/^[0-9]+$/i", 'message' => 'Sólo se aceptan números'],
];

	}


public function attributeLabels()
{
return [
"fecha inicio"=>'Fecha de inicio:',
'fecha termino'=>'Fecha de término:',
"valorcombustible"=> 'Valor del combustible',

];

}



public function email_existe($attribute,$params)
{

$email=["manuel@mail.com","antonio@mail.com"];
foreach ($email as $val ) 
{
if($this->email==$val)
{

$this->addError($attribute,"El mail seleccionado existe");
return true;
}

else
{
	
return false;

}

}


}



public function validateFechainicio($attribute, $params){
	$fecha_actual=date('Y-m-d');
if(strtotime($this->fecha_inicio)>strtotime($fecha_actual)){
	$this->addError('fecha_inicio', 'la fecha de inicio no puede ser superior a la actual');
}

}







public function validateFechatermino($attribute, $params) {
   $fecha_actual=date('Y-m-d');

if(strtotime($this->fecha_inicio)>strtotime($this->fecha_termino)){

	 $this->addError('fecha_termino', 'la fecha de termino no puede ser menor que la fecha de inicio');
}

   if(strtotime($this->fecha_termino)>strtotime($fecha_actual)){

	 $this->addError('fecha_termino', 'la fecha de termino no puede ser superior a la actual');
}


    }






}


?>