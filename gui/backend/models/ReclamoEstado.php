<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reclamo_estado".
 *
 * @property integer $re_id
 * @property integer $es_id
 * @property string $re_es_fecha
 *
 * @property AppReclamo $re
 * @property WebEstado $es
 */
class ReclamoEstado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reclamo_estado';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_movil');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['re_id', 'es_id'], 'required'],
            [['re_id', 'es_id'], 'integer'],
            [['re_es_fecha'], 'safe'],
            [['re_id'], 'exist', 'skipOnError' => true, 'targetClass' => AppReclamo::className(), 'targetAttribute' => ['re_id' => 're_id']],
            [['es_id'], 'exist', 'skipOnError' => true, 'targetClass' => WebEstado::className(), 'targetAttribute' => ['es_id' => 'es_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            're_id' => 'Re ID',
            'es_id' => 'Es ID',
            're_es_fecha' => 'Re Es Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRe()
    {
        return $this->hasOne(AppReclamo::className(), ['re_id' => 're_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEs()
    {
        return $this->hasOne(WebEstado::className(), ['es_id' => 'es_id']);
    }
}
