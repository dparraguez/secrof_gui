<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tramo".
 *
 * @property integer $id
 * @property integer $id_rutas
 * @property string $geometry
 *
 * @property Rutas $idRutas
 */
class Tramo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tramo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_rutas'], 'integer'],
            [['geometry'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_rutas' => 'Id Rutas',
            'geometry' => 'Geometry',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRutas()
    {
        return $this->hasOne(Rutas::className(), ['id' => 'id_rutas']);
    }
}
