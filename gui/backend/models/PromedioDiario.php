<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "promedio_diario".
 *
 * @property integer $id
 * @property integer $id_cuadra
 * @property integer $id_zona_servicio
 * @property string $fecha
 * @property integer $promedio
 * @property integer $cantidad_posiciones
 *
 * @property Cuadra $idCuadra
 * @property ZonaServicio $idZonaServicio
 */
class PromedioDiario extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promedio_diario';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_cuadra', 'id_zona_servicio', 'promedio', 'cantidad_posiciones'], 'integer'],
            [['fecha'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_cuadra' => 'Id Cuadra',
            'id_zona_servicio' => 'Id Zona Servicio',
            'fecha' => 'Fecha',
            'promedio' => 'Promedio',
            'cantidad_posiciones' => 'Cantidad Posiciones',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCuadra()
    {
        return $this->hasOne(Cuadra::className(), ['id' => 'id_cuadra']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdZonaServicio()
    {
        return $this->hasOne(ZonaServicio::className(), ['id' => 'id_zona_servicio']);
    }
}
