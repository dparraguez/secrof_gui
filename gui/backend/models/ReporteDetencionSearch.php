<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReporteDetencion;

/**
 * ReporteInicioDescargadoSearch represents the model behind the search form about `app\models\ReporteInicioDescargado`.
 */
class ReporteDetencionSearch extends ReporteDetencion
{
    public $patenteVehiculo;
    public $fechaInicio;
    public $fechaFin;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'inicio', 'tiempo', 'tipo', 'id_vehiculo'], 'integer'],
            [['patenteVehiculo', 'fechaInicio', 'fechaFin'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReporteDetencion::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'inicio',
                'tiempo',
                'tipo',
                'patenteVehiculo' => [
                    'asc' => ['vehiculo.patente' => SORT_ASC],
                    'desc' => ['vehiculo.patente' => SORT_DESC],
                    'label' => 'Vehículo'
                ]
            ]
        ]);


        if (!($this->load($params) && $this->validate())) {
            /**
             * The following line will allow eager loading with country data
             * to enable sorting by country on initial loading of the grid.
             */
            $query->joinWith(['vehiculo']);
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'inicio' => $this->inicio,
            'tiempo' => $this->tiempo,
            'tipo' => $this->tipo,
            'id_vehiculo' => $this->id_vehiculo,
        ]);

        if (!empty($this->fechaInicio)) {
            $query->andFilterWhere([
                '>=', 'inicio', strtotime($this->fechaInicio)+3600*3
            ]);
        }

        if (!empty($this->fechaFin)) {
            $query->andFilterWhere([
                '<=', 'inicio', (strtotime($this->fechaFin)+3600*27)
            ]);
        }
        if (!empty($this->patenteVehiculo)) {
            $query->joinWith(['vehiculo' => function ($q) {
                $q->where('vehiculo.id ='.$this->patenteVehiculo);
            }]);
        }
        return $dataProvider;
    }
}
