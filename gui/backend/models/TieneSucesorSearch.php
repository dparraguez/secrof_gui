<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TieneSucesor;

/**
 * TieneSucesorSearch represents the model behind the search form about `app\models\TieneSucesor`.
 */
class TieneSucesorSearch extends TieneSucesor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['es_id', 'web_es_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TieneSucesor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'es_id' => $this->es_id,
            'web_es_id' => $this->web_es_id,
        ]);

        return $dataProvider;
    }
}
