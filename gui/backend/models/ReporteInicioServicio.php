<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reporte_inicio_servicio".
 *
 * @property integer $id
 * @property integer $id_zona_servicio
 * @property integer $fecha_inicio_real
 * @property integer $fecha_inicio_programada
 *
 * @property ZonaServicio $idZonaServicio
 */
class ReporteInicioServicio extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reporte_inicio_servicio';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_zona_servicio', 'fecha_inicio_real', 'fecha_inicio_programada'], 'integer'],
            [['id_zona_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => ZonaServicio::className(), 'targetAttribute' => ['id_zona_servicio' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_zona_servicio' => 'Id Zona Servicio',
            'fecha_inicio_real' => 'Fecha Inicio Real',
            'fecha_inicio_programada' => 'Fecha Inicio Programada',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZonaServicio()
    {
        return $this->hasOne(ZonaServicio::className(), ['id' => 'id_zona_servicio']);
    }

    /* Getter for country name */
    public function getNombreZonaServicio()
    {
        return $this->zonaServicio->nombre;
    }
}
