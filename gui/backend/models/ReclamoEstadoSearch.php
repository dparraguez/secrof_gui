<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReclamoEstado;

/**
 * ReclamoEstadoSearch represents the model behind the search form about `app\models\ReclamoEstado`.
 */
class ReclamoEstadoSearch extends ReclamoEstado
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['re_id', 'es_id'], 'integer'],
            [['re_es_fecha'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReclamoEstado::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            're_id' => $this->re_id,
            'es_id' => $this->es_id,
            're_es_fecha' => $this->re_es_fecha,
        ]);

        return $dataProvider;
    }
}
