<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "calle".
 *
 * @property integer $id
 * @property string $nombre
 * @property string $puntos
 */
class Calle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['puntos'], 'string'],
            [['nombre'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'puntos' => 'Puntos',
        ];
    }
}
