<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_reclamo".
 *
 * @property integer $re_id
 * @property string $re_descripcion
 * @property string $re_fecha_app
 * @property string $re_estadoactual
 * @property string $re_hora_app
 * @property string $re_foto
 * @property string $re_latitud
 * @property string $re_longitud
 * @property string $re_prioridad
 * @property integer $us_id
 * @property integer $ti_id
 *
 * @property AppUsuario $us
 * @property WebTipoReclamo $ti
 * @property ReclamoEstado[] $reclamoEstados
 * @property WebEstado[] $es
 */
class AppReclamo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_reclamo';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_movil');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['re_estadoactual'], 'string', 'max' => 15],
            [['ti_id'], 'required', 'message' => 'Campo requerido'],
            [['re_descripcion'], 'required', 'message' => 'Campo requerido'],
            [['re_latitud', 're_longitud'], 'safe'],
            ['re_descripcion','string','min' => 3, 'max' => 250, 'message' => 'Mínimo 3 y máximo 25 caracteres'],
            ['re_descripcion','validatexto'],
            ['re_descripcion','validatexto2'],
            ['re_descripcion','validatexto3'],
            [['re_hora_app'], 'string', 'max' => 5],
            [['re_foto'], 'string', 'max' => 100],
            [['us_id'], 'exist', 'skipOnError' => true, 'targetClass' => AppUsuario::className(), 'targetAttribute' => ['us_id' => 'us_id']],
            [['ti_id'], 'exist', 'skipOnError' => true, 'targetClass' => WebTipoReclamo::className(), 'targetAttribute' => ['ti_id' => 'ti_id']],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            're_id' => 'Identificador',
            're_descripcion' => 'Descripción',
            're_fecha_app' => 'Fecha App',
            're_estadoactual' => 'Estado Actual',
            're_hora_app' => 'Hora App',
            're_foto' => 'Foto',
            're_latitud' => 'Latitud',
            're_longitud' => 'Longitud',
            're_prioridad' => 'Prioridad',
            'us_id' => 'Id. Usuario',
            'ti_id' => 'Tipo Reclamo',
        ];
        
    }
 public function validatexto($attribute, $params) {
        $pattern = '/^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,3}[a-zA-ZñÑÁÉÍÓÚáéíóú]+)*)$/';
        
        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'Error sólo letras o verifique que no tenga espacios al final');
    }

  public function validatexto2($attribute, $params) {
        $pattern = '/^([a-zA-ZñÑÁÉÍÓÚáéíóú0-9º°\.\,\'\"\)\(\-\@\:\/\+]+([[:space:]]{0,2}[a-zA-ZñÑÁÉÍÓÚáéíóú0-9º°\.\,\'\"\)\(\-\@\:\/\+]+)*)$/';
        $pattern2 = '/^([0-9º°\.\,\'\"\)\(\-\@\:\/\+]+)$/';
        
        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'Error sólo letras o letras y número, verifique que no tenga espacios al final o muchos en medio.');
        if (preg_match($pattern2, $this->$attribute))
            $this->addError($attribute, 'Error No puede ser solo números o caracteres especiales');
    }

    public function validatexto3($attribute, $params) {
        $pattern2 = '/(a{2}|e{2}|i{2}|o{2}|u{2}|b{2}|c{2}|d{2}|f{2}|g{2}|h{2}|j{2}|k{2}|l{3}|m{2}|n{2}|ñ{2}|p{2}|q{3}|r{2}|s{2}|t{2}|v{2}|w{2}|x{2}|y{2}|z{2}|º{2}|°{2}|\.{2}|\'{2}|\"{2}|\){2}|\({2}|\,{2}|\-{2}|\@{2}|\:{2}|\/{3}|\+{2})/i';
        $pattern3 = '/(A{2}|E{2}|I{2}|O{2}|U{2}|B{2}|C{2}|D{2}|F{2}|G{2}|H{2}|J{2}|K{2}|L{3}|M{2}|N{2}|Ñ{2}|P{2}|Q{3}|R{2}|S{2}|T{2}|V{2}|W{2}|X{2}|Y{2}|Z{2})/i';
        $pattern4 = '/(á{2}|Á{2}|é{2}|É{2}|í{2}|Í{2}|ó{2}|Ó{2}|ú{2}|Ú{2})/i';
        $pattern5 = '/([0-9]{13})/i';
        

        if (preg_match($pattern2, $this->$attribute) OR preg_match($pattern3, $this->$attribute) OR preg_match($pattern4, $this->$attribute))
            $this->addError($attribute, 'Error, verifique que no este repetidos continuamente los caracteres');
        if (preg_match($pattern5, $this->$attribute))
            $this->addError($attribute, 'Error, no puede haber un número superior a 9999999999999');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUs()
    {
        return $this->hasOne(AppUsuario::className(), ['us_id' => 'us_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTi()
    {
        return $this->hasOne(WebTipoReclamo::className(), ['ti_id' => 'ti_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReclamoEstados()
    {
        return $this->hasMany(ReclamoEstado::className(), ['re_id' => 're_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEs()
    {
        return $this->hasMany(WebEstado::className(), ['es_id' => 'es_id'])->viaTable('reclamo_estado', ['re_id' => 're_id']);
    }
}
