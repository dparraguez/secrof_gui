<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cuadra".
 *
 * @property integer $id
 * @property integer $id_calle
 * @property string $geography
 * @property string $latitud_inicio
 * @property string $longitud_inicio
 * @property string $latitud_fin
 * @property string $longitud_fin
 */
class Cuadra extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cuadra';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_calle'], 'integer'],
            [['geography'], 'string'],
            [['latitud_inicio', 'longitud_inicio', 'latitud_fin', 'longitud_fin'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_calle' => 'Id Calle',
            'geography' => 'Geography',
            'latitud_inicio' => 'Latitud Inicio',
            'longitud_inicio' => 'Longitud Inicio',
            'latitud_fin' => 'Latitud Fin',
            'longitud_fin' => 'Longotud Fin',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalle()
    {
        return $this->hasOne(Calle::className(), ['id' => 'id_calle']);
    }

    
}
