<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_inspector".
 *
 * @property integer $in_id
 * @property string $in_nombres
 * @property string $in_apellidos
 * @property string $in_rut
 * @property string $in_fono
 * @property string $in_clave
 *
 * @property AppReporte[] $appReportes
 */
class AppInspector extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_inspector';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_movil');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['in_nombres', 'in_apellidos', 'in_clave'], 'string', 'max' => 25],
            [['in_rut', 'in_fono'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'in_id' => 'ID',
            'in_nombres' => 'Nombres',
            'in_apellidos' => 'Apellidos',
            'in_rut' => 'Rut',
            'in_fono' => 'Fono',
            'in_clave' => 'Clave',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppReportes()
    {
        return $this->hasMany(AppReporte::className(), ['in_id' => 'in_id']);
    }
}
