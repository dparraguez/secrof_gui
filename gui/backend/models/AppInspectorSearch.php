<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppInspector;

/**
 * AppInspectorSearch represents the model behind the search form about `app\models\AppInspector`.
 */
class AppInspectorSearch extends AppInspector
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['in_id'], 'integer'],
            [['in_nombres', 'in_apellidos', 'in_rut', 'in_fono', 'in_clave'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppInspector::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'in_id' => $this->in_id,
        ]);

        $query->andFilterWhere(['like', 'in_nombres', $this->in_nombres])
            ->andFilterWhere(['like', 'in_apellidos', $this->in_apellidos])
            ->andFilterWhere(['like', 'in_rut', $this->in_rut])
            ->andFilterWhere(['like', 'in_fono', $this->in_fono])
            ->andFilterWhere(['like', 'in_clave', $this->in_clave]);

        return $dataProvider;
    }
}
