<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppSugerencia;

/**
 * AppSugerenciaSearch represents the model behind the search form about `app\models\AppSugerencia`.
 */
class AppSugerenciaSearch extends AppSugerencia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['su_id', 'us_id'], 'integer'],
            [['su_tipo', 'su_descripcion', 'su_fecha_app', 'su_hora_app', 'su_estado', 'su_fecha_hora_web'], 'safe'],
            [['su_latitud', 'su_longitud'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppSugerencia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'su_id' => $this->su_id,
            'su_fecha_app' => $this->su_fecha_app,
            'su_latitud' => $this->su_latitud,
            'su_longitud' => $this->su_longitud,
            'su_fecha_hora_web' => $this->su_fecha_hora_web,
            'us_id' => $this->us_id,
        ]);

        $query->andFilterWhere(['like', 'su_tipo', $this->su_tipo])
            ->andFilterWhere(['like', 'su_descripcion', $this->su_descripcion])
            ->andFilterWhere(['like', 'su_hora_app', $this->su_hora_app])
            ->andFilterWhere(['like', 'su_estado', $this->su_estado]);

        return $dataProvider;
    }
}
