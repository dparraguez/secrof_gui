<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WebEstado;

/**
 * WebEstadoSearch represents the model behind the search form about `app\models\WebEstado`.
 */
class WebEstadoSearch extends WebEstado
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['es_id'], 'integer'],
            [['es_nombre', 'es_descripcion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WebEstado::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'es_id' => $this->es_id,
        ]);

        $query->andFilterWhere(['like', 'es_nombre', $this->es_nombre])
            ->andFilterWhere(['like', 'es_descripcion', $this->es_descripcion]);

        return $dataProvider;
    }
}
