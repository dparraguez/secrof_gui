<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reporte_inicio_descargado".
 *
 * @property integer $id
 * @property integer $entrada_estacionamiento
 * @property integer $salida_vertedero
 * @property integer $tiempo
 * @property integer $id_vehiculo
 *
 * @property Vehiculo $idVehiculo
 */
class ReporteInicioDescargado extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reporte_inicio_descargado';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entrada_estacionamiento', 'salida_vertedero', 'tiempo', 'id_vehiculo'], 'integer'],
            [['id_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['id_vehiculo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'entrada_estacionamiento' => 'Entrada Estacionamiento',
            'salida_vertedero' => 'Salida Vertedero',
            'tiempo' => 'Tiempo',
            'id_vehiculo' => 'Id Vehiculo',
            'patenteVehiculo' => Yii::t('app', 'Vehículo')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }

    /* Getter for country name */
    public function getPatenteVehiculo() {
        return $this->vehiculo->patente;
    }
}
