<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reporte_detencion".
 *
 * @property integer $id
 * @property integer $inicio
 * @property integer $tiempo
 * @property integer $tipo
 * @property integer $id_vehiculo
 *
 * @property Vehiculo $idVehiculo
 */
class ReporteDetencion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reporte_detencion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['inicio', 'tiempo', 'tipo', 'id_vehiculo'], 'integer'],
            [['id_vehiculo'], 'exist', 'skipOnError' => true, 'targetClass' => Vehiculo::className(), 'targetAttribute' => ['id_vehiculo' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inicio' => 'Inicio',
            'tiempo' => 'Tiempo',
            'tipo' => 'Tipo',
            'id_vehiculo' => 'Id Vehiculo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehiculo()
    {
        return $this->hasOne(Vehiculo::className(), ['id' => 'id_vehiculo']);
    }

    /* Getter for country name */
    public function getPatenteVehiculo()
    {
        return $this->vehiculo->patente;
    }

    /**
    * Tipos de detencion
    */
    const TIPO_NO_PERMITIDO=0;
    const TIPO_PERMITIDO=1;

    static public $tipos =[
        self::TIPO_PERMITIDO => 'Permitido',
        self::TIPO_NO_PERMITIDO => 'No permitido'
    ];
}
