<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ZonaServicio;

/**
 * ZonaServicioSearch represents the model behind the search form about `app\models\ZonaServicio`.
 */
class ZonaServicioSearch extends ZonaServicio
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'horario', 'dia_lunes', 'dia_martes', 'dia_miercoles', 'dia_jueves', 'dia_viernes', 'dia_sabado', 'dia_domingo'], 'integer'],
            [['observacion', 'nombre', 'zona', 'puntos', 'hora_inicio', 'hora_fin'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ZonaServicio::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'horario' => $this->horario,
            'dia_lunes' => $this->dia_lunes,
            'dia_martes' => $this->dia_martes,
            'dia_miercoles' => $this->dia_miercoles,
            'dia_jueves' => $this->dia_jueves,
            'dia_viernes' => $this->dia_viernes,
            'dia_sabado' => $this->dia_sabado,
            'dia_domingo' => $this->dia_domingo,
        ]);

        $query->andFilterWhere(['like', 'observacion', $this->observacion])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'zona', $this->zona])
            ->andFilterWhere(['like', 'puntos', $this->puntos])
            ->andFilterWhere(['like', 'hora_inicio', $this->hora_inicio])
            ->andFilterWhere(['like', 'hora_fin', $this->hora_fin]);

        return $dataProvider;
    }
}
