<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posicion_zona".
 *
 * @property integer $id
 * @property integer $id_zona
 * @property integer $id_posicion
 * @property integer $fecha
 * @property integer $estado
 *
 * @property Posicion $idPosicion
 * @property Zona $idZona
 */
class PosicionZona extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'posicion_zona';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_zona', 'id_posicion', 'fecha'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_zona' => 'Id Zona',
            'id_posicion' => 'Id Posicion',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdPosicion()
    {
        return $this->hasOne(Posicion::className(), ['id' => 'id_posicion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdZona()
    {
        return $this->hasOne(Zona::className(), ['id' => 'id_zona']);
    }
}
