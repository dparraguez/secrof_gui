<?php

namespace backend\controllers;

use Yii;
use app\models\TieneSucesor;
use app\models\TieneSucesorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TieneSucesorController implements the CRUD actions for TieneSucesor model.
 */
class TieneSucesorController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TieneSucesor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TieneSucesorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TieneSucesor model.
     * @param integer $es_id
     * @param integer $web_es_id
     * @return mixed
     */
    public function actionView($es_id, $web_es_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($es_id, $web_es_id),
        ]);
    }

    /**
     * Creates a new TieneSucesor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TieneSucesor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'es_id' => $model->es_id, 'web_es_id' => $model->web_es_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TieneSucesor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $es_id
     * @param integer $web_es_id
     * @return mixed
     */
    public function actionUpdate($es_id, $web_es_id)
    {
        $model = $this->findModel($es_id, $web_es_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'es_id' => $model->es_id, 'web_es_id' => $model->web_es_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TieneSucesor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $es_id
     * @param integer $web_es_id
     * @return mixed
     */
    public function actionDelete($es_id, $web_es_id)
    {
        $this->findModel($es_id, $web_es_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TieneSucesor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $es_id
     * @param integer $web_es_id
     * @return TieneSucesor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($es_id, $web_es_id)
    {
        if (($model = TieneSucesor::findOne(['es_id' => $es_id, 'web_es_id' => $web_es_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
