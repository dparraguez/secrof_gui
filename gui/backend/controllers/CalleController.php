<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use app\models\Calle;
use app\models\Cuadra;
use app\models\CalleSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CalleController implements the CRUD actions for Calle model.
 */
class CalleController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' =>\yii\filters\AccessControl::className(),
                //'only' => ['logout', 'user', 'admin'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['index', 'view','create','update','delete','mapa','seleccionarcuadra','findModel'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            if(Yii::$app->user->identity->tipo==User::AUTH_ADMIN){
                                return true;
                            }
                            else false;

                        },
                    ],
                    
                ],
            ],
            //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
            //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Calle models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CalleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Calle model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Calle model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Calle();
        
        if ($model->load(Yii::$app->request->post())) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                if( $model->save()){
                    $array_puntos = explode("_", $model->puntos);

                    $i=0;

                    foreach ($array_puntos as $punto) {
                        if($i==0){
                            $puntoBase = $punto;
                            $i=1;
                        } else {
                            $cuadra = new Cuadra();

                            $cuadra->id_calle = $model->id;
                            $punto_inicio = explode(",", $puntoBase);
                            $cuadra->latitud_inicio = $punto_inicio[0];
                            $cuadra->longitud_inicio = $punto_inicio[1];

                            $punto_fin = explode(",", $punto);
                            $cuadra->latitud_fin = $punto_fin[0];
                            $cuadra->longitud_fin = $punto_fin[1];

                            $cuadra->save();

                            $puntoBase = $punto;
                        }
                    }
                    $sql = "update cuadra set geography=ST_GeographyFromText('LINESTRING(' || latitud_inicio ||' ' || longitud_inicio ||' , ' || latitud_fin ||' ' || longitud_fin ||' )')";
                    $result =  $connection->createCommand($sql)->queryAll();

                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $model->id]);
                }

            } catch (Exception $e) {
                $transaction->rollBack();
            }

        } 


        return $this->render('create', [
            'model' => $model,
        ]);
        
    }

    /**
     * Updates an existing Calle model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                if( $model->save()){

                    $array_puntos = explode("_", $model->puntos);

                    $i=0;

                    foreach ($array_puntos as $punto) {
                        if($i==0){
                            $puntoBase = $punto;
                            $i=1;
                        } else {
                            $cuadra = new Cuadra();

                            $cuadra->id_calle = $model->id;
                            $punto_inicio = explode(",", $puntoBase);
                            $cuadra->latitud_inicio = $punto_inicio[0];
                            $cuadra->longitud_inicio = $punto_inicio[1];

                            $punto_fin = explode(",", $punto);
                            $cuadra->latitud_fin = $punto_fin[0];
                            $cuadra->longitud_fin = $punto_fin[1];

                            $cuadra->save();

                            $puntoBase = $punto;
                        }
                    }
                    $sql = "update cuadra set geography=ST_GeographyFromText('LINESTRING(' || latitud_inicio ||' ' || longitud_inicio ||' , ' || latitud_fin ||' ' || longitud_fin ||' )') where id_calle=".$model->id;
                    $result =  $connection->createCommand($sql)->queryAll();

                    $transaction->commit();

                    return $this->redirect(['view', 'id' => $model->id]);
                }

            } catch (Exception $e) {
                $transaction->rollBack();
            }
        }


        return $this->render('update', [
            'model' => $model,
        ]);
        
    }

     public function actionMapa()
    {

        $calles = Calle::find()->all();

        $array_calles = array();

        foreach ($calles as $calle) {

            $array_puntos = explode("_", $calle->puntos);

            $i=0;
            foreach ($array_puntos as $punto) {
                if($i == 0){
                    //primer caso
                    $punto_base = $punto;
                    $i=1;
                } else {

                    $coordenadas_base = explode(",", $punto_base);
                    $coordenadas  = explode(",", $punto);

                    $linea=array();

                    $linea['primer_punto']=$coordenadas_base[0].",".$coordenadas_base[1];
                    $linea['segundo_punto']=$coordenadas[0].",".$coordenadas[1];

                    $lineas[]=$linea;

                    $punto_base=$punto;
                }
            }
            $array_calles[] = $lineas;

        }
        return $this->render('mapa', [
            'array_calles' => $array_calles,
        ]);

    }

    public function actionSeleccionarcuadra()
    {

        $cuadras = Cuadra::find()->all();

        
        return $this->render('seleccionarcuadra', [
            'cuadras' => $cuadras,
        ]);

    }


    /**
     * Deletes an existing Calle model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Calle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Calle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Calle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
