<?php
namespace backend\controllers;


use Yii;
use common\models\User;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use common\models\FormularioReporte;
use app\models\Posicion;
use app\models\ZonaServicio;
use app\models\Zona;
use app\models\Vehiculo;
use app\models\ReporteIndice;
use app\models\ReporteInicioDescargado;
use app\models\ReporteInicioDescargadoSearch;
use app\models\ReporteInicioServicio;
use app\models\ReporteInicioServicioSearch;
use app\models\ReporteDetencion;
use app\models\ReporteDetencionSearch;
use app\models\ReporteComportamiento;
use app\models\PosicionSearch;
use app\models\PosicionZona;
use yii\filters\VerbFilter;
use yii\widgets\DetailView;

/**
 * Site controller
 */
class ReporteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' =>\yii\filters\AccessControl::className(),
                'only' => ['eventozona'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['eventozona','calculariniciodescargado'],
                    ],
                    [
                        'actions' => ['index', 'view','create','update','delete','findModel','detencion', 'comportamiento', 'inicioruta', 'iniciodescargado'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            if (Yii::$app->user->identity->tipo==User::AUTH_FISCALIZADOR || Yii::$app->user->identity->tipo==User::AUTH_ADMIN) {
                                return true;
                            } else {
                                false;
                            }
                        },
                    ]
                    
                ],
            ],
            //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
            //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        return $this->render('reportes');
    }

    public function actionCalcularDetencion()
    {
        ini_set('max_execution_time', 3000);
        $reporteIndice = ReporteIndice::findOne(['tipo' => ReporteIndice::TIPO_CALCULO_DETENCION]);
        if (empty($reporteIndice->fecha)) {
            //para generar tabla, en la inicial se usa normalmente normalmente,
            //para reiniciar los datos se deve vaciar la tabla del reporte y borrar la
            //tupla del reporte en ReporteIndice
            $fecha_inicial= 1419984000; //Fecha base 31:12:2014 00:00:00 Hora local
            $fecha_inicial += 3600*3; //Fecha base 31:12:2014 00:00:00 UTC
        } else {
            $fecha_inicial=$reporteIndice->fecha;
        }

        $dia_segundos = 86400;

        $fecha_final = $fecha_inicial + 86400;

        $vehiculos = Vehiculo::find()->all();
        $zonas = Zona::find()->all();

        foreach ($vehiculos as $vehiculo) {
            $posiciones = Posicion::find()
                ->andWhere(['>=', 'fecha', $fecha_inicial])
                ->andWhere(['<', 'fecha', $fecha_final])
                ->andWhere(['=', 'id_vehiculo', $vehiculo->id])
                ->orderBy('fecha ASC')
                ->all();

            $primera_posicion = true;
            $tiempo_detencion = 0;
            foreach ($posiciones as $posicion) {
                if ($primera_posicion) {
                    $posicion_base = $posicion;
                    $primera_posicion=false;
                } else {
                    //Verificar si la posición permanece a 50 metros de la posición base
                    $q_detenido = "select ST_Covers( ST_Buffer('".$posicion_base->geo."'::geography ,50)::geometry , '".$posicion->geo."'::geometry ) AS detenido";
                    $connection = \Yii::$app->db;
                    $row_detenido =  $connection->createCommand($q_detenido)->queryAll();
                    if ($row_detenido[0]['detenido']=='t') {
                        //Si permanece dentro de 50 metros, se calcula el tiempo
                        $tiempo_detencion = $posicion->fecha - $posicion_base->fecha;
                    } else {
                        //No permanece dentro de 50, se verifica el tiempo de detención, si es mayor a 15 minutos se almacena.
                        if ($tiempo_detencion>900) {
                            $tipo_detencion=0;
                            //Verifica si la posicion se encuentra dentro de la zona, lo que significa que esta autorizado
                            foreach ($zonas as $zona) {
                                $sql = "select ST_Covers('$zona->poligono'::geometry, '$posicion_base->geo'::geometry) as estado";
                                //echo $sql."<br>";
                                $connection = \Yii::$app->db;
                                $resultado_estado =  $connection->createCommand($sql)->queryAll();

                                if (!$resultado_estado[0]['estado']) {
                                    $tipo_detencion=1;
                                }
                            }

                            $reporteDetencion = new ReporteDetencion();
                            $reporteDetencion->inicio = $posicion->fecha;
                            $reporteDetencion->tiempo = $tiempo_detencion;
                            $reporteDetencion->tipo = $tipo_detencion;
                            $reporteDetencion->id_vehiculo = $vehiculo->id;
                            $reporteDetencion->save();
                        }
                        //se actualiza la posición base y se elimina el tiempo de detencion
                        $posicion_base = $posicion;
                        $tiempo_detencion = 0;
                    }
                }
            }
            echo "tiempo_detencion ".$tiempo_detencion."<br>";
            if ($tiempo_detencion>900) {
                $tipo_detencion=0;
                //Verifica si la posicion se encuentra dentro de la zona, lo que significa que esta autorizado
                foreach ($zonas as $zona) {
                    $sql = "select ST_Covers('$zona->poligono'::geometry, '$posicion_base->geo'::geometry) as estado";
                    //echo $sql."<br>";
                    $connection = \Yii::$app->db;
                    $resultado_estado =  $connection->createCommand($sql)->queryAll();

                    if (!$resultado_estado[0]['estado']) {
                        $tipo_detencion=1;
                    }
                }

                $reporteDetencion = new ReporteDetencion();
                $reporteDetencion->inicio = $posicion->fecha;
                $reporteDetencion->tiempo = $tiempo_detencion;
                $reporteDetencion->tipo = $tipo_detencion;
                $reporteDetencion->id_vehiculo = $vehiculo->id;
                $reporteDetencion->save();
            }
        }

        //Al terminar guarda la fecha en el ReporteIndice
        if (empty($reporteIndice->fecha)) {
            $reporteIndice = new ReporteIndice();
            $reporteIndice->tipo = ReporteIndice::TIPO_CALCULO_DETENCION;
            $reporteIndice->fecha = $fecha_final;
            $reporteIndice->save();
        } else {
            $reporteIndice->fecha = $fecha_final;
            $reporteIndice->save();
        }
    }

    public function actionHistorico()
    {
        $searchModel = new PosicionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('historico', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDetencion()
    {
        $searchModel = new ReporteDetencionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('detencion', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

/*
    public function actionDetencion()
    {
        $model = new FormularioReporte();
        $model->scenario = 'detencion';
        $detenciones = [];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $dia_segundos = 86400;
            $fecha_inicio_segundos = strtotime($model->fecha_inicio);
            $fecha_fin = strtotime($model->fecha_fin);
            $fecha_fin_segundos += $dia_segundos;

            $fecha_inicio_segundos += 10800; //UTC
            $fecha_fin_segundos += 10800; //UTC

            $sql = "select * from vehiculo ";
            //$vehiculos = Vehiculo::find()->all();
            //echo $sql."<br>";
            $connection = \Yii::$app->db;
            $vehiculos =  $connection->createCommand($sql)->queryAll();

            $zonas = Zona::findAll(['tipo'=>[1,2]]);
            $j=0;

            foreach ($vehiculos as $vehiculo) {
                $sql = "select * from posicion where id_vehiculo=".$vehiculo['id']." and fecha<$fecha_fin_segundos and fecha>$fecha_inicio_segundos order by fecha asc";
                //echo $sql."<br>";
                $connection = \Yii::$app->db;
                $posiciones =  $connection->createCommand($sql)->queryAll();
                $i=0;
                $k=0;

                foreach ($posiciones as $posicion) {
                    if ($i==0) {
                        $posicion_base = $posicion;
                        $i=1;
                    } else {
                        $diferencia = $posicion['fecha'] - $posicion_base['fecha'];
                        
                        if ($diferencia > 3500) {
                            $q_detenido = "select ST_Covers( ST_Buffer('".$posicion['geo']."'::geography ,100)::geometry , '".$posicion_base['geo']."'::geometry ) AS detenido";
                            $row_detenido =  $connection->createCommand($q_detenido)->queryAll();
                            $detenido = 0;
                            if ($row_detenido[0]['detenido']=='t') {
                                $detenido = 1;
                            }

                            if ($k==0) {
                                $estacionado = 0;
                                $nombre_zona = "";

                                //Verificar si el vahiculo se encuentra en una zona de estacionamiento permitida
                                foreach ($zonas as $zona) {
                                    $q_estacionado = "select ST_Covers('".$zona->poligono."', '".$posicion_base['geo']."'::geometry ) AS estacionado";
                                    //echo $q_estacionado;
                                    $row_estacionado =  $connection->createCommand($q_estacionado)->queryAll();

                                    if ($row_estacionado[0]['estacionado']=='t') {
                                        $estacionado = 1;
                                        $nombre_zona = $zona->nombre;
                                    }
                                }

                                $detencion[$j]['patente'] = $vehiculo['patente'];
                                $detencion[$j]['id_posicion'] = $posicion_base['id'];
                                $detencion[$j]['fecha'] = date("d-m-Y H:i:s", ($posicion_base['fecha']-3600*3));
                                $detencion[$j]['diferencia'] = $diferencia;
                                if ($estacionado == 1) {
                                    $detencion[$j]['tipo'] = "Permitido ".$nombre_zona;
                                    //echo $vehiculo['patente']." Fecha: ".date("d-m-Y H:i:s", ($posicion_base['fecha']-3600*3))."  ".$posicion_base['fecha']." Detenido: ".$diferencia." segundos ESTACIONADO".$posicion_base['velocidad']." ".$posicion_base['id']."<br>";
                                } else {
                                    $detencion[$j]['tipo'] = "No permitido";
                                    //echo $vehiculo['patente']." Fecha: ".date("d-m-Y H:i:s", ($posicion_base['fecha']-3600*3))."  ".$posicion_base['fecha']." Detenido: ".$diferencia." segundos ".$posicion_base['velocidad']." ".$posicion_base['id']."<br>";
                                }
                                
                                $k=1;
                            } elseif ($detenido == 1) {
                                $detencion[$j]['diferencia'] += $diferencia;
                                $k=1;
                            } else {
                                $detenciones[]= $detencion[$j];
                                $j++;
                                $k=0;
                            }
                        }
                        $posicion_base = $posicion;
                    }
                }

                if ($k==1) {
                    $detencion[$j]['diferencia'] += $diferencia;
                    $detenciones[]= $detencion[$j];
                    $j++;
                }
            }
        }

        return $this->render('detencion', [
            'model' => $model,
            'detenciones' => $detenciones,
        ]);
    }
*/
    public function actionDetencionform()
    {
        $model = new FormularioReporte();
        return $this->render('detencionform', [
                'model' => $model,
            ]);
    }

    public function actionCalcularComportamiento()
    {
        ini_set('max_execution_time', 3000);

        $zonasServicio = ZonaServicio::find()->all();
        $reporteIndice = ReporteIndice::findOne(['tipo' => ReporteIndice::TIPO_CALCULO_COMPORTAMIENTO]);
        if (empty($reporteIndice->fecha)) {
            //para generar tabla, en la inicial se usa normalmente normalmente,
            //para reiniciar los datos se deve vaciar la tabla del reporte y borrar la
            //tupla del reporte en ReporteIndice
            $fecha_inicial= 1419984000; //Fecha base 31:12:2014 00:00:00 Hora local
            $fecha_inicial += 3600*3; //Fecha base 31:12:2014 00:00:00 UTC
        } else {
            $fecha_inicial=$reporteIndice->fecha;
        }

        $dia_segundos = 86400;

        // Para generar tabla es recomendable avanzar varios días
        //$fecha_final = $fecha_inicial+($dia_segundos*10); //10 días

        // Para funcionamiento normal 1 día
        $fecha_final = $fecha_inicial+($dia_segundos); //10 días

        $vehiculos = Vehiculo::find()->all();
        $zonas = Zona::find()->all();

        foreach ($vehiculos as $vehiculo) {
            echo $vehiculo->patente;
            $posiciones = Posicion::find()
                ->andWhere(['>=', 'fecha', $fecha_inicial])
                ->andWhere(['<', 'fecha', $fecha_final])
                ->andWhere(['=', 'id_vehiculo', $vehiculo->id])
                ->orderBy('fecha ASC')
                ->all();

            $tipo_detencion = 0;
            foreach ($posiciones as $posicion) {
                //Si la posicion esta fuera de las zonas de estacionamiento entonces el vehículo tuvo movimiento
                foreach ($zonas as $zona) {
                    //Verifica si la posicion se encuentra dentro de la zona
                    $sql = "select ST_Covers('$zona->poligono'::geometry, '$posicion->geo'::geometry) as estado";
                    //echo $sql."<br>";
                    $connection = \Yii::$app->db;
                    $resultado_estado =  $connection->createCommand($sql)->queryAll();

                    if (!$resultado_estado[0]['estado']) {
                        $tipo_detencion=1;
                    }
                }
            }

            echo "   ".date("Y-m-d", $fecha_inicial)."  ";
            echo "   ".date("Y-m-d", $fecha_final)."  ";
            echo "   ".$fecha_inicial."  ";
            echo "   ".$fecha_final."  ";
            if ($tipo_detencion == 0) {
                echo "Sin movimiento<br>";
            } else {
                echo "Con movimiento<br>";
            }

            $reporteComportamiento = new ReporteComportamiento();
            $reporteComportamiento->fecha = date("Y-m-d", $fecha_inicial);
            $reporteComportamiento->comportamiento = $tipo_detencion;
            $reporteComportamiento->id_vehiculo = $vehiculo->id;
            $reporteComportamiento->save();
        }

        //Al terminar guarda la fecha en el ReporteIndice
        if (empty($reporteIndice->fecha)) {
            $reporteIndice = new ReporteIndice();

            $reporteIndice->tipo = ReporteIndice::TIPO_CALCULO_COMPORTAMIENTO;
            $reporteIndice->fecha = $fecha_final;
            $reporteIndice->save();
        } else {
            $reporteIndice->fecha = $fecha_final;
            $reporteIndice->save();
        }

    }

    public function actionComportamiento()
    {
        $model = new FormularioReporte();
        $model->scenario = 'comportamiento';
        $detenciones = [];
        $grafico_vehiculos = [];
        $grafico_diario = [];
        $fechas = [];
        $patentes = [];
        $datos_vehiculos = [];
        $datos_diarios = [];

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $fecha_inicio = strtotime($model->fecha_inicio);
            $fecha_fin = strtotime($model->fecha_fin);

            $fecha_inicio = date("Y-m-d", $fecha_inicio);
            $fecha_fin = date("Y-m-d", $fecha_fin);

            $vehiculos = Vehiculo::find()->all();

            foreach ($vehiculos as $vehiculo) {
                $reportesComportamiento = ReporteComportamiento::find()
                    ->andWhere(['>=', 'fecha', $fecha_inicio])
                    ->andWhere(['<=', 'fecha', $fecha_fin])
                    ->andWhere(['=', 'id_vehiculo', $vehiculo->id])
                    ->orderBy('fecha ASC')
                    ->all();

                $cantidad_fechas = 0;

                //Limite de dias para el grafico, el grafico muestra hasta esta cantidad de dias, para evitar problemas de espacio.
                $limite_dias=30;

                foreach ($reportesComportamiento as $reporteComportamiento) {
                    //Inicializar campos del grafico por vehiculo
                    if (empty($datos_vehiculos['con_movimiento'][$vehiculo->patente])) {
                        $datos_vehiculos['con_movimiento'][$vehiculo->patente] = 0;
                    }
                    if (empty($datos_vehiculos['sin_movimiento'][$vehiculo->patente])) {
                        $datos_vehiculos['sin_movimiento'][$vehiculo->patente] = 0;
                    }

                    //inicializar campos del grafico por dia
                    if (empty($datos_diarios['con_movimiento'][$reporteComportamiento->fecha])) {
                        $datos_diarios['con_movimiento'][$reporteComportamiento->fecha]=0;
                    }
                    if (empty($datos_diarios['sin_movimiento'][$reporteComportamiento->fecha])) {
                        $datos_diarios['sin_movimiento'][$reporteComportamiento->fecha]=0;
                    }
                    

                    //setear campos de los graficos
                    if ($reporteComportamiento->comportamiento==1) {
                         $datos_vehiculos['con_movimiento'][$vehiculo->patente] += 1;
                         $datos_diarios['con_movimiento'][$reporteComportamiento->fecha] += 1;
                    } else {
                         $datos_vehiculos['sin_movimiento'][$vehiculo->patente] += 1;
                         $datos_diarios['sin_movimiento'][$reporteComportamiento->fecha] += 1;
                    }

                    //Setear columnas
                    if (!in_array($reporteComportamiento->fecha, $fechas)) {
                        $fechas[]=$reporteComportamiento->fecha;
                    }

                    if (!in_array($vehiculo->patente, $patentes)) {
                        $patentes[]=$vehiculo->patente;
                    }

                    $cantidad_fechas++;
                    if ($cantidad_fechas == $limite_dias) {
                        break;
                    }
                    
                }
                
            }

            
            //Transformar los datos al formato de de los graficos
            foreach ($datos_vehiculos as $value) {
                $grafico_vehiculos['con_movimiento'][] = $value;
                
            }

            foreach ($datos_vehiculos as $value) {
                $grafico_vehiculos['sin_movimiento'][] = $value;
            }

            foreach ($datos_diarios as $value) {
                $grafico_diario['con_movimiento'][] = $value;
            }

            foreach ($datos_diarios as $value) {
                $grafico_diario['sin_movimiento'][] = $value;
            }
        }

        return $this->render('comportamiento', [
            'model' => $model,
            'grafico_vehiculos' => $grafico_vehiculos,
            'grafico_diario' => $grafico_diario,
            'fechas' => $fechas,
            'patentes' => $patentes,
        ]);

    }

    public function actionInicioruta()
    {
        $searchModel = new ReporteInicioServicioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('inicioruta', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionCalcularinicioruta()
    {
        ini_set('max_execution_time', 3000); //3000 segundos
        $zonasServicio = ZonaServicio::find()->all();
        $reporteIndice = ReporteIndice::findOne(['tipo' => ReporteIndice::TIPO_CALCULO_INICIO_RUTA]);

        if (empty($reporteIndice->fecha)) {
            $fecha_inicial = 1419984000; //Fecha base 31:12:2014 00:00:00
            $fecha_inicial += 10800; //UTC
        } else {
            $fecha_inicial = $reporteIndice->fecha;
            $inicio = 1;
        }

        $dia_segundos = 86400;
        $fecha_mayor = $fecha_inicial;
        $fecha_final = $fecha_inicial+($dia_segundos*100);

        $inicio_rutas = [];

        foreach ($zonasServicio as $zona_servicio) {
            //horario
            $array_hora_inicio = explode(":", trim($zona_servicio->hora_inicio));
            $array_hora_fin = explode(":", trim($zona_servicio->hora_fin));
            $tiempo_inicio = ($array_hora_inicio[0]*3600 + $array_hora_inicio[1]*60);
            $tiempo_fin = ($array_hora_fin[0]*3600 + $array_hora_fin[1]*60);
            $horario_segundos = $tiempo_fin - $tiempo_inicio;

            $fecha_base = $fecha_inicial;

            echo "fecha_base ".date("Y-m-d H:i:s", $fecha_base)."<br>";

            //Seleccionar las cuadras que componen la zona de servicio
            $sql = "select * from cuadra 
                where ST_Covers('$zona_servicio->geography'::geometry, geography::geometry)";
            $connection = \Yii::$app->db;
            $cuadras =  $connection->createCommand($sql)->queryAll();
            $hora_inicio = [];

            do {
                //inicio y fin de reporte de acuerdo a horario
                $fecha_inicio_segundos = $fecha_base + $tiempo_inicio;

                echo "fecha_inicio_segundos ".date("Y-m-d H:i:s", $fecha_inicio_segundos)."<br>";
                $fecha_fin_segundos = $fecha_inicio_segundos+$horario_segundos;
                echo "fecha_fin_segundos ".date("Y-m-d H:i:s", $fecha_fin_segundos)."<br>";
                $dia_segundos = 86400; //segundos de un dia

                //inicio de reporte 2 horas antes del horario
                $fecha_inicio_segundos -= (3600*2);
                //fin de reporte 2 horas despues del horario
                $fecha_fin_segundos += (3600*2);
                //Seleccionar los vehiculos junto a la cantidad de posiciones en la zona de servicio en el horario definido
                //Esto se utiliza para filtrar los vehiculos
                $sql = "select id_vehiculo, vehiculo.patente, count(*) as cantidad 
                    from posicion join vehiculo on vehiculo.id=posicion.id_vehiculo 
                    where ST_Covers('$zona_servicio->geography'::geometry, geo::geometry) and fecha>$fecha_inicio_segundos and fecha<".$fecha_fin_segundos." 
                    group by id_vehiculo, vehiculo.patente order by count(*) desc";
                $connection = \Yii::$app->db;
                $vehiculos =  $connection->createCommand($sql)->queryAll();

                $vehiculos_sirviendo = "";

                $vehiculos_array = array();
                $patentes = array();

                $fecha_inicio_programada = $fecha_inicio_segundos+(3600*2); //UTC y menos las 2 horas de holgura.

                if (count($vehiculos) > 0) {
                    foreach ($vehiculos as $key => $vehiculo) {
                        //se extraen los datos
                        $vehiculos_array[$vehiculo['id_vehiculo']] = $vehiculo['cantidad'];
                        $patentes[$vehiculo['id_vehiculo']] = $vehiculo['patente'];
                        //Se filtran con los vehiculos que tienen menos de 20 posiciones
                        if ($vehiculo['cantidad'] > 20) {
                            $vehiculos_sirviendo .= $vehiculo['id_vehiculo'].",";
                        }
                    }

                    $vehiculos_sirviendo = trim($vehiculos_sirviendo, ",");
                    $i=0;
                    $vehiculos = array();
                    $promedio_cuadra = array();

                    if ($vehiculos_sirviendo != "") {
                        $sql = "select * from posicion 
                            where ST_Covers('$zona_servicio->geography'::geometry, geo::geometry) 
                            and fecha>$fecha_inicio_segundos and fecha<".$fecha_fin_segundos." 
                            and id_vehiculo in (".$vehiculos_sirviendo.") 
                            order by fecha asc limit 1";
                        $posiciones =  $connection->createCommand($sql)->queryAll();
                        $fecha_inicio_real = $posiciones[0]['fecha']; //UTC
                        $reporteInicioServicio = new ReporteInicioServicio();
                        $reporteInicioServicio->id_zona_servicio = $zona_servicio->id;
                        $reporteInicioServicio->fecha_inicio_real = $fecha_inicio_real;
                        $reporteInicioServicio->fecha_inicio_programada = $fecha_inicio_programada;
                        $reporteInicioServicio->save();

                        $inicio_ruta = [];
                        $inicio_ruta['nombre'] = $zona_servicio->nombre;
                        $inicio_ruta['hora_inicio_programada'] = date("Y-m-d H:i:s", $fecha_inicio_programada);
                        $inicio_ruta['hora_inicio_real'] = date("Y-m-d H:i:s", $posiciones[0]['fecha']-10800);
                        $inicio_rutas[] = $inicio_ruta;
                    } else {
                        $reporteInicioServicio = new ReporteInicioServicio();
                        $reporteInicioServicio->id_zona_servicio = $zona_servicio->id;
                        $reporteInicioServicio->fecha_inicio_programada = $fecha_inicio_programada;
                        $reporteInicioServicio->save();

                        print_r($reporteInicioServicio);

                        $inicio_ruta = [];
                        $inicio_ruta['nombre'] = $zona_servicio->nombre;
                        $inicio_ruta['hora_inicio_programada'] = date("Y-m-d H:i:s", $fecha_inicio_programada);
                        $inicio_rutas[] = $inicio_ruta;
                    }
                } else {
                    $reporteInicioServicio = new ReporteInicioServicio();
                    $reporteInicioServicio->id_zona_servicio = $zona_servicio->id;
                    $reporteInicioServicio->fecha_inicio_programada = $fecha_inicio_programada;
                    $reporteInicioServicio->save();

                    print_r($reporteInicioServicio);

                    $inicio_ruta = [];
                    $inicio_ruta['nombre'] = $zona_servicio->nombre;
                    $inicio_ruta['hora_inicio_programada'] = date("Y-m-d H:i:s", $fecha_inicio_programada);
                    $inicio_rutas[] = $inicio_ruta;

                }
                $fecha_base+=$dia_segundos;
            } while ($fecha_base < $fecha_final);

        }

        echo "<pre>";
        print_r($inicio_rutas);

        //Al terminar guarda la fecha final en el ReporteIndice
        if (empty($reporteIndice->fecha)) {
            $reporteIndice = new ReporteIndice();
            $reporteIndice->tipo = ReporteIndice::TIPO_CALCULO_INICIO_RUTA;
            $reporteIndice->fecha = $fecha_final;
            $reporteIndice->save();
        } else {
            $reporteIndice->fecha = $fecha_final;
            $reporteIndice->save();
        }
    }


    public function actionEventozona()
    {
        ini_set('max_execution_time', 3000); //300 seconds = 5 minutes
        $inicio = 0;

        //Vertedero 1
        //Estacionamiento 2

        $zonas = Zona::find()->all();

        $vehiculos = Vehiculo::find()->all();

        $reporteIndice = ReporteIndice::findOne(['tipo' => ReporteIndice::TIPO_EVENTO_ZONA]);

        if (empty($reporteIndice->fecha)) {
            $posicionInicial = Posicion::find()->orderBy('fecha ASC')->one();
            $fecha_inicial=$posicionInicial->fecha;
        } else {
            $fecha_inicial=$reporteIndice->fecha;
            $inicio = 1;
        }

        $dia_segundos = 86400;
        $fecha_mayor = $fecha_inicial;
        $fecha_final = $fecha_inicial+($dia_segundos*10);
        echo date("d-m-Y H:i:s", $fecha_inicial)." ".date("d-m-Y H:i:s", $fecha_final)."<br>";
        //die (print_r($reporteIndice, true));

        foreach ($vehiculos as $vehiculo) {
            $posiciones = Posicion::find()
                ->andWhere(['>=', 'fecha', $fecha_inicial])
                ->andWhere(['<', 'fecha', $fecha_final])
                ->andWhere(['=', 'id_vehiculo', $vehiculo->id])
                ->orderBy('fecha ASC')
                ->all();

            echo $vehiculo->id ."  ". count($posiciones)."<br>";
            foreach ($zonas as $zona) {
                //Definir estado inicial
                // Si es la primera ejecucion el estado anterior es fuera de zona
                if ($inicio==0) {
                    $estado_zona_anterior = 0; //fuera de zona
                } else {
                    //Busca la ultima PosicionZona registrada de la zona
                    $posicionZonaAnterior = PosicionZona::find()
                        ->andWhere(['<=', 'fecha', $fecha_inicial])
                        ->andWhere(['=', 'id_zona', $zona->id])
                        ->andWhere(['=', 'id_vehiculo', $vehiculo->id])
                        ->orderBy('fecha DESC')
                        ->one();

                    //Si no existe una PosicionZona el estado anterior es fuera de zona
                    if (empty($posicionZonaAnterior)) {
                        $estado_zona_anterior = 0; //fuera de zona
                    //Si existe se recupera el ultimo estado para la zona
                    } else {
                        $estado_zona_anterior = $posicionZonaAnterior->estado;
                    }
                }

                //Recorre todas las posiciones
                foreach ($posiciones as $posicion) {
                    //Verifica si la posicion se encuentra dentro de la zona
                    $sql = "select ST_Covers('$zona->poligono'::geometry, '$posicion->geo'::geometry) as estado";
                    //echo $sql."<br>";
                    $connection = \Yii::$app->db;
                    $resultado_estado =  $connection->createCommand($sql)->queryAll();

                    /*
                    *   0 Fuera de zona, no se almacena
                        1 Entrando a zona
                        2 Dentro de zona
                        3 Saliendo de zona
                    */
                    
                    // Dentro de zona
                    if ($resultado_estado[0]['estado']) {
                        // Se define el estado de acuerdo a al estado anterior
                        switch ($estado_zona_anterior) {
                            //Si antes estaba fuera de zona ahora esta entrando a zona
                            case 0:
                                $estado_zona_actual = 1;
                                break;
                            //Si antes estaba entrando a zona ahora esta dentro de zona
                            case 1:
                                $estado_zona_actual = 2;
                                break;
                            //Si antes estaba dentro de zona ahora esta dentro de zona
                            case 2:
                                $estado_zona_actual = 2;
                                break;
                            //Si antes estaba saliendo de zona ahora esta entrando a zona
                            default:
                                $estado_zona_actual = 1;
                                break;
                        }
                    // Fuera de zona
                    } else {
                        switch ($estado_zona_anterior) {
                            //Si antes estaba fuera de zona ahora esta fuera de zona
                            case 0:
                                $estado_zona_actual = 0;
                                break;
                            //Si antes estaba entrando a zona ahora esta saliendo de zona
                            case 1:
                                $estado_zona_actual = 3;
                                break;
                            //Si antes estaba dentro de zona ahora esta saliendo de zona
                            case 2:
                                $estado_zona_actual = 3;
                                break;
                            //Si antes estaba saliendo de zona ahora esta fuera a zona
                            default:
                                $estado_zona_actual = 0;
                                break;
                        }

                    }

                    // Se guardan los estados Entrando a zona, Dentro de zona, Saliendo de zona
                    if ($estado_zona_actual!=0) {
                        $posicionZona = new PosicionZona();
                        $posicionZona->id_zona = $zona->id;
                        $posicionZona->id_posicion = $posicion->id;
                        $posicionZona->id_vehiculo = $vehiculo->id;
                        $posicionZona->fecha = $posicion->fecha;
                        $posicionZona->estado = $estado_zona_actual;
                        $posicionZona->save();
                    }

                    if ($posicion->fecha>$fecha_mayor) {
                        $fecha_mayor = $posicion->fecha;
                    }
                    //echo $zona->id."  ".$estado_zona_anterior." ".$estado_zona_actual." ".$resultado_estado[0]['estado']." ".date("d-m-Y H:i:s",$posicion->fecha)."<br>";
                    
                    //El estado actual pasa a ser el estado anterior para la siguiente iteración
                    $estado_zona_anterior = $estado_zona_actual;
                }

            }

        }

        //Al terminar guarda la fecha en el ReporteIndice
        if (empty($reporteIndice->fecha)) {
            $reporteIndice = new ReporteIndice();
            $reporteIndice->tipo = ReporteIndice::TIPO_EVENTO_ZONA;
            $reporteIndice->fecha = $fecha_mayor;
            $reporteIndice->save();
        } else {
            $reporteIndice->fecha = $fecha_mayor;
            $reporteIndice->save();
        }

        echo "TERMINADO";

        

        /*
        $sql = "select id, fecha from posicion where ST_Covers('$model->poligono'::geometry, geo::geometry) and id>0 order by id asc";
        $connection = \Yii::$app->db;
        $posiciones =  $connection->createCommand($sql)->queryAll();

        echo $sql."<br>";

        foreach ($posiciones as $posicion) {
            echo $posicion['id']."<br>";
            $posicionZona = new PosicionZona();
            $posicionZona->id_posicion = $posicion['id'];
            $posicionZona->id_zona = 1;
            $posicionZona->fecha = $posicion['fecha'];
            $posicionZona->save();
        }
        */

    }

    public function actionCalculariniciodescargado()
    {
        ini_set('max_execution_time', 3000);
        $inicio = 0;

        $estacionamiento = Zona::find()->where(['tipo' => Zona::TIPO_ESTACIONAMIENTO])->one();
        $vertedero = Zona::find()->where(['tipo' => Zona::TIPO_VERTEDERO])->one();

        echo $estacionamiento->nombre." ".$vertedero->nombre."<br>";

        $reporteIndice = ReporteIndice::findOne(['tipo' => ReporteIndice::TIPO_CALCULO_INICIO_DESCARGADO]);

        if (empty($reporteIndice->fecha)) {
            $posicionInicial = PosicionZona::find()->orderBy('fecha ASC')->one();
            $fecha_inicial=$posicionInicial->fecha;
        } else {
            $fecha_inicial=$reporteIndice->fecha;
            $inicio = 1;
        }

        $dia_segundos = 86400;
        $fecha_mayor = $fecha_inicial;
        $fecha_final = $fecha_inicial+($dia_segundos*30);
        echo date("d-m-Y H:i:s", $fecha_inicial)." ".date("d-m-Y H:i:s", $fecha_final)."<br>";
        $vehiculos = Vehiculo::find()->all();

        foreach ($vehiculos as $vehiculo) {
            $ingresosEstacionamiento = PosicionZona::find()
                    ->andWhere(['>', 'fecha', $fecha_inicial])
                    ->andWhere(['<=', 'fecha', $fecha_final])
                    ->andWhere(['=', 'id_zona', 2])
                    ->andWhere(['=', 'estado', 1])
                    ->andWhere(['=', 'id_vehiculo', $vehiculo->id])
                    ->orderBy('fecha ASC')
                    ->all();

            foreach ($ingresosEstacionamiento as $ingresoEstacionamiento) {
                if ($ingresoEstacionamiento->fecha>$fecha_mayor) {
                    $fecha_mayor = $ingresoEstacionamiento->fecha;
                }
                $salidaVertedero = PosicionZona::find()
                ->andWhere(['<=', 'fecha', $ingresoEstacionamiento->fecha])
                ->andWhere(['=', 'id_zona', 1])
                ->andWhere(['=', 'estado', 3])
                ->andWhere(['=', 'id_vehiculo', $vehiculo->id])
                ->orderBy('fecha DESC')
                ->one();
                if (!empty($salidaVertedero)) {
                    //echo date("d-m-Y H:i:s",$ingresoEstacionamiento->fecha)."   ".date("d-m-Y H:i:s",$salidaVertedero->fecha)."<br>";
                    $dato = [];

                    $reporteInicioDescargado = new ReporteInicioDescargado();
                    $reporteInicioDescargado->id_vehiculo = $vehiculo->id;
                    $reporteInicioDescargado->entrada_estacionamiento = $ingresoEstacionamiento->fecha;
                    $reporteInicioDescargado->salida_vertedero = $salidaVertedero->fecha;
                    $reporteInicioDescargado->tiempo = ($ingresoEstacionamiento->fecha-$salidaVertedero->fecha);
                    $reporteInicioDescargado->save();

                    $ingresoEstacionamiento->fecha -= 10800; //UTC a hora local
                    $salidaVertedero->fecha -= 10800; //UTC a hora local

                    $dato['patente'] = $vehiculo->patente;
                    $dato['tipo'] = $vehiculo->tipo;
                    $dato['estacionamiento'] = date("d-m-Y H:i:s", $ingresoEstacionamiento->fecha);
                    $dato['vertedero'] = date("d-m-Y H:i:s", $salidaVertedero->fecha);
                    $dato['tiempo'] = ($ingresoEstacionamiento->fecha-$salidaVertedero->fecha);
                    $datos[] = $dato;
                }
            }

        }

        //Al terminar guarda la fecha en el ReporteIndice
        if (empty($reporteIndice->fecha)) {
            $reporteIndice = new ReporteIndice();

            $reporteIndice->tipo = ReporteIndice::TIPO_CALCULO_INICIO_DESCARGADO;
            $reporteIndice->fecha = $fecha_mayor;
            $reporteIndice->save();
        } else {
            $reporteIndice->fecha = $fecha_mayor;
            $reporteIndice->save();
        }
    }


    public function actionIniciodescargado()
    {
        $searchModel = new ReporteInicioDescargadoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('iniciodescargado', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionVerruta($inicio, $fin, $id_vehiculo)
    {
        $lineas[]=' ';
        $posiciones = Posicion::find()
            ->andWhere(['>=', 'fecha', $inicio])
            ->andWhere(['<=', 'fecha', $fin])
            ->andWhere(['=', 'id_vehiculo', $id_vehiculo])
            ->orderBy('fecha ASC')
            ->all();
        
        $i = 0;
        foreach ($posiciones as $posicion) {
            if ($i == 0) {
                //primer caso
                $puntoaux = $posicion;
                $i=1;
            } else {
                $linea=array();
                $linea['primer_punto']=$puntoaux['latitud'].",".$puntoaux['longitud'];
                $linea['segundo_punto']=$posicion['latitud'].",".$posicion['longitud'];
                $linea['orientacion']=$puntoaux['orientacion'];
                $linea['fecha']=date("d-m-Y H:i:s", $puntoaux['fecha']);
                
                $lineas=$linea;

                $puntoaux=$posicion;
            }
        }
        return $this->render('verruta', [
            'lineas' => $lineas
            ]);
    }
}
