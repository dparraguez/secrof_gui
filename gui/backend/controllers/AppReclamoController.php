<?php
//TODO: Mejorar webservice incorporandolo como controlador en yii
namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\UserSearch;
use yii\db\Query;
use app\models\AppReclamo;
use app\models\WebEstado;
use app\models\ReclamoEstado;
use app\models\AppReclamoSearch;
use app\models\WebEstadoSearch;
use app\models\WebTipoReclamo;
use app\models\AppSugerencia;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Command;
use yii\web\Session;
use yii\helpers\Time;
use app\models\ValidarFormulario;
use app\models\ValidarFormularioAjax;
use yii\data\ActiveDataProvider;
use mPDF;
//permisos necesarios 

/**
 * AppReclamoController implements the CRUD actions for AppReclamo model.
 */
class AppReclamoController extends Controller
{
    public function behaviors()
    {
        return [
        'access' => [
                'class' =>\yii\filters\AccessControl::className(),
                //'only' => ['logout', 'user', 'admin'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['index','mensaje','estado', 'view','samplepdf','appgrafico','create','view2','foto','prioridadReclamo','diferencia','update','delete','ver-mapa-general','ver-mapa-reclamo','findModel'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            if(Yii::$app->user->identity->tipo==User::AUTH_FISCALIZADOR){
                                return true;
                            }
                            else false;

                        },
                    ],
                [
                        //Secretaria tiene permisos sobre las siguientes acciones
                        'actions' => [ 'view','create','prioridadReclamo','update','findModel'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Comprueba si es secretaria
                            if(Yii::$app->user->identity->tipo==User::AUTH_SECRETARIA){
                                return true;
                            }
                            else false;

                        },
                    ],
                    
            ],],
            //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
            //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AppReclamo models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new AppReclamoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    

    //Cambia el estado de un reclamo de Recepcionado a Leído, de Leído a Cerrado,
    //en caso que el reclamo sea tipo Escombros pasa de Recepcionado a Derivado
    //los cambios de estado se realizan de manera manual al pulsar un boton en la lista del index
    public function actionEstado($id,$estado){

        $sql = 'SELECT * FROM web_estado WHERE es_nombre='."'".$estado."'";
        $es = WebEstado::findBySql($sql)->one(); 

        //Guarda el estado en el que queda el reclamo, sirve para discriminar que reclamos ver en la lista que aparece en el index.
        $reclamo = AppReclamo::findOne($id);
        $reclamo->re_estadoactual = $estado;
        $reclamo->save(false);

        //Guarda el id del reclamo, el id del estado con el que queda y la fecha en que se hizo el cambio
        //sirve para saber cuanto se demora en cambiar de un estado a otro
        $model = new ReclamoEstado;
        $model->re_id = $id;
        $model->es_id = $es->es_id;
        $model->re_es_fecha = date( 'd/m/Y H:i:s' );
        if($model->save(false)){
               return $this->redirect(['index']);
        }

    }

    /**
     * Displays a single AppReclamo model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    //Genera en pdf la información que se muestra en el grafico
    public function actionSamplepdf()
    {
    //Crea el pdf con los datos que desea mostrar, consultar necesarias
        $connection=\Yii::$app->db_movil;
        //consultas a la bd para obtener los valores según tipo de reclamo para el gráfico
            $vehiculoabandonado=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Vehículo abandonado'")->queryScalar();
            $vehiculofiltracion=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Vehículo filtración'")->queryScalar();
            $escombros=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Escombros'")->queryScalar();
            $microbasural=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Microbasural'")->queryScalar();
            $trabajadores=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Trabajadores'")->queryScalar();
            $calles=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Calles'")->queryScalar();
            $residuos=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Residuos'")->queryScalar();
            $sumideros=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Sumideros'")->queryScalar();
            $vehiculosotros=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Vehículo otros'")->queryScalar();

            //consultas a la bd para obtener los valores según tipo de Fiscalización para el gráfico
             $reclamo=$connection->createCommand("SELECT Count(*) from app_reclamo")->queryScalar();
             $felicitacion=$connection->createCommand("SELECT Count(*) from app_sugerencia where su_tipo='Felicitación'")->queryScalar();
             $sugerencia=$connection->createCommand("SELECT Count(*) from app_sugerencia where su_tipo='Sugerencia'")->queryScalar();
             $total=$reclamo+$felicitacion+$sugerencia;

            //cantidad de usuarios que han realizado RFS
            $usuario=$connection->createCommand("SELECT Count(*) from app_usuario")->queryScalar();
            
           // retorno de valores 
            $content= $this->render('view3',['vehiculoabandonado'=>$vehiculoabandonado,
                                               'vehiculofiltracion'=>$vehiculofiltracion,
                                               'escombros'=>$escombros,
                                               'microbasural'=>$microbasural,
                                               'trabajadores'=>$trabajadores,
                                               'calles'=>$calles,
                                               'residuos'=>$residuos,
                                               'sumideros'=>$sumideros,
                                               'vehiculosotros' =>$vehiculosotros,
                                               'reclamo'=>$reclamo,
                                               'felicitacion'=>$felicitacion,
                                               'sugerencia'=>$sugerencia,
                                               'usuario'=>$usuario,
                                               'total'=>$total,
                                              ]);
            //Crea pdf permisos pertinentes.
            $mpdf = new mPDF;
            $mpdf->WriteHTML($content);
            $mpdf->Output();
    }

    //funcion para lo gráficos app movil muestra cantidad reclamos por tipo, cantidad total reclamos, sugerencias y felicitaciones
    public function actionAppgrafico(){
        //conexión a la base de datos appMovil
        $connection=\Yii::$app->db_movil;
        //consultas a la bd para obtener los valores según tipo de reclamo para el gráfico
        $vehiculoabandonado=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Vehículo abandonado'")->queryScalar();
        $vehiculofiltracion=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Vehículo filtración'")->queryScalar();
        $escombros=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Escombros'")->queryScalar();
        $microbasural=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Microbasural'")->queryScalar();
        $trabajadores=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Trabajadores'")->queryScalar();
        $calles=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Calles'")->queryScalar();
        $residuos=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Residuos'")->queryScalar();
        $sumideros=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Sumideros'")->queryScalar();
        $vehiculosotros=$connection->createCommand("SELECT Count(*) from app_reclamo A,web_tipo_reclamo W where A.ti_id = W.ti_id AND W.ti_nombre='Vehículo otros'")->queryScalar();

        //consultas a la bd para obtener los valores según tipo de Fiscalización para el gráfico
         $reclamo=$connection->createCommand("SELECT Count(*) from app_reclamo")->queryScalar();
         $felicitacion=$connection->createCommand("SELECT Count(*) from app_sugerencia where su_tipo='Felicitación'")->queryScalar();
         $sugerencia=$connection->createCommand("SELECT Count(*) from app_sugerencia where su_tipo='Sugerencia'")->queryScalar();
         $total=$reclamo+$felicitacion+$sugerencia;

        //cantidad de usuarios que han realizado RFS
        $usuario=$connection->createCommand("SELECT Count(*) from app_usuario")->queryScalar();
        
       // retorno de valores 
        return $this->render('appgrafico',['vehiculoabandonado'=>$vehiculoabandonado,
                                           'vehiculofiltracion'=>$vehiculofiltracion,
                                           'escombros'=>$escombros,
                                           'microbasural'=>$microbasural,
                                           'trabajadores'=>$trabajadores,
                                           'calles'=>$calles,
                                           'residuos'=>$residuos,
                                           'sumideros'=>$sumideros,
                                           'vehiculosotros' =>$vehiculosotros,
                                           'reclamo'=>$reclamo,
                                           'felicitacion'=>$felicitacion,
                                           'sugerencia'=>$sugerencia,
                                           'usuario'=>$usuario,
                                           'total'=>$total,
                                          ]);

    }

    /**
     * Creates a new AppReclamo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
         date_default_timezone_set ('America/Santiago');//hora para Chile
      
       $connection = \Yii::$app->db_movil;//abre la conexión a la bd

        $model = new AppReclamo();
        $model->us_id=$id;//obtiene el id actual dl usuario
       //Selecciona del la tabla web_estado los que tengan nombre "recepcionado"
        $sql = $connection->createCommand("SELECT es_id FROM web_estado where es_nombre ='Recepcionado'")->queryScalar();
        
        $model3 = new ReclamoEstado();
        
       //compara si es_id son los mismos del web_estado y de reclamo_estado
        $model3->es_id = $sql;
        $model3->re_es_fecha=date( 'd/m/Y H:i:s' );//almacena la hora en reclamo_estado
       
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           $prioridad = $this->prioridadReclamo($model->re_id);
       echo $prioridad;
        
            $model3->re_id = $model->re_id;
            $model3->save();//guarda los datos en el modelo
            return $this->redirect(['view', 'id' => $model->re_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

    }


    public function actionView2($id)
    {
        
        return $this->render('view2', [
            'model' => $this->findModel($id),
        ]);
    }
      public function actionMensaje()
    {
        
        return $this->render('mensaje');
    }


    //funcion creada para mostrar la foto del reclamo
    public function actionFoto($id)
    {
        
        $model = $this->findModel($id);
       
       // $model=$model->findModel($id);
        return $this->render('foto', [
                'model' => $model,
            ]);
    }


    //se calcula la diferencia de dias que hay entre que un reclamo es Recepcionado y pasa a Cerrado 
    //se genera un pdf con la información
    public function actionDiferencia()
    {
        date_default_timezone_set ('America/Santiago');//hora para Chile
        $models= AppReclamo::find()->all();
        $vehiculoabandonado[]=null;
        $vehiculosotros[]=null;
        $vehiculofiltracion[]=null;
        $escombros[]=null;
        $microbasural[]=null;
        $trabajadores[]=null;
        $calles[]=null;
        $residuos[]=null;
        $sumideros[]=null;
       
        # Filtra los reclamos por los estados asociados
        # Solo se muestran los con estado "Recepcionado"
         //$estado = count($model->reclamoEstados);
        foreach ($models as $model) {
            if (count($model->reclamoEstados)==3) {
                switch ($model->ti->ti_nombre) {
                    case 'Vehículo abandonado':
                            $Fecha1=new \DateTime($model->reclamoEstados[0]->re_es_fecha);
                            $Fecha2=new \DateTime($model->reclamoEstados[2]->re_es_fecha);
                            $interval = $Fecha2->diff($Fecha1); 
                            $vehiculoabandonado[]=$interval->format('%a');
                        break;
                    case 'Vehículo filtración':
                            $Fecha1=new \DateTime($model->reclamoEstados[0]->re_es_fecha);
                            $Fecha2=new \DateTime($model->reclamoEstados[2]->re_es_fecha);
                            $interval = $Fecha2->diff($Fecha1); 
                            $vehiculofiltracion[]=$interval->format('%a');
                        break;
                        case 'Vehículo otros':
                            $Fecha1=new \DateTime($model->reclamoEstados[0]->re_es_fecha);
                            $Fecha2=new \DateTime($model->reclamoEstados[2]->re_es_fecha);
                            $interval = $Fecha2->diff($Fecha1); 
                            $vehiculosotros[]=$interval->format('%a');
                        break;
                        case 'Microbasural':
                            $Fecha1=new \DateTime($model->reclamoEstados[0]->re_es_fecha);
                            $Fecha2=new \DateTime($model->reclamoEstados[2]->re_es_fecha);
                            $interval = $Fecha2->diff($Fecha1); 
                            $microbasural[]=$interval->format('%a');
                        break;
                        case 'Trabajadores':
                            $Fecha1=new \DateTime($model->reclamoEstados[0]->re_es_fecha);
                            $Fecha2=new \DateTime($model->reclamoEstados[2]->re_es_fecha);
                            $interval = $Fecha2->diff($Fecha1); 
                            $trabajadores[]=($interval->format('%a'));
                            
                        break;
                        case 'Calles':
                            $Fecha1=new \DateTime($model->reclamoEstados[0]->re_es_fecha);
                            $Fecha2=new \DateTime($model->reclamoEstados[2]->re_es_fecha);
                            $interval = $Fecha2->diff($Fecha1); 
                             $calles[]=$interval->format('%a');
                        break;
                        case 'Residuos':
                            $Fecha1=new \DateTime($model->reclamoEstados[0]->re_es_fecha);
                            $Fecha2=new \DateTime($model->reclamoEstados[2]->re_es_fecha);
                            $interval = $Fecha2->diff($Fecha1); 
                            $residuos[]=$interval->format('%a');
                        break;
                        case 'Sumideros':
                            $Fecha1=new \DateTime($model->reclamoEstados[0]->re_es_fecha);
                            $Fecha2=new \DateTime($model->reclamoEstados[2]->re_es_fecha);
                            $interval = $Fecha2->diff($Fecha1); 
                            $sumideros[]=$interval->format('%a');
                        break;
                    
                }
            }

        }
        $content= $this->render('view4',['vehiculoabandonado'=>array_sum($vehiculoabandonado)/count($vehiculoabandonado),
                                         'vehiculofiltracion'=>array_sum($vehiculofiltracion)/count($vehiculofiltracion),
                                         'microbasural'=>array_sum($microbasural)/count($microbasural),
                                         'trabajadores'=>array_sum($trabajadores)/count($trabajadores),
                                         'calles'=>array_sum($calles)/count($calles),
                                         'residuos'=>array_sum($residuos)/count($residuos),
                                         'sumideros'=>array_sum($sumideros)/count($sumideros),
                                         'vehiculosotros' =>array_sum($vehiculosotros)/count($vehiculosotros),
                                         'reclamo'=>count($models),
                                        ]);
            //Crea pdf 
            $mpdf = new mPDF;
            $mpdf->WriteHTML($content);
            $mpdf->Output();
    }


    //calcula la prioridad ('Alta','Media','Baja','Derivar') de un reclamo segun su tipo y fecha
    public function prioridadReclamo($id){
        date_default_timezone_set ('America/Santiago');//hora para Chile
        $model = $this->findModel($id);
        $ti_id=$model->ti_id;
        $model2= WebTipoReclamo::findOne($ti_id);

        if(strcmp ($model2->ti_nombre,'Escombros') != 0){
                 
                 $prioridad = $model2->ti_prioridad;
                 $prioridad = $prioridad+5; //5 porque es ingresado directamente en la web y su fecha de creación 
                                            //y de almacenamiento en el server es la misma

                 //Aumenta dos puntos si el reclamo es tipo sumidero y esta entre los meses de mayo a septiembre
                  $fecha_hoy = date("Y-m-d H:i");
                   $fecha_hoy= new \DateTime($fecha_hoy);
                 if(strcmp ($model2->ti_nombre,'Sumideros') == 0){
                  $mes_hoy=$fecha_hoy->format('m');
                  if(($mes_hoy>=5)&&($mes_hoy<=9))
                       $prioridad=$prioridad+2;
                 }
                 //Obtiene el nivel de prioridad
                 if($prioridad>4){
                      if($prioridad>7) $model->re_prioridad="Alta";
                      else $model->re_prioridad= "Media";
                 }
                 else $model->re_prioridad="Baja"; 
        }
        else  $model->re_prioridad= "Derivar"; 
        $model->save();
    }

    /**
     * Updates an existing AppReclamo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->re_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AppReclamo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AppReclamo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AppReclamo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AppReclamo::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //Muestra en un mapa los reclamos sugerencias y felicitaciones 
    public function actionVerMapaGeneral()
    {
        $reclamos = AppReclamo::find()->all();
        $reclamosVisibles = null;

        // Filtra los reclamos por los estados asociados
        // Solo se muestran los con estado "Recepcionado"
        foreach ($reclamos as $reclamo) {
            if (count($reclamo->es)==1) {
                if ($reclamo->es[0]->es_nombre=="Recepcionado") {
                    $reclamosVisibles[] = $reclamo;
                }
            }
        }

        $sugerenciasVisibles = AppSugerencia::find()->where(['su_estado'=>'Recepcionado'])->all();
        if($reclamosVisibles!=null and $sugerenciasVisibles!=null){
        return $this->render('vermapageneral', [
            'reclamosVisibles' => $reclamosVisibles,
            'sugerenciasVisibles' => $sugerenciasVisibles,
            ]);
    }
    else return $this->redirect(['mensaje']);
    }

    //Muestra en un mapa los reclamos según su tipo
    public function actionVerMapaReclamo()
    {
        $reclamos = AppReclamo::find()->all();
        $reclamosVisibles = null;

        //Filtra los reclamos por los estados asociados
        //Solo se muestran los con estado "Recepcionado"
        foreach ($reclamos as $reclamo) {
            if (count($reclamo->es)==1) {
                if ($reclamo->es[0]->es_nombre=="Recepcionado") {
                    $reclamosVisibles[] = $reclamo;
                }
            }
        }
        if($reclamosVisibles!=null){
        return $this->render('vermapareclamo', [
            'reclamosVisibles' => $reclamosVisibles,
            ]);
    }
    else return $this->redirect(['mensaje']);
    }
}
