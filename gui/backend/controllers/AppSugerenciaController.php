<?php

namespace backend\controllers;

use Yii;
use app\models\AppSugerencia;
use app\models\AppSugerenciaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AppSugerenciaController implements the CRUD actions for AppSugerencia model.
 */
class AppSugerenciaController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AppSugerencia models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AppSugerenciaSearch();
        //muestra solo las sugerencias que tengan su_estado = Recepcionado
        $dataProvider = $searchModel->search(['AppSugerenciaSearch'=>['su_estado'=>'Recepcionado']]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AppSugerencia model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
      public function actionView2($id)
    {
        return $this->render('view2', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AppSugerencia model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        date_default_timezone_set ('America/Santiago');//obtiene la hora de Chile
        $model = new AppSugerencia();
         $model->us_id=$id;//pasa el id del usuario a la sugerencia
         $model->su_estado='Recepcionado';
         $model->su_fecha_hora_web=date( 'd/m/Y H:i:s' );//almacena la hora

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->su_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    //Cambia el estado de la sugerencia de Recepcionado a Leído
    public function actionEstado($id){
        $model=$this->findModel($id);
        $model->su_estado ='Leído';
        if($model->save(false)){
               return $this->redirect(['index']);
        }
      
    }

    /**
     * Updates an existing AppSugerencia model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->su_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AppSugerencia model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AppSugerencia model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AppSugerencia the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AppSugerencia::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
