<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use common\models\LoginForm;
use common\models\FormularioReporte;
use app\models\Posicion;
use app\models\ZonaServicio;
use app\models\Zona;
use yii\filters\VerbFilter;
use yii\widgets\DetailView;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'reclamos', 'reportes', 'detencion', 'comportamiento', 'inicioruta', 'detencionform','datagrafico','datareporte'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
$conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres");
 $inicio='2015-05-11';
$fin='2015-05-17';
$datos_resultado=null;
$litros=null;
$valor=null;
$valorcombustible=570;
$q=0;
$dia_actual=null;
//Extraer resultado
$sql="SELECT * from consumo_diario where fecha BETWEEN '$inicio' and '$fin'";
$resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
//Guardo el resultado en un array
$datos = array();
while($row = pg_fetch_assoc($resultado)) {
  $datos[] = $row;
}
pg_free_result($resultado);


foreach ($datos as $key) {
$litros=floatval(($key['consumo']/1000)*30);
$valor=$litros*$valorcombustible;

$fechats = strtotime($key['fecha']); //a timestamp 

//el parametro w en la funcion date indica que queremos el dia de la semana 
//lo devuelve en numero 0 domingo, 1 lunes,.... 
switch (date('w', $fechats)){ 
    case 0: $dia_actual="Domingo"; break; 
    case 1: $dia_actual="Lunes"; break; 
    case 2: $dia_actual="Martes"; break; 
    case 3: $dia_actual="Miercoles"; break; 
    case 4: $dia_actual="Jueves"; break; 
    case 5: $dia_actual="Viernes"; break; 
    case 6: $dia_actual="Sabado"; break; 
}

$datos_resultado[$q]=array(
'litros'=>$litros,
'valor'=>$valor,
'fecha'=>$key['fecha'],
'dia'=>$dia_actual
    );

$q++;

}
//$dia_actual=date("Y-m-d");
$dia_actual= "2015-05-11";
$hora_actual=date('H:i:s');
$valorcombustible=527;

$dia_anterior=strtotime('-1 Day',strtotime($dia_actual));
$dia_anterior= date('Y-m-d',$dia_anterior);

$fecha_inicio=$dia_anterior." "."00:00:01";
$fecha_termino= $dia_anterior." "."23:59:59";

$litros_total=0;
$valor_total=0;

//Extraer los resultados de la tabla 
$conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres");
$sql="SELECT * from consumo_diario_vehiculo where fecha='$dia_anterior'";
$resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");

//Guardo el resultado en un array
$array = array();
while($row = pg_fetch_assoc($resultado)) {
  $array[] = $row;
}
pg_free_result($resultado);
pg_close($conexion);


foreach ($array as $key ) {
    

$litros_total=$litros_total+ $key['consumo'];



}
$litros_total=$litros_total*30;

$valor_total=$litros_total*$valorcombustible;


return $this->render('datagraficos',['consumo_diario'=>$datos_resultado,'inicio'=>$inicio,'fin'=>$fin,'datos_final'=>$array,'valorcombustible'=>$valorcombustible,'fecha_inicio'=>$fecha_inicio,'fecha_termino'=>$fecha_termino,'litros'=>$litros_total,'valorTotal'=>$valor_total]);

        //return $this->render('index');
    }

    


    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }



public function actionDatagrafico()
{
$conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres");
 $inicio='2015-05-11';
$fin='2015-05-17';
$datos_resultado=null;
$litros=null;
$valor=null;
$valorcombustible=570;
$q=0;
$dia_actual=null;
//Extraer resultado
$sql="SELECT * from consumo_diario where fecha BETWEEN '$inicio' and '$fin'";
$resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
//Guardo el resultado en un array
$datos = array();
while($row = pg_fetch_assoc($resultado)) {
  $datos[] = $row;
}
pg_free_result($resultado);


foreach ($datos as $key) {
$litros=floatval(($key['consumo']/1000)*30);
$valor=$litros*$valorcombustible;

$fechats = strtotime($key['fecha']); //a timestamp 

//el parametro w en la funcion date indica que queremos el dia de la semana 
//lo devuelve en numero 0 domingo, 1 lunes,.... 
switch (date('w', $fechats)){ 
    case 0: $dia_actual="Domingo"; break; 
    case 1: $dia_actual="Lunes"; break; 
    case 2: $dia_actual="Martes"; break; 
    case 3: $dia_actual="Miercoles"; break; 
    case 4: $dia_actual="Jueves"; break; 
    case 5: $dia_actual="Viernes"; break; 
    case 6: $dia_actual="Sabado"; break; 
}

$datos_resultado[$q]=array(
'litros'=>$litros,
'valor'=>$valor,
'fecha'=>$key['fecha'],
'dia'=>$dia_actual
    );

$q++;

}










return $this->render('datagraficos',['consumo_diario'=>$datos_resultado,'inicio'=>$inicio,'fin'=>$fin]);
}




public function actionDatareporte()
{
//$dia_actual=date("Y-m-d");
$dia_actual= "2015-05-11";
$hora_actual=date('H:i:s');
$valorcombustible=527;

$dia_anterior=strtotime('-1 Day',strtotime($dia_actual));
$dia_anterior= date('Y-m-d',$dia_anterior);

$fecha_inicio=$dia_anterior." "."00:00:01";
$fecha_termino= $dia_anterior." "."23:59:59";

$litros_total=0;
$valor_total=0;

//Extraer los resultados de la tabla 
$conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres");
$sql="SELECT * from consumo_diario_vehiculo where fecha='$dia_anterior'";
$resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");

//Guardo el resultado en un array
$array = array();
while($row = pg_fetch_assoc($resultado)) {
  $array[] = $row;
}
pg_free_result($resultado);
pg_close($conexion);


foreach ($array as $key ) {
    

$litros_total=$litros_total+ $key['consumo'];



}
$litros_total=$litros_total*30;

$valor_total=$litros_total*$valorcombustible;


return $this->render('datareporte',['datos_final'=>$array,'valorcombustible'=>$valorcombustible,'fecha_inicio'=>$fecha_inicio,'fecha_termino'=>$fecha_termino,'litros'=>$litros_total,'valorTotal'=>$valor_total]);


}





}
