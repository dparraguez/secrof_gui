<?php

namespace backend\controllers;

use Yii;
use app\models\AppReporte;
use app\models\AppReporteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\AppUsuario;
use app\models\AppUsuarioSearch;
use mPDF;

/**
 * AppReporteController implements the CRUD actions for AppReporte model.
 */
class AppReporteController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AppReporte models.
     * @return mixed
     */
    public function actionIndex($id)
    {
         
        $searchModel = new AppReporteSearch();
        //muestra los reportes asociados a un id (el id corresponde al identificador del inspector)
        $dataProvider = $searchModel->search(['AppReporteSearch'=>['in_id'=>$id]]);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
 
    //muestra la foto de un reporte
    public function actionFoto($id)
    {
        
        $model = $this->findModel($id);
        return $this->render('foto', [
                'model' => $model,
            ]);
    }

    //Genera un pdf con la información del reporte
    public function actionPdf($id,$nombre,$apellido,$fecha,$rut,$descripcion){
        //contenido del pdf
        $content= $this->render('view3',['id'=>$id,
                                           'nombre'=>$nombre,
                                           'apellido'=>$apellido,
                                           'fecha'=>$fecha,
                                           'rut'=>$rut,
                                           'descripcion'=>$descripcion
                                           
                                           
                                          ]);
        //Crea el pdf
        $mpdf = new mPDF;
        $mpdf->WriteHTML($content);
        $mpdf->Output();




    }
    /**
     * Displays a single AppReporte model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
       
    }

    /**
     * Creates a new AppReporte model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AppReporte();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->rp_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AppReporte model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->rp_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AppReporte model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AppReporte model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AppReporte the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AppReporte::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
