<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use app\models\ZonaServicio;
use app\models\Cuadra;
use app\models\PromedioDiario;
use app\models\ZonaServicioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ZonaServicioController implements the CRUD actions for ZonaServicio model.
 */
class ZonaservicioController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' =>\yii\filters\AccessControl::className(),
                //'only' => ['logout', 'user', 'admin'],
                'rules' => [
                    [
                        //El administrador tiene permisos sobre las siguientes acciones
                        'actions' => ['index', 'view','create','update','delete','findModel','histograma','histogramacuadra'],
                        //Esta propiedad establece que tiene permisos
                        'allow' => true,
                        //Usuarios autenticados, el signo ? es para invitados
                        'roles' => ['@'],
                        //Este método nos permite crear un filtro sobre la identidad del usuario
                        //y así establecer si tiene permisos o no
                        'matchCallback' => function ($rule, $action) {
                            //Llamada al método que comprueba si es un administrador
                            if(Yii::$app->user->identity->tipo==User::AUTH_ADMIN){
                                return true;
                            }
                            else false;

                        },
                    ],
                    
                ],
            ],
            //Controla el modo en que se accede a las acciones, en este ejemplo a la acción logout
            //sólo se puede acceder a través del método post
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ZonaServicio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZonaServicioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ZonaServicio model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ZonaServicio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ZonaServicio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $array_puntos = explode("_", $model->puntos);

            $i=0;

            $sql = "update zona_servicio set geography=ST_GeographyFromText('POLYGON((";

            foreach ($array_puntos as $punto) {
                if($i==0){
                    $primerPunto = $punto;
                    $i=1;

                    $sql .=str_replace(",", " ", $punto);

                } else {
                    $sql .=",".str_replace(",", " ", $punto);
                }
            }
            $sql .=",".str_replace(",", " ", $primerPunto)."))')  where id = ".$model->id;
            $connection = \Yii::$app->db;
            $result =  $connection->createCommand($sql)->queryAll();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ZonaServicio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $array_puntos = explode("_", $model->puntos);

            $i=0;

            $sql = "update zona_servicio set geography=ST_GeographyFromText('POLYGON((";

            foreach ($array_puntos as $punto) {
                if($i==0){
                    $primerPunto = $punto;
                    $i=1;

                    $sql .=str_replace(",", " ", $punto);

                } else {
                    $sql .=",".str_replace(",", " ", $punto);
                }
            }
            $sql .=",".str_replace(",", " ", $primerPunto)."))')  where id = ".$model->id;
            $connection = \Yii::$app->db;
            $result =  $connection->createCommand($sql)->queryAll();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDiario(){
        $model=$this->findModel(5);

        //echo $model->geography."<br>";


        $sql = "select * from cuadra where ST_Covers('$model->geography'::geometry, geography::geometry)";

        //echo $sql."<br>";
        $connection = \Yii::$app->db;
        $cuadras =  $connection->createCommand($sql)->queryAll();

        $sql = "select id_vehiculo, vehiculo.patente, count(*) as cantidad from posicion join vehiculo on vehiculo.id=posicion.id_vehiculo where ST_Covers('$model->geography'::geometry, geo::geometry) and fecha>1430649001 and fecha<1430688601 group by id_vehiculo, vehiculo.patente order by count(*) desc";

        //echo $sql."<br>";
        $connection = \Yii::$app->db;
        $vehiculos =  $connection->createCommand($sql)->queryAll();

        $vehiculos_sirviendo = "";

        $vehiculos_array = array();
        $patentes = array();

        foreach ($vehiculos as $key => $vehiculo) {
            //echo $vehiculo['id_vehiculo']." ".$vehiculo['cantidad']."<br>";
            $vehiculos_array[$vehiculo['id_vehiculo']] = $vehiculo['cantidad'];
            $patentes[$vehiculo['id_vehiculo']] = $vehiculo['patente'];
            if($vehiculo['cantidad']>20)
                $vehiculos_sirviendo .= $vehiculo['id_vehiculo'].",";
        }

        $vehiculos_sirviendo = trim($vehiculos_sirviendo, ",");

        //echo count($cuadras)."<br>";

        $i=0;

        $vehiculos = array();
        $promedio_cuadra = array();

        foreach ($cuadras as $cuadra) {

            //echo $cuadra['id']."<br>";

            $sql = "select * from posicion where ST_Intersects(ST_Buffer('".$cuadra['geography']."'::geography,20), ST_Buffer(geo::geography,10) )='t' and fecha>1430649001 and fecha<1430688601 and id_vehiculo in (".$vehiculos_sirviendo.")";
            $puntos =  $connection->createCommand($sql)->queryAll();

            //echo $sql."<br>";
            //echo count($puntos)."<br>";
            $cantidad_puntos = count($puntos);
            $total_fecha = 0;

            foreach ($puntos as $punto) {
                $total_fecha += $punto['fecha'];
                //echo $punto['id_vehiculo']."<br>";
                //echo date("d-m-Y H:i:s", $punto['fecha'])."  ".$punto['fecha']."<br>";
                $i++;
            }
            if($cantidad_puntos>0){
                //echo $total_fecha." ".$cantidad_puntos." = ".($total_fecha/$cantidad_puntos)." ".date("d-m-Y H:i:s", round(($total_fecha/$cantidad_puntos)))."<br>";
                $promedio_cuadra[$cuadra['id']] = round(($total_fecha/$cantidad_puntos));
            } else {
                $promedio_cuadra[$cuadra['id']] = 0;
            }
        }

        //echo "<br>".$i."<br>"."<br>";

        //echo "<br>"."PROMEDIOS"."<br>"."<br>";

        foreach ($promedio_cuadra as $key => $value) {
            //echo $key." ".date("d-m-Y H:i:s", $value)."<br>";
        }

        return $this->render('promedio', [
                'model' => $model,
                'cuadras' => $cuadras,
                'promedio_cuadra' => $promedio_cuadra,
                'vehiculos_array' =>$vehiculos_array,
                'patentes' =>$patentes,
            ]);
    }

    public function actionCalculo(){
        $model=$this->findModel(5);

        //echo $model->geography."<br>";

        //1430562601 base 
        //39600 horario de servicio

        //86400 1 dia

        $horario_segundos = 39600;

        $dia_segundos = 86400;

        $fecha_base = 1430562601;

        $sql = "select * from cuadra where ST_Covers('$model->geography'::geometry, geography::geometry)";

        //echo $sql."<br>";
        $connection = \Yii::$app->db;
        $cuadras =  $connection->createCommand($sql)->queryAll();
        do {

            $sql = "select id_vehiculo, vehiculo.patente, count(*) as cantidad from posicion join vehiculo on vehiculo.id=posicion.id_vehiculo where ST_Covers('$model->geography'::geometry, geo::geometry) and fecha>$fecha_base and fecha<".($fecha_base+$horario_segundos)." group by id_vehiculo, vehiculo.patente order by count(*) desc";

            //echo $sql."<br>";
            $connection = \Yii::$app->db;
            $vehiculos =  $connection->createCommand($sql)->queryAll();

            $vehiculos_sirviendo = "";

            $vehiculos_array = array();
            $patentes = array();
            if(count($vehiculos) >0){
                foreach ($vehiculos as $key => $vehiculo) {
                    //echo $vehiculo['id_vehiculo']." ".$vehiculo['cantidad']."<br>";
                    $vehiculos_array[$vehiculo['id_vehiculo']] = $vehiculo['cantidad'];
                    $patentes[$vehiculo['id_vehiculo']] = $vehiculo['patente'];
                    if($vehiculo['cantidad']>20)
                        $vehiculos_sirviendo .= $vehiculo['id_vehiculo'].",";
                }

                $vehiculos_sirviendo = trim($vehiculos_sirviendo, ",");

                echo count($vehiculos)."<br>";
                
                $i=0;

                $vehiculos = array();
                $promedio_cuadra = array();

                foreach ($cuadras as $cuadra) {

                    //echo $cuadra['id']."<br>";

                    $sql = "select * from posicion where ST_Intersects(ST_Buffer('".$cuadra['geography']."'::geography,20), ST_Buffer(geo::geography,10) )='t' and fecha>$fecha_base and fecha<".($fecha_base+$horario_segundos)." and id_vehiculo in (".$vehiculos_sirviendo.")";
                    $puntos =  $connection->createCommand($sql)->queryAll();

                    echo $sql."<br>";
                    //echo count($puntos)."<br>";
                    $cantidad_puntos = count($puntos);
                    $total_fecha = 0;

                    foreach ($puntos as $punto) {
                        $total_fecha += $punto['fecha'];
                        //echo $punto['id_vehiculo']."<br>";
                        //echo "dato: ".date("d-m-Y H:i:s", $punto['fecha'])."  ".$punto['fecha']."<br>";
                        $i++;
                    }
                    if($cantidad_puntos>0){
                        echo $total_fecha." ".$cantidad_puntos." = ".($total_fecha/$cantidad_puntos)." ".date("d-m-Y H:i:s", round(($total_fecha/$cantidad_puntos)))."<br>";
                        $promedio_cuadra[$cuadra['id']]['promedio'] = round(($total_fecha/$cantidad_puntos), 0, PHP_ROUND_HALF_UP);
                        $promedio_cuadra[$cuadra['id']]['cantidad_puntos'] = $cantidad_puntos;
                    } else {
                        //$promedio_cuadra[$cuadra['id']]['promedio'] = 0;
                        //$promedio_cuadra[$cuadra['id']]['cantidad_puntos'] = 0;

                    }
                }

                //echo "<br>".$i."<br>"."<br>";

                echo "<br>"."PROMEDIOS"."<br>"."<br>";

                foreach ($promedio_cuadra as $key => $value) {
                    echo $key." ".date("d-m-Y H:i:s", $value['promedio'])."<br>";
                    $promedio_diario = new PromedioDiario();
                    $promedio_diario->id_cuadra = $key;
                    $promedio_diario->id_zona_servicio = $model->id;
                    $promedio_diario->fecha = date("d-m-Y", $fecha_base);
                    $promedio_diario->promedio = $value['promedio'];
                    $promedio_diario->cantidad_posiciones = $value['cantidad_puntos'];

                    //$promedio_diario->save();
                }
            }
                

            $fecha_base+=$dia_segundos;



        } while ( $fecha_base<1435726801);



    }

    public function actionHistograma(){
        $model=$this->findModel(5);
        $sql = "select id_cuadra, dia, count(*) from histograma where frecuencia>0 group by id_cuadra, dia having sum(frecuencia)>3";

        //echo $sql."<br>";
        $connection = \Yii::$app->db;
        $cuadras =  $connection->createCommand($sql)->queryAll();

        $sql = "select distinct hora_inicio from histograma order by hora_inicio";
        $horas =  $connection->createCommand($sql)->queryAll();

        $horas_array = [];

        foreach ($horas as $key => $hora) {
            $horas_array[] = $hora['hora_inicio'];
        }

        //print_r($horas_array);

        echo "<br>";

        $i=0;

        $histograma_array = [];

        foreach ($cuadras as $cuadra) {
            //echo $cuadra['id_cuadra']."<br>";
            $sql = "select * from histograma where dia='".$cuadra['dia']."' and id_cuadra = ".$cuadra['id_cuadra']." order by hora_inicio";
            $histogramas =  $connection->createCommand($sql)->queryAll();
            $histograma_array[$i] = [];
            $histograma_array[$i]['dia']=$cuadra['dia'];
            foreach ($histogramas as $histograma) {
                //echo $histograma['hora_inicio']." ".$histograma['frecuencia']."<br>";
                $histograma_array[$i]['frecuencia'][]=$histograma['frecuencia'];
            }
            $i++;
            //print_r($histograma_array);
        }

        return $this->render('histograma', [
                'histogramas' => $histograma_array,
                'horas' => $horas_array,
            ]);
    }

    public function actionHistogramacuadra($id){

        $json = [];
        $model=$this->findModel(5);
        

        $dias = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];

        foreach ($dias as $dia) {
            $sql = "select * from histograma where id_cuadra=$id and dia='$dia' order by dia, hora_inicio";
            $connection = \Yii::$app->db;
            $histograma =  $connection->createCommand($sql)->queryAll();

            foreach ($histograma as $value) {
                $json[$dia][] = $value['frecuencia'];
            }
        }

        //echo $sql."<br>";

        $sql = "select distinct hora_inicio from histograma order by hora_inicio";
        $horas =  $connection->createCommand($sql)->queryAll();

        $horas_array = [];

        foreach ($horas as $hora) {
            $horas_array[] = $hora['hora_inicio'];
        }


        $json['columnas'] = $horas_array;

        echo json_encode($json);
        
    }


    /**
     * Deletes an existing ZonaServicio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);


    }

    /**
     * Finds the ZonaServicio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ZonaServicio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ZonaServicio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionVerruta($id_vehiculo)
    {

        $sql = "select * from posicion where id_vehiculo=$id_vehiculo and fecha>1430649001 and fecha<1430688601 order by fecha asc";
        $connection = \Yii::$app->db;
        $result =  $connection->createCommand($sql)->queryAll();
        echo "<div style='height:100px'></div>";
        
        $i=0;
        foreach ($result as $key => $value) {
            if($i == 0){
                //primer caso
                $puntoaux = $value;
                $i=1;
            } else {

                $linea=array();

                $linea['primer_punto']=$puntoaux['latitud'].",".$puntoaux['longitud'];
                $linea['segundo_punto']=$value['latitud'].",".$value['longitud'];
                $linea['orientacion']=$puntoaux['orientacion'];
                $linea['fecha']=date("d-m-Y H:i:s", $puntoaux['fecha']);
                $lineas[]=$linea;

                $puntoaux=$value;
            }
        }



        return $this->render('verruta', [
                'lineas' => $lineas,
            ]);


    }

    

    
}
