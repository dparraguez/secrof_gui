<?php

//namespace app\controllers;
namespace backend\controllers;
//int_set('memory_limit','-1');
set_time_limit(0);

date_default_timezone_set ("America/Buenos_Aires");

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Cuadra;
use app\models\Histograma;
use app\models\ZonaServicio;
use common\models\FormularioReporte;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\ValidarFormulario;
use app\models\ValidarFormularioAjax;
use yii\widgets\ActiveForm;
use yii\web\Response;
use app\models\FormAlumnos;
use app\models\Alumnos;
use app\models\FormSearch;
use yii\helpers\Html;
use app\models\FormRegister;
use app\models\Users;
use app\models\Usuarios;
use yii\helpers\Url;
use yii\web\Session;
use app\models\FormRecoverPass;
use app\models\FormResetPass;
//use kartik\mpdf\Pdf;
//use kartik\mpdf\mPDF;
use \mPDF;
use DOMDocument;
use DOMXpath; 
//require('../../fpdf/fpdf.php');
//use app\controllers\fpdf;
class DataController extends Controller
{    






public function actionPrueba2(){

  return $this->render('prueba2');
}


    public function actionVerHistograma()
    {
        //$formularioReporte = new FormularioReporte('histograma');
        //$cuadras = null;

        //if ($formularioReporte->load(Yii::$app->request->post()) && $formularioReporte->validate()) {

            //$zonaServicio = zonaServicio::find()
            //    ->where(['id' => $formularioReporte->id_zona_servicio])
            //    ->one();

            //if(!empty($zonaServicio)){
                //$sql = "select * from cuadra where ST_Covers('$zonaServicio->geography'::geometry, geography::geometry)";
                //echo $sql."<br>";
                //$connection = \Yii::$app->db;
                //$cuadras =  $connection->createCommand($sql)->queryAll();

            //}

        //}

        $cuadras = Cuadra::find()->all();
        
        return $this->render('verhistograma', [
            //'formularioReporte' => $formularioReporte,
            'cuadras' => $cuadras,
            ]);

    }

    public function actionVerGraficoHistograma($id_cuadra)
    {
        
        $datos = [];
        $dias = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];

        foreach ($dias as $dia) {

            $histogramas = Histograma::findAll([
                'id_cuadra' => $id_cuadra,
                'dia' => $dia
            ]);

            $i=0;

            foreach ($histogramas as $histograma) {
                $datos[$dia][$i] = $histograma->frecuencia;
                if(!isset($datos['total'][$i])){
                    $datos['Total'][$i]=$histograma->frecuencia;
                } else {
                    $datos['Total'][$i]=$datos['Total'][$i]+$histograma->frecuencia;
                }
                $i++;
            }

        }

        //echo $sql."<br>";

        $horas = Histograma::find()
            ->select('hora_inicio')
            ->distinct()
            ->orderBy('hora_inicio')
            ->all();

        $horas_array = [];

        foreach ($horas as $hora) {
            $horas_array[] = $hora->hora_inicio;
        }

        $datos['columnas'] = $horas_array;
        
        return $this->render('vergraficohistograma', [
            //'formularioReporte' => $formularioReporte,
            'datos' => $datos,
            ]);

    }

    public function actionVerProbabilidad()
    {
        $model = new FormularioReporte();
        $model->scenario='probabilidad';
        $resultado = null;
        $resultado_total=null;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $id_calle_B=$model->id_cuadra;
            $hora_B=trim($model->hora_inicio).":00";

            $resultado_total = $this->calcularProbabilidad($id_calle_B, $hora_B);

            $resultado =  "la probabilidad de que pase el camion es"." ".$resultado_total[0]["probabilidadOcurrencia"]." "." ";

            $resultado .= "por la calle,"." ".$resultado_total[0]["calle"]." "." ";

            $resultado .= "a la,"." ".$resultado_total[0]["hora"]." "." ";

            $resultado .= "con fecha"." ".$resultado_total[0]["Fecha"];

        }

        $cuadras = Cuadra::find()->all();

        return $this->render('verprobabilidad', [
            'model' => $model,
            'cuadras' => $cuadras,
            'resultado' => $resultado,
            'resultado_total'=> $resultado_total,
            ]);

    }

    

    public function actionConsumovd()
    {
    //funcion para poblar tabla consumo_diario_vehiculo
    // rango de los datos  '2015-05-01 00:00:01' a '2015-06-28 23:59:59'
        $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
        $fecha_inicio=null;
        $fecha_termino=null;
        $datos_vehiculo=null;
        $datos_resultado=null;
        $inicio_control=null;
        $termino_control=null;
        $valorcombustible=570;
        $q=0;
        $k=0;
//extraer fecha maxima de posicion
        $sql="SELECT to_timestamp(MAX(fecha)) as fecha from posicion";
        $resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
        $fecha_termino=pg_fetch_result ($resultado,0,0);
        pg_free_result($resultado);
//extraer fecha minima
        $sql="SELECT to_timestamp(MIN(fecha)) as fecha from posicion";
        $resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
        $fecha_inicio=pg_fetch_result ($resultado,0,0);
        pg_free_result($resultado);
//$fecha_inicio= date('Y-m-d H:i:s',$fecha_inicio);
//$fecha_termino= date('Y-m-d H:i:s',$fecha_termino);

//Extraer los id_vehiculos, que en posicion
        $sql="SELECT DISTINCT id_vehiculo as id from posicion";
        $resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
//Guardo el resultado en un array
        $array = array();
        while($row = pg_fetch_assoc($resultado)) {
          $array[] = $row;
      }
      pg_free_result($resultado);
//extraer datos de vehiculos segun su id
      foreach ($array as $key ) {


       $id=intval($key['id']);
       $sql="SELECT * from vehiculo where id =$id";
       $resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
       $fila=pg_fetch_assoc ($resultado);
       if($fila!=null){
        $datos_vehiculo[$q]=array(
            'id'=>intval($fila['id']),
            'patente'=>$fila['patente'],
            'conductor'=>$fila['conductor'],
            'flota'=>$fila['flota'],
            'tipo'=>$fila['tipo']
            );

        $q++;
    }
}



$termino_control = strtotime($fecha_termino);
foreach ($datos_vehiculo as $value) {
    //ejecutar PA para calculo
    $id=$value['id'];
    $inicio_control = strtotime($fecha_inicio);
    while($inicio_control<=$termino_control){

        $inicio_control= date('Y-m-d ',$inicio_control);
        $comienzo=$inicio_control." "."00:00:01";
        $fin=$inicio_control." "."23:59:59";
        $sql="SELECT costo_total_vehiculo_diario('$comienzo','$fin',$id)";
        $resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");

$litros= floatval(pg_fetch_result ($resultado,0,0)); //litros de combustible

//$litros=pg_fetch_object($resultado);

$valor=$litros*$valorcombustible;

if($litros>0){

    $datos_resultado[$k]=array(
        'patente'=>$value['patente'],
        'conductor'=>$value['conductor'],
        'flota'=>$value['flota'],
        'tipo'=>$value['tipo'],
        'litros'=>$litros,
        'valor'=>$valor,
        'fecha'=>$inicio_control
        );

    $k++;
}

$inicio_control=strtotime('+1 day',strtotime($inicio_control));

pg_free_result($resultado);
}

}




return $this->render('curl',['datos_vehiculo'=>$datos_resultado]);


}

public function actionCalculoconsumovehiculo()
{
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $fecha_inicio=null;
    $fecha_termino=null;
    $datos_vehiculo=null;
    $datos_resultado=null;
    $valorcombustible=570;
    $q=0;
    $k=0;


//Extraer los id_vehiculos, que en posicion
    $sql="SELECT DISTINCT id_vehiculo as id from posicion";
    $resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
//Guardo el resultado en un array
    $array = array();
    while($row = pg_fetch_assoc($resultado)) {
      $array[] = $row;
  }
  pg_free_result($resultado);


//extraer datos de vehiculos segun su id
  foreach ($array as $key ) {


   $id=intval($key['id']);
   $sql="SELECT * from vehiculo where id =$id";
   $resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
   $fila=pg_fetch_assoc ($resultado);
   if($fila!=null){
    $datos_vehiculo[$q]=array(
        'id'=>intval($fila['id']),
        'patente'=>$fila['patente'],
        'conductor'=>$fila['conductor'],
        'flota'=>$fila['flota'],
        'tipo'=>$fila['tipo']
        );
    $q++;
}
}



foreach ($datos_vehiculo as $key ) {
    $id=$key['id'];
    $sql="SELECT costo_total_vehiculo_diario('2015-06-28 00:00:01','2015-06-28 23:59:59',$id)";
    $resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
$litros= floatval(pg_fetch_result ($resultado,0,0)); //litros de combustible
//$litros=pg_fetch_object($resultado);
$valor=$litros*$valorcombustible;

if($litros>0){

    $datos_resultado[$k]=array(
        'id'=>$id,
        'patente'=>$key['patente'],
        'conductor'=>$key['conductor'],
        'flota'=>$key['flota'],
        'tipo'=>$key['tipo'],
        'litros'=>$litros,
        'valor'=>$valor,
        'fecha'=>'2015-06-28'
        );

    $k++;
}


pg_free_result($resultado);

}


foreach ($datos_resultado as $key) {

    $fecha= $key['fecha'];
    $consumo= $key['litros'];
    $patente=$key['patente'];
    $conductor=$key['conductor'];
    $flota=$key['flota'];
    $tipo=$key['tipo'];
    $valor=$key['valor'];

    $sql="INSERT INTO consumo_diario_vehiculo (fecha,consumo,patente,conductor,flota,tipo,valor)
    VALUES ('$fecha',$consumo,'$patente','$conductor','$flota','$tipo',$valor);";
    $resultado = pg_query ($conexion,$sql) or die("Error en la consulta SQL");



}






return $this->render('curl',['datos_resultado'=>$datos_resultado]);
}







public function actionCalculoconsumo()
{
    set_time_limit(0);
     $data=null;
    $model = new ValidarFormularioAjax;
    $msg = 0;
    $result = null;
    $valor=0;
    if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
    }

    if ($model->load(Yii::$app->request->post()))
    {
        if ($model->validate())
        {
            $fechainicio=$model->fecha_inicio;
            $fechatermino=$model->fecha_termino;
            $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres");
            $sql="  SELECT *from consumo_diario_vehiculo where fecha BETWEEN '$fechainicio' and '$fechatermino' ";
            $result = pg_query ($conexion,$sql) or die("Error en la consulta SQL");
            $registros= pg_num_rows($result);

if($result!=false){


        for($l=0;$l<pg_num_rows($result);$l++)
        {

            $row=pg_fetch_array($result,$l);
            $data[$l]=array( 
  'fecha'=> $row['fecha'],
  'consumo'=> $row['consumo'],
  'patente' =>$row['patente'],
  'conductor' =>$row['conductor'],
  'fecha' =>$row['fecha'],
  'flota'=> $row['flota'],
  'tipo' =>intval($row['tipo']),
  'litros'=>$row['consumo'],
  'valor'=>$row['consumo']*$model->valorcombustible,
                );
$msg=$msg+$row['consumo'];
$valor=$valor+ ($row['consumo']*$model->valorcombustible);
        }


    }
    else{ $datos=null;}
    pg_free_result($result);
    pg_close($conexion);

        $content=$this->render('view', ['valor' => $valor, 'msg' => $msg,'query'=>$data,'fecha_inicio'=>$fechainicio,'fecha_termino'=>$fechatermino,'valorcombustible'=>$model->valorcombustible]);
        

        $mpdf = new \mPDF;
        $mpdf->WriteHTML($content);
        $mpdf->Output();

        exit;
    }
    else
    {
        $model->getErrors();
    }

}

return $this->render("consumo", ['model' => $model]);
}

















public function behaviors()
{
    return [
    'access' => [
    'class' => AccessControl::className(),
    'only' => ['logout'],
    'rules' => [
    [
    'actions' => ['logout'],
    'allow' => true,
    'roles' => ['@'],
    ],
    ],
    ],
    'verbs' => [
    'class' => VerbFilter::className(),
    'actions' => [
    'logout' => ['post'],
    ],
    ],
    ];
}

public function actions()
{
    return [
    'error' => [
    'class' => 'yii\web\ErrorAction',
    ],
    'captcha' => [
    'class' => 'yii\captcha\CaptchaAction',
    'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
    ],
    ];
}

public function actionIndex()
{
    return $this->render('index');
}

public function actionLogin()
{
    if (!\Yii::$app->user->isGuest) {
        return $this->goHome();
    }

    $model = new LoginForm();
    if ($model->load(Yii::$app->request->post()) && $model->login()) {
        return $this->goBack();
    } else {
        return $this->render('login', [
            'model' => $model,
            ]);
    }
}

public function actionLogout()
{
    Yii::$app->user->logout();

    return $this->goHome();
}

public function actionContact()
{
    $model = new ContactForm();
    if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
        Yii::$app->session->setFlash('contactFormSubmitted');

        return $this->refresh();
    } else {
        return $this->render('contact', [
            'model' => $model,
            ]);
    }
}

public function actionAbout()
{
    return $this->render('about');
}




public function actionPrueba()
{
//rangos de fechas probados en tabla posicion en la bd del servidor
// '2012-01-01 00:00:01' and '2012-01-15 23:59:00' no valido
//'2013-01-01 00:00:01' and '2013-01-15 23:59:00' no valido
// '2014-01-01 00:00:01' and '2014-01-15 23:59:00' no valido
//// '2015-01-01 00:00:01' and '2015-01-15 23:59:00' no valido


//rangos que serviran para extraer los datos
//'2015-05-01 00:00:01' and '2015-05-8 23:59:00' rango valido ya almacenado
//'2015-05-08 23:59:59' and '2015-05-16 23:59:59' rango valido ya almacenado

//'2015-05-17 00:00:01' and '2015-05-24 23:59:59' rango valido ya almacenado
//'2015-05-25 00:00:01' and '2015-05-31 23:59:59' rango    valido ya almacenado

//'2015-06-01 00:00:01' and '2015-06-7 23:59:59' rango valido ya alamcenado
// '2015-06-08 00:00:01' and '2015-06-14 23:59:59' rango valido ya almacenado 
//'2015-06-15 00:00:01' and '2015-06-21 23:59:59'  rango valido ya almacenado
//'2015-06-22 00:00:01' and '2015-06-28 23:59:59' rango valido ya almacenado
//'2015-06-29 00:00:01' and '2015-07-01 23:59:59' rango no valido no hay info en la bd del servidor
    $msg=null;
    $cuadras=null;
    $k=0;
    $puntos_asociados=null;
    $cantidad_cuadras=0;
    $suma=0;
    $promedio=0;
    $idcuadra=null;
    $contenido=null;
    $q=0;
    $fecha_minima=null;
    $fecha_actual=null;
    $fecha_maxima=null;
    $count=0;
//extraer la info de la bd remota
/*$sql="SELECT *,to_timestamp(p.fecha) from posicion p where to_timestamp(p.fecha) between '2015-06-29 00:00:01' and '2015-07-01 23:59:59';";
$conexion=pg_connect("host=localhost port=5434 dbname=secrof_gui user=postgres password=postgres...");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

if(pg_num_rows($result)>0){

for($i=0;$i<pg_num_rows($result);$i++)
{
$datos[$i]=pg_fetch_array($result,$i);
}

}
else{ $datos=null;}
pg_free_result($result);
pg_close($conexion);*/


//comprobando conexion a bd local



//1.extraer todas las cuadras
$sql="SELECT * from cuadra";
$conexion=pg_connect("host=localhost port=5435 dbname=BD_espacial user=postgres password=708c50efc7");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
if(pg_num_rows($result)>0){

    for($i=0;$i<pg_num_rows($result);$i++)
    {
        $cuadras[$i]=pg_fetch_array($result,$i);
    }

    $msg="enorabuena se ejecuto correctamente la consulta";
}
else{ $cuadras=null;}
pg_free_result($result);
pg_close($conexion);


//extraer fecha minima
$sql="SELECT MIN(fecha) as minima from posicion";
$conexion=pg_connect("host=localhost port=5435 dbname=BD_espacial user=postgres password=708c50efc7");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
$row=pg_fetch_array($result,0);

$fecha_minima=$row['minima'];
pg_free_result($result);
pg_close($conexion);

//extraer fecha maxima
$sql="SELECT MAX(fecha) as maxima from posicion";
$conexion=pg_connect("host=localhost port=5435 dbname=BD_espacial user=postgres password=708c50efc7");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
$row=pg_fetch_array($result,0);

$fecha_maxima=$row['maxima'];
pg_free_result($result);
pg_close($conexion);


$fecha_minima=date('Y/m/d', intval($fecha_minima));
//$fecha_minima=intval($fecha_minima);
$fecha_maxima=date('Y/m/d', intval($fecha_maxima));
//$fecha_maxima=intval($fecha_maxima);
$fecha_actual=$fecha_minima;

//while($q<60){


//1 extraer puntos asociados a las calles y guardarlos en un array asociativo
for($i=0;$i<count($cuadras);$i++)
{
//extraer con select
  $latitud1=$cuadras[$i]['latitud1']; 
  $longitud1=$cuadras[$i]['longitud1'];
  $latitud2=$cuadras[$i]['latitud2']; 
  $longitud2=$cuadras[$i]['longitud2'];
  $id=intval($cuadras[$i]['id_cuadra']);
  //$radio=0.1;
$radio=0.0002;//100 metros
//$inicio=date('Y/m/d', intval($fecha_actual)).' '.'00:00:01';
//$fin=date('Y/m/d', intval($fecha_actual)).' '.'23:59:59';
$fecha1='2015-05-21';
$inicio=$fecha1.' '.'00:00:01';
$fin=$fecha1.' '.'23:59:59';




$sql="SELECT * from posicion p where to_timestamp(p.fecha) BETWEEN '$inicio' and '$fin' and  ST_Contains(ST_Buffer(ST_GeomFromText('LINESTRING($latitud1 $longitud1,$latitud2 $longitud2)',4326),$radio)  , punto );";
$conexion=pg_connect("host=localhost port=5435 dbname=BD_espacial user=postgres password=708c50efc7");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");


if($result!=false){


    for($l=0;$l<pg_num_rows($result);$l++)
    {

        $row=pg_fetch_array($result,$l);
        $puntos_asociados[$cantidad_cuadras][$l]=array(
            'id_cuadra'=>$id,
            'id'=>$row['id'],
            'fecha'=>$row['fecha'],

            );

    }
    $cantidad_cuadras++;

}
pg_free_result($result);
pg_close($conexion);
}//fin del for cuadras

$p=0;



foreach($puntos_asociados as $calle)
{

    $suma1=0;
    $cantidad_puntos1=0;

    $count=0;
    foreach ($calle as $row) {

        if($count==0){
            $idcuadra=intval($row["id_cuadra"]);

        }
        $hora=date('H:i:s',intval($row["fecha"]));
        $hora=strtotime($hora);
        $suma1=$suma1+intval($hora);

        $cantidad_puntos1++;

        $count=1;

    }




$promedio1=intval($suma1/$cantidad_puntos1);// promedio como integer que contiene el timestamp
$time1=date('H:i:s', $promedio1);// extraigo la hora promedio en formato hora, minutos, segundos
//$fecha1= date('Y/m/d', $promedio1); // traigo la fecha en formato año, mes , dia

$fechats= strtotime($fecha1);

switch (date('w',$fechats)) {
    case 0: $dia1="Domingo";break;
    case 1: $dia1="Lunes";break;
    case 2: $dia1="Martes";break;
    case 3: $dia1="Miercoles";break;
    case 4: $dia1="Jueves";break;
    case 5: $dia1="Viernes";break; 
    case 6: $dia1="Sabado";break;  
}




$sql="INSERT INTO promedio_cuadra (id_cuadra,promedio_hora_pasada,dia,fecha) VALUES ($idcuadra,'$time1','$dia1','$fecha1');";
$conexion=pg_connect("host=localhost port=5435 dbname=BD_espacial user=postgres password=708c50efc7");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
pg_free_result($result);
pg_close($conexion);
//$k=0;

}// fin del for que recorre todos los puntos asociados a als calles*/

//$fecha_actual=strtotime('+1 day',strtotime($fecha_actual));
//$fecha_actual= date("Y-m-d",$fecha_actual);
//$cantidad_cuadras=0;
//$q++;
//$k=0;
//}//fin del while





//guardar la  info en la BD local  
 /* for($i=0;$i<count($datos);$i++)
  {
$id=$datos[$i]['id'];
$latitud=$datos[$i]['latitud'];
$longitud=$datos[$i]['longitud'];
$velocidad=$datos[$i]['velocidad'];
$fecha=$datos[$i]['fecha'];
$orientacion=$datos[$i]['orientacion'];
$id_vehiculo=$datos[$i]['id_vehiculo'];
$sql="INSERT INTO posicion (id,latitud,longitud,velocidad,fecha,orientacion,id_vehiculo)
VALUES ($id,$latitud,$longitud,$velocidad,$fecha,$orientacion,$id_vehiculo); ";
$conexion=pg_connect("host=localhost port=5435 dbname=BD_espacial user=postgres password=708c50efc7");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
if($result)
{
    $count++;
}

  }


pg_free_result($result);
pg_close($conexion);
if($count>0)
{

    $msg="enorabuena se ejecuto correctamente la consulta";
}
*/


return $this->render("prueba", ['cuadras' => $cuadras,'msg'=>$msg,'puntos_asociados'=>$puntos_asociados,'contenido'=>$contenido,'fecha_minima'=>$fecha_minima,'fecha_maxima'=>$fecha_maxima]);

}




public function actionModificartablapromedio()

{

//1. ir sacando los datos de tabla promedio_diario he ir guardandolos en un arreglo que contendra una replica pero con el formato deseado
//2. luego ir recorriendo el arreglo he ir asiendo el update a los atributos nuevos
$diferencia='03:00:00'; // es la diferencia que hay entra la hora que esta en la bd y la hora local
$datos=null;
$dia=null;
$sql="SELECT id,promedio,fecha from promedio_diario";
$conexion=pg_connect("host=localhost port=5432 dbname=BD_espacial user=postgres password=708c50efc7");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");






if($result!=false){


    for($l=0;$l<pg_num_rows($result);$l++)
    {

        $row=pg_fetch_array($result,$l);

//procesar datos de fecha por dia, y hora en int a time

        $hora=date('H:i:s',intval($row['promedio']));
        $hora=strtotime($hora)-strtotime($diferencia);
        $hora=date('H:i:s',$hora);
        $fecha1=$row['fecha'];
        $fechats= strtotime($fecha1);

        switch (date('w',$fechats)) {
            case 0: $dia="Domingo";break;
            case 1: $dia="Lunes";break;
            case 2: $dia="Martes";break;
            case 3: $dia="Miercoles";break;
            case 4: $dia="Jueves";break;
            case 5: $dia="Viernes";break; 
            case 6: $dia="Sabado";break;  
        }

        $datos[$l]=array(
            'id'=>intval($row['id']),
            'hora_promedio'=>$hora,
            'dia'=>$dia,

            );

    }


}
else {$datos=null;}
pg_free_result($result);
pg_close($conexion);



foreach ($datos as $row) {
    $dia=$row['dia'];
    $id=$row['id'];
    $hora=$row['hora_promedio'];

    $sql="UPDATE promedio_diario SET dia ='$dia' , promedio_hora_pasada='$hora' WHERE id=$id;";
    $conexion=pg_connect("host=localhost port=5432 dbname=BD_espacial user=postgres password=708c50efc7");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");


}


pg_free_result($result);
pg_close($conexion);

return $this->render("update_table",['datos'=>$datos]);
}


// funcion para hacer el histograma con los datos de los horaros promedios
public function actionNewhistograma()
{

    $histograma=null;
    $id_cuadras=null;
    $k=0;
    $dias=array("Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo");

    $hora_inicio='07:00:00';
    $hora_fin='07:30:00';
    $msg="no se ejecuto la consulta";
//1.extraer todos los id_cuadra distintos en promedio_cuadra
    $sql="SELECT DISTINCT(id_cuadra) from promedio_diario ORDER BY id_cuadra ";
    $conexion=pg_connect("host=localhost port=5432 dbname=BD_espacial user=postgres password=708c50efc7");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

    if($result!=false){


        for($l=0;$l<pg_num_rows($result);$l++)
        {

            $row=pg_fetch_array($result,$l);
            $id_cuadras[$l]=array(
                'id_cuadra'=>intval($row['id_cuadra']),


                );

        }


    }
    else{ $id_cuadras=null;}
    pg_free_result($result);
    pg_close($conexion);





    $i=0;
// entre los dias lunes y domingo
    while($i<7){
        $dia=$dias[$i];
// el rango de hora es entra las 7:00:00  a 23:30:00 
        $hora_inicio='07:00:00';
        $hora_fin='07:30:00';
        while($hora_fin!='23:30:00'){

//2 recorre las cuadras, y calcular su histograma
            foreach ($id_cuadras as $row) {
                $idcuadra=$row['id_cuadra'];

  //$sql="SELECT COUNT(promedio_hora_pasada) as frecuencia from promedio_cuadra WHERE promedio_hora_pasada BETWEEN '$hora_inicio' and '$hora_fin' and dia='$dia' and id_cuadra=$idcuadra";
                $sql="SELECT COUNT(*) as frecuencia from promedio_diario WHERE promedio_hora_pasada>'$hora_inicio' and promedio_hora_pasada<'$hora_fin' and dia='$dia' and id_cuadra=$idcuadra";
                $conexion=pg_connect("host=localhost port=5432 dbname=BD_espacial user=postgres password=708c50efc7");
                $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

                $var=pg_fetch_array($result,0);
                $histograma[$k]=array(
                    'id_cuadra'=>$idcuadra,
                    'dia'=>$dia,
                    'hora_inicio'=>$hora_inicio,
                    'hora_fin'=>$hora_fin,
                    'frecuencia'=>intval($var['frecuencia']),
                    );

                $k++;
            }
            pg_free_result($result);
            pg_close($conexion);

            $hora_inicio=strtotime('+1 second',strtotime($hora_fin));
            $hora_inicio= date('H:i:s',$hora_inicio);
            $hora_fin=strtotime('+30 minute',strtotime($hora_fin));
            $hora_fin= date('H:i:s',$hora_fin);
        }





        $i++;
//$hora_inicio='07:00:00';
//$hora_fin='07:30:00';

    }

//se almacena la informacion obtenida
    foreach ($histograma as $fila) {
      $idcuadra=$fila['id_cuadra'];
      $dia=$fila['dia'];
      $hora_inicio=$fila['hora_inicio'];
      $hora_fin=$fila['hora_fin'];
      $frecuencia=$fila['frecuencia'];

      $sql="INSERT INTO histograma(id_cuadra,dia,hora_inicio,hora_fin,frecuencia) VALUES($idcuadra,'$dia','$hora_inicio','$hora_fin',$frecuencia)";
      $conexion=pg_connect("host=localhost port=5432 dbname=BD_espacial user=postgres password=708c50efc7");
      $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
  }
  pg_free_result($result);
  pg_close($conexion);




  return $this->render("Histograma",['id_cuadras'=>$id_cuadras,'msg'=>$msg,'histograma'=>$histograma]);


}// fin del actionNewhistograma







public function actionNewprobabilidad()
{
    $id_cuadras=null;
    $k=0;
    $i=0;
    $prueba=null;
    $dia=null;
    $dias=array("Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo");

//1.extraer todos los id_cuadra distintos en promedio_cuadra
    $sql="SELECT DISTINCT(id_cuadra) from histograma ORDER BY id_cuadra ";
    $conexion=pg_connect("host=localhost port=5432 dbname=BD_espacial user=postgres password=708c50efc7");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

    if($result!=false){


        for($l=0;$l<pg_num_rows($result);$l++)
        {

            $row=pg_fetch_array($result,$l);
            $id_cuadras[$l]=array(
                'id_cuadra'=>intval($row['id_cuadra']),
                );
        }


    }
    else{ $id_cuadras=null;}
    pg_free_result($result);
    pg_close($conexion);



    while($i<7)
    {
        $dia=$dias[$i];

        foreach ($id_cuadras as $row ) {
         $idcuadra=$row['id_cuadra'];

         $sql="SELECT SUM(frecuencia) as casosposibles from histograma WHERE dia='$dia' and id_cuadra=$idcuadra";
         $conexion=pg_connect("host=localhost port=5432 dbname=BD_espacial user=postgres password=708c50efc7");
         $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
         $var=pg_fetch_array($result,0);

         $casosPosibles=floatval($var['casosposibles']);
         $prueba[$k]=$casosPosibles;
         $k++;
         pg_free_result($result);
         pg_close($conexion);
         if($casosPosibles!=0){

            $sql="UPDATE histograma set probabilidad=(((frecuencia::double precision)/$casosPosibles)*100)::double precision where dia='$dia' and id_cuadra=$idcuadra";
            $conexion=pg_connect("host=localhost port=5432 dbname=BD_espacial user=postgres password=708c50efc7");
            $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
        }
        else{
            $sql="UPDATE histograma set probabilidad=0 where dia='$dia' and id_cuadra=$idcuadra";
            $conexion=pg_connect("host=localhost port=5432 dbname=BD_espacial user=postgres password=708c50efc7");
            $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
        }


    }

    $i++;
}





return $this->render("probabilidad",['prueba'=>$prueba]);
}



public function calcularProbabilidad($id_calle_B, $hora_B)
{

//1. obtener todos los puntos que estan en la zona de la cuadra a consultar
//2. a partir de los puntos obtenidos, obtener todas las calles que contienen a esos puntos
//3. realizar el calculo p(B/A), para todas las calles encontradas, elegir la probabilidad mayor esto implica obtener P(A), P(B), P(A/B)
//4. definir un umbral (rango de probabilidad ), para establecer si sera servido o no la calle.


//cada zona tiene uno o mas horarios de servicios (horario de servicio 7:30 AM a 5:00 PM, zona a analizar)

//datos de entrada
 //$id_calle_B=549; // es el input que representa la calle (id_calle) , es de tipo integer
 $hora_B='11:00:00'; // es el input que representa la hora a consultar , es de tipo Time // comenta esto cuando tengas los datos en tiempo real

// p(a) entre hora_inicio y hora_actual   P(B) entre hora_actual y hora_B   p(a/b) la combinacion de las 2 anteriores
// ademas hay que tener encuenta que la diferencia entre hora actual y hora_b no se ha superior a 30 minutos, para que encuentre una tupla que se adapte a la consulta.    
 $dia_actual='2015-05-02';  // comenta esto cuando tengas los datos en tiempo real
 $hora_actual='10:30:00';// comenta esto cuando tengas los datos en tiempo real
 $dia="Sabado"; // comenta esto cuando tengas los datos en tiempo real

//$dia_actual=date("Y-m-d"); // descomenta esto cuando tengas los datos en tiempo real
//$hora_actual=date('H:i:s'); // descomenta esto cuando tengas los datos en tiempo real


$fechat_time = strtotime($dia_actual); //a timestamp 

//el parametro w en la funcion date indica que queremos el dia de la semana 
//lo devuelve en numero 0 domingo, 1 lunes,.... 
switch (date('w', $fechat_time)){ 
    case 0: $dia="Domingo"; break; 
    case 1: $dia="Lunes"; break; 
    case 2: $dia="Martes"; break; 
    case 3: $dia="Miercoles"; break; 
    case 4: $dia="Jueves"; break; 
    case 5: $dia="Viernes"; break; 
    case 6: $dia="Sabado"; break; 
}

$rango_time=strtotime($hora_B)-strtotime($hora_actual);
$rango_time= date('H:i:s',$rango_time);

$hora_inicio_zona=null;
$datos_posicion=null;
$info_zona=null;
$geography_zona=null;
$id_zona=null;
$datos_posicion_filtrados=null;
//crear el inicio del rango de hora entre($hora_inicio, $hora_actual)
$hora_inicio=strtotime($hora_actual)-strtotime($rango_time);
$hora_inicio= date('H:i:s',$hora_inicio);

//a partir de la calle a analizar identificar la zona
$sql="SELECT DISTINCT( id_zona_servicio) from promedio_diario where id_cuadra='$id_calle_B' LIMIT 1";
$conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
$resultado = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

if($resultado){
    for($l=0;$l<pg_num_rows($resultado);$l++)
    {

        $row=pg_fetch_array($resultado,$l);
        $id_zona=intval($row["id_zona_servicio"]);

    }

}

else{ $id_zona=null;}
pg_free_result($resultado);
pg_close($conexion);
//extraer la info de la zona y guardarla
$sql="SELECT geography,id,hora_inicio from zona_servicio where id=$id_zona LIMIT 1";
$conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...");
$resultado = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
if($resultado){
    for($l=0;$l<pg_num_rows($resultado);$l++)
    {

        $row=pg_fetch_array($resultado,$l);
        $info_zona[$l]=array(
            'id'=>intval($row["id"]),
            'geography'=>$row['geography'],
            'hora_inicio_zona'=>$row['hora_inicio'],
            );

    }

}

else{ $info_zona=null;}
pg_free_result($resultado);
pg_close($conexion);


// update a atributo hora de posicion

foreach ($info_zona as $key) {
    $geography_zona=$key['geography'];
    $hora_inicio_zona=$key['hora_inicio_zona'];
}


$sql="SELECT id,fecha,id_vehiculo,latitud,longitud,geo FROM posicion p WHERE  to_timestamp(p.fecha)::time between '$hora_inicio' and '$hora_actual' and to_timestamp(p.fecha)::date= 
'$dia_actual' and  ST_Covers('$geography_zona'::geography,geo::geography) and EXISTS (Select id_vehiculo from posicion where id_vehiculo=p.id_vehiculo and ST_Covers('$geography_zona'::geography,geo::geography)  and  to_timestamp(fecha)::date= 
    '$dia_actual' and to_timestamp(fecha)::time between '$hora_inicio_zona' and '$hora_actual' group by id_vehiculo having count(id_vehiculo)>10 ) order by id_vehiculo,fecha asc ";

$conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...");
$resultado = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
if($resultado){
    for($l=0;$l<pg_num_rows($resultado);$l++)
    {

        $row=pg_fetch_array($resultado,$l);
        $datos_posicion[$l]=array(
            'id'=>intval($row['id']),
            'fecha'=>intval($row['fecha']),
            'id_vehiculo'=>intval($row['id_vehiculo']),
            'latitud'=>($row['latitud']),
            'longitud'=>($row['longitud']),
            'punto'=>($row['geo']),
            );

    }

}
else{ $datos_posicion=null;}

pg_free_result($resultado);
pg_close($conexion);


//verificar si el o los vehiculos estan siviendo realmente la zona , extraer la ultima posicion
$cambios=0;
$latitud=null;
$longitud=null;
$id_tupla=null;
$id_vehiculo=null;
$contador_cambio=null;
$punto=null;
$i=0;
$k=0;
$mayor=$datos_posicion[0]['fecha'];
$id=$datos_posicion[0]['id_vehiculo'];


if(count($datos_posicion)>0){

    foreach ($datos_posicion as $key ) {
     if($id!=$key['id_vehiculo']){$cambios++;}
 }


 if($cambios==0){

    foreach ($datos_posicion as $key ) {
     if ($mayor<$key['fecha']) {
         $mayor=$key['fecha'];

     }

     $id_tupla=$key['id'];
     $latitud=$key['latitud'];
     $longitud = $key['longitud'];
     $punto= $key['punto'];

 }
 $datos_posicion_filtrados[0]=array(
    'id'=>$id_tupla,
    'fecha'=>$mayor,
    'id_vehiculo'=>$id,
    'latitud'=>$latitud,
    'longitud'=>$longitud,
    'punto'=>$punto,
    );
}
if($cambios>0){
    $id= $datos_posicion[0]['id_vehiculo'];
    $reciente=$datos_posicion[0];
    $count=0;
    $datos_posicion_filtrados=null;
    $k=0;
    $es=0;
    while($count<count($datos_posicion))
    {

       $id=$datos_posicion[$count]['id_vehiculo'];
       while(  ($count<count($datos_posicion) )&& ($id==$datos_posicion[$count]['id_vehiculo'] ))
       {

        if($reciente['fecha']<$datos_posicion[$count]['fecha']) $reciente=$datos_posicion[$count];
        $count++;
    }
    //almacenar la posicion mas reciente de cada id_vechiculo en array2

    $datos_posicion_filtrados[$k]=$reciente;
    $k++;
}

}
}

//asociar puntos a la calle mas cercana o en al que estan contenidas
$calles_asociadas=null;
$h=0;
$radio=0.0001;//100 metros
if(count($datos_posicion_filtrados)>0){
    foreach ($datos_posicion_filtrados as $key) 
    {
        $punto=$key['punto'];
        $sql="SELECT * from cuadra  where  ST_Contains(ST_Buffer(geography::geometry,$radio)  , '$punto' );";
        $conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...");
        $resultado = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
        for($l=0;$l<pg_num_rows($resultado);$l++)
        {

            $row=pg_fetch_array($resultado,$l);
            $calles_asociadas[$h]=array(
                'id'=>intval($row['id']),
                'latitud_inicio'=>$row['latitud_inicio'],
                'longitud_inicio'=>$row['longitud_inicio'],
                'latitud_fin'=>$row['latitud_fin'],
                'longitud_fin'=>$row['longitud_fin'],
                'id_calle'=>$row['id_calle'],
                );
            $h++;
        }

    }

}else{$calles_asociadas=null;}
//algoritmo de probabilidad
$probabilidadA=null;
$probabilidadB=null;
$probabilidadAdadoB=null;
$probabilidadBdadoA=null;

$calle_probabilidad=null;
$h=0;
if(count($calles_asociadas)>0){

    foreach ($calles_asociadas as $key ) {   
        $id_calle_A=intval($key['id']);
//P(A)
        $sql="SELECT probabilidad from histograma2 WHERE dia='$dia' AND '$hora_inicio'<=hora_inicio and '$hora_actual'>=hora_fin and id_cuadra=$id_calle_A limit 1;";
        $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
        $resultado = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

        if(pg_num_rows($resultado)>0){
            for($l=0;$l<pg_num_rows($resultado);$l++)
            {

                $row=pg_fetch_array($resultado,$l);
                $probabilidadA=floatval($row["probabilidad"]);
            }

        }
        else{ $probabilidadA=0;}
        pg_free_result($resultado);
        pg_close($conexion);
//P(B)
        $sql="SELECT probabilidad from histograma2 WHERE dia='$dia' AND '$hora_actual'<=hora_inicio and '$hora_B'>=hora_fin and id_cuadra=$id_calle_B limit 1;";
        $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
        $resultado = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
//$var=pg_fetch_array($result,0);
//$probabilidadA=$resultado;
        if(pg_num_rows($resultado)>0){
            for($l=0;$l<pg_num_rows($resultado);$l++)
            {

                $row=pg_fetch_array($resultado,$l);
                $probabilidadB=floatval($row["probabilidad"]);

            }

        }
        else{ $probabilidadB=0;}
        pg_free_result($resultado);
        pg_close($conexion);
// A y B dos eventos que no son mutuamente excluyentes => p(A inter B)!=0 
        $probabilidadAdadoB=($probabilidadA*$probabilidadB)/$probabilidadB;

        if($probabilidadAdadoB==0){$probabilidadAdadoB=0;}
        if($probabilidadA!=0){
            $probabilidadBdadoA=floatval(($probabilidadB*$probabilidadAdadoB)/$probabilidadA);
        }
        else{$probabilidadBdadoA=0;}
        $calle_probabilidad[$h]=array(
            'id_calle'=>intval($key['id_calle']),
            'P(A)'=>$probabilidadA,
            'P(B)'=>$probabilidadB,
            'P(A/B)'=>$probabilidadAdadoB,
            'P(B/A)'=>$probabilidadBdadoA,
            );

        $h++;
    }

}
$resultado_total=null;
$probabilidadOcurrencia=$calle_probabilidad[0]['P(B/A)'];
if(count($calle_probabilidad)>0){
    foreach ($calle_probabilidad as $key) {
     if($probabilidadOcurrencia<$key['P(B/A)']){$probabilidadOcurrencia=$key['P(B/A)'];}
 }
}

if($probabilidadOcurrencia==null){$probabilidadOcurrencia=0;}

$id_calle_nombre=null;
//a partir de la cuadra obtengo el id de la calle y luego su nombre
$sql="SELECT id_calle from cuadra where id='$id_calle_B' LIMIT 1";
$conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...");
$resultado = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
if($resultado){
    for($l=0;$l<pg_num_rows($resultado);$l++)
    {

        $row=pg_fetch_array($resultado,$l);
        $id_calle_nombre=intval($row["id_calle"]);
    }

}

else{ $id_calle_nombre=null;}
pg_free_result($resultado);
pg_close($conexion);
$nombre_calle=null;
$sql="SELECT nombre from calle where id='$id_calle_nombre' LIMIT 1";
$conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...");
$resultado = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
if($resultado){
    for($l=0;$l<pg_num_rows($resultado);$l++)
    {

        $row=pg_fetch_array($resultado,$l);
        $nombre_calle=strval($row["nombre"]);

    }

}

else{ $id_calle_nombre=null;}
pg_free_result($resultado);
pg_close($conexion);

$resultado_total[0]=array(
    'Dia'=>$dia,
    'hora'=>$hora_actual,
    'Fecha'=>$dia_actual,
    'calle'=>$nombre_calle,
    'probabilidadOcurrencia'=>$probabilidadOcurrencia,
    );
return $resultado_total;
}// fin funcion calcularProbabilidad


}


