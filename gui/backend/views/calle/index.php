<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calle-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Calle', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Ver mapa de calles', ['mapa'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Seleccionar cuadra', ['seleccionarcuadra'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
