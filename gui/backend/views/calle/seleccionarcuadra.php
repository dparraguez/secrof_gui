<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>
<?php 

use miloschuman\highcharts\HighchartsAsset;
HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']); 
?>


<script>
var map;
var lineas = [];
var color = '#FF0000';
var color_seleccion = '#FF0000';
var color_no_seleccion = '#0000FF';
var seleccionada = null;
var infowindow = new google.maps.InfoWindow({
      content: "    "
  });

function initialize() {
	var latlng = new google.maps.LatLng(-36.817531,-73.049212);
  var mapOptions = {
    zoom: 13,
        center: latlng,
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);
 
}
function timeConverter(UNIX_timestamp){
  if(UNIX_timestamp==0)
    return "Sin datos";
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = "0" + a.getMonth();
  var date = "0" + a.getDate();
  var hour = "0" + a.getHours();
  var min = "0" + a.getMinutes();
  var sec = "0" + a.getSeconds();
  var time = date.substr(-2) + ' ' + month.substr(-2) + ' ' + year + ' ' + hour.substr(-2) + ':' + min.substr(-2) + ':' + sec.substr(-2) ;
  return time;
}

function agregar_linea(inicio, fin, id){
	var flightPlanCoordinates = [
		inicio,
		fin
	];

	

	if(id>0){
		color = '#0000FF';
	}
	var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		//geodesic: true,
		strokeColor: color,
		strokeOpacity: 1.0,
		strokeWeight: 2
	});



  flightPath.id_cuadra = id+"";

  google.maps.event.addListener(flightPath, 'click', function(event)
  {
    if(seleccionada!=null){
      seleccionada.setOptions({strokeColor: color_no_seleccion});
    }
    infowindow.setContent("<div id='grafxico'></div>");

    $.ajax({
      url: "index.php?r=zonaservicio/histogramacuadra&id="+this.id_cuadra,
      context: document.body,
      dataType: "json"
    }).done(function(data) {
      console.log("ajax");
      console.log(data);

      dias = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];

      for (i = 0; i < dias.length; i++) {  
          console.log(dias[i]);
          $('#'+dias[i]).highcharts({
            chart: {
                type: 'column',
            },
            title: {
                text: 'Hora de servicio dia '+dias[i]
            },
            xAxis: {
                reversed: false,
                title: {
                    enabled: true,
                    text: 'Frecuencia'
                },
                maxPadding: 0.05,
                showLastLabel: true,
                categories: data['columnas']
            },
            yAxis: {
                title: {
                    text: 'Hora'
                },
                lineWidth: 2
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                spline: {
                    marker: {
                        enable: false
                    }
                }
            },
            series: [{
                name: 'Hora',
                data: data[dias[i]]
            }]
          });
          
        }

      });

      

    
    infowindow.setPosition(event.latLng);
    //infowindow.open(map);
    $('#id_cuadra').val(this.id_cuadra);
    console.log($('#id_cuadra'));
    this.setOptions({strokeColor: color_seleccion});
    seleccionada = this;

  });

  function lineClick(line)
  {
      alert("Line clicked with myID=" + line.myID);
  }

	 flightPath.setMap(map);
}
window.onload = function() {
        initialize();
        var locations = [
			<?php $i= 0; ?>
			<?php foreach ($cuadras as $cuadra) { ?>
            <?php if($i!=0){ echo ','; } else {$i=1;} ?>
            [<?= $cuadra['latitud_inicio'].",".$cuadra['longitud_inicio'].",".$cuadra['latitud_fin'].",".$cuadra['longitud_fin'].",".$cuadra['id'] ?>]
          <?php } ?>

        ];
        console.log(locations);
        for (i = 0; i < locations.length; i++) {
          agregar_linea(new google.maps.LatLng(locations[i][0], locations[i][1]), new google.maps.LatLng(locations[i][2], locations[i][3]), locations[i][4]);
        }
      };

</script>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\ZonaServicio;
?>
<div class="rutas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

       
    </p>


</div>


<div id="map_canvas" style="height:400px" ></div>

<input type="texbox" id="id_cuadra">

<div id='Lunes'></div>
<div id='Martes'></div>
<div id='Miercoles'></div>
<div id='Jueves'></div>
<div id='Viernes'></div>
<div id='Sabado'></div>
<div id='Domingo'></div>
</table>

