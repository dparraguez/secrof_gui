<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Calle */

$this->title = 'Create Calle';
$this->params['breadcrumbs'][] = ['label' => 'Calles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calle-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
