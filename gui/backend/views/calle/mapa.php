<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $array_calles array() */

$this->title = 'Mapa calles';
$this->params['breadcrumbs'][] = ['label' => 'Calles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>

<script>
var map;
var lineas = [];

function initialize() {
    var latlng = new google.maps.LatLng(-36.817531,-73.049212);
  var mapOptions = {
    zoom: 13,
        center: latlng,
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);

}

function agregar_linea(inicio, fin, cumplimiento){
    var flightPlanCoordinates = [
        inicio,
        fin
    ];

    var color = '#FF0000';

    if(cumplimiento>0){
        color = '#0000FF';
    }
    var flightPath = new google.maps.Polyline({
        path: flightPlanCoordinates,
        //geodesic: true,
        strokeColor: color,
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

     flightPath.setMap(map);
}
window.onload = function() {
    initialize();

    <?php foreach ($array_calles as $calle) { ?>
        var locations = [
            <?php $i= 0; ?>
            <?php foreach ($calle as $value) { ?>
            <?php if($i!=0){ echo ','; } else {$i=1;} ?>


            [<?= $value['primer_punto'].",".$value['segundo_punto'].",2" ?>]

            <?php } ?>
        ];



        console.log(locations);

        for (i = 0; i < locations.length; i++) {  
          console.log(locations[i]);
          agregar_linea(new google.maps.LatLng(locations[i][0], locations[i][1]), new google.maps.LatLng(locations[i][2], locations[i][3]), locations[i][4]);

          
        }

    <?php } ?>

};


</script>

<div class="calle-mapa">

    <h1><?= Html::encode($this->title) ?></h1>

</div>

<div id="map_canvas" style="height:400px" ></div>