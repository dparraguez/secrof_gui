<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AppUsuario */

//$this->title = $model->us_id;
$this->title = 'Usuario creado exitosamente';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];

?>
<div class="app-usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>
<?php echo "<br>";?>
   
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'us_id',
            'us_nombres',
            'us_apellidos',
            'us_rut',
            'us_fono',
        ],
    
])?>
<?php echo "<br>";?>
 <div class="form-group">
<?= Html::a('Agregar Reclamo', ['/app-reclamo/create', 'id'=>$model->us_id], ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

<?= Html::a('Agregar Sugerencia', ['/app-sugerencia/create', 'id'=>$model->us_id], ['class' => $model->isNewRecord ? 'btn btn-danger' : 'btn btn-danger']) ?>
 </div>
</div>
