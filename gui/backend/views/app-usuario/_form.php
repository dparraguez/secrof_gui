<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppUsuario */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'method' => 'post',
 'id' => 'formulario',
 'enableClientValidation' => false,
 'enableAjaxValidation' => true,
]);
?>

<div class="app-usuario-form">
    <?php $form = ActiveForm::begin(); ?>

     <div class="form-group">
     <?= $form->field($model, 'us_nombres',[
        'template' => '{label} <div class="row"><div class="col-xs-4">{input}{error}</div></div>'
    ])->textInput(['placeholder' => 'Ej: Juana María']) ?>
    </div>
    <div class="form-group">
    <?= $form->field($model, 'us_apellidos',[
        'template' => '{label} <div class="row"><div class="col-xs-4">{input}{error}</div></div>'
    ])->textInput(['placeholder' => 'Ej: Castro Pérez']) ?>
     </div>
     <div class="form-group">
    <?= $form->field($model, 'us_rut',[
        'template' => '{label} <div class="row"><div class="col-xs-4">{input}{error}</div></div>'
    ])->textInput(['placeholder' => 'Ej: 11111111-1']) ?>
     </div>

     <div class="form-group">
    <?= $form->field($model, 'us_fono',[
        'template' => '{label} <div class="row"><div class="col-xs-4">{input}{error}</div></div>'
    ])->textInput(['placeholder' => 'Ej: 423111919']) ?>
    </div>
   


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
