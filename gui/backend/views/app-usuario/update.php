<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppUsuario */

$this->title = 'Update App Usuario: ' . ' ' . $model->us_id;
$this->params['breadcrumbs'][] = ['label' => 'App Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->us_id, 'url' => ['view', 'id' => $model->us_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="app-usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
