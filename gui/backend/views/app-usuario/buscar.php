<?php



use yii\helpers\Html;
use yii\grid\GridView;
use app\models\AppUsuario;
use app\models\AppReclamo;


/* @var $this yii\web\View */
/* @var $searchModel app\models\AppUsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios Aplicación Móvil';
$this->params['breadcrumbs'][] = $this->title;
//echo "<br>";
?>
<div class="app-usuario-buscar">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <?php  echo $this->render('_search', ['model' => $searchModel]); echo "<br>";?>

    
   

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
           ['class' => 'yii\grid\SerialColumn'],

            'us_nombres',
            'us_apellidos',
            'us_rut',
            //'us_id',
            
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Agregar',
                'template' => '{recorrido} {sugerencia}',
                'buttons' => [
                    //view button
                    'recorrido' => function ($url, $model) {
                        return Html::a('<span class="fa fa-search"></span>Reclamo', 
                                    [
                                        'app-reclamo/create',
                                        'id'=>$model->us_id
                                    ], 
                                    [
                                        //'target'=>'_blank',
                                        'title' => Yii::t('app', 'Agregar'),
                                        'class'=>'btn btn-primary btn-sm',                              
                                    ]
                        );
                    },
                //'template' => '{sugerencia}',
               // 'buttons' => [
                    //view button
                    'sugerencia' => function ($url, $model) {
                        return Html::a('<span class="fa fa-search"></span>Sugerencia', 
                                    [
                                        'app-sugerencia/create',
                                        'id'=>$model->us_id
                                    ], 
                                    [
                                        //'target'=>'_blank',
                                        'title' => Yii::t('app', 'Agregar'),
                                        'class'=>'btn btn-info btn-sm',                              
                                    ]
                        );
                    },
               // ],
              ],],],
            ]); ?>
</div>
