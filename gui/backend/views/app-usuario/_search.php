<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppUsuarioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-usuario-search">

    <?php $form = ActiveForm::begin([
        'action' => ['buscar'],
        'method' => 'get',


    ]); ?>
     <?= $form->field($model, 'us_rut',[
        'template' => '{label} <div class="row"><div class="col-xs-4">{input}{error}</div></div>'
    ])->textInput(['placeholder' => 'Ej: 11111111-1']) ?>


    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
