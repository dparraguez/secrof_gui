<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>
<script>
  var map;
  var messagediv;
  var routePoints=new Array(0);
  var routeMarkers=new Array(0);
  var routePath;
  var temp = new Array();
  var markers=[];
  var lines=[];
  var polygonPoints=[];
  var polygon=null;
  var lineColor="#ff0000";
  var lineWidth=1;
  var mode=0;
  var splitpoint1;
  var splitpoint2;
  var listner;
  function initialize() 
  {
    messagediv=document.getElementById('messagediv');
    messagediv.innerHTML='Loading ...';
    var latlng = new google.maps.LatLng(0,0);
    var myOptions = {zoom:2,center:latlng,mapTypeControlOptions:{style:google.maps.MapTypeControlStyle.DROPDOWN_MENU},draggableCursor:'crosshair'};
    map = new google.maps.Map(document.getElementById('map_canvas'),myOptions);
    listner=google.maps.event.addListener(map, 'click', clickmap);
    messagediv.innerHTML='Ready';
  }
  function clickmap(event)
  {
    if (mode==0)
    {
      routePoints.push(event.latLng);
      messagediv.innerHTML="Adding Point ("+(routePoints.length)+")...";
      CheckPointsForSplit(routePoints.length);
      var marker=placeMarker(event.latLng,routePoints.length);
      routeMarkers.push(marker);

//remove old polyline first
if (!(routePath==undefined))
{
  routePath.setMap(null);
}
routePath=getRoutePath();
routePath.setMap(map);

CheckPointsForSplit(routePoints.length);
}
}
function placeMarker(location,number) 
{
  var iconFile = 'http://www.daftlogic.com/images/gmmarkersv3/stripes.png'; 
  var marker = new google.maps.Marker({position:location,map:map,icon:iconFile,title:number.toString(),draggable:true});

  google.maps.event.addListener(marker, 'dragend', function(event)
  {
    routePoints[number-1]=event.latLng;
    //remove polyline
    routePath.setMap(null);
    //add new polyline
    routePath=getRoutePath();
    routePath.setMap(map);
  });

  google.maps.event.addListener(marker, 'click', function(event)
  {
    if (mode==2)
    {
      messagediv.innerHTML="Splitting"; 
      splitpoint2=event.latLng;
      SplitRoute(splitpoint1,splitpoint2);
      mode=0;

    }
    if (mode==1)
    {
      messagediv.innerHTML="Click on the second marker";
      mode=2;
      splitpoint1=event.latLng;
    }
  });

  return marker;
}
function getRoutePath()
{
  var routePath = new google.maps.Polyline({
    path: routePoints,
    strokeColor: lineColor,
    strokeOpacity: 1.0,
    strokeWeight: lineWidth,
    geodesic: true
  });
  return routePath;
}
function SplitRoute(pnt1,pnt2)
{
  var midpoint=geo.math.midpoint(pnt1,pnt2);
  var midpoint = new google.maps.LatLng(midpoint.lat(),midpoint.lng());
  var tmppoints=[];
  var insertpoint=getlowestbetween(getindexofpoint(pnt1),getindexofpoint(pnt2));

  for(var i=0;i<routePoints.length;++i)
  { 
    tmppoints.push(routePoints[i]);
    if (i==insertpoint)
    {
      tmppoints.push(midpoint);
    }
  }

  routePoints=tmppoints;
  redisply();
  messagediv.innerHTML="Split Complete";
}
function redisply()
{
  if (routeMarkers) 
  {
    for (i in routeMarkers) 
    {
      routeMarkers[i].setMap(null);
    }
  }
  routeMarkers=new Array(0);
//remove polyline
if (!(routePath==undefined))
{
  routePath.setMap(null);
}
routePath=getRoutePath();
routePath.setMap(map);

if (routePoints) 
{
  var count=1;
  for (i in routePoints) 
  {
    var marker=placeMarker(routePoints[i],count);
    routeMarkers.push(marker);
    count++;
  }
} 
}
function getlowestbetween(i1,i2)
{
  if (i1>i2)
  {
    return (i2);
  }
  else
  {
    return (i1);
  }
}
function getindexofpoint(point)
{
  var index;
  for(var i=0;i<routePoints.length;++i)
  {
    if ((routePoints[i].lat()==point.lat())&&(routePoints[i].lng()==point.lng()))
    {
      index=i;
    }
  }
  return (index);
}
function CheckPointsForSplit(nopoints)
{
  if (nopoints>1)
  {
    document.getElementById("btn_split").disabled=false;
  }
  else
  {
    document.getElementById("btn_split").disabled=true;
  }
}
function MakeSplit()
{
  messagediv.innerHTML="Click on the first marker";
  mode=1;
}
function ClearMap()
{
  if (routeMarkers) 
  {
    for (i in routeMarkers) 
    {
      routeMarkers[i].setMap(null);
    }
  }
  //remove polyline
  if (!(routePath==undefined))
  {
    routePath.setMap(null);
  }

  routePath=null;
  routePoints=new Array(0);
  routeMarkers=new Array(0);

  CheckPointsForSplit(routePoints.length);
  mode=0;
  messagediv.innerHTML="Map Cleared";
}
function ShoowPoints(){
  if (routeMarkers) 
  {
    for (i in routeMarkers) 
    {
      console.log(routeMarkers[i].position.toString());
    }
  }
  
}
google.maps.event.addDomListener(window, "load", initialize);
    </script>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rutas */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
	
	<div class="col-md-12">

		<div class="rutas-form" >

		    <?php $form = ActiveForm::begin(); ?>

		    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

		    <?= $form->field($model, 'observacion')->textInput(['maxlength' => true]) ?>

		    <div class="form-group">
		        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>

		</div>
	</div>

</div>

<div id="map_canvas" style="height:200px" ></div>
<div align="center" id="messagediv" class="mapdivstyle">ddddddddddd</div>
<form method="get" action="javascript:void(0);">
<input type="button" id="btn_clear" value="Clear Map" onclick="ClearMap();">
<input type="button" id="btn_show" value="Shoow Points" onclick="ShoowPoints();">
<input type="button" id="btn_split" value="Make a Split" onclick="MakeSplit();" disabled="">
</form>