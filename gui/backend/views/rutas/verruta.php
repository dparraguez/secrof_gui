<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>

<script>
var map;
var lineas = [];

function initialize() {
	var latlng = new google.maps.LatLng(-36.817531,-73.049212);
  var mapOptions = {
    zoom: 13,
        center: latlng,
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);

  

 
}

function agregar_linea(inicio, fin, cumplimiento){
	var flightPlanCoordinates = [
		inicio,
		fin
	];

	var color = '#FF0000';

	if(cumplimiento>0){
		color = '#0000FF';
	}
	var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		//geodesic: true,
		strokeColor: color,
		strokeOpacity: 1.0,
		strokeWeight: 2
	});

	 flightPath.setMap(map);
}
window.onload = function() {
        initialize();

        var locations = [
			<?php $i= 0; ?>
			<?php foreach ($lineas as $value) { ?>
            <?php if($i!=0){ echo ','; } else {$i=1;} ?>


            [<?= $value['primer_punto'].",".$value['segundo_punto'].",2" ?>]
          <?php } ?>

        ];

        console.log(locations);

        for (i = 0; i < locations.length; i++) {  
          console.log(locations[i]);
          agregar_linea(new google.maps.LatLng(locations[i][0], locations[i][1]), new google.maps.LatLng(locations[i][2], locations[i][3]), locations[i][4]);

          
        }

        

      };


</script>
<?php

use yii\helpers\Html;
?>
<div class="rutas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Cumplimiento', ['cumplimiento', 'id' => 3], ['class' => 'btn btn-primary']) ?>

       
    </p>


</div>


<div id="map_canvas" style="height:400px" ></div>
