

<?php use miloschuman\highcharts\Highcharts; ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<?php echo Highcharts::widget([
   'options' => [
      'title' => ['text' => 'Grafico de consumo de combustible Semanal'],
      'xAxis' => [
         'categories' =>  [  $consumo_diario[0]['dia'] , $consumo_diario[1]['dia'] , $consumo_diario[2]['dia'] , $consumo_diario[3]['dia'] , $consumo_diario[4]['dia'] ,$consumo_diario[5]['dia'] ,$consumo_diario[6]['dia']]
      ],
      'yAxis' => [
         'title' => ['text' => 'Consumo Total de combustible en litros']
      ],
      'series' => [
         ['name' => 'Zona 5', 'data' => [$consumo_diario[0]['litros'] , $consumo_diario[1]['litros'] , $consumo_diario[2]['litros'] ,$consumo_diario[3]['litros'] ,$consumo_diario[4]['litros'] , $consumo_diario[5]['litros'] ,$consumo_diario[6]['litros']]]
        
      ]
   ]
]); ?>



</body>
</html>






