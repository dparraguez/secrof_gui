<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

use dosamigos\datepicker\DatePicker;
?>
<h1>Calculo de consumo</h1>

<?php  $form=ActiveForm::begin([
"method"=>"post",
"id"=>"formulario",
"enableClientValidation"=> false,
"enableAjaxValidation"=>true,

	]); ?>



<?= $form->field($model, 'fecha_inicio')->widget(
    DatePicker::className(), [
        // inline too, not bad
        // 'inline' => true, 
             'language' => 'es',
         // modify template for custom rendering
       
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
]);?>


<?= $form->field($model, 'fecha_termino')->widget(
    DatePicker::className(), [
        // inline too, not bad
        // 'inline' => true, 
             'language' => 'es',
         // modify template for custom rendering
       
        'clientOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd'
        ]
]);?>










	<div class="form-group">
<?= $form->field($model,"valorcombustible")->input("text"); ?>
	</div>


	<?= Html::submitButton("Enviar",["class"=>"btn btn-prymary"]) ;?>

<?php $form->end() ?>