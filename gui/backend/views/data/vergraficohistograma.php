<?php
use miloschuman\highcharts\HighchartsAsset;

HighchartsAsset::register($this)->withScripts(['highstock', 'modules/exporting', 'modules/drilldown']); 

use yii\bootstrap\Tabs;
echo Tabs::widget([
    'items' => [
        [
            'label' => 'Total',
            'content' => '<div id=\'Total\'></div>',
            'active' => true
        ],
        [
            'label' => 'Lunes',
            'content' => '<div id=\'Lunes\'></div>',
        ],
        [
            'label' => 'Martes',
            'content' => '<div width="100%" id=\'Martes\'></div>',
            //'headerOptions' => [...],
        ],
        [
            'label' => 'Miércoles',
            'content' => '<div id=\'Miercoles\'></div>',
            //'headerOptions' => [...],
        ],
        [
            'label' => 'Jueves',
            'content' => '<div id=\'Jueves\'></div>',
            //'headerOptions' => [...],
        ],
        [
            'label' => 'Viernes',
            'content' => '<div id=\'Viernes\'></div>',
            //'headerOptions' => [...],
        ],
        [
            'label' => 'Sábado',
            'content' => '<div id=\'Sabado\'></div>',
            //'headerOptions' => [...],
        ],
        [
            'label' => 'Domingo',
            'content' => '<div id=\'Domingo\'></div>',
            //'headerOptions' => [...],
        ],


        //Ejemplo de tab con dropdownlist en selector
        /*[
            'label' => 'Dropdown',
            'items' => [
                 [
                     'label' => 'DropdownA',
                     'content' => 'DropdownA, Anim pariatur cliche...',
                 ],
                 [
                     'label' => 'DropdownB',
                     'content' => 'DropdownB, Anim pariatur cliche...',
                 ],
            ],
        ],*/
    ],
]);




?>
<script type="text/javascript">
data = [];
<?php 
    foreach ($datos as $key_dato => $dato) { 
        echo "data['$key_dato'] = []; ";
        foreach ($dato as $key => $value) {
            if($key_dato=="columnas")
                echo "data['$key_dato'][$key] = '$value'; ";
            else{
                echo "data['$key_dato'][$key] = $value; ";
            }
        }
        
    }

?>
window.onload = function() {
    dias = ['Total','Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
    $('#id_cuadra');
    console.log(data);

    for (i = 0; i < dias.length; i++) { 

        console.log(data[dias[i]]); 


        $('#'+dias[i]).highcharts({
            chart: {
                type: 'column',
                width: $('#Total').width()
            },
            title: {
                text: 'Hora de servicio día '+dias[i]
            },
            xAxis: {
                reversed: false,
                title: {
                    enabled: true,
                    text: 'Hora'
                },
                maxPadding: 0.05,
                showLastLabel: true,
                categories: data['columnas']
            },
            yAxis: {
                title: {
                    text: 'Frecuencia'
                },
                lineWidth: 2
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                spline: {
                    marker: {
                        enable: false
                    }
                }
            },
            series: [{
                name: 'Hora',
                data: data[dias[i]]
            }]
        });

    }

}


</script>