<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>


<script>
var map;
var lineas = [];
var color = '#FF0000';
var color_seleccion = '#FF0000';
var color_no_seleccion = '#0000FF';
var seleccionada = null;
var infowindow = new google.maps.InfoWindow({
      content: "    "
  });

function initialize() {
	var latlng = new google.maps.LatLng(-36.817531,-73.049212);
  var mapOptions = {
    zoom: 13,
        center: latlng,
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);
 
}
function timeConverter(UNIX_timestamp){
  if(UNIX_timestamp==0)
    return "Sin datos";
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = "0" + a.getMonth();
  var date = "0" + a.getDate();
  var hour = "0" + a.getHours();
  var min = "0" + a.getMinutes();
  var sec = "0" + a.getSeconds();
  var time = date.substr(-2) + ' ' + month.substr(-2) + ' ' + year + ' ' + hour.substr(-2) + ':' + min.substr(-2) + ':' + sec.substr(-2) ;
  return time;
}

function agregar_linea(inicio, fin, id){
	var flightPlanCoordinates = [
		inicio,
		fin
	];

	

	if(id>0){
		color = '#0000FF';
	}
	var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		//geodesic: true,
		strokeColor: color,
		strokeOpacity: 1.0,
		strokeWeight: 5
	});



  flightPath.id_cuadra = id+"";

  google.maps.event.addListener(flightPath, 'click', function(event)
  {
    if(seleccionada!=null){
      seleccionada.setOptions({strokeColor: color_no_seleccion});
    }
    infowindow.setContent("<div id='grafxico'></div>");
    
    infowindow.setPosition(event.latLng);
    //infowindow.open(map);
    $('#formularioreporte-id_cuadra').val(this.id_cuadra);
    console.log($('#id_cuadra'));
    this.setOptions({strokeColor: color_seleccion});
    seleccionada = this;

  });

  function lineClick(line)
  {
      alert("Line clicked with myID=" + line.myID);
  }

	 flightPath.setMap(map);
}
window.onload = function() {
        initialize();
        var locations = [
			<?php $i= 0; ?>
			<?php foreach ($cuadras as $cuadra) { ?>
            <?php if($i!=0){ echo ','; } else {$i=1;} ?>
            [<?= $cuadra['latitud_inicio'].",".$cuadra['longitud_inicio'].",".$cuadra['latitud_fin'].",".$cuadra['longitud_fin'].",".$cuadra['id'] ?>]
          <?php } ?>

        ];
        console.log(locations);
        for (i = 0; i < locations.length; i++) {
          agregar_linea(new google.maps.LatLng(locations[i][0], locations[i][1]), new google.maps.LatLng(locations[i][2], locations[i][3]), locations[i][4]);
        }
      };

</script>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\ZonaServicio;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use dosamigos\datetimepicker\DateTimePicker;
?>
<div class="rutas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

       
    </p>


</div>
<?php if(!empty($resultado)){ ?>
<div class="alert alert-info" role="alert"><b>Resultado:</b>
        <?php echo $resultado; ?>.</div>

<?php } ?>


<?php /* if(!empty($resultado_total)){ ?>
<div class="alert alert-info" role="alert"><b>Resultado:</b>
        <?php var_dump($resultado_total) ?>.</div>

<?php } */?>



<div class="alert alert-info" role="alert"><b>Seleccionar cuadra:</b>
         Para seleccionar una cuadra presione sobre una línea azul, la cuadra seleccionada cambia a color rojo.</div>
<div id="map_canvas" style="height:400px" ></div>

<input type="hidden" id="id_cuadra">

<div class="calle-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'hora_inicio')->widget(DateTimePicker::className(), [
        'language' => 'es',
        'size' => 'ms',
        'template' => '{input}',
        'clientOptions' => [
            'startView' => 1,
            'minView' => 0,
            'maxView' => 1,
            'autoclose' => true,
            //'linkFormat' => 'HH:ii P', // if inline = true
             'format' => 'HH:ii P', // if inline = false
            'todayBtn' => true
        ]
    ]);?>

    <?= $form->field($model, 'id_cuadra')->textInput(['maxlength' => true, 'readonly'=>true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Ver probabilidad', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


