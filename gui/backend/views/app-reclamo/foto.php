<?php
//vista que muestra la foto que contiene un determinado reclamo
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\AppReclamo;
use app\models\AppUsuario;

/* @var $this yii\web\View */
/* @var $model app\models\AppReclamo */

$this->title = $model->re_foto;
$this->params['breadcrumbs'][] = ['label' => 'Reclamos', 'url' => ['index']];
$this->title = 'Foto reclamo';
?>
<div class="app-reclamo-foto">


    <h1><?= Html::encode($this->title) ?></h1>

<footer class="footer" style="height:500px; background-color:rgba(255,255,255,0.8); border-top:0px">
        <div class="container">

        <p class="pull-left"><?=  Html::img(Yii::getAlias($model->re_foto), ['class' => 'thing']);
        ?></p>

        </div>
    </footer>
</div