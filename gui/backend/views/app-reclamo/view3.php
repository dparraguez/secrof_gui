<?php //vista para generar en pdf la información que muestran los graficos?>
<!DOCTYPE html>
<html>
<body>
<h2><center>Informe reclamos</center></h2>
<br></br>
<table align="center" width="300" cellspacing="10" cellpadding="10" border="1" >
  <?php $i=1?>
  <tr>  
    <td align="center"><h4>Tipo reclamo</h4></td>
    <td align="center"><h4>Cantidad</h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Vehículo Abandonado</h4></td>
    <td  align="right"><h4><?=$i=$vehiculoabandonado; ?></h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Vehículo Filtración</h4></td>
    <td align="right"><h4><?=$i=$vehiculofiltracion; ?></h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Vehículos otros</h4></td>
    <td align="right"><h4><?=$i=$vehiculosotros; ?></h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Escombros</h4></td>
    <td align="right"><h4><?=$i=$escombros; ?></h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Microbasural</h4></td>
    <td align="right"><h4><?=$i=$microbasural; ?><h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Trabajadores</h4></td>
    <td align="right"><h4><?=$i=$trabajadores; ?></h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Calles</h4></td>
    <td align="right"><h4><?=$i=$calles; ?><h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Residuos Domiciliarios</h4></td>
    <td align="right"><h4><?=$i=$residuos; ?></h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Sumideros agua lluvia</h4></td>
    <td align="right"><h4><?=$i=$sumideros; ?></h4></td>
  </tr>
   <tr>  
    <td ><h4>Total Reclamos</h4></td>
    <td align="right"><h4><?=$i=$reclamo; ?></h4></td>
  </tr>
</table>

<h2><center>Informe felicitación/sugerencia</h2></center>
<br></br>
<table align="center" width="300" cellspacing="10" cellpadding="10" border="1" >
  <?php $i=1?>
  <tr>  
    <td align="center"><h4>Tipo</h4></td>
    <td align="center"><h4>Cantidad</h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Felicitaciones</h4></td>
    <td  align="right"><h4><?=$i=$felicitacion; ?></h4></td>
  </tr>
  <tr>  
    <td bgcolor="#DEDEDE"><h4>Sugerencias</h4></td>
    <td align="right"><h4><?=$i=$sugerencia; ?></h4></td>
  </tr>
  <tr>  
    <td ><h4>Total</h4></td>
    <td align="right"><h4><?=$i=$sugerencia+$felicitacion; ?></h4></td>
  </tr>
</table>

</body>
</html>

