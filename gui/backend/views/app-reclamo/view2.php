<?php
//vista cuando del detalle de un reclamo cuando se a pinchado 'ver detalle' en el mapa

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\ReclamoEstado;
use app\models\AppReclamo;

/* @var $this yii\web\View */
/* @var $model app\models\AppReclamo */
/* @var $model2 app\models\ReclamoEstado */

//$this->title = $model->re_id;
$this->params['breadcrumbs'][] = ['label' => 'Reclamos', 'url' => ['index']];
$this->title = 'Reclamo ';
?>
<div class="app-reclamo-view2">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo "<br>";?>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            're_id',
            're_descripcion',
            //'re_fecha_app',
            //'re_hora_app',
            //'re_foto',
            //'re_latitud',
            //'re_longitud',
            're_prioridad',
            //'us_id',
            //'ti_id', 
            [
            'attribute' => 'Tipo Reclamo',
            'value' => $model->ti->ti_nombre,
        ],
            [
            'attribute' => 'Usuario',
            'value' => $model->us->us_nombres.' '.$model->us->us_apellidos,
        ],
        [
            'attribute'=>'Foto',
            'value'=>$model->re_foto,
            'format' => ['image',['width'=>'500','height'=>'500']],
        ],
        ],
    ]) ?>


     


</div>


