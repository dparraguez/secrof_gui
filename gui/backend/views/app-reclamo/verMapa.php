<?php
use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>


<script type="text/javascript">
var map;
// This example displays a marker at the center of Australia.
// When the user clicks the marker, an info window opens.

function initialize() {
  var myLatlng = new google.maps.LatLng(-36.817531,-73.049212);
  var mapOptions = {
    zoom: 14,
    center: myLatlng
  };

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

}



google.maps.event.addDomListener(window, 'load', initialize);

window.onload = function() {

  <?php foreach ($models as $model) { 

    $detailview = DetailView::widget([
              "model" => $model,
              "attributes" => [
                  "re_id",
                  "re_tipo",
                  "re_descripcion",
                  "re_fecha_app",
                  "re_hora_app",
                  "re_latitud",
                  "re_longitud",
                  "re_estado",
                  "re_fecha_hora_web",
                  "us_id",
              ],
          ]);

    $detailview = str_replace("\n", "", $detailview);

    ?>
    var contentString<?=$model->re_id?> = '<div id="content" style="width:400px">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<h1 id="firstHeading" class="firstHeading">Reclamo</h1>'+
          '<div id="bodyContent">'+
          '<p><b>Contenido</b></p>'+
          '<?= $detailview ?>'+
          '<?= Html::a("Ver reclamo", ["view2", "id"=>$model->re_id], ["class" => "btn btn-success"]) ?>'+
          //'<p><img src="images/indice.png" alt="..."></p>'+
          '</div>'+
          '</div>';

    var infowindow<?=$model->re_id?> = new google.maps.InfoWindow({
        content: contentString<?=$model->re_id?>
    });

    var myLatlng = new google.maps.LatLng(<?=$model->re_latitud?>,<?=$model->re_longitud?>);

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Uluru (Ayers Rock)'
    });
    google.maps.event.addListener(marker, 'click', function(event) {
      infowindow<?=$model->re_id?>.open(map,marker);
      infowindow<?=$model->re_id?>.position = event.latLng;
      console.log(event);
    });

  <?php } ?>
}
</script>

<div id="map-canvas" style="height:500px" ></div>