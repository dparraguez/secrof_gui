<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\AppReclamo;
use app\models\AppUsuario;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AppReclamoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reclamos';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="app-reclamo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            're_descripcion',
             're_prioridad',
             [
            'attribute' => 'Tipo Reclamo',
            'value' => 'ti.ti_nombre',
            ],
               [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Foto',
                'template' => '{fotos}',
                'buttons' => [
                    //view button
                    'fotos' => function ($url, $model) {
                        if($model->re_foto != null){
                        return Html::a('<span class="fa fa-search"></span>Ver Foto', 
                                    [
                                        'app-reclamo/foto',
                                        'id'=>$model->re_id
                                    ], 
                                    [
                                        'title' => Yii::t('app', 'Foto'),
                                        'class'=>'btn btn-primary btn-sm',                              
                                    ]
                        );
                    }
                    },


        ],
        ],
         [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Detalles',
                'template' => '{detalle}',
                'buttons' => [
                    //view button
                    'detalle' => function ($url, $model) {
                        return Html::a('<span class="fa fa-search"></span>Ver Detalle', 
                                    [
                                        'app-reclamo/view2',
                                        'id'=>$model->re_id
                                    ], 
                                    [
                                       // 'target'=>'_blank',
                                        'title' => Yii::t('app', 'Detalle'),
                                        'class'=>'btn btn-primary btn-sm',                              
                                    ]
                        );
                    
                    },

           // ['class' => 'yii\grid\ActionColumn'],
        ],
        ],
        [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Cambiar Estado',
                'template' => '{fotos}',
                'buttons' => [
                    //view button
                    'fotos' => function ($url, $model) {
                        $estado = count($model->reclamoEstados);
                        if($model->re_prioridad== 'Derivar'){
                                return Html::a('<span class="fa fa-search"></span>Derivar', 
                                    [
                                        'app-reclamo/estado',
                                        'id'=>$model->re_id,
                                        'estado'=>'Derivado'
                                    ], 
                                    [
                                       // 'target'=>'_blank',
                                        'title' => Yii::t('app', 'Derivar'),
                                        'class'=>'btn btn-primary btn-sm',                              
                                    ]
                                ); 
                        }
                        else{
                            if($model->re_estadoactual== 'Recepcionado'){
                               return Html::a('<span class="fa fa-search"></span>Leído', 
                                    [
                                        'app-reclamo/estado',
                                        'id'=>$model->re_id,
                                        'estado'=>'Leído'
                                    ], 
                                    [
                                       // 'target'=>'_blank',
                                        'title' => Yii::t('app', 'Leído'),
                                        'class'=>'btn btn-primary btn-sm',                              
                                    ]
                                ); 
                            }
                            elseif($model->re_estadoactual == 'Leído'){
                                return Html::a('<span class="fa fa-search"></span>Cerrar', 
                                    [
                                        'app-reclamo/estado',
                                        'id'=>$model->re_id,
                                        'estado'=>'Cerrado'
                                    ], 
                                    [
                                       // 'target'=>'_blank',
                                        'title' => Yii::t('app', 'Cerrar'),
                                        'class'=>'btn btn-primary btn-sm',                              
                                    ]
                                ); 
                            }
                        }
                        
                    
                    },


        ],
        ],
              
        ],
    ]); ?>

</div>


