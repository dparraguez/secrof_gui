<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

$icono_reclamo = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%e2%80%A2|00A600|000000";
$icono_sugerencia = "http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=|0A5BC4|000000";

?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>


<script type="text/javascript">
var map;
// This example displays a marker at the center of Australia.
// When the user clicks the marker, an info window opens.

function initialize() {
  var myLatlng = new google.maps.LatLng(-36.817531,-73.049212);
  var mapOptions = {
    zoom: 14,
    center: myLatlng
  };

  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

}

google.maps.event.addDomListener(window, 'load', initialize);

window.onload = function() {

  <?php foreach ($reclamosVisibles as $reclamo) { 

    $detailview = DetailView::widget([
        "model" => $reclamo,
        "attributes" => [
            [                      // the owner name of the model
                'label' => 'Tipo',
                'value' => $reclamo->ti->ti_nombre,
            ],
            "re_descripcion",
            [                      // the owner name of the model
                'label' => 'Fecha',
                'value' => $reclamo->reclamoEstados[0]->re_es_fecha,
            ],
        ],
    ]);

    $detailview = str_replace("\n", "", $detailview);

    ?>

    var contentString = '<div id="content" style="width:400px">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h3 id="firstHeading" class="firstHeading">Reclamo</h3>'+
      '<div id="bodyContent">'+
      '<?= $detailview ?>'+
      '<?= Html::a("Ver reclamo", ["app-reclamo/view2", "id"=>$reclamo->re_id], ["class" => "btn btn-success"]) ?>'+
      //'<p><img src="images/indice.png" alt="..."></p>'+
      '</div>'+
      '</div>';

    var infowindow_reclamo_<?=$reclamo->re_id?> = new google.maps.InfoWindow({
        content: contentString
    });

    var myLatlng = new google.maps.LatLng(<?=$reclamo->re_latitud?>,<?=$reclamo->re_longitud?>);

    var marker_reclamo_<?=$reclamo->re_id?> = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: 'Reclamo',
        icon: '<?=$icono_reclamo?>'
    });

    google.maps.event.addListener(marker_reclamo_<?=$reclamo->re_id?>, 'click', function(event) {
      infowindow_reclamo_<?=$reclamo->re_id?>.open(map,marker_reclamo_<?=$reclamo->re_id?>);
      console.log(event);
    });

  <?php } ?>

  <?php foreach ($sugerenciasVisibles as $sugerencia) { 

    $detailview = DetailView::widget([
        "model" => $sugerencia,
        "attributes" => [
            "su_descripcion",
            [                      // the owner name of the model
                'label' => 'Fecha App',
                'value' => $sugerencia->su_fecha_app." ".$sugerencia->su_hora_app,
            ],
            [                      // the owner name of the model
                'label' => 'Fecha Web',
                'value' =>$sugerencia->su_fecha_hora_web,
            ],
        ],
    ]);

    $detailview = str_replace("\n", "", $detailview);

    ?>

    var contentString = '<div id="content" style="width:400px">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h3 id="firstHeading" class="firstHeading"><?=$sugerencia->su_tipo ?></h3>'+
      '<div id="bodyContent">'+
      '<?= $detailview ?>'+
      '<?= Html::a("Ver ".$sugerencia->su_tipo, ["app-sugerencia/view2", "id"=>$sugerencia->su_id], ["class" => "btn btn-success"]) ?>'+
      //'<p><img src="images/indice.png" alt="..."></p>'+
      '</div>'+
      '</div>';

    var infowindow_sugerencia_<?=$sugerencia->su_id?> = new google.maps.InfoWindow({
        content: contentString
    });

    var myLatlng = new google.maps.LatLng(<?=$sugerencia->su_latitud?>,<?=$sugerencia->su_longitud?>);

    var marker_sugerencia_<?=$sugerencia->su_id?> = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: '<?=$sugerencia->su_tipo ?>',
        icon: '<?=$icono_sugerencia?>'
    });
    google.maps.event.addListener(marker_sugerencia_<?=$sugerencia->su_id?>, 'click', function(event) {
      infowindow_sugerencia_<?=$sugerencia->su_id?>.open(map,marker_sugerencia_<?=$sugerencia->su_id?>);
      console.log(event);
    });

  <?php } ?>
}
</script>

<div id="map-canvas" style="height:500px" ></div>



<div class="row">
  <div class="col-md-1">
    Icono
  </div>
  <div class="col-md-2">
    Tipo
  </div>
</div>
<div class="row">
  <div class="col-md-1">
    <?=  Html::img($icono_reclamo, ['alt'=>"Reclamo", 'height'=>"50px"]) ?>
  </div>
  <div class="col-md-2">
    Reclamo
  </div>
</div>
<div class="row">
  <div class="col-md-1">
    <?=  Html::img($icono_sugerencia, ['alt'=>"Sugerencia", 'height'=>"50px"]) ?>
  </div>
  <div class="col-md-2">
    Sugerencia
  </div>
</div>
