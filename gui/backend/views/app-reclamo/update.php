<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppReclamo */

$this->title = 'Actualizar Reclamo: ' . ' ' . $model->re_id;
$this->params['breadcrumbs'][] = ['label' => 'Actualizar Reclamo', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->re_id, 'url' => ['view', 'id' => $model->re_id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="app-reclamo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form2', [
        'model' => $model,
    ]) ?>

</div>
