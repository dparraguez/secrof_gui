<?php use miloschuman\highcharts\Highcharts; 

echo Highcharts::widget([
    'options' => [
        'title' => ['text' => 'Gráfico por tipo de Reclamo'],
         
        'plotOptions' => [
            'pie' => [
                'cursor' => 'pointer',
            ],
        ],
        'series' => [
            [ // new opening bracket
                'type' => 'pie',
                'name' => 'Elements',
                'data' => [
                    ['Vehículo abandonado',(int)$vehiculoabandonado],
                    ['Vehículo filtración',(int)$vehiculofiltracion],
                    ['Escombros',(int)$escombros],
                    ['Microbasurales',(int)$microbasural],
                    ['Trabajadores',(int)$trabajadores],
                    ['Calles',(int)$calles],
                    ['Residuos Domiciliarios',(int)$residuos],
                    ['Sumideros de agua lluvia',(int)$sumideros],
                    ['Vehículos otros',(int)$vehiculosotros],
                ],
            ] // new closing bracket
        ],
    ],
]);
?>
<!DOCTYPE html>
<br></br><br></br>
</html>
<?php

echo Highcharts::widget([  
  'options' => [        
        'chart' => [            
            'type' => 'column',            
            'margin' => 80,            
            'options3d' => [                
            'enabled' => true,               
            'alpha' => 10,                
            'beta' => 10,                
            'depth' => 10,                
            'viewDistance' => 10           
            ],        
        ],        
        'title' => ['text' => 'Fiscalizaciones'],  
            'subtitle'=>['text'=>'3D donut in Highcharts'],
            'subtitle'=>['number'=> '(int)$total'],
            
            'plotOptions' => [
                'column' => [
                    'depth' => 3]],        
                'xAxis' => [            
                   // 'categories' => 'Highcharts.getOptions().lang.shortMonths'],
                 'categories' => ['Fiscalizaciones'],
                 'ceiling'=>2,
                 ],
                    'yAxis' => [            
                            'title' => ['text'=>'Cantidad Fiscalizaciones']], 
                            'series' => [ 
                                [
                                'name' => 'Reclamos', 
                                'data' =>[
                                ['Cantidad',(int)$reclamo],],],
                                 ['name' => 'Sugerencias', 
                                'data' =>[
                                ['Cantidad',(int)$sugerencia],],],
                                ['name' => 'Felicitaciones', 
                                'data' =>[
                                ['Cantidad',(int)$felicitacion],],]
                                ],
    
                                ]]

                    );?>
<!DOCTYPE html>
<div class="row">
<br>
<br>

<center><h2> Informe Reclamo</h2></center>




<center><table border="1">

    <tr bgcolor="#DEDEDE" >
<th bgcolor="#DEDEDE">Total Reclamos</th>
     <th bgcolor="#DEDEDE">Vehículo Abandonado</th>
     <th bgcolor="#DEDEDE">Vehículo Filtración</th>
        <th bgcolor="#DEDEDE">Escombros</th>
        <th bgcolor="#DEDEDE">Microbasural</th>
        <th bgcolor="#DEDEDE">Trabajadores</th>
        <th bgcolor="#DEDEDE">Calles</th>
        <th bgcolor="#DEDEDE">Residuos Domiciliarios</th>
        <th bgcolor="#DEDEDE">Sumideros de agua lluvia</th>
        <th bgcolor="#DEDEDE">Vehículos otros</th>
              
    </tr></center>

  
<?php $i=1?>
   <td><?=$i=$reclamo; ?></td>
<td><?=$i=$vehiculoabandonado; ?></td>
<td><?=$i=$vehiculofiltracion; ?></td>
<td><?=$i=$escombros; ?></td>
<td><?=$i=$microbasural; ?></td>
<td><?=$i=$trabajadores; ?></td>
<td><?=$i=$calles; ?></td>
<td><?=$i=$residuos; ?></td>
<td><?=$i=$sumideros; ?></td>
<td><?=$i=$vehiculosotros; ?></td>


<br></br>


<table class="table">
<tr>
<th> </th>
<th> </th>
<th> </th>
<th> </th>
<th> </th>                           
</tr>
</tr>
</table>
 
<br></br>
<center><h2> Informe Felicitación/Sugerencia</h2></center>
 <br></br>



<center><table border="1">

    <tr bgcolor="#DEDEDE" >
   <center> <th bgcolor="#DEDEDE">Felicitaciones</th></center> 
    <center>  <th bgcolor="#DEDEDE">Sugerencias</th></center> 

      
    </tr></center>   
<?php $i=1?>
   <td><?=$i=$felicitacion; ?></td>
<td><?=$i=$sugerencia; ?></td>

</table>

</div>

</body>
<br></br><br></br>
</html>





