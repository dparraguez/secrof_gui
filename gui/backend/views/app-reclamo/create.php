<?php

use yii\helpers\Html;



/* @var $this yii\web\View */
/* @var $model app\models\AppReclamoSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $model app\models\AppUsuarioSearch*/


$this->title = 'Crear reclamo';
$this->params['breadcrumbs'][] = ['label' => 'Crear Reclamo'];
//a$this->params['breadcrumbs'][] = $this->title;
?>



<div class="app-reclamo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
