<?php
//Vista cuando un  reclamo se a creado exitosamente en la web

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AppReclamo */

$this->title = $model->re_id;
$this->params['breadcrumbs'][] = ['label' => 'Buscar usuario', 'url' => ['/app-usuario/buscar']];
$this->title = 'Reclamo creado exitosamente';
?>
<div class="app-reclamo-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo "<br>";?>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            're_id',
            're_descripcion',
            //'re_fecha_app',
            //'re_hora_app',
            //'re_foto',
            //'re_latitud',
            //'re_longitud',
            're_prioridad',
              [
            'attribute' => 'Tipo Reclamo',
            'value' => $model->ti->ti_nombre,
            ],
            [
            'attribute' => 'Usuario',
            'value' => $model->us->us_nombres.' '.$model->us->us_apellidos,
        ],
        
        ],
        
    ]) ?>
     <p>
        <?= Html::a('Actualizar', ['update', 'id' => $model->re_id], ['class' => 'btn btn-primary']) ?>
        
    </p>



</div>


