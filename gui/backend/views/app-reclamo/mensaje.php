<?php
//vista cuando no hay fiscalizaciones que visualizar en el mapa general o el de reclamos
use yii\helpers\Html;
use app\models\AppUsuario;


/* @var $this yii\web\View */
/* @var $model app\models\AppSugerencia */
echo "<br>";echo "<br>";
$this->title = 'No se han recibido nuevas Fiscalizaciones ';
?>
<div class="app-reclamo-mensaje">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo "<br>";?>
 </div>