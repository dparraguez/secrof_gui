<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\ValidarFormulario;
use app\models\ValidarFormularioAjax;

use dosamigos\datepicker\DatePicker;
?>
<h1>PDF</h1>

<?php 
 $model = new ValidarFormularioAjax;
    if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
    }

    if ($model->load(Yii::$app->request->post()))
    {
        if ($model->validate())
        {
        $connection = \Yii::$app->db_movil;
        $reclamo=$connection->createCommand("SELECT Count(*) from app_reclamo")->queryScalar();
        $result = pg_query ($conexion,$reclamo) or die("Error en la consulta SQL");
        $registros= pg_num_rows($result);

if($result!=false){

    for($l=0;$l<pg_num_rows($result);$l++){

            $row=pg_fetch_array($result,$l);
            $data[$l]=array( 
  'Reclamos'=> $row['reclamo'],
  'Vehículo Abandonado'=> $row['vehiculoabandonado'],
  'Vehículo Filtracion' =>$row['vehiculofiltracion'],
  'Escombros' =>$row['escombros'],
  'Microbasural' =>$row['microbasural'],
  'Trabajadores'=> $row['trabajadores'],
  'Calles' =>$row['calles'],
  'Residuos Domiciliarios'=>$row['residuos'],
  'Sumideros Agua lluvia'=>$row['sumideros'],
  'Vehículos Otros'=>$row['vehiculosotros'],
                );
        }


    }
    else{ $datos=null;}
    pg_free_result($result);
    pg_close($conexion);

        $content=$this->render('view3');
        

        $mpdf = new mPDF;
        $mpdf->WriteHTML($content);
        $mpdf->Output();

        exit;
    }
   
}?>