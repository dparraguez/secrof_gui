<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\AppUsuario;
use app\models\AppUsuarioSearch;


/* @var $this yii\web\View */
/* @var $model app\models\AppReclamoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-reclamo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 're_id') ?>

    <?= $form->field($model, 're_descripcion') ?>

    <?= $form->field($model, 're_fecha_app') ?>

    <?= $form->field($model, 're_hora_app') ?>

    <?= $form->field($model, 're_foto') ?>

    <?php // echo $form->field($model, 're_latitud') ?>

    <?php // echo $form->field($model, 're_longitud') ?>

    <?php // echo $form->field($model, 're_prioridad') ?>

    <?php // echo $form->field($model, 'us_id') ?>

    <?php // echo $form->field($model, 'ti_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
