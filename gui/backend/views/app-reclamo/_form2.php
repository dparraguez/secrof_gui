<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\WebTipoReclamo;


/* @var $this yii\web\View */
/* @var $model app\models\AppReclamo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-reclamo-form2">

	<?php $form = ActiveForm::begin(); ?>

	<div class="form-group">
        <?php echo "<br>";?>
     <?= $form->field($model, 're_descripcion',[
        'template' => '{label} <div class="row"><div class="col-xs-6">{input}{error}</div></div>'
    ])->textArea(['placeholder' => 'Escriba lo sucedido']) ?>
    </div>


<?php echo "<br>";?>
    <div class="form-group">

        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
