<?php
use yii\widgets\DetailView;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use dosamigos\datepicker\DatePicker;
$this->title = 'Reporte de comportamiento';
$this->params['breadcrumbs'][] = $this->title;
                //'datos_por_vehiculo' => $datos_por_vehiculo,grafico_vehiculos
                //'grafico_diario' => $grafico_diario,
?>

<h1><?= Html::encode($this->title) ?></h1>
<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
<div class="row">
    <div class="col-md-5">
        <?= $form->field($model, 'fecha_inicio')->widget(
            DatePicker::className(), [
                'language' => 'es',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'dd-mm-yyyy',
                ]
            ]
        );?>

    </div>
    <div class="col-md-5">
        <?= $form->field($model, 'fecha_fin')->widget(
            DatePicker::className(), [
                'language' => 'es',
                'clientOptions' => [
                    'autoclose' => true,
                    'format' => 'dd-mm-yyyy',
                ]
            ]
        );?>
  </div>
  <div class="col-md-2" style="padding-top:25px">
    <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
  </div>
</div>
<?php ActiveForm::end(); ?>


<?php if(!empty($grafico_diario)){ ?>
<div class="row">
<?php
    echo Highcharts::widget([
       'options' => [
          'chart' => ['type' => 'column'],

          'title' => ['text' => 'Movimiento por flota'],
          'stackLabels'=> [
                'enabled'=> true,
          ],
          'xAxis' => [
             'categories' => $fechas
          ],
          'yAxis' => [
             'title' => ['text' => 'Cantidad de vehículos']
          ],
          'plotOptions' => [
            'column' =>[
                'stacking'=> 'normal',
            ],
            'series' =>[
                'pointWidth' => 20,
            ],
          ],
          'series' => [
            ['name' => 'Con movimiento', 'data' => $grafico_diario['con_movimiento']],
            ['name' => 'Sin movimiento', 'data' => $grafico_diario['sin_movimiento']]
          ]
       ]
    ]);
        ?>
    </div>
<?php } ?>
<?php if (!empty($grafico_vehiculos)) { ?>
    <div class="row">
<?php 
    echo Highcharts::widget([
       'options' => [
          'chart' => ['type' => 'column'],

          'title' => ['text' => 'Movimiento por vehículo'],
          'stackLabels'=> [
                'enabled'=> true,
          ],
          'xAxis' => [
             'categories' => $patentes
          ],
          'yAxis' => [
             'title' => ['text' => 'Cantidad de días']
          ],
          'plotOptions' => [
            'column' =>[
                'stacking'=> 'normal',
            ]
          ],
          'series' => [
             ['name' => 'Con movimiento', 'data' => $grafico_vehiculos['con_movimiento']],
             ['name' => 'Sin movimiento', 'data' => $grafico_vehiculos['sin_movimiento']]
          ]
       ]
    ]);
        ?>
    </div>

<?php } ?>