<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reporte de estacionado con carga';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reporte-iniciodescargado">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'patenteVehiculo',
            [
                'attribute' => 'entrada_estacionamiento',
                'value' => function ($data) {
                    return date("d-m-Y H:i:s", $data->entrada_estacionamiento-3600*3);
                },
            ],
            [
                'attribute' => 'salida_vertedero',
                'value' => function ($data) {
                    return date("d-m-Y H:i:s", $data->salida_vertedero-3600*3);
                },
            ],
            [
                'attribute' => 'tiempo',
                'value' => function ($data) {
                    $init = $data->tiempo;
                    $seconds = $data->tiempo;
                    $H = floor($seconds / 3600);
                    $i = ($seconds / 60) % 60;
                    $s = $seconds % 60;
                    return sprintf("%02d:%02d:%02d", $H, $i, $s);

                },
            ],


            //['class' => 'yii\grid\ActionColumn'],
            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Ver recorrido',
                'template' => '{recorrido}',
                'buttons' => [
                    //view button
                    'recorrido' => function ($url, $model) {
                        return Html::a('<span class="fa fa-search"></span>Ver recorrido', 
                                    [
                                        'asdf/verruta', 
                                        'inicio' => $model->salida_vertedero, 
                                        'fin'=> $model->entrada_estacionamiento,
                                        'id_vehiculo'=>$model->id_vehiculo
                                    ], 
                                    [
                                        'target'=>'_blank',
                                        'title' => Yii::t('app', 'Ver recorrido'),
                                        'class'=>'btn btn-primary btn-xs',                              
                                    ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>

</div>
