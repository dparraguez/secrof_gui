<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\ReporteDetencion;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reporte de detención';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reporte-detencion">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'patenteVehiculo',
            [
                'attribute' => 'inicio',
                'value' => function ($data) {
                    return date("d-m-Y H:i:s", $data->inicio-3600*3);
                },
            ],
            [
                'attribute' => 'tipo',
                'value' => function ($data) {
                    return ReporteDetencion::$tipos[$data->tipo];
                },
            ],
            [
                'attribute' => 'tiempo',
                'value' => function ($data) {
                    $init = $data->tiempo;
                    $seconds = $data->tiempo;
                    $H = floor($seconds / 3600);
                    $i = ($seconds / 60) % 60;
                    $s = $seconds % 60;
                    return sprintf("%02d:%02d:%02d", $H, $i, $s);
                },
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Ver posicion',
                'template' => '{posicion}',
                'buttons' => [
                    //view button
                    'posicion' => function ($url, $model) {
                        return Html::a(
                            '<span class="fa fa-search"></span>Ver posición',
                            [
                                'verposicion',
                                'inicio' => $model->inicio,
                                'id_vehiculo'=>$model->id_vehiculo
                            ],
                            [
                                'target'=>'_blank',
                                'title' => Yii::t('app', 'Ver posición'),
                                'class'=>'btn btn-primary btn-xs',
                            ]
                        );
                    },
                ],
            ],
        ],
    ]); ?>

</div>
