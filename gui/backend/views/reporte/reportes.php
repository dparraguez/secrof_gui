<?php
use yii\helpers\Html;

?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Cumplimiento de Contrato</h3>
  </div>
  <div class="panel-body">
    <p class="text-center">
        <?= Html::a('Detención', ['detencion'], ['class' => 'btn btn-primary']) ?>
    </p>
    <p class="text-center">
        <?= Html::a('Comportamiento', ['comportamiento'], ['class' => 'btn btn-primary']) ?>
    </p>
    <p class="text-center">
        <?= Html::a('Inicio Ruta', ['inicioruta'], ['class' => 'btn btn-primary']) ?>
    </p>
  </div>
</div>