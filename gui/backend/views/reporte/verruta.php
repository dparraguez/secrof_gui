<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZHAMAumbLrX-oNvuKJCIMsjnE7pHEyvs&v=3.exp&signed_in=true&libraries=drawing"></script>

<script>
var map;
var lineas = [];
var infowindow = new google.maps.InfoWindow({
      content: "    "
  });

function obtener_orientacion(angulo,velocidad){
var imagen;
if(angulo==0 && velocidad==0) {imagen='sin_orientacion';}
else if(angulo>=0 && angulo<11 && velocidad>0) {imagen='0';}
else if(angulo>=11 && angulo<33){imagen='22';}
else if(angulo>=33 && angulo<56){imagen='45';}
else if(angulo>=56 && angulo<78){imagen='67';}
else if(angulo>=78 && angulo<101){imagen='90';}
else if(angulo>=101 && angulo<123){imagen='112';}
else if(angulo>=123 && angulo<146){imagen='135';}
else if(angulo>=146 && angulo<168){imagen='157';}
else if(angulo>=168 && angulo<191){imagen='180';}
else if(angulo>=191 && angulo<213){imagen='202';}
else if(angulo>=213 && angulo<236){imagen='225';}
else if(angulo>=236 && angulo<258){imagen='247';}
else if(angulo>=258 && angulo<281){imagen='270';}
else if(angulo>=281 && angulo<303){imagen='292';}
else if(angulo>=303 && angulo<326){imagen='315';}
else if(angulo>=326 && angulo<348){imagen='337';}
else if(angulo>=348 ){imagen='360';}
else{imagen='sin_orientacion';}
return imagen;
}

function initialize() {
	var latlng = new google.maps.LatLng(-36.817531,-73.049212);
  var mapOptions = {
    zoom: 13,
        center: latlng,
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);

  

 
}

function agregar_linea(inicio, fin, orientacion, fecha){

  var flecha=obtener_orientacion(orientacion);
  var icono_point = new google.maps.MarkerImage("images/iconos_flechas/"+flecha+".png",
    new google.maps.Size(12, 12),
    new google.maps.Point(0,0),
    new google.maps.Point(6, 6)); 
  marker_point = new google.maps.Marker({
    position: inicio, 
    icon: icono_point,
    map: map, 
    zIndex: 2
  }); 

  marker_point.html  = "<div style='width:200px'><span style='font-size: 7pt; font-family: verdana'>Fecha: " + fecha +" </span></div>";     

  google.maps.event.addListener(marker_point, 'click', function(){
    infoWindow = new google.maps.InfoWindow({content: '<div style="width:200px"></div>', maxWidth: 320});
    infoWindow.setContent(this.html);
    infoWindow.open(map, this);
  });

	var flightPlanCoordinates = [
		inicio,
		fin
	];

	var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		//geodesic: true,
		strokeColor: '#0000FF',
		strokeOpacity: 1.0,
		strokeWeight: 1
	});

	 flightPath.setMap(map);
}
window.onload = function() {
        initialize();

        var locations = [
			<?php $i= 0; ?>
			<?php foreach ($lineas as $value) { ?>
            <?php if($i!=0){ echo ','; } else {$i=1;} ?>


            <?php  [$value.'primer_punto'.",".$value.'segundo_punto'.",".$value.'orientacion'.",'".$value.'fecha'."'"] ?>
          <?php } ?>

        ];

        for (i = 0; i < locations.length; i++) {  
          console.log(locations[i]);
          agregar_linea(new google.maps.LatLng(locations[i][0], locations[i][1]), new google.maps.LatLng(locations[i][2], locations[i][3]), locations[i][4], locations[i][5]);
        }
      };


</script>
<?php

use yii\helpers\Html;
?>
<div class="rutas-view">

    <h1><?= Html::encode($this->title) ?></h1>


</div>


<div id="map_canvas" style="height:400px" ></div>
