<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Ingresar usuario';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="user-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->errorSummary($model); ?>
	    <input type="text" name="SignupForm[email]" style="display:none" value="fake input" /> 

	    <?= $form->field($model, 'username')->textInput() ?>

	    <?= $form->field($model, 'email')->textInput() ?>

	    <?= $form->field($model, 'tipo')->dropdownList(
		    User::$listadoAuth,
		    ['prompt'=>'Seleccionar tipo']
		); ?>

		<?= $form->field($model, 'password1')->passwordInput() ?>

		<?= $form->field($model, 'password2')->passwordInput() ?>

	    <div class="form-group">
	        <?= Html::submitButton('Ingresar', ['class' => 'btn btn-success']) ?>
	    </div>

    <?php ActiveForm::end(); ?>

</div>
