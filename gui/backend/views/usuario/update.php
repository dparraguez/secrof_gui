<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $form yii\widgets\ActiveForm */
/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Update User: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-update">

    <h1><?= Html::encode($this->title) ?></h1>

	<div class="user-form">

	    <?php $form = ActiveForm::begin(); ?>

	    <?= $form->field($model, 'username')->textInput(['inputOptions' => [
				'autocomplete' => 'off',
			]]) ?>

	    <?= $form->field($model, 'email')->textInput(['inputOptions' => [
				'autocomplete' => 'off',
			]]) ?>

	    <?= $form->field($model, 'tipo')->dropdownList(
		    User::$listadoAuth,
		    ['prompt'=>'Seleccionar tipo']
		); ?>

	    <div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Ingresar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
