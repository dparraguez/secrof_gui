<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\WebEstado */

$this->title = $model->es_id;
$this->params['breadcrumbs'][] = ['label' => 'Web Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-estado-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->es_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->es_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'es_id',
            'es_nombre',
            'es_descripcion',
        ],
    ]) ?>

</div>
