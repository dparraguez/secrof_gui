<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WebEstado */

$this->title = 'Update Web Estado: ' . ' ' . $model->es_id;
$this->params['breadcrumbs'][] = ['label' => 'Web Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->es_id, 'url' => ['view', 'id' => $model->es_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="web-estado-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
