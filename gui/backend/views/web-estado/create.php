<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WebEstado */

$this->title = 'Create Web Estado';
$this->params['breadcrumbs'][] = ['label' => 'Web Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-estado-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
