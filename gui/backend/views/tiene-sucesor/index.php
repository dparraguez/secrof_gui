<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TieneSucesorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tiene Sucesors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiene-sucesor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tiene Sucesor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'es_id',
            'web_es_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
