<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TieneSucesor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiene-sucesor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'es_id')->textInput() ?>

    <?= $form->field($model, 'web_es_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
