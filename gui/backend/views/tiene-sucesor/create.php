<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TieneSucesor */

$this->title = 'Create Tiene Sucesor';
$this->params['breadcrumbs'][] = ['label' => 'Tiene Sucesors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiene-sucesor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
