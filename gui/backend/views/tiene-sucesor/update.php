<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TieneSucesor */

$this->title = 'Update Tiene Sucesor: ' . ' ' . $model->es_id;
$this->params['breadcrumbs'][] = ['label' => 'Tiene Sucesors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->es_id, 'url' => ['view', 'es_id' => $model->es_id, 'web_es_id' => $model->web_es_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tiene-sucesor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
