<?php
use backend\assets\AppAsset;
use common\models\User;
use common\models\UserSearch;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\Controler\Vehiculo;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandLabel' => 'SECROF',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => 'Inicio', 'url' => ['/site/index']],
                ['label' => 'Reportes', 'url' => ['/reporte']],
                ['label' => 'Administración', 'items' => [
                    ['label' => 'Usuarios', 'url' => ['/usuario']],
                    '<li class="divider"></li>',
                    ['label' => 'Vehículos', 'url' => ['/vehiculo']],
                    '<li class="divider"></li>',
                    ['label' => 'Zonas', 'url' => ['/zonaservicio']],
                    '<li class="divider"></li>',
                    ['label' => 'Calles', 'url' => ['/calle']],
                ],],
                ['label' => 'Data', 'items' => [
                    ['label' => 'ProbabilidadBA', 'url' => ['/data/probabilidadba']],
                    '<li class="divider"></li>',
                    ['label' => 'Calculo de consumo', 'url' => ['/data/calculoconsumo']],
                    '<li class="divider"></li>',
                    ['label' => 'contenido home', 'url' => ['/data/home']],
                ['label' => 'example curl', 'url' => ['/data/curl']],
                ],],
            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
            } else {
                if (Yii::$app->user->identity->tipo==User::AUTH_SECRETARIA){
                $menuItems[] = ['label' => 'Ciudadanos','items' => [
                                ['label' => 'Buscar Usuario', 'url' => ['/app-usuario/buscar']],
                                ['label' => 'Registar Usuario', 'url' => ['/app-usuario/create']],
                            ],];

                        }
                  if (Yii::$app->user->identity->tipo==User::AUTH_FISCALIZADOR){
                $menuItems[] = ['label' => 'Ciudadanos', 'items' => [
                            ['label' => 'Resumen', 'items'=>[
                            ['label'=>'Gráficos','url' => ['/app-reclamo/appgrafico']],
                            ['label'=>'PDF por tipo reclamo ','url' => ['/app-reclamo/samplepdf']],
                            ['label'=>'PDF resolucíon reclamo','url' => ['/app-reclamo/diferencia']],
                            ],],
                            '<li class="divider"></li>',
                            ['label' => 'Mapa fiscalizaciones','items' => [
                                ['label' => 'Mapa general', 'url' => ['/app-reclamo/ver-mapa-general']],
                                ['label' => 'Mapa reclamo', 'url' => ['/app-reclamo/ver-mapa-reclamo']],
                            ],],
                            '<li class="divider"></li>',
                            ['label' => 'Reclamos', 'url' => ['/app-reclamo']],
                            '<li class="divider"></li>',
                            ['label' => 'Sugerencias', 'url' => ['/app-sugerencia']],
                            '<li class="divider"></li>',
                            ['label' => 'Usuarios', 'url' => ['/app-usuario']],
                            '<li class="divider"></li>',
                             ['label' => 'Inspectores', 'url' => ['/app-inspector']],
                            ],];

                        }
                        
                $menuItems[] = [
                    'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
            ]);
            NavBar::end();
        ?>

        <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
