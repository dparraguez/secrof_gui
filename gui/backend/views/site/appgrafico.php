<?php use miloschuman\highcharts\Highcharts; 

echo Highcharts::widget([
    'options' => [
        'title' => ['text' => 'Gráfico por tipo de Reclamo'],
         
        'plotOptions' => [
            'pie' => [
                'cursor' => 'pointer',
            ],
        ],
        'series' => [
            [ // new opening bracket
                'type' => 'pie',
                'name' => 'Elements',
                'data' => [
                    ['Vehiculo',(int)$vehiculo],
                    ['Escombros',(int)$escombros],
                    ['Microbasurales',(int)$microbasural],
                    ['Trabajadores',(int)$trabajadores],
                    ['Calles',(int)$calles],
                    ['Residuos Domiciliarios',(int)$domiciliarias],
                    ['Sumideros de agua lluvia',(int)$sumideros],

                ],
            ] // new closing bracket
        ],
    ],
]);
  
echo "$total";
echo Highcharts::widget([  
  'options' => [        
        'chart' => [            
            'type' => 'column',            
            'margin' => 80,            
            'options3d' => [                
            'enabled' => true,               
            'alpha' => 10,                
            'beta' => 10,                
            'depth' => 10,                
            'viewDistance' => 10           
            ],        
        ],        
        'title' => ['text' => 'Fiscalizaciones'],  
            'subtitle'=>['text'=>'3D donut in Highcharts'],
            'subtitle'=>['number'=> '(int)$total'],
            
            'plotOptions' => [
                'column' => [
                    'depth' => 3]],        
                'xAxis' => [            
                   // 'categories' => 'Highcharts.getOptions().lang.shortMonths'],
                 'categories' => ['Fiscalizaciones'],
                 'ceiling'=>2,
                 ],
                    'yAxis' => [            
                            'title' => ['text'=>'Cantidad Fiscalizaciones']], 
                            'series' => [ 
                                [
                                'name' => 'Reclamos', 
                                'data' =>[
                                ['Cantidad',(int)$reclamo],],],
                                 ['name' => 'Sugerencias', 
                                'data' =>[
                                ['Cantidad',(int)$sugerencia],],],
                                ['name' => 'Felicitaciones', 
                                'data' =>[
                                ['Cantidad',(int)$felicitacion],],]
                                ],
    
                                ]]

                    );



?>
