

<?php use miloschuman\highcharts\Highcharts; ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
<div>
<?php echo Highcharts::widget([
   'options' => [
      'title' => ['text' => 'Gráfico de consumo de combustible Semanal'.'(desde el '.$inicio.' hasta '.$fin.')'],
      'xAxis' => [
         'categories' =>  [  $consumo_diario[0]['dia'] , $consumo_diario[1]['dia'] , $consumo_diario[2]['dia'] , $consumo_diario[3]['dia'] , $consumo_diario[4]['dia'] ,$consumo_diario[5]['dia'] ,$consumo_diario[6]['dia']]
      ],
      'yAxis' => [
         'title' => ['text' => 'Consumo Total de combustible en litros']
      ],
      'series' => [
         ['name' => 'Zona 5', 'data' => [$consumo_diario[0]['litros'] , $consumo_diario[1]['litros'] , $consumo_diario[2]['litros'] ,$consumo_diario[3]['litros'] ,$consumo_diario[4]['litros'] , $consumo_diario[5]['litros'] ,$consumo_diario[6]['litros']]]
        
      ]
   ]
]); ?>

</div>


<div>
<br>
<br>

<center><h2> Informe de consumo de combustible</h2></center>

<h5><b>Período</b></h5>
<br>
<table class="table">
<tr>
<th><h5><b>Desde el:</b> <font color="red"><?=" ".$fecha_inicio?></font> </h5></th>
<th> </th>
<th> </th>
<th> </th>
<th> </th>
<th> </th>
<th><h5>Valor calculado según:</h5></th>   
<th> </th>                              
</tr>

<tr>
<th><h5><b>Hasta:</b> <font color="red"><?=" ".$fecha_termino?></font> </h5></th>
<th> </th>
<th> </th>
<th> </th>
<th> </th>  
<th> </th>

<th><h5><b>Petróle diesel:</b> <font color="red"><?=" ".$valorcombustible?></font> </h5></th>
                     

</tr>
</table>
 



<table border="1">

   



    <tr bgcolor="#DEDEDE" >
     <th bgcolor="#DEDEDE">Número</th>
     <th bgcolor="#DEDEDE">Patente</th>
        <th bgcolor="#DEDEDE">Conductor</th>
        <th bgcolor="#DEDEDE">Flota</th>
        <th bgcolor="#DEDEDE">Tipo</th>
        <th bgcolor="#DEDEDE">Consumo en litros</th>
        <th bgcolor="#DEDEDE">Gasto($)</th>
      
    </tr>
  
<?php $i=0; while($i<count($datos_final))   {?>
  

   <tr>
   
<td><?=$i+1; ?></td>
         <td><?=$datos_final[$i]['patente'] ?></td>
        <td><?=$datos_final[$i]['conductor']  ?></td>
        <td><?=$datos_final[$i]['flota']  ?></td>
        <td><?=$datos_final[$i]['tipo']  ?></td>
        <td ><?=($datos_final[$i]['consumo']*30)  ?></td>
        <td ><?=($datos_final[$i]['valor']*30)  ?></td>
        
    </tr>


<?php $i++;}?>
   
<tr>
 <td><b>Total consumo</b></h5></td>
 <td><b>  </b></h5></td>
        
        <td></td>
        <td></td>
        <td></td>
        <td><?=$litros ?></td>
        <td><?=$valorTotal ?></td>
     
    </tr>






</table>

</div>




</body>
</html>






