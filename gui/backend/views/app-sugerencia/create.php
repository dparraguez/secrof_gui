<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AppSugerencia */

$this->title = 'Crear sugerencia';
//$this->params['breadcrumbs'][] = ['label' => 'Crear sugerencias'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-sugerencia-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
