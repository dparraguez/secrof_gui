<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\AppUsuario;
use app\models\AppSugerencia;

/* @var $this yii\web\View */
/* @var $model app\models\AppSugerencia */

$this->title = $model->su_id;
$this->params['breadcrumbs'][] = ['label' => 'Sugerencias', 'url' => ['index']];
$this->title = 'Sugerencia/Felicitación';
?>
<div class="app-sugerencia-view2">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo "<br>";?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'su_id',
            'su_tipo',
            'su_descripcion',
            //'su_fecha_app',
            //'su_hora_app',
            //'su_latitud',
            //'su_longitud',
            'su_estado',
            'su_fecha_hora_web',
            //'us_id',
            [
            'attribute' => 'Usuario',
            'value' => $model->us->us_nombres.' '.$model->us->us_apellidos,
        ],
        ],
    ]) ?>


</div>
