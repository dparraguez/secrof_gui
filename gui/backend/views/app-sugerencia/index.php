<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AppSugerenciaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sugerencias/Felicitaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-sugerencia-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

 
            'su_tipo',
            'su_descripcion',
             'su_estado',
            
               [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Cambiar Estado',
                'template' => '{leido}',
                'buttons' => [
                    
                    'leido' => function ($url, $model) {
                        if($model->su_estado == 'Recepcionado'){
                        return Html::a('<span class="fa fa-search"></span>Leído', 
                                    [
                                        'app-sugerencia/estado',
                                        'id'=>$model->su_id
                                    ], 
                                    [
                                  
                                        'title' => Yii::t('app', 'Leído'),
                                        'class'=>'btn btn-primary btn-sm',                              
                                    ]
                           );
                        }
                    },

                 ],
            ],

          
        ],
    ]); ?>

</div>
