<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\AppSugerencia */
/* @var $form yii\widgets\ActiveForm */
?>
<script>
var map;
var marker=null;
var lineas = [];
var color = '#FF0000';
var color_seleccion = '#FF0000';
var color_no_seleccion = '#0000FF';
var seleccionada = null;
var infowindow = new google.maps.InfoWindow({
      content: "    "
  });

function initialize() {
    var latlng = new google.maps.LatLng(-36.817531,-73.049212);
    var mapOptions = {
    zoom: 13,
        center: latlng,
    mapTypeId: google.maps.MapTypeId.TERRAIN
    };

    map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);

    google.maps.event.addListener(map, 'click', function(event) {
        if(marker!=null){
            marker.setMap(null);

        }
        marker = new google.maps.Marker({position: event.latLng, map: map});

        console.log(event.latLng.lat());

        $('#appsugerencia-su_latitud').val(event.latLng.lat());
        $('#appsugerencia-su_longitud').val(event.latLng.lng());

    });
 
}

window.onload = function() {
    initialize();


};

</script>

<div class="app-sugerencia-form">


    <?php $form = ActiveForm::begin(); ?>

     <div class="alert alert-info" role="alert"><b>Seleccionar posición:</b>
    Utilice el mapa para seleccionar la posición de la sugerencia o felicitación. 
    </div>

    <div id="map_canvas" style="height:400px" ></div>

    <?php echo "<br>";?>
    <?php echo "<br>";?>

    <?= $form->field($model, 'su_tipo',['template' => '{label} <div class="row"><div class="col-xs-6">{input}{error}</div></div>'])->dropDownList([ 'Sugerencia' => 'Sugerencia', 'Felicitación' => 'Felicitación', ], ['prompt' => 'Elija su opción']) ?>

     <div class="form-group">
     <?= $form->field($model, 'su_descripcion',[
        'template' => '{label} <div class="row"><div class="col-xs-6">{input}{error}</div></div>'
    ])->textArea(['placeholder' => 'Escriba lo sucedido']) ?>

    <?= $form->field($model, 'su_latitud',[
        'template' => '{label} <div class="row"><div class="col-xs-6">{input}{error}</div></div>'
    ])->textInput(['readonly' => true]) ?>

    <?= $form->field($model, 'su_longitud',[
        'template' => '{label} <div class="row"><div class="col-xs-6">{input}{error}</div></div>'
    ])->textInput(['readonly' => true]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
