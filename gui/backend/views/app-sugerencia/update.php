<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppSugerencia */

$this->title = 'Actualizar Sugerencia: ' . ' ' . $model->su_id;
//$this->params['breadcrumbs'][] = ['label' => 'App Sugerencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->su_id, 'url' => ['view', 'id' => $model->su_id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="app-sugerencia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form2', [
        'model' => $model,
    ]) ?>

</div>
