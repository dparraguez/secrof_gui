<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\AppUsuario;
use app\models\AppSugerencia;

/* @var $this yii\web\View */
/* @var $model app\models\AppSugerencia */

//$this->title = $model->su_id;
$this->params['breadcrumbs'][] = ['label' => 'Buscar usuario', 'url' => ['/app-usuario/buscar']];
$this->title = 'Sugerencia/Felicitación creada exitosamente';
?>
<div class="app-sugerencia-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo "<br>";?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'su_id',
            'su_tipo',
            'su_descripcion',
            //'su_fecha_app',
            //'su_hora_app',
            //'su_latitud',
            //'su_longitud',
            'su_estado',
            //'su_fecha_hora_web',
            //'us_id',
            [
            'attribute' => 'Usuario',
            'value' => $model->us->us_nombres.' '.$model->us->us_apellidos,
        ],
        ],
    ]) ?>
    <p>
        <?php echo "<br>";?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->su_id], ['class' => 'btn btn-primary']) ?>
    </p>


</div>
