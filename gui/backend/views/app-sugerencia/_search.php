<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppSugerenciaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-sugerencia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'su_id') ?>

    <?= $form->field($model, 'su_tipo') ?>

    <?= $form->field($model, 'su_descripcion') ?>

    <?= $form->field($model, 'su_fecha_app') ?>

    <?= $form->field($model, 'su_hora_app') ?>

    <?php // echo $form->field($model, 'su_latitud') ?>

    <?php // echo $form->field($model, 'su_longitud') ?>

    <?php // echo $form->field($model, 'su_estado') ?>

    <?php // echo $form->field($model, 'su_fecha_hora_web') ?>

    <?php // echo $form->field($model, 'us_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
