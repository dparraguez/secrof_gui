<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReclamoEstado */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reclamo-estado-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 're_id')->textInput() ?>

    <?= $form->field($model, 'es_id')->textInput() ?>

    <?= $form->field($model, 're_es_fecha')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
