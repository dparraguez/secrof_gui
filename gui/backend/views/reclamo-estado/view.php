<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ReclamoEstado */

$this->title = $model->re_id;
$this->params['breadcrumbs'][] = ['label' => 'Reclamo Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reclamo-estado-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 're_id' => $model->re_id, 'es_id' => $model->es_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 're_id' => $model->re_id, 'es_id' => $model->es_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            're_id',
            'es_id',
            're_es_fecha',
        ],
    ]) ?>

</div>
