<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ReclamoEstado */

$this->title = 'Update Reclamo Estado: ' . ' ' . $model->re_id;
$this->params['breadcrumbs'][] = ['label' => 'Reclamo Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->re_id, 'url' => ['view', 're_id' => $model->re_id, 'es_id' => $model->es_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="reclamo-estado-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
