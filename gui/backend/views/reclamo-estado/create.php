<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ReclamoEstado */

$this->title = 'Create Reclamo Estado';
$this->params['breadcrumbs'][] = ['label' => 'Reclamo Estados', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reclamo-estado-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
