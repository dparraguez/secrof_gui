<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WebTipoReclamo */

$this->title = 'Update Web Tipo Reclamo: ' . ' ' . $model->ti_id;
$this->params['breadcrumbs'][] = ['label' => 'Web Tipo Reclamos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ti_id, 'url' => ['view', 'id' => $model->ti_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="web-tipo-reclamo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
