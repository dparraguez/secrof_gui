<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\WebTipoReclamoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Web Tipo Reclamos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-tipo-reclamo-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Web Tipo Reclamo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ti_id',
            'ti_nombre',
            'ti_prioridad',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
