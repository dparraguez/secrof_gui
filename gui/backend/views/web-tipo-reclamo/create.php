<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\WebTipoReclamo */

$this->title = 'Create Web Tipo Reclamo';
$this->params['breadcrumbs'][] = ['label' => 'Web Tipo Reclamos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="web-tipo-reclamo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
