<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AppInspector */

$this->title = 'Crear Inspector';
$this->params['breadcrumbs'][] = ['label' => 'Inspectores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-inspector-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
