<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppInspector */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-inspector-form">

    <?php $form = ActiveForm::begin(); ?>

     <div class="form-group">
     <?= $form->field($model, 'in_nombres',[
        'template' => '{label} <div class="row"><div class="col-xs-4">{input}{error}</div></div>'
    ])->textInput(['placeholder' => 'Ej: Juana María']) ?>
    </div>

    <div class="form-group">
    <?= $form->field($model, 'in_apellidos',[
        'template' => '{label} <div class="row"><div class="col-xs-4">{input}{error}</div></div>'
    ])->textInput(['placeholder' => 'Ej: Castro Pérez']) ?>
     </div>
     <div class="form-group">
    <?= $form->field($model, 'in_rut',[
        'template' => '{label} <div class="row"><div class="col-xs-4">{input}{error}</div></div>'
    ])->textInput(['placeholder' => 'Ej: 11111111-1']) ?>
     </div>

     <div class="form-group">
    <?= $form->field($model, 'in_fono',[
        'template' => '{label} <div class="row"><div class="col-xs-4">{input}{error}</div></div>'
    ])->textInput(['placeholder' => 'Ej: 423111919']) ?>
    </div>
   

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
