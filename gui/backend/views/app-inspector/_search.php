<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppInspectorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-inspector-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'in_id') ?>

    <?= $form->field($model, 'in_nombres') ?>

    <?= $form->field($model, 'in_apellidos') ?>

    <?= $form->field($model, 'in_rut') ?>

    <?= $form->field($model, 'in_fono') ?>

    <?php // echo $form->field($model, 'in_clave') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
