<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppInspector */

$this->title = 'Update App Inspector: ' . ' ' . $model->in_id;
$this->params['breadcrumbs'][] = ['label' => 'App Inspectors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->in_id, 'url' => ['view', 'id' => $model->in_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="app-inspector-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
