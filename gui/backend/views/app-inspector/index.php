<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AppInspectorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Inspectores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-inspector-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
       // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'in_nombres',
            'in_apellidos',
            'in_rut',
              [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Reporte',
                'template' => '{reporte}',
                'buttons' => [
                    //view button
                    'reporte' => function ($url, $model) {
                        return Html::a('<span class="fa fa-search"></span>Ver Reporte', 
                                    [
                                        'app-reporte/index',
                                        'id'=>$model->in_id
                                    ], 
                                    [
                                        //'target'=>'_blank',
                                        'title' => Yii::t('app', 'Ver Reporte'),
                                        'class'=>'btn btn-primary btn-sm',                              
                                    ]
                        );
                    },
        ],],],
    ]); ?>

</div>
