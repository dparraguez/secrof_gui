<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AppReporte */

$this->title = $model->rp_id;
$this->params['breadcrumbs'][] = ['label' => 'App Reportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-reporte-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->rp_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->rp_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'rp_id',
            'rp_descripcion',
            'rp_fecha_app',
            'rp_hora_app',
            'rp_foto',
            'rp_latitud',
            'rp_longitud',
            'rp_estado',
            'rp_fecha_hora_web',
            'in_id',
        ],
    ]) ?>

</div>
