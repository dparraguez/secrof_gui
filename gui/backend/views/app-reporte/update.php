<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AppReporte */

$this->title = 'Update App Reporte: ' . ' ' . $model->rp_id;
$this->params['breadcrumbs'][] = ['label' => 'App Reportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->rp_id, 'url' => ['view', 'id' => $model->rp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="app-reporte-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
