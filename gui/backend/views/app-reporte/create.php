<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AppReporte */

$this->title = 'Create App Reporte';
$this->params['breadcrumbs'][] = ['label' => 'App Reportes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-reporte-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
