<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\AppReporte;
use app\models\AppInspector;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AppReporteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reportes Inspector';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-reporte-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'rp_id',
            'rp_descripcion',
            //'rp_fecha_app',
            //'rp_hora_app',
            //'rp_foto',
            // 'rp_latitud',
            // 'rp_longitud',
            // 'rp_estado',
             'rp_fecha_hora_web',
             //agrega los botones foto y pdf
              [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Seleccionar',
                'template' => '{foto} {pdf}',
                'buttons' => [
                    //view button
                    'foto' => function ($url, $model) {
                         if($model->rp_foto != null){
                            return Html::a('<span class="fa fa-search"></span>Foto', 
                                    [
                                        'app-reporte/foto',
                                        'id'=>$model->rp_id,
                                    ], 
                                    [
                                        //'target'=>'_blank',
                                        'title' => Yii::t('app', 'Foto'),
                                        'class'=>'btn btn-primary btn-m',                              
                                    ]
                        );
                        }
                    },
                    'pdf' => function ($url, $model) {
                        return Html::a('<span class="fa fa-search"></span>PDF', 
                                    [
                                        'app-reporte/pdf',
                                        'id'=>$model->in_id,
                                        'nombre'=>$model->in->in_nombres,
                                        'apellido'=>$model->in->in_apellidos,
                                        'fecha'=>$model->rp_fecha_hora_web,
                                        'rut'=>$model->in->in_rut,
                                        'descripcion'=>$model->rp_descripcion
                                        
                                    ], 
                                    [
                                        //'target'=>'_blank',
                                        'title' => Yii::t('app', 'Agregar'),
                                        'class'=>'btn btn-info btn-m',                              
                                    ]
                        );
                    },
              ],],],
            // 'in_id',

            //['class' => 'yii\grid\ActionColumn'],
        
    ]); ?>

</div>
