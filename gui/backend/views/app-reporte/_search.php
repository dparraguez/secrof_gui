<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppReporteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-reporte-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'rp_id') ?>

    <?= $form->field($model, 'rp_descripcion') ?>

    <?= $form->field($model, 'rp_fecha_app') ?>

    <?= $form->field($model, 'rp_hora_app') ?>

    <?= $form->field($model, 'rp_foto') ?>

    <?php // echo $form->field($model, 'rp_latitud') ?>

    <?php // echo $form->field($model, 'rp_longitud') ?>

    <?php // echo $form->field($model, 'rp_estado') ?>

    <?php // echo $form->field($model, 'rp_fecha_hora_web') ?>

    <?php // echo $form->field($model, 'in_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
