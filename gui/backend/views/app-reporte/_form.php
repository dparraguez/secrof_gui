<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AppReporte */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="app-reporte-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rp_descripcion')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rp_fecha_app')->textInput() ?>

    <?= $form->field($model, 'rp_hora_app')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rp_foto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rp_latitud')->textInput() ?>

    <?= $form->field($model, 'rp_longitud')->textInput() ?>

    <?= $form->field($model, 'rp_estado')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rp_fecha_hora_web')->textInput() ?>

    <?= $form->field($model, 'in_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
