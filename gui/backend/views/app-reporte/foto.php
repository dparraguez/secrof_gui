<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\AppReporte;
use app\models\AppInspector;

/* @var $this yii\web\View */
/* @var $model app\models\AppReporte */
//muestra la foto del reporte 
//obtiene la foto para mostrarla,almacenada en rp_foto con una ruta definina .
$this->title = $model->rp_foto;
$this->params['breadcrumbs'][] = ['label' => 'Reporte', 'url' => ['index', 'id' => $model->in_id]];
$this->title = 'Foto Reporte';
?>

<div class="app-reporte-foto">


    <h1><?= Html::encode($this->title) ?></h1>

<footer class="footer" style="height:130px; background-color:rgba(255,255,255,0.8); border-top:0px">
        <div class="container">

        <p class="pull-left"><?=  Html::img(Yii::getAlias($model->rp_foto), ['class' => 'thing']); ?></p>

        </div>
    </footer>
</div