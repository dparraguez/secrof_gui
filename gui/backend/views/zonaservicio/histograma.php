<?php

use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
?>
<div class="rutas-view">

    <h1>Ejemplos de histograma</h1>

    <?php foreach ($histogramas as $histograma) { ?>
    	
     

    <div class="row">
<?php
    echo Highcharts::widget([
	   'options' => [
	      'chart' => ['type' => 'column'],

	      'title' => ['text' => 'Hora de servicio'],
	      'xAxis' => [
	         'categories' => $horas
	      ],
	      'yAxis' => [
	         'title' => ['text' => 'Frecuencia']
	      ],
	      'series' => [
	         ['name' => 'Frecuencia', 'data' => $histograma['frecuencia']]
	      ]
	   ]
	]);
		/* $this->Widget('ext.highcharts.HighchartsWidget', array(
		   'options' => array(
		      'exporting' => array('enabled' => true),
		      'title' => array('text' => 'Mes'),
		      'theme' => 'grid',
		      'rangeSelector' => array('selected' => 1),
		      'xAxis' => array(
		         'categories' => $horas
		      ),
		      'yAxis' => array(
		         'title' => array('text' => 'Cantidad')
		      ),
		      'series' => array(
		         array('name' => 'Total', 'data' => $histograma),
		      )
		   )
		)); */
		?>
    </div>

    <?php } ?>


</div>


<div id="map_canvas" style="height:400px" ></div>