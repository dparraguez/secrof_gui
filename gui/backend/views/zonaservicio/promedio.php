<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>

<script>
var map;
var lineas = [];
var infowindow = new google.maps.InfoWindow({
      content: "    "
  });

function initialize() {
	var latlng = new google.maps.LatLng(-36.817531,-73.049212);
  var mapOptions = {
    zoom: 13,
        center: latlng,
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  map = new google.maps.Map(document.getElementById('map_canvas'),
      mapOptions);

  

 
}
function timeConverter(UNIX_timestamp){
  if(UNIX_timestamp==0)
    return "Sin datos";
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = "0" + a.getMonth();
  var date = "0" + a.getDate();
  var hour = "0" + a.getHours();
  var min = "0" + a.getMinutes();
  var sec = "0" + a.getSeconds();
  var time = date.substr(-2) + ' ' + month.substr(-2) + ' ' + year + ' ' + hour.substr(-2) + ':' + min.substr(-2) + ':' + sec.substr(-2) ;
  return time;
}

function agregar_linea(inicio, fin, cumplimiento){
	var flightPlanCoordinates = [
		inicio,
		fin
	];

	var color = '#FF0000';

	if(cumplimiento>0){
		color = '#0000FF';
	}
	var flightPath = new google.maps.Polyline({
		path: flightPlanCoordinates,
		//geodesic: true,
		strokeColor: color,
		strokeOpacity: 1.0,
		strokeWeight: 2
	});

  flightPath.fecha = timeConverter(cumplimiento);

  google.maps.event.addListener(flightPath, 'click', function(event)
    {
        infowindow.setContent(this.fecha);
        infowindow.setPosition(event.latLng);
        infowindow.open(map);

    console.log(event.latLng);
    });

  function lineClick(line)
  {
      alert("Line clicked with myID=" + line.myID);
  }

	 flightPath.setMap(map);
}
window.onload = function() {
        initialize();
        var locations = [
			<?php $i= 0; ?>
			<?php foreach ($cuadras as $cuadra) { ?>
            <?php if($i!=0){ echo ','; } else {$i=1;} ?>
            [<?= $cuadra['latitud_inicio'].",".$cuadra['longitud_inicio'].",".$cuadra['latitud_fin'].",".$cuadra['longitud_fin'].",".$promedio_cuadra[$cuadra['id']] ?>]
          <?php } ?>

        ];
        console.log(locations);
        for (i = 0; i < locations.length; i++) {
          agregar_linea(new google.maps.LatLng(locations[i][0], locations[i][1]), new google.maps.LatLng(locations[i][2], locations[i][3]), locations[i][4]);
        }
      };

</script>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\ZonaServicio;
?>
<div class="rutas-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>

       
    </p>


</div>

<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            [
              'label' => 'Horario',
              'value' => ZonaServicio::$horarios[$model->horario],
            ],
            'hora_inicio',
            'hora_fin',
            [
              'label' => 'Fecha',
              'value' => "12-05-2015",
            ],
        ],
    ]) ?>
<div id="map_canvas" style="height:400px" ></div>


<table class='table table-striped' > <tr><td>Patente</td> <td>cantidad posiciones</td> <td></td></tr>

<?php foreach ($vehiculos_array as $key => $vehiculo) {

  echo "<tr><td>".$patentes[$key]."</td> <td>".$vehiculo."</td> <td>".Html::a('Ver ruta', ['verruta', 'id_vehiculo' => $key], ['class' => 'btn btn-primary'])."</td></tr>";

}
?>
</table>

