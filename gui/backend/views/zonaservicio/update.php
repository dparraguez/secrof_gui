<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ZonaServicio */

$this->title = 'Update Zona Servicio: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Zona Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="zona-servicio-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
