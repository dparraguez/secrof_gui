<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ZonaServicio */

$this->title = 'Create Zona Servicio';
$this->params['breadcrumbs'][] = ['label' => 'Zona Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zona-servicio-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
