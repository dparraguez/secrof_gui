<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ZonaServicioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="zona-servicio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'observacion') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'zona') ?>

    <?= $form->field($model, 'puntos') ?>

    <?php // echo $form->field($model, 'horario') ?>

    <?php // echo $form->field($model, 'dia_lunes') ?>

    <?php // echo $form->field($model, 'dia_martes') ?>

    <?php // echo $form->field($model, 'dia_miercoles') ?>

    <?php // echo $form->field($model, 'dia_jueves') ?>

    <?php // echo $form->field($model, 'dia_viernes') ?>

    <?php // echo $form->field($model, 'dia_sabado') ?>

    <?php // echo $form->field($model, 'dia_domingo') ?>

    <?php // echo $form->field($model, 'hora_inicio') ?>

    <?php // echo $form->field($model, 'hora_fin') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
