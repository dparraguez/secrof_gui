<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=drawing"></script>


    <script type="text/javascript">
//<![CDATA[

// This application is provided by Kjell Scharning
//  Licensed under the Apache License, Version 2.0;
//  http://www.apache.org/licenses/LICENSE-2.0

function gob(e){if(typeof(e)=='object')return(e);if(document.getElementById)return(document.getElementById(e));return(eval(e))}
var map;
var polyShape;
var tmpPolyLine;
var startMarker;
var markers = [];
var midmarkers = [];
var polyPoints = [];
var pointsArray = [];
var mylistener;
var editing = false;

var imageNormal = new google.maps.MarkerImage(
  "images/square.png",
  new google.maps.Size(11, 11),
  new google.maps.Point(0, 0),
  new google.maps.Point(6, 6)
);
var imageHover = new google.maps.MarkerImage(
  "images/square_over.png",
  new google.maps.Size(11, 11),
  new google.maps.Point(0, 0),
  new google.maps.Point(6, 6)
);
var imageNormalMidpoint = new google.maps.MarkerImage(
  "images/square_transparent.png",
  new google.maps.Size(11, 11),
  new google.maps.Point(0, 0),
  new google.maps.Point(6, 6)
);
/*var imageHoverMidpoint = new google.maps.MarkerImage(
  "square_transparent_over.png",
  new google.maps.Size(11, 11),
  new google.maps.Point(0, 0),
  new google.maps.Point(6, 6)
);*/

function initmap(){
    var latlng = new google.maps.LatLng(-36.817531,-73.049212);
    var myOptions = {
        zoom: 15,
        center: latlng,
        draggableCursor: 'default',
        draggingCursor: 'pointer',
        mapTypeControl: true,
        mapTypeControlOptions:{style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
        mapTypeId: google.maps.MapTypeId.HYBRID};
    map = new google.maps.Map(gob('map_canvas'),myOptions);
    polyPoints = new google.maps.MVCArray(); // collects coordinates
    preparePolyline(); // create a Polyline object

    mylistener = google.maps.event.addListener(map, 'click', function(e) {
          addLatLng(e.latLng);
        });
      
}

function preparePolyline(){
    var polyOptions = {
        path: polyPoints,
        strokeColor: "#FF0000",
        strokeOpacity: 1,
        strokeWeight: 3};
    polyShape = new google.maps.Polyline(polyOptions);
    polyShape.setMap(map);
    var tmpPolyOptions = {
      strokeColor: "#2E2EFE",
      strokeOpacity: 1,
      strokeWeight: 3
    };
    tmpPolyLine = new google.maps.Polyline(tmpPolyOptions);
    tmpPolyLine.setMap(map);
}

function addLatLng(latLng){
    if(!editing){
      polyPoints = polyShape.getPath();
      polyPoints.insertAt(polyPoints.length, latLng); // or: polyPoints.push(latLng)
      var stringtobesaved = latLng.lat().toFixed(6) + ',' + latLng.lng().toFixed(6);
      pointsArray.push(stringtobesaved);
      logCode1(); // write kml for polyline
    }
}

function setstartMarker(point){
}

function kmlheading() {
    var heading = "";
    var styleforpolygon = "";
    var styleforrectangle = "";
    var styleforpolyline = "";
    heading = '';

    styleforpolyline += '';

    return heading+styleforpolygon+styleforrectangle+styleforpolyline;
}
function kmlend() {
    var ending;
    return ending = '';
}
// write kml for polyline in text area
function logCode1(){
    var kmltext = '';
    for(var i = 0; i < pointsArray.length; i++) {
        if(i!=0)  kmltext += '_';
        kmltext += pointsArray[i];
    }
    gob('zonaservicio-puntos').value = kmltext;
}


function clearMap() {
    if(editing) stopediting();
    if(startMarker) startMarker.setMap(null);
    polyShape.setMap(null);
    polyPoints = [];
    pointsArray = [];
    preparePolyline();
}

function stopediting(){
    editing = false;
    gob('EditButton').value = 'Editar ruta';
    for(var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    for(var i = 0; i < midmarkers.length; i++) {
        midmarkers[i].setMap(null);
    }
    polyPoints = polyShape.getPath();
    markers = [];
    midmarkers = [];
}
// the "Edit lines" button has been pressed
function editlines(){
    if(editing == true){
        stopediting();
    }else{
        polyPoints = polyShape.getPath();
        if(polyPoints.length > 0){

            if(polyShape) polyShape.setMap(null);
            preparePolyline();
            logCode1();

            for(var i = 0; i < polyPoints.length; i++) {
                var marker = setmarkers(polyPoints.getAt(i));
                markers.push(marker);
                if(i > 0) {
                    var midmarker = setmidmarkers(polyPoints.getAt(i));
                    midmarkers.push(midmarker);
                }
            }
            editing = true;
            gob('EditButton').value = 'Terminar edición';
        }
    }
}
function setmarkers(point) {
    var marker = new google.maps.Marker({
      position: point,
      map: map,
      icon: imageNormal,
        raiseOnDrag: false,
      draggable: true
    });
    google.maps.event.addListener(marker, "mouseover", function() {
      marker.setIcon(imageHover);
    });
    google.maps.event.addListener(marker, "mouseout", function() {
      marker.setIcon(imageNormal);
    });
    google.maps.event.addListener(marker, "drag", function() {
        for (var i = 0; i < markers.length; i++) {
            if (markers[i] == marker) {
                polyShape.getPath().setAt(i, marker.getPosition());
                movemidmarker(i);
                break;
            }
        }
        polyPoints = polyShape.getPath();
        var stringtobesaved = marker.getPosition().lat().toFixed(6) + ',' + marker.getPosition().lng().toFixed(6);
        pointsArray.splice(i,1,stringtobesaved);
        logCode1();
    });
    google.maps.event.addListener(marker, "click", function() {
        for (var i = 0; i < markers.length; i++) {
            if (markers[i] == marker && markers.length != 1) {
                marker.setMap(null);
                markers.splice(i, 1);
                polyShape.getPath().removeAt(i);
                removemidmarker(i);
                break;
            }
        }
        polyPoints = polyShape.getPath();
        if(markers.length > 0) {
            pointsArray.splice(i,1);
            logCode1();
        }
    });
    return marker;
}
function setmidmarkers(point) {
    var prevpoint = markers[markers.length-2].getPosition();
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(
        point.lat() - (0.5 * (point.lat() - prevpoint.lat())),
        point.lng() - (0.5 * (point.lng() - prevpoint.lng()))
      ),
      map: map,
      icon: imageNormalMidpoint,
        raiseOnDrag: false,
      draggable: true
    });
    google.maps.event.addListener(marker, "mouseover", function() {
      marker.setIcon(imageNormal);
    });
    google.maps.event.addListener(marker, "mouseout", function() {
      marker.setIcon(imageNormalMidpoint);
    });
    google.maps.event.addListener(marker, "dragstart", function() {
      for (var i = 0; i < midmarkers.length; i++) {
        if (midmarkers[i] == marker) {
          var tmpPath = tmpPolyLine.getPath();
          tmpPath.push(markers[i].getPosition());
          tmpPath.push(midmarkers[i].getPosition());
          tmpPath.push(markers[i+1].getPosition());
          break;
        }
      }
    });
    google.maps.event.addListener(marker, "drag", function() {
      for (var i = 0; i < midmarkers.length; i++) {
        if (midmarkers[i] == marker) {
          tmpPolyLine.getPath().setAt(1, marker.getPosition());
          break;
        }
      }
    });
    google.maps.event.addListener(marker, "dragend", function() {
      for (var i = 0; i < midmarkers.length; i++) {
        if (midmarkers[i] == marker) {
          var newpos = marker.getPosition();
          var startMarkerPos = markers[i].getPosition();
          var firstVPos = new google.maps.LatLng(
            newpos.lat() - (0.5 * (newpos.lat() - startMarkerPos.lat())),
            newpos.lng() - (0.5 * (newpos.lng() - startMarkerPos.lng()))
          );
          var endMarkerPos = markers[i+1].getPosition();
          var secondVPos = new google.maps.LatLng(
            newpos.lat() - (0.5 * (newpos.lat() - endMarkerPos.lat())),
            newpos.lng() - (0.5 * (newpos.lng() - endMarkerPos.lng()))
          );
          var newVMarker = setmidmarkers(secondVPos);
          newVMarker.setPosition(secondVPos);//apply the correct position to the midmarker
          var newMarker = setmarkers(newpos);
          markers.splice(i+1, 0, newMarker);
          polyShape.getPath().insertAt(i+1, newpos);
          marker.setPosition(firstVPos);
          midmarkers.splice(i+1, 0, newVMarker);
          tmpPolyLine.getPath().removeAt(2);
          tmpPolyLine.getPath().removeAt(1);
          tmpPolyLine.getPath().removeAt(0);
          break;
        }
      }
        polyPoints = polyShape.getPath();
        var stringtobesaved = newpos.lat().toFixed(6) + ',' + newpos.lng().toFixed(6);
        pointsArray.splice(i+1,0,stringtobesaved);
        logCode1();
    });
    return marker;
}
function movemidmarker(index) {
    var newpos = markers[index].getPosition();
    if (index != 0) {
      var prevpos = markers[index-1].getPosition();
      midmarkers[index-1].setPosition(new google.maps.LatLng(
        newpos.lat() - (0.5 * (newpos.lat() - prevpos.lat())),
        newpos.lng() - (0.5 * (newpos.lng() - prevpos.lng()))
      ));
    }
    if (index != markers.length - 1) {
      var nextpos = markers[index+1].getPosition();
      midmarkers[index].setPosition(new google.maps.LatLng(
        newpos.lat() - (0.5 * (newpos.lat() - nextpos.lat())),
        newpos.lng() - (0.5 * (newpos.lng() - nextpos.lng()))
      ));
    }
}
function removemidmarker(index) {
    if (markers.length > 0) {//clicked marker has already been deleted
      if (index != markers.length) {
        midmarkers[index].setMap(null);
        midmarkers.splice(index, 1);
      } else {
        midmarkers[index-1].setMap(null);
        midmarkers.splice(index-1, 1);
      }
    }
    if (index != 0 && index != markers.length) {
      var prevpos = markers[index-1].getPosition();
      var newpos = markers[index].getPosition();
      midmarkers[index-1].setPosition(new google.maps.LatLng(
        newpos.lat() - (0.5 * (newpos.lat() - prevpos.lat())),
        newpos.lng() - (0.5 * (newpos.lng() - prevpos.lng()))
      ));
    }
}

//]]>

window.onload = function() {
        initmap();

        //-36.817668,-73.053900_-36.818716,-73.053342_-36.819300,-73.054501_-36.820382,-73.053814_
        <?php if(!$model->isNewRecord){ ?>
          <?php $array_puntos = explode("_", $model->puntos); ?>


          var locations = [
          <?php $i= 0; ?>
          <?php foreach ($array_puntos as $value) { ?>
             <?php if($i!=0){ echo ','; } else {$i=1;} ?>
            [<?= $value ?>]
          <?php } ?>

          ];

        <?php } ?>

        for (i = 0; i < locations.length; i++) {  
            
          addLatLng(new google.maps.LatLng(locations[i][0], locations[i][1]));
          
        }



      };
</script>

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use app\models\ZonaServicio;

/* @var $this yii\web\View */
/* @var $model app\models\Rutas */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="row">
    
    <div class="col-md-12">

        <div class="rutas-form" >

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'observacion')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'horario')->dropDownList(ZonaServicio::$horarios, ['prompt'=>'Seleccione horario']); ?>

            <?= $form->field($model, 'hora_inicio')->widget(TimePicker::classname(), ['pluginOptions' => [
                  'readonly' => true,
                  'showMeridian' => false,
                  'showSeconds' => false
                ],'options' => [
                  'readonly' => true,
                  ],
              ]); ?>

            <?= $form->field($model, 'hora_fin')->widget(TimePicker::classname(), ['pluginOptions' => [
                  'showMeridian' => false,
                  'readonly' => true,
                  'showSeconds' => false
                ],'options' => [
                    'readonly' => true,
                  ],
              ]); ?>

        <div class="row">
          <div class="col-md-1">
          Frecuencia
          </div>

          <div class="col-md-1">
            <div class="form-group field-rutas-dia_lunes">
              <input type="hidden" name="ZonaServicio[dia_lunes]" value="0"><label><input type="checkbox" id="rutas-dia_lunes" name="ZonaServicio[dia_lunes]" value="1" <?=$model->dia_lunes==1?'checked':''?>> Lunes</label>
            </div>
          </div>
          <div class="col-md-1"> 
            <div class="form-group field-rutas-dia_martes">
              <input type="hidden" name="ZonaServicio[dia_martes]" value="0"><label><input type="checkbox" id="rutas-dia_martes" name="ZonaServicio[dia_martes]" value="1" <?=$model->dia_martes==1?'checked':''?>> Martes</label>
            </div>
          </div>
          <div class="col-md-2"> 
            <div class="form-group field-rutas-dia_miercoles">
              <input type="hidden" name="ZonaServicio[dia_miercoles]" value="0"><label><input type="checkbox" id="rutas-dia_miercoles" name="ZonaServicio[dia_miercoles]" value="1" <?=$model->dia_miercoles==1?'checked':''?>> Miercoles</label>
            </div>
          </div>
          <div class="col-md-1"> 
            <div class="form-group field-rutas-dia_jueves">
              <input type="hidden" name="ZonaServicio[dia_jueves]" value="0"><label><input type="checkbox" id="rutas-dia_jueves" name="ZonaServicio[dia_jueves]" value="1" <?=$model->dia_jueves==1?'checked':''?>> Jueves</label>
            </div>
          </div>
          <div class="col-md-1"> 
            <div class="form-group field-rutas-dia_viernes">
              <input type="hidden" name="ZonaServicio[dia_viernes]" value="0"><label><input type="checkbox" id="rutas-dia_viernes" name="ZonaServicio[dia_viernes]" value="1" <?=$model->dia_viernes==1?'checked':''?>> Viernes</label>
            </div>
          </div>
          <div class="col-md-1"> 
            <div class="form-group field-rutas-dia_sabado">
              <input type="hidden" name="ZonaServicio[dia_sabado]" value="0"><label><input type="checkbox" id="rutas-dia_sabado" name="ZonaServicio[dia_sabado]" value="1" <?=$model->dia_sabado==1?'checked':''?>> Sabado</label>
            </div>
          </div>
          <div class="col-md-2"> 
            <div class="form-group field-rutas-dia_domingo">
              <input type="hidden" name="ZonaServicio[dia_domingo]" value="0"><label><input type="checkbox" id="rutas-dia_domingo" name="ZonaServicio[dia_domingo]" value="1" <?=$model->dia_domingo==1?'checked':''?>> Domingo</label>
            </div>
          </div>
        </div>

        
          <br>

         <div class="alert alert-info" role="alert"><b>Editar línea:</b>
         Para mover un punto haga click sin soltar sobre un marcador principal y muevalo.</div>

         <div class="alert alert-info" role="alert"><b>Crear línea:</b>
         Para crear un nuevo punto intermedio haga click sobre un marcador secundario y muevalo.</div>

         <div class="alert alert-info" role="alert"><b>Eliminar punto:</b>
         Para eliminar un punto haga click sobre el marcador principal.</div>

         <input class="btn btn-info" type="button" onclick="editlines();" value="Editar ruta" id="EditButton">
         <br>

        
        <br>

        <div id="map_canvas" style="height:400px" ></div>

        <?= $form->field($model, 'puntos')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Ingresar' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>