<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ZonaServicioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Zona Servicios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zona-servicio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Zona Servicio', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            // 'horario',
            // 'dia_lunes',
            // 'dia_martes',
            // 'dia_miercoles',
            // 'dia_jueves',
            // 'dia_viernes',
            // 'dia_sabado',
            // 'dia_domingo',
            'hora_inicio',
            'hora_fin',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
