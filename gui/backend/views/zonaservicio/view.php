<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\ZonaServicio;

/* @var $this yii\web\View */
/* @var $model app\models\ZonaServicio */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Zona Servicios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zona-servicio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'nombre',
            [
                'label' => 'Horario',
                'value' => ZonaServicio::$horarios[$model->horario],
            ],
            'hora_inicio',
            'hora_fin',
            'dia_lunes',
            'dia_martes',
            'dia_miercoles',
            'dia_jueves',
            'dia_viernes',
            'dia_sabado',
            'dia_domingo',
        ],
    ]) ?>

</div>
