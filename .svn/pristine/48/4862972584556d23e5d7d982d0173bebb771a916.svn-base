<?php

namespace backend\controllers;

use Yii;
use app\models\ReclamoEstado;
use app\models\ReclamoEstadoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReclamoEstadoController implements the CRUD actions for ReclamoEstado model.
 */
class ReclamoEstadoController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ReclamoEstado models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReclamoEstadoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ReclamoEstado model.
     * @param integer $re_id
     * @param integer $es_id
     * @return mixed
     */
    public function actionView($re_id, $es_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($re_id, $es_id),
        ]);
    }

    /**
     * Creates a new ReclamoEstado model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ReclamoEstado();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 're_id' => $model->re_id, 'es_id' => $model->es_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ReclamoEstado model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $re_id
     * @param integer $es_id
     * @return mixed
     */
    public function actionUpdate($re_id, $es_id)
    {
        $model = $this->findModel($re_id, $es_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 're_id' => $model->re_id, 'es_id' => $model->es_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ReclamoEstado model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $re_id
     * @param integer $es_id
     * @return mixed
     */
    public function actionDelete($re_id, $es_id)
    {
        $this->findModel($re_id, $es_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ReclamoEstado model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $re_id
     * @param integer $es_id
     * @return ReclamoEstado the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($re_id, $es_id)
    {
        if (($model = ReclamoEstado::findOne(['re_id' => $re_id, 'es_id' => $es_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
