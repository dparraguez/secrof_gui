package com.example.nieves.appmovil;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

//Menú principal de la aplicación
public class MenuPrincipal extends AppCompatActivity {
    private double latitud;
    private double longitud;
    private int status;
    private String mensaje;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
        findViewById(R.id.btnSugerencia).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuPrincipal.this, Sugerencia.class));
            }
        });
        findViewById(R.id.btnReclamo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuPrincipal.this, TipoReclamo.class));
            }
        });
        findViewById(R.id.btnMisFiscalizaciones).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuPrincipal.this, MisFiscalizaciones.class));
            }
        });
        findViewById(R.id.btnInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoGps();
                ConexionInternet conn = new ConexionInternet();
                if(conn.estaConectado(MenuPrincipal.this)){
                    new infoServicio().execute();
                }
                else{
                    mensaje = "No dispone de internet en este momento, intente más tarde.";
                    mensajeInfoServicio(mensaje);
                }
            }
        });
        findViewById(R.id.btnAyuda).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MenuPrincipal.this,Ayuda.class));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //obtener datos gps, latitud y longitud. Se utilizan en Información del servicio
    public void infoGps(){
        PosicionGps gps = new PosicionGps(MenuPrincipal.this);
        latitud = gps.getLatitud();
        longitud = gps.getLongitud();
    }

    //crear json para envio de latitud y longitud. Se utiliza en Información del servicio
    public String jsonInfoServicio(){

        JSONObject jsonObj= new JSONObject();
        try {
            jsonObj.put("latitud",latitud);
            jsonObj.put("longitud", longitud);
            return jsonObj.toString();
        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    //Mensaje que se utiliza en información del servicio
    public void mensajeInfoServicio(String mensaje){
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipal.this);
        builder.setTitle("Información del Servicio");
        builder.setMessage(mensaje);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            //función onClick para volver al Menu_Principal luego de apretar 'OK' en el AlertDialog
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getBaseContext(), MenuPrincipal.class);
                startActivity(intent);
            }
        });
        builder.create();
        builder.show();
    }


    //Se envia la lat y long en un json para consultar en Información de servicio que dias pasa el camión por ese lugar el tipo de horario
    //la hora de inicion y de fin
    private class infoServicio extends AsyncTask<String, Void, String> {
        ProgressDialog ringProgressDialog;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            ringProgressDialog = ProgressDialog.show(MenuPrincipal.this, "Por favor espere ...", "Consultando información...", false);
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection conn = null;
            URL url;
            String data = "";
            StringBuilder total = new StringBuilder("");
            String lnEnd = "\r\n";
            String dbHyphen = "--";
            String boundary =  "*****";
            try
            {
                url = new URL("http://cuchodechile.no-ip.org/ubbb/webservice/gui-horario.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                OutputStream os = conn.getOutputStream();

                //datos reclamo
                data = jsonInfoServicio();

                Log.e("Resp: ", "> data:" + data);
                os.write((dbHyphen + boundary + lnEnd).getBytes());
                os.write(("Content-Disposition: form-data; name=\"data\"" + lnEnd).getBytes());
                os.write(("Content-Type: text/plain; charset=UTF-8"+ lnEnd).getBytes());
                os.write(lnEnd.getBytes());
                os.write(data.getBytes());os.write(lnEnd.getBytes());
                os.write(lnEnd.getBytes());

                os.close();

                conn.connect();

                status = conn.getResponseCode();
                Log.e("Resp: ", "> " + status);

                InputStream in = conn.getInputStream();
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line);
                }
                Log.e("Resp: ", "> resp " + total.toString());
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Resp: ", "> " + e.toString());
            }finally{
                if(conn!=null)
                    conn.disconnect();
            }
            return total.toString();
        }

        @Override
        protected void onPostExecute(String result){
            try {
                if(status == 200){
                    JSONObject json_data = new JSONObject(result);
                    String estado = json_data.getString("estado");
                    String dias[] = {"Lunes","Martes","Miércoles","Jueves","Viernes","Sábado","Domingo"};
                    String servicio[] = new String[7];
                    switch(estado) {
                        case "1":
                            mensaje = "Usted se encuentra fuera de la zona del servicio de recolección.";
                            Log.e("Resp: ", "> mensaje:" + mensaje);
                            mensajeInfoServicio(mensaje);
                            break;
                        case "2":
                            String nombre = json_data.getString("nombre");
                            Log.e("Resp: ", "> mensaje servicio:" + servicio[0]);
                            servicio[0] = json_data.getString("dia_lunes");
                            servicio[1] = json_data.getString("dia_martes");
                            servicio[2] = json_data.getString("dia_miercoles");
                            servicio[3] = json_data.getString("dia_jueves");
                            servicio[4] = json_data.getString("dia_viernes");
                            servicio[5] = json_data.getString("dia_sabado");
                            servicio[6] = json_data.getString("dia_domingo");
                            String horario = json_data.getString("horario");
                            String hora_inicio = json_data.getString("hora_inicio");
                            String hora_fin = json_data.getString("hora_fin");
                            mensaje = "El nombre de la zona donde se encuentra es " + nombre + "\n" +
                                    "Los días de servicio son: ";
                            for (int i = 0; i < 7; i++) {
                                if (servicio[i].equals("1")) {
                                    mensaje = mensaje + dias[i] + " ,";
                                }
                            }
                            if (horario.equals("1"))
                                mensaje = mensaje + "\n" + "Tipo de horario: Diurno.";
                            else
                                mensaje = mensaje + "\n" + "Tipo de horario: Nocturno.";

                            mensaje = mensaje + "\n" + "Hora de inicio: " + hora_inicio + "\n" +
                                    "Hora de fin: " + hora_fin;

                            Log.e("Resp: ", "> mensaje:" + mensaje);
                            mensajeInfoServicio(mensaje);
                            break;
                    }
                }else{
                    mensaje = "En este momento no se puede realizar su petición, intente más tarde.";
                    mensajeInfoServicio(mensaje);
                }
            } catch (Exception e) {
                Log.d("appp", e.getMessage());
            }
            ringProgressDialog.dismiss();

        }

    }

    @Override
    public void onBackPressed() {
    }
}
