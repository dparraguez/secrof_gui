<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "app_sugerencia".
 *
 * @property integer $su_id
 * @property string $su_tipo
 * @property string $su_descripcion
 * @property string $su_fecha_app
 * @property string $su_hora_app
 * @property string $su_latitud
 * @property string $su_longitud
 * @property string $su_estado
 * @property string $su_fecha_hora_web
 * @property integer $us_id
 *
 * @property AppUsuario $us
 */
class AppSugerencia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_sugerencia';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_movil');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['su_tipo'], 'required', 'message' => 'Campo requerido'],
            [['su_fecha_hora_web'], 'safe'],
            [['su_latitud', 'su_longitud'], 'safe'],
            [['su_latitud', 'su_longitud'], 'required', 'message' => 'Campo requerido, seleccione un punto en el mapa'],
            [['us_id'], 'integer'],
             [['su_descripcion'], 'required', 'message' => 'Campo requerido'],
            ['su_descripcion','string','min' => 3, 'max' => 250, 'message' => 'Mínimo 3 y máximo 25 caracteres'],
            ['su_descripcion','validatexto'],
            ['su_descripcion','validatexto2'],
            ['su_descripcion','validatexto3'],
            [['su_hora_app'], 'string', 'max' => 5],
            [['su_estado'], 'string', 'max' => 15],
            [['us_id'], 'exist', 'skipOnError' => true, 'targetClass' => AppUsuario::className(), 'targetAttribute' => ['us_id' => 'us_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'su_id' => 'Identificador',
            'su_tipo' => 'Tipo',
            'su_descripcion' => 'Descripción',
            'su_fecha_app' => 'Fecha App',
            'su_hora_app' => 'Hora App',
            'su_latitud' => 'Latitud',
            'su_longitud' => 'Longitud',
            'su_estado' => 'Estado',
            'su_fecha_hora_web' => ' Fecha Hora Web',
            'us_id' => 'ID. Usuario',
        ];
    }
      public function validatexto($attribute, $params) {
        $pattern = '/^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,3}[a-zA-ZñÑÁÉÍÓÚáéíóú]+)*)$/';
        
        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'Error sólo letras o verifique que no tenga espacios al final');
    }

  public function validatexto2($attribute, $params) {
        $pattern = '/^([a-zA-ZñÑÁÉÍÓÚáéíóú0-9º°\.\,\'\"\)\(\-\@\:\/\+]+([[:space:]]{0,2}[a-zA-ZñÑÁÉÍÓÚáéíóú0-9º°\.\,\'\"\)\(\-\@\:\/\+]+)*)$/';
        $pattern2 = '/^([0-9º°\.\,\'\"\)\(\-\@\:\/\+]+)$/';
        
        if (!preg_match($pattern, $this->$attribute))
            $this->addError($attribute, 'Error sólo letras o letras y número, verifique que no tenga espacios al final o muchos en medio.');
        if (preg_match($pattern2, $this->$attribute))
            $this->addError($attribute, 'Error No puede ser solo números o caracteres especiales');
    }

    public function validatexto3($attribute, $params) {
        $pattern2 = '/(a{3}|e{3}|i{4}|o{3}|u{3}|b{3}|c{3}|d{3}|f{3}|g{3}|h{3}|j{3}|k{3}|l{4}|m{3}|n{3}|ñ{3}|p{3}|q{3}|r{3}|s{3}|t{3}|v{3}|w{4}|x{3}|y{3}|z{3}|º{2}|°{2}|\.{2}|\'{2}|\"{2}|\){2}|\({2}|\,{2}|\-{2}|\@{2}|\:{2}|\/{3}|\+{2})/i';
        $pattern3 = '/(A{3}|E{3}|I{4}|O{3}|U{3}|B{3}|C{3}|D{3}|F{3}|G{3}|H{3}|J{3}|K{3}|L{4}|M{3}|N{3}|Ñ{3}|P{3}|Q{3}|R{3}|S{3}|T{3}|V{3}|W{4}|X{3}|Y{3}|Z{3})/i';
        $pattern4 = '/(á{3}|Á{3}|é{3}|É{3}|í{3}|Í{3}|ó{3}|Ó{3}|ú{3}|Ú{3})/i';
        $pattern5 = '/([0-9]{13})/i';
        

        if (preg_match($pattern2, $this->$attribute) OR preg_match($pattern3, $this->$attribute) OR preg_match($pattern4, $this->$attribute))
            $this->addError($attribute, 'Error, verifique que no este repetidos continuamente los caracteres');
        if (preg_match($pattern5, $this->$attribute))
            $this->addError($attribute, 'Error, no puede haber un número superior a 9999999999999');
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUs()
    {
        return $this->hasOne(AppUsuario::className(), ['us_id' => 'us_id']);
    }
}
