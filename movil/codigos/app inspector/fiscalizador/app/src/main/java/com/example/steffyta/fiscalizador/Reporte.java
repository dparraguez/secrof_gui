package com.example.steffyta.fiscalizador;


import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.PolyUtil;

//clase para el reporte que realiza el inspector en terreno
//TODO: Mejorar la validación del texto en la descripcion del reporte
public class Reporte extends AppCompatActivity implements View.OnClickListener {


    private Button btnCamara;
    private Button btnEnviar;
    private EditText re_Descripcion;
    private EditText editClave;
    private TextView textFoto;
    private String descripcion; //descripcion del Reporte
    private String hora;//hora creación del Reporte
    private String fecha;//fecha creación del Reporte
    private String latitud;
    private String longitud;
    private String rutaFile;//ruta de la foto
    private AlertDialog.Builder builder;
    private Uri uri;
    private String clave = null;
    private boolean hay_foto = false;
    private boolean respuesta;
    private int re_id_local;
    private int cons = 0;
    private final String ruta_fotos = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/misfotos/";
    private File file = new File(ruta_fotos);
    private String nombreFoto = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reporte);
        init();
    }

    //inicialización de los componentes del layout de reporte
    public void init(){
        //Si no existe crea la carpeta donde se guardaran las fotos
        file.mkdirs();

        re_Descripcion =(EditText) findViewById (R.id.re_Descripcion);
        btnCamara = (Button)findViewById(R.id.btnCamara);
        btnCamara.setOnClickListener(this);
        btnEnviar = (Button)findViewById(R.id.btnEnviar);
        btnEnviar.setOnClickListener(this);
        textFoto = (TextView)findViewById(R.id.textFoto);
        editClave = (EditText)findViewById(R.id.editClave);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reporte, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //metodo que guarda en una base de datos interna los datos ingresados por el usuario
    public boolean guardar(View v) {
        String[] desc = new String[2];
        descripcion = re_Descripcion.getText().toString();
        desc = validarDescripción(descripcion);
        if (desc[0].equals("false")) { //valida si la descripción es erronea
            re_Descripcion.setError(desc[1]);
            return false;
        } else {
                if (gps(latitud, longitud)) { //valida la lat y long son correctos
                    Log.e("Resp: ", "gps guardar");
                    Sigeco admin = new Sigeco(this);
                    SQLiteDatabase bd = admin.getWritableDatabase();

                    //obtener fecha y hora
                    SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
                    Date fechaHoy = Calendar.getInstance().getTime();
                    hora = formatoHora.format(fechaHoy);
                    fecha = formato.format(fechaHoy);

                    ContentValues Reporte = new ContentValues();

                    Reporte.put("Descripcion", descripcion);
                    Reporte.put("Fecha_C", fecha);
                    Reporte.put("Hora_C", hora);
                    Reporte.put("Latitud", latitud);
                    Reporte.put("Longitud", longitud);
                    Reporte.put("Estado", "Guardado");
                    Log.e("Resp: ", "> lat guardada" + latitud);
                    Log.e("Resp: ", "> long guardada" + longitud);
                    //guardar en bd ruta de la foto
                    if (hay_foto == true) Reporte.put("Foto", rutaFile);
                    else Reporte.put("Foto", "vacio");
                    re_id_local = (int) bd.insert("Reporte", null, Reporte);
                    bd.close();
                    return true;

                } else {
                    return false;
                }
            }
    }

    //consulta por los valores de latitud y longitud
    public boolean gps(String lat, String lng){
        respuesta = false;
        PolygonOptions concepcion;
        Boolean en_conce;
        AlertDialog.Builder builder;
        if((lat) == null && (lng == null)) { //si lat y long son nulos se obtienen de la clase PosicionGps
            Log.e("Resp: ", "> nulo");
            PosicionGps gps = new PosicionGps(Reporte.this);
            latitud = Double.toString(gps.getLatitud());
            longitud = Double.toString(gps.getLongitud());
        }
        LatLng point = new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud));
        if((latitud.equals("0.000000")) && (longitud.equals("0.000000"))){ //si lat y long son 0.00000 se pide que active el gps
            Log.e("Resp: ", "> 00000");
            String mensaje = "No tiene activado el GPS en este momento, " +
                    "presione Ajustes para activarlo.";
            mensajeAjuste(mensaje);
        }else {
            concepcion = PoligonoConcepcion.Concepcion();
            en_conce = PolyUtil.containsLocation(point, concepcion.getPoints(), false);
            if(!en_conce){//para saber si lat y long estan dentro de concepción
                builder = new AlertDialog.Builder(Reporte.this);
                builder.setTitle("Importante");
                builder.setMessage("Usted se encuentra fuera del área de Concepción, " +
                        "por favor seleccione un punto en el mapa.");
                builder.setPositiveButton("MAPA", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent mapa = new Intent(Reporte.this, Mapa.class);
                        startActivityForResult(mapa, 1);
                    }
                });
                builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                builder.create();
                builder.show();
                Log.e("Resp: ", "> ajuste");

            }else{
                return respuesta = true;
            }
        }
        Log.e("Resp: ", "> respggg:" + respuesta);
        return respuesta;
    }

    //metodo que prepara el envio al server consultando si hay internet y que datos son los que se enviaran
    public void enviarReclamoServer(){
        String mensaje;
        JsonEnvioServer jsonEnvio = new JsonEnvioServer();
        String datosEnvio;
        String[] datosUsuario;
        EnvioServer envio = new EnvioServer();
        ConexionInternet conn = new ConexionInternet();
        if (conn.estaConectado(this)){
            Sigeco db = new Sigeco(this);
            String idServer = db.idServerInspector();
            if(idServer.equals("0")){ //si Id_Server es O significa que es primera vez que envia y a los datos del repote se + los del inspector
                datosUsuario = db.datosInspector();
                if (hay_foto==true){
                    datosEnvio = jsonEnvio.jsonReporteDatosUsuario(descripcion, fecha, hora, latitud, longitud,
                            nombreFoto, datosUsuario[0], datosUsuario[1], datosUsuario[2], getNumeroTelefono());
                } else {
                    datosEnvio = jsonEnvio.jsonReporteDatosUsuario(descripcion, fecha, hora, latitud, longitud,
                            null,datosUsuario[0], datosUsuario[1], datosUsuario[2], getNumeroTelefono());
                }
            }else {
                if (hay_foto==true){
                    datosEnvio = jsonEnvio.jsonReporteDatosIdUsuario(descripcion, fecha, hora, latitud, longitud,
                            nombreFoto,idServer);
                }else {
                    datosEnvio = jsonEnvio.jsonReporteDatosIdUsuario(descripcion, fecha, hora, latitud, longitud,
                            null,idServer);
                }
            }
            envio.datosEnvio(datosEnvio,Reporte.this,hay_foto,Integer.toString(re_id_local),rutaFile);
            envio.enviarServer();
        } else {
            mensaje = "No tiene conexión a internet en este momento, " +
                    "presione Ajustes para conectarlo o ingrese a Mis Reportes" +
                    " cuando tenga conexión para volver a enviar.  ";
            mensajeAjuste(mensaje);
        }
    }

    //mensaje de ajuste
    public void mensajeAjuste(String mensaje){
        builder = new AlertDialog.Builder(Reporte.this);
        builder.setTitle("Importante");
        builder.setMessage(mensaje);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(getBaseContext(), MenuPrincipal.class);
                startActivity(intent);
            }
        });
        builder.setNeutralButton("AJUSTES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        });
        builder.create();
        builder.show();
    }

    //obtener número de telefono del dispositivo
    public String getNumeroTelefono()
    {
        return ((TelephonyManager) getSystemService(TELEPHONY_SERVICE))
                .getLine1Number();
    }

    // Metodo privado que genera un codigo unico para la foto segun la hora y fecha del sistema
    @SuppressLint("SimpleDateFormat")
    private String getCode()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
        String date = dateFormat.format(new Date());
        String photoCode = "pic_" + date;
        return photoCode;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnCamara:
                nombreFoto=  getCode() + ".png";
                rutaFile = ruta_fotos + nombreFoto;

                File mi_foto = new File(rutaFile);
                try {
                    mi_foto.createNewFile();
                } catch (IOException ex) {
                    Log.e("ERROR ", "Error:" + ex);
                }
                uri = Uri.fromFile( mi_foto );
                //Abre la camara para tomar la foto
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //Guarda imagen
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                //Retorna a la actividad
                hay_foto = true;
                startActivityForResult(cameraIntent, 2);
                break;
            case R.id.btnEnviar:
                    Sigeco db = new Sigeco(this);
                    String pass = db.claveInspector(); //obtiene la clave que el inspector ingreso cuando se registro en la app
                    if(editClave.getText().toString().equals(pass)){ //valida que la clave ingresada en el layout sea la misma que registro
                        boolean g = guardar(v);
                        if(g){
                            enviarReclamoServer();
                        }
                    }
                    else{
                        Toast.makeText(this, "Su clave es incorrecta.", Toast.LENGTH_LONG).show();
                    }
                break;
        }
    }

    //recibe la latitud y longitud del mapa
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Resp: ", "> forresult ");
            if(resultCode == RESULT_OK) {
                if (requestCode == 1) {
                    latitud = data.getStringExtra("latitud");
                    longitud = data.getStringExtra("longitud");
                    Log.e("Resp: ", "> lat:" + latitud + " long:" + longitud);
                } else if (requestCode == 2) {
                    textFoto.setText("Foto adjunta");
                }
            }
    }

    //valida el texto ingresado en la descripción del resporte
    public String[] validarDescripción(String descripcion) {
        String[] respuesta = new  String[2];
        String reg = "^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,2}[a-zA-ZñÑÁÉÍÓÚáéíóú0-9\\.|\\'|\\\"|\\)|\\#|\\_|\\$|\\%|\\(|\\,|\\-|\\@|\\?|\\¿|\\:|\\/|\\+]+)*)$";
        Log.e("Resp: ", "> reg:" + reg );
        if(descripcion.equals("")) {
            respuesta[0]="false"; respuesta[1]="La descripción esta vacía.";
            return respuesta;
        }else{
            if (descripcion.length() >= 15){ //descripción mayor igual a 15 caracteres
                if(descripcion.length() <= 250){ //descripción menor igual a 250 caracteres
                    Pattern pattern = Pattern.compile(reg);
                    Log.e("Resp: ", "> compila");
                    Matcher matcher = pattern.matcher(descripcion);
                    Log.e("Resp: ", "> matcher:"+matcher);
                    boolean valid = matcher.find();
                    Log.e("Resp: ", "> vali:"+valid);
                    if (valid){
                        respuesta[0]="true";
                        return respuesta;
                    }
                    else{
                        respuesta[0]="false"; respuesta[1]="La descripción no es válida.";
                        return respuesta;
                    }
                }
                else{
                    respuesta[0]="false"; respuesta[1]="La descripción ingresada es demasiado larga, máximo 250 caracteres.";
                    return respuesta;
                }
            }
            else{
                respuesta[0]="false"; respuesta[1]="La descripción ingresada es muy corta.";
                return respuesta;
            }

        }

    }


    @Override
    public void onBackPressed() {
        startActivity(new Intent(getBaseContext(), MenuPrincipal.class));
    }
}
