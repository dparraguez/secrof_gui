package com.example.steffyta.fiscalizador;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class EnvioServer {
    private String datos;
    private Context context;
    private boolean hay_foto = false;
    private String rutaFoto;
    private int status;
    private String id_local; //id local del reporte que se envia

    //inicializa los valores necesarios para el envio de información al server
    public void datosEnvio(String datos, Context context, boolean hay_foto, String id_local, String rutaFoto){
        this.datos = datos; //json con los datos del reporte
        this.context = context;
        this.hay_foto = hay_foto; //para saber si el reporte tiene foto
        this.id_local = id_local; //id local del reporte para hacer la actualización del id_server una vez almacenado en el server
        this.rutaFoto = rutaFoto; //ruta en el dispositivo de la foto
    }

    //metodo que llama a metodo AsyncTask que realiza el envio de los datos al server
    public void enviarServer(){
        new enviar().execute();
    }

    //Crea un Bitmap para en envio de la foto (si existe) al server
    public Bitmap createBitMap(String ruta){
        Bitmap foto = null;
        File imgFile = new  File(ruta);
        if(imgFile.exists()){
            foto = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(foto,300, 300, false);
        return  resizedBitmap;
    }

    //metodo que muestra un mensaje una vez realizado el envio
    public void mensajeEnvio(String mensaje){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Importante");
        builder.setMessage(mensaje);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            //función onClick para volver al Menu_Principal luego de apretar 'OK' en el AlertDialog
            @Override
            public void onClick(DialogInterface dialog, int which){
                String nombreClass = context.getClass().getSimpleName();
                if(nombreClass.equals("MisReportes")){
                    Intent intent = new Intent(context, MisReportes.class);
                    context.startActivity(intent);
                }else{
                    Intent intent = new Intent(context, MenuPrincipal.class);
                    context.startActivity(intent);
                }
            }
        });
        builder.create();
        builder.show();
    }

    //Actualiza estado y id_server de los datos enviado
    public void datosRespServer(int control, String us_id, String id_server, String id_local ){
        Sigeco db = new Sigeco(context);
        switch (control){
            case 1: //Id_Server Inspector +Id_Server Reporte +  Estado Reporte 'Enviado'
                db.actualizaIdServer(1, us_id, null); // 1.- Inspector
                db.actualizaIdServer(2, id_server, id_local); // 2.- Reporte
                db.actualizaEstadoReporte(2, id_local); // 2.- Estado reporte 'Enviado'
                break;
            case 2: //Id_Server Reporte + Estado Reporte 'Enviado'
                db.actualizaIdServer(2, id_server, id_local); //2.- Reporte
                db.actualizaEstadoReporte(2, id_local); // 2.- Estado Reporte 'Enviado'
                break;
        }
        db.close();
    }


    //AsyncTask para el envio de los datos al server
    private class enviar extends AsyncTask<String, Void, String> {
        ProgressDialog ringProgressDialog;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            ringProgressDialog = ProgressDialog.show(context, "Por favor espere ...", "Enviando datos...", false);
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection conn = null;
            URL url;
            String data = "";
            StringBuilder total = new StringBuilder("");
            String lnEnd = "\r\n";
            String dbHyphen = "--";
            String boundary =  "*****";
            try
            {
                url = new URL("http://cuchodechile.no-ip.org/ubbb/webservice/app-inspector.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("imageToUpload", "ups.jpg");
                OutputStream os = conn.getOutputStream();

                //datos envio
                data = datos;

                Log.e("Resp: ", "> data:" + data);
                os.write((dbHyphen + boundary + lnEnd).getBytes());
                os.write(("Content-Disposition: form-data; name=\"data\"" + lnEnd).getBytes());
                os.write(("Content-Type: text/plain; charset=UTF-8"+ lnEnd).getBytes());
                os.write(lnEnd.getBytes());
                os.write(data.getBytes());os.write(lnEnd.getBytes());
                os.write(lnEnd.getBytes());
                if (hay_foto==true){
                    os.write((dbHyphen + boundary + lnEnd).getBytes());
                    os.write(("Content-Disposition: form-data; name=\"imageToUpload\";filename="+ "ups.jpg" + lnEnd).getBytes());
                    os.write(("Content-Type: image/jpeg; charset=UTF-8" + lnEnd).getBytes());
                    os.write((lnEnd).getBytes());
                    Log.e("Resp: ", "> if hay foto");
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    Bitmap foto = createBitMap(rutaFoto);
                    foto.compress(Bitmap.CompressFormat.PNG, 100, bos);
                    byte[] bitmapdata = bos.toByteArray();
                    os.write(bitmapdata);
                    os.write((lnEnd).getBytes());
                    os.write((dbHyphen + boundary + dbHyphen + lnEnd).getBytes());
                }
                os.close();
                conn.connect();

                status = conn.getResponseCode();

                InputStream in = conn.getInputStream();
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                if(conn!=null)
                    conn.disconnect();
            }
            return total.toString();
        }

        @Override
        protected void onPostExecute(String result){
            try {
                String mensaje;
                //consulta si se realizo la conexión con el server
                if(status == 200) {
                    JSONObject json_data = new JSONObject(result);
                    String control = json_data.getString("control");
                    String in_id, rp_id;
                    //utiliza la variable control para saber si fue primera vez que se envian datos al server
                    switch (control) {
                        case "1":
                            in_id = json_data.getString("in_id"); //id con que se almaceno el inspector en el server
                            rp_id = json_data.getString("rp_id"); //Id con que se almaceno el reporte en el Server
                            datosRespServer(1, in_id, rp_id, id_local);
                            mensaje = "El reporte ha sido enviado exitosamente,registrado con el número: #" + rp_id +
                                    ", número de inspector es #" + in_id+".";
                            mensajeEnvio(mensaje);
                            break;
                        case "2":
                            rp_id = json_data.getString("rp_id"); //id con que se almaceno el reporte en el server
                            datosRespServer(2, null, rp_id, id_local);
                            mensaje = "El reporte ha sido enviado exitosamente, registrado con el número: #" + rp_id+".";
                            mensajeEnvio(mensaje);
                            break;
                    }
                }else{
                    mensaje = "En este momento no se puede realizar su petición, intente más tarde.";
                    mensajeEnvio(mensaje);
                }
            } catch (Exception e) {
                Log.d("appp", e.getMessage());
            }
            ringProgressDialog.dismiss();

        }

    }
}
