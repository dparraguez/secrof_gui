package com.example.steffyta.fiscalizador;

//Tabla Inspector base de datos del dispositivo
public class EInspector {
    public static final String TABLE_NAME_INSPECTOR = "Inspector";
    public static final String FIELD_ID = " Id ";
    public static final String FIELD_Nombres = " Nombres ";
    public static final String FIELD_Apellidos = " Apellidos ";
    public static final String FIELD_Rut = " Rut ";
    public static final String FIELD_Clave = " Clave ";
    public static final String FIELD_Id_Server="Id_Server";
    public static final String CREATE_DB_TABLE=" CREATE TABLE "+ TABLE_NAME_INSPECTOR + "(" +
            FIELD_ID + " integer primary key autoincrement,"+
            FIELD_Nombres + " text,"+
            FIELD_Apellidos + " text,"+
            FIELD_Rut + " text,"+
            FIELD_Clave + "text,"+
            FIELD_Id_Server + " Integer Default 0); ";

    private int Id;
    private String Nombres;
    private String Apellidos;
    private String Rut;
    private String Clave;
    private int Id_Server;


    public EInspector(String nombres, String apellidos, String rut, String clave) {
        this.Nombres = nombres;
        this.Apellidos = apellidos;
        this.Rut = rut;
        this.Clave = clave;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        this.Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        this.Apellidos = apellidos;
    }

    public String getRut() {
        return Rut;
    }

    public void setRut(String rut) {
        this.Rut = rut;
    }

    public String getClave() {
        return Clave;
    }

    public void setClave(String clave) {
        this.Clave = clave;
    }

    public int getId_Server() {
        return Id_Server;
    }

    public void setId_Server(int id_Server) {
        this.Id_Server = id_Server;
    }
}

