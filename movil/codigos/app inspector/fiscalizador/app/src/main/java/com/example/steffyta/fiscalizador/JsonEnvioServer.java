package com.example.steffyta.fiscalizador;

import org.json.JSONException;
import org.json.JSONObject;

//Crea los json para el envio de la información al server
public class JsonEnvioServer{

    //crear json para envio de datos Reporte + datos Usuario
    public String jsonReporteDatosUsuario(String descripcion, String fecha, String hora,  String lat,
                                          String lng, String foto, String nombres, String apellidos, String rut, String fono) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("control", 1);
            jsonObj.put("descripcion", descripcion);
            jsonObj.put("fecha", fecha);
            jsonObj.put("hora", hora);
            jsonObj.put("latitud", lat);
            jsonObj.put("longitud", lng);
            jsonObj.put("foto", foto);
            jsonObj.put("nombres", nombres);
            jsonObj.put("apellidos", apellidos);
            jsonObj.put("rut", rut);
            jsonObj.put("fono", fono);
            return jsonObj.toString();
        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }


    //crear json para envio de datos Reporte + Id Usuario
    public String jsonReporteDatosIdUsuario( String descripcion, String fecha, String hora,
                                             String lat, String lng, String foto,String id_usuario) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("control", 2);
            jsonObj.put("descripcion", descripcion);
            jsonObj.put("fecha", fecha);
            jsonObj.put("hora", hora);
            jsonObj.put("foto", foto);
            jsonObj.put("latitud", lat);
            jsonObj.put("longitud", lng);
            jsonObj.put("usuario", id_usuario);
            return jsonObj.toString();
        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

}
