package com.example.steffyta.fiscalizador;

//Tabla Reporte en la base de datos del dispositivo
public class EReporte {
    public static final  String TABLE_NAME_EReporte = "Reporte";
    public static final String FIELD_ID = " Id ";
    public static final String FIELD_Descripcion = " Descripcion ";
    public static final String FIELD_Fecha_C = " Fecha_C ";
    public static final String FIELD_Hora_C= " Hora_C ";
    public static final String FIELD_Latitud = " Latitud ";
    public static final String FIELD_Longitud = " Longitud ";
    public static final String FIELD_Foto = " Foto ";
    public static final String FIELD_Estado = " Estado ";
    public static final String FIELD_Id_Server="Id_Server";
    public static final String CREATE_DB_TABLE=" CREATE TABLE "+TABLE_NAME_EReporte+ "(" +
            FIELD_ID + " integer primary key autoincrement,"+
            FIELD_Descripcion + " text not null,"+
            FIELD_Fecha_C + " text,"+
            FIELD_Hora_C + " text,"+
            FIELD_Latitud + " text,"+
            FIELD_Longitud + " text,"+
            FIELD_Foto + " text,"+
            FIELD_Estado + " text,"+
            FIELD_Id_Server + " Integer Default 0); ";


    private int Id;
    private String Descripcion;
    private String Latitud;
    private String Longitud;
    private String Fecha_C;
    private String Hora_C;
    private String Foto;
    private String Estado;
    private int Id_Server;

    public EReporte(String descripcion, String latitud, String longitud, String fecha_C, String hora_C,String Foto, String estado) {

        this.Descripcion = descripcion;
        this.Latitud = latitud;
        this.Longitud = longitud;
        this.Fecha_C = fecha_C;
        this.Hora_C = hora_C;
        this.Foto = Foto;
        this.Estado = estado;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.Descripcion = descripcion;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String latitud) {
        this.Latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        this.Longitud = longitud;
    }

    public String getFecha_C() {
        return Fecha_C;
    }

    public void setFecha_C(String fecha_C) {
        this.Fecha_C = fecha_C;
    }

    public String getHora_C() {
        return Hora_C;
    }

    public void setHora_C(String hora_C) {
        this.Hora_C = hora_C;
    }

    public String getFoto() {return Foto;}

    public void setFoto(String foto) {this.Foto = foto;}

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        this.Estado = estado;
    }

    public int getId_Server() {
        return Id_Server;
    }

    public void setId_Server(int id_Server) {
        this.Id_Server = id_Server;
    }
}
