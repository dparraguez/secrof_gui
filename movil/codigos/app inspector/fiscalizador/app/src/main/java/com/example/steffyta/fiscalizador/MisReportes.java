package com.example.steffyta.fiscalizador;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.database.Cursor;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.PolyUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

//muestra los resportes que inspector a realizado el id, la fecha y el estado del reporte
public class MisReportes extends AppCompatActivity {

    private TabHost th;
    private ListView listReporte;
    private TextView textoR;
    private Sigeco db;
    private String[] datosUsuario;
    private String idServer;
    private String nombreFoto;
    private String latitud;
    private String longitud;
    private String seleccion;
    private String[] datos;
    private EnvioServer envio = new EnvioServer();
    private JsonEnvioServer jsonEnvio = new JsonEnvioServer();
    private String datosEnvio;
    private Boolean hay_foto = false;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_mis_reportes);

            //Inicialización TabHost
            th = (TabHost) findViewById(R.id.tabHost);
            //Tab1 Pestaña Reportes
            th.setup();
            TabHost.TabSpec ts1 = th.newTabSpec("Tab1");
            ts1.setIndicator("Reportes");
            ts1.setContent(R.id.tab1);
            th.addTab(ts1);

            textoR = (TextView) findViewById(R.id.tvReportes);

            db = new Sigeco(this);
            Cursor reportes = db.reportesTodos(); //cursor con todos los reportes realizados por el inspector

            idServer = db.idServerInspector(); //Obtiene el id server del inspector
            datosUsuario = db.datosInspector(); //Obtiene los datos del inspector

            //Llena una lista con los reportes realizados
            if (reportes.moveToFirst()) {
                listReporte = (ListView) findViewById(R.id.listReporte);
                ArrayList<HashMap<String, String>> feedList = new ArrayList<HashMap<String, String>>();
                HashMap<String, String> map;
                do {
                    String id = reportes.getString(0);
                    String fecha = reportes.getString(1);
                    String estado = reportes.getString(2);
                    map = new HashMap<String, String>();
                    map.put("Id", id);
                    map.put("Fecha", fecha);
                    map.put("Estado", estado);
                    feedList.add(map);
                } while (reportes.moveToNext());
                SimpleAdapter simpleAdapter = new SimpleAdapter(this, feedList, R.layout.view_item, new String[]{"Id", "Fecha", "Estado"},
                        new int[]{R.id.textViewId, R.id.textViewFecha, R.id.textViewEstado});
                listReporte.setAdapter(simpleAdapter);
                listReporte.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    //para mostrar los datos del resporte que se selecciona
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position,
                                            long id) {
                        seleccion = ((TextView) view.findViewById(R.id.textViewId)).getText().toString(); //Obtiene el id local del reporte
                        datos = db.datosReporte(Integer.parseInt(seleccion)); //Obtiene los datos almacenados del reporte seleccionado
                        builder = new AlertDialog.Builder(MisReportes.this);
                        builder.setTitle("Reporte");
                        builder.setMessage("Mensaje: " + datos[1]);
                        Log.e("Resp: ", "> mensaje:"+ datos[1]+" "+datos[2]+" "+datos[3]+" "+datos[4]+" "+datos[5]+" "+datos[6]+" "+datos[7]+" "+datos[8]);
                        if (!datos[6].equals("vacio")) { //para mostrar la foto en un mensaje si es que hay
                            hay_foto = true;
                            ImageView imageView = new ImageView(getBaseContext());
                            imageView.setAdjustViewBounds(true);
                            imageView.setImageBitmap(createBitMap(datos[6]));
                            builder.setView(imageView);
                            Log.e("Resp: ", "> despues fotos");
                        }
                        if (datos[7].equals("Guardado")) { //si el reporte tiene estado guardado muestra btn cancelar, eliminar y enviar sino solo btn OK
                            ConexionInternet conn = new ConexionInternet();
                            Log.e("Resp: ", "> dsp internet" );
                            if (conn.estaConectado(MisReportes.this)) {
                                Log.e("Resp: ", "> tiene internet" );
                                builder.setPositiveButton("ENVIAR", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Log.e("Resp: ", "> btn enviar");
                                        Log.e("Resp: ", "> lat:"+datos[4]+"long:"+datos[5]);
                                        if (esConce(datos[4], datos[5])) {
                                            Log.e("Resp: ", "> dentro de conce lat:" + datos[4] + " longitud:" + datos[5]);
                                            if (idServer.equals("0")) {
                                                if (hay_foto == true) {
                                                    datosEnvio = jsonEnvio.jsonReporteDatosUsuario(datos[1], datos[2], datos[3], datos[4], datos[5],
                                                            nombreFoto, datosUsuario[0], datosUsuario[1], datosUsuario[2], getNumeroTelefono());
                                                } else {
                                                    datosEnvio = jsonEnvio.jsonReporteDatosUsuario(datos[1], datos[2], datos[3], datos[4], datos[5], null,
                                                            datosUsuario[0], datosUsuario[1], datosUsuario[2], getNumeroTelefono());
                                                }
                                            } else {
                                                if (hay_foto == true) {
                                                    datosEnvio = jsonEnvio.jsonReporteDatosIdUsuario(datos[1], datos[2], datos[3], datos[4], datos[5],
                                                    nombreFoto, idServer);
                                                } else {
                                                    datosEnvio = jsonEnvio.jsonReporteDatosIdUsuario(datos[1], datos[2], datos[3], datos[4], datos[5],
                                                            null, idServer);
                                                }
                                            }
                                            envio.datosEnvio(datosEnvio, MisReportes.this, hay_foto, seleccion, datos[6]);
                                            envio.enviarServer();
                                        } else {
                                            mapa(2);
                                        }
                                    }
                                });
                            }
                            builder.setNeutralButton("ELIMINAR", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    db.actualizaEstadoReporte(1, seleccion);
                                    startActivity(new Intent(MisReportes.this, MisReportes.class));
                                }
                            });
                            builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });
                        } else {
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                        }
                        builder.create();
                        builder.show();

                    }
                });
            } else {
                textoR.setText("No ha realizado ningún reporte.");
            }
    }

    //obtener número de telefono del dispositivo
    public String getNumeroTelefono()
    {
        return ((TelephonyManager) getSystemService(TELEPHONY_SERVICE))
                .getLine1Number();
    }

    //bitmap para mostrar una imagen si es que el reporte tuviese
    public Bitmap createBitMap(String ruta){
        Bitmap foto = null;
        File imgFile = new  File(ruta);
        nombreFoto = imgFile.getName();
        if(imgFile.exists()){
            foto = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }

        Bitmap resizedBitmap = Bitmap.createScaledBitmap(foto,200, 200, false);
        return  resizedBitmap;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_mis_reportes, menu);
            return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
    }

    //consulta si la lat y long corresponden a concepción
    public boolean esConce(String latitud, String longitud){
        PolygonOptions concepcion;
        Boolean en_conce;
        AlertDialog.Builder builder;
        LatLng point = new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud));
        concepcion = PoligonoConcepcion.Concepcion();
        en_conce = PolyUtil.containsLocation(point, concepcion.getPoints(), false);
        if(en_conce) return true;
        else return false;
    }

    //muestra un mapa para seleccionar un punto en caso que no se este en concepción
    public void mapa(final int control){
        String mensaje = "";
        if(control == 1){
            mensaje = "La dirección de su sugerencia o felicitación esta fuera de Concepción," +
                    "por favor seleccione un punto en el mapa. ";
        }
        else if(control ==2){
            mensaje = "La dirección de su reclamo esta fuera de Concepción, por favor seleccione" +
                    " un punto en el mapa.";
        }
        Log.e("Resp: ", "> mapa");
        builder = new AlertDialog.Builder(MisReportes.this);
        builder.setTitle("Importante");
        builder.setMessage(mensaje);
        builder.setPositiveButton("MAPA", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent mapa = new Intent(MisReportes.this, Mapa.class);
                startActivityForResult(mapa,control);
            }
        });
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.create();
        builder.show();
    }

    //obtiene la latitud y longitud que se seleccionaron en el mapa
    @Override
    protected void  onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            latitud = data.getStringExtra("latitud");
            longitud = data.getStringExtra("longitud");
            if(requestCode == 1){
                db.actualizaLatLng(latitud,longitud,seleccion);
                Toast.makeText(this, "Se ha actualizado la ubicación del reporte.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getBaseContext(), MenuPrincipal.class));
    }
}
