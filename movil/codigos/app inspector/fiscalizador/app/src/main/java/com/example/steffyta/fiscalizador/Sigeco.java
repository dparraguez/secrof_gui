package com.example.steffyta.fiscalizador;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//Para crear la base de datos del dispositivo
public class Sigeco extends SQLiteOpenHelper {
    //crea base de datos llamada app
    private static final String DB_NAME= "app";
    private static final int SCREME_VERSION =1;
    private SQLiteDatabase app;

    public Sigeco(Context context){
        super(context, DB_NAME, null, SCREME_VERSION);
        app=this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(EInspector.CREATE_DB_TABLE);
        db.execSQL(EReporte.CREATE_DB_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    //Retorna id_server (id_server : valor que se le asigana en la bd web al inspector)
    public String claveInspector(){
        Cursor c =  app.rawQuery("SELECT Clave FROM Inspector", null);
        String clave = null;
        if (c.moveToFirst()) { //verificamos se el cursor tiene valores
            clave = c.getString(0);
        }
        return clave;
    }

    //Retorna id_server (id_server : valor que se le asigana en la bd web al inspector)
    public String idServerInspector(){
        Cursor c =  app.rawQuery("SELECT Id_Server FROM Inspector", null);
        String id = null;
        if (c.moveToFirst()) { //verificamos se el cursor tiene valores
            id = c.getString(0);
        }
        return id;
    }

    //Retorna un arreglo con el nombres, apellidos, rut y clave del inspector
    public String[] datosInspector(){
        String resultado[] = new String[4];
        Cursor c =  app.rawQuery("SELECT Nombres, Apellidos, Rut, Clave FROM Inspector", null);
        if (c.moveToFirst()) { //verificamos se el cursor tiene valores
            resultado[0] = c.getString(0); //Nombres
            resultado[1] = c.getString(1); //Apellidos
            resultado[2] = c.getString(2); //Rut
            resultado[3] = c.getString(3); //Clave
        }
        return resultado;
    }

    //Retorna un cursor con todos los reportes (guardados y enviados) realizados por el inspector
    public Cursor reportesTodos(){
        return app.rawQuery("SELECT  Id, Fecha_C, Estado FROM Reporte Where Estado != 'Eliminado'", null);
    }

    //Retorna los datos de un reporte en específico según su id
    public String[] datosReporte( int id){
        String resultado[] = new String[9];
        Cursor c = app.rawQuery("Select * From Reporte Where Id=" + id, null);
        if (c.moveToFirst()) { //verificamos se el cursor tiene valores
            resultado [0] = c.getString(0); //Id
            resultado [1] = c.getString(1); //Descripción
            resultado [2] = c.getString(2); //Fecha creación
            resultado [3] = c.getString(3); //Hora creación
            resultado [4] = c.getString(4); //Latitud
            resultado [5] = c.getString(5); //Longitud
            resultado [6] = c.getString(6); //Foto
            resultado [7] = c.getString(7); //Estado
            resultado [8] = c.getString(8); //Id_Server
        }
        return resultado;
    }

    //Actualiza el estado de un reporte según su id
    public void actualizaEstadoReporte(int accion, String id){
        switch (accion){
            case 1: // Estado Eliminado
                app.execSQL("UPDATE Reporte SET Estado='Eliminado' WHERE Id=" + id);
                break;
            case 2: // Estado Enviado
                app.execSQL("UPDATE Reporte SET Estado='Enviado' WHERE Id=" + id);
                break;
        }
    }

    //Actualiza id_server del Inspector y Reporte según la variable tabla : 1.-Inspector, 2.-Reporte
    public void actualizaIdServer(int tabla, String id_server , String id_local){
        switch (tabla){
            case 1: // Inspector
                app.execSQL("UPDATE Inspector SET Id_Server=" + id_server);
                break;
            case 2: // Reporte
                app.execSQL("UPDATE Reporte SET Id_Server=" + id_server + " WHERE Id=" + id_local);
                break;
        }
    }

    //Actualiza la latitud y longitud en caso que se haya guardado una que no era de concepción
    public void actualizaLatLng(String latitud, String longitud , String id_local){
        Log.e("Resp: ", "> act lat lng reporte");
        app.execSQL("UPDATE Reporte SET Latitud=" + latitud + ", Longitud ="+ longitud+" WHERE Id=" + id_local);
    }

    public void close(){
        app.close();
    }
}
