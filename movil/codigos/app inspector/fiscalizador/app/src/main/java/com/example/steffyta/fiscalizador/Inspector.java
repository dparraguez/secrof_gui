package com.example.steffyta.fiscalizador;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Class para el registro de los datos del inspector nombres, apellidos, rut y clave
//TODO: Mejorar la validación de texto en nombres y apellidos
public class Inspector extends AppCompatActivity implements View.OnClickListener {

    Button in_Login;
    EditText in_Nombres, in_Apellidos, in_Rut, in_Clave;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inspector);

        in_Nombres = (EditText) findViewById(R.id.in_Nombres);
        in_Apellidos = (EditText) findViewById(R.id.in_Apellidos);
        in_Rut = (EditText) findViewById(R.id.in_Rut);
        in_Clave = (EditText) findViewById(R.id.in_Clave);
        in_Login = (Button) findViewById(R.id.in_Login);
        in_Login.setOnClickListener(this);
        findViewById(R.id.in_Login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean g = guardar(v);
                if (g) startActivity(new Intent(Inspector.this, MenuPrincipal.class));
            }
        });

    }

    //Guarda los datos del inspector en la base de datos del dispositivo
    public boolean guardar(View v) {
        String[] nombre = new String[2];
        String[] apellido = new String[2];
        nombre = validarNombres(in_Nombres.getText().toString());
        apellido = validarApellidos(in_Apellidos.getText().toString());
        if (nombre[0].equals("true")) { //valida los nombres
            if ( apellido[0].equals("true")){ //valida los apellidos
                if(!in_Clave.getText().toString().equals("")) {
                    if (validarRut(in_Rut.getText().toString())) {
                        Sigeco admin = new Sigeco(this);
                        SQLiteDatabase bd = admin.getWritableDatabase();
                        String nombres = in_Nombres.getText().toString();
                        String apellidos = in_Apellidos.getText().toString();
                        String clave = in_Clave.getText().toString();
                        String rut = in_Rut.getText().toString();
                        rut = formatear(rut);
                        ContentValues registro = new ContentValues();
                        registro.put("Nombres", nombres);
                        registro.put("Apellidos", apellidos);
                        registro.put("Clave", clave);
                        registro.put("Rut", rut);
                        bd.insert("Inspector", null, registro);
                        bd.close();
                        Toast.makeText(this, "Se han guardado sus datos exitosamente.", Toast.LENGTH_SHORT).show();
                        return true;
                    } else {
                        in_Rut.setError("Ingrese R.U.T. válido.");
                        return false;
                    }
                }else{
                    in_Clave.setError("La clave esta vacía.");
                    return false;
                }
        } else {
            in_Apellidos.setError(apellido[1]);
            return false;
        }
    } else {
        in_Nombres.setError(nombre[1]);
        return false;
    }
    }

    //valida el rut ingresado
    public static boolean validarRut(String rut) {
        boolean validacion = false;
        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            if(!rut.equals("11111111")){
                int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

                char dv = rut.charAt(rut.length() - 1);

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10) {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char) (s != 0 ? s + 47 : 75)) {
                    validacion = true;
                }
            }
            else validacion = false;

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

    //Le da un formato al rut ingresado, rut sin puntos y con guion
    static public String formatear(String rut){
        int cont=0;
        String format;
        if(rut.length() == 0){
            return "";
        }else{
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            format = "-"+rut.substring(rut.length()-1);
            for(int i = rut.length()-2;i>=0;i--){
                format = rut.substring(i, i+1)+format;
                cont++;
            }
            return format;
        }
    }

    //valida los nombres ingresados
    public String[] validarNombres(String nombres) {
        String[] respuesta = new  String[2];
        String reg = "^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,3}[a-zA-ZñÑÁÉÍÓÚáéíóú]+)*)$";
        Log.e("Resp: ", "> reg:" + reg);
        if (nombres.length() >= 3){ //nombre mayor igual a 3 caracteres
            if(nombres.length() <= 25){ //nombre menor igual a 25 caracteres
                Pattern pattern = Pattern.compile(reg);
                Log.e("Resp: ", "> compila");
                Matcher matcher = pattern.matcher(nombres);
                Log.e("Resp: ", "> matcher:"+matcher);
                boolean valid = matcher.find();
                Log.e("Resp: ", "> vali:"+valid);
                if (valid){
                    respuesta[0]="true";
                    return respuesta;
                }
                else{
                    respuesta[0]="false"; respuesta[1]="El nombre ingresado no es válido.";
                    return respuesta;
                }
            }
            else{
                respuesta[0]="false"; respuesta[1]="El nombre ingresado es demasiado largo, máximo 25 caracteres.";
                return respuesta;
            }
        }
        else{
            respuesta[0]="false"; respuesta[1]="El nombre ingresado es muy corto.";
            return respuesta;
        }
    }

    //Valida los apellidos ingresados
    public String[] validarApellidos(String apellidos) {
        String[] respuesta = new  String[2];
        String reg = "^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,2}[a-zA-ZñÑÁÉÍÓÚáéíóú]+)*)$";
        Log.e("Resp: ", "> reg:" + reg );
        if (apellidos.length() >= 2){ //apellidos mayor igual a 2 caracteres
            if(apellidos.length() <= 25){ //apellidos menor igual a 25 caracteres
                Pattern pattern = Pattern.compile(reg);
                Log.e("Resp: ", "> compila");
                Matcher matcher = pattern.matcher(apellidos);
                Log.e("Resp: ", "> matcher:"+matcher);
                boolean valid = matcher.find();
                Log.e("Resp: ", "> vali:"+valid);
                if (valid){
                    respuesta[0]="true";
                    return respuesta;
                }
                else{
                    respuesta[0]="false"; respuesta[1]="El apellido ingresado no es válido.";
                    return respuesta;
                }
            }
            else{
                respuesta[0]="false"; respuesta[1]="El apellido ingresado es demasiado largo, máximo 25 caracteres.";
                return respuesta;
            }
        }
        else{
            respuesta[0]="false"; respuesta[1]="El apellido ingresado es muy corto.";
            return respuesta;
        }

    }


    public void onClick(View v) {
        Intent intent = new Intent(Inspector.this, MenuPrincipal.class);
        startActivity(intent);
    }

}