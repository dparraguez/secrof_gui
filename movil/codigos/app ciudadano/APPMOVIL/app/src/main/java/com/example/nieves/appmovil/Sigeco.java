package com.example.nieves.appmovil;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//crea la base de datos interna
public class Sigeco extends SQLiteOpenHelper {
    //crea base de datos llamada app
    private static final String DB_NAME= "app";
    private static final int SCREME_VERSION =1;
    private SQLiteDatabase app;

    public Sigeco(Context context){
        super(context,DB_NAME,null,SCREME_VERSION);
        app=this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(EUsuario.CREATE_DB_TABLE);
        db.execSQL(EReclamo.CREATE_DB_TABLE);
        db.execSQL(ESugerencia.CREATE_DB_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    //Retorna id_server (id_server : valor que se le asigana en la bd web al usuario)
    public String idServerUsuario(){
        Cursor c =  app.rawQuery("SELECT Id_Server FROM Usuario", null);
        String id = null;
        if (c.moveToFirst()) { //verificamos se el cursor tiene valores
            id = c.getString(0);
        }
        return id;
    }

    //Retorna un arreglo con el nombres, apellidos y rut del usuario
    public String[] datosUsuario(){
        String resultado[] = new String[3];
        Cursor c =  app.rawQuery("SELECT Nombres, Apellidos, Rut FROM Usuario", null);
        if (c.moveToFirst()) { //verificamos se el cursor tiene valores
            resultado[0] = c.getString(0); //Nombres
            resultado[1] = c.getString(1); //Apellidos
            resultado[2] = c.getString(2); //Rut
        }
        return resultado;
    }

    //Retorna un cursor con todos los reclamos (guardados y enviados) realizados por el usuario
    public Cursor reclamosTodos(){
       return app.rawQuery("SELECT  Id, Tipo, Fecha_C, Estado FROM Reclamo Where Estado != 'Eliminado'", null);
    }

    //Retorna un cursor con todos las sugerencias y felicitaciones (guardadas y enviadas) realizados por el usuario
    public Cursor sugerenciasTodas(){
        return app.rawQuery("SELECT Id, Tipo, Fecha_C, Estado FROM Sugerencia Where Estado != 'Eliminado' ", null);
    }

    //Retorna los datos de un reclamo en específico según su id
    public String[] datosReclamo( int id){
        String resultado[] = new String[10];
        Cursor c = app.rawQuery("Select * From Reclamo Where Id=" + id, null);
        if (c.moveToFirst()) { //verificamos se el cursor tiene valores
            resultado [0] = c.getString(0); //Id
            resultado [1] = c.getString(1); //Tipo
            resultado [2] = c.getString(2); //Descripción
            resultado [3] = c.getString(3); //Fecha creación
            resultado [4] = c.getString(4); //Hora creación
            resultado [5] = c.getString(5); //Foto
            resultado [6] = c.getString(6); //Latitud
            resultado [7] = c.getString(7); //Longitud
            resultado [8] = c.getString(8); //Estado
            resultado [9] = c.getString(9); //Id_Server
        }
        return resultado;
    }

    //Retorna los datos de una sugerencia en específica según su id
    public String[] datosSugerencia( int id){
        String resultado[] = new String[9];
        Cursor c = app.rawQuery("Select * From Sugerencia Where Id=" + id, null);
        if (c.moveToFirst()) { //verificamos se el cursor tiene valores
            resultado[0] = c.getString(0); //Id
            resultado[1] = c.getString(1); //Tipo
            resultado[2] = c.getString(2); //Descripción
            resultado[3] = c.getString(3); //Fecha creación
            resultado[4] = c.getString(4); //Hora creación
            resultado[5] = c.getString(5); //Latitud
            resultado[6] = c.getString(6); //Longitud
            resultado[7] = c.getString(7); //Estado
            resultado[8] = c.getString(8); //Id_Server
        }
        return resultado;
    }

    //Actualiza el estado de un reclamo según su id
    public void actualizaEstadoReclamo(int accion, String id){
        switch (accion){
            case 1: // Estado Eliminado
                app.execSQL("UPDATE Reclamo SET Estado='Eliminado' WHERE Id=" + id);
                break;
            case 2: // Estado Enviado
                app.execSQL("UPDATE Reclamo SET Estado='Enviado' WHERE Id=" + id);
                break;
        }
    }

    //Actualiza el estado de una sugerencia según su id
    public void actualizaEstadoSugerencia(int accion, String id_local){
        switch (accion){
            case 1: // Estado Eliminado
                app.execSQL("UPDATE Sugerencia SET Estado='Eliminado' WHERE Id=" + id_local);
                break;
            case 2: // Estado Enviado
                app.execSQL("UPDATE Sugerencia SET Estado='Enviado' WHERE Id=" + id_local);
                break;
        }
    }

    //Actualiza id_server de Usuario, Sugerencia o Reclamo, según el la variable tabla : 1.-Usuario, 2.-Sugerencia, 3.-Reclamo
    public void actualizaIdServer(int tabla, String id_server , String id_local){
        switch (tabla){
            case 1: // Usuario
                app.execSQL("UPDATE Usuario SET Id_Server=" + id_server);
                break;
            case 2: // Sugerencia
                app.execSQL("UPDATE Sugerencia SET Id_Server=" + id_server + " WHERE Id=" + id_local);
                break;
            case 3: // Reclamo
                app.execSQL("UPDATE Reclamo SET Id_Server=" + id_server + " WHERE Id=" + id_local);
                break;
        }
    }

    //Actualiza latitud y longitud en caso de que se haya guardado una que no era de concepción
    public void actualizaLatLng(int tabla, String latitud, String longitud , String id_local){
        switch (tabla){
            case 1: // Sugerencia
                Log.e("Resp: ", "> act lat lng sug");
                app.execSQL("UPDATE Sugerencia SET Latitud=" + latitud + ", Longitud ="+ longitud+" WHERE Id=" + id_local);
                break;
            case 2: // Reclamo
                Log.e("Resp: ", "> act lat lng recl");
                String update = "UPDATE Reclamo SET Latitud=" + latitud + ", Longitud ="+ longitud+" WHERE Id=" + id_local;
                Log.e("Resp: ", "> consulta:"+update);
                app.execSQL("UPDATE Reclamo SET Latitud=" + latitud + ", Longitud ="+ longitud+" WHERE Id=" + id_local);
                break;
        }
    }

    public void close(){
        app.close();
    }
}
