package com.example.nieves.appmovil;

import android.content.Context;
import android.util.Log;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//valida nombres, apellidos y descripcón
//TODO: Mejorar las validaciones del texto ingresado en nombres, apellidos y descripción de los reclamos y sugerencias
public class Validaciones {
    public Context context;

    public void Validaciones(Context context){
        this.context = context;
    }

    //metodo que valida nombres
    public String[] validarNombres(String nombres) {
        String[] respuesta = new  String[2];
        String reg = "^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,3}[a-zA-ZñÑÁÉÍÓÚáéíóú]+)*)$";
        Log.e("Resp: ", "> reg:" + reg );
        if (nombres.length() >= 3){ //nombres mayor igual que 3 caracteres
            if(nombres.length() <= 25){ //nombres menor igual que 25 caracteres
                Pattern pattern = Pattern.compile(reg);
                Log.e("Resp: ", "> compila");
                Matcher matcher = pattern.matcher(nombres);
                Log.e("Resp: ", "> matcher:"+matcher);
                boolean valid = matcher.find();
                Log.e("Resp: ", "> vali:"+valid);
                if (valid){
                    respuesta[0]="true";
                    return respuesta;
                }
                else{
                    respuesta[0]="false"; respuesta[1]="El nombre ingresado no es válido.";
                    return respuesta;
                }
            }
            else{
                respuesta[0]="false"; respuesta[1]="El nombre ingresado es demasiado largo, máximo 25 caracteres.";
                return respuesta;
            }
        }
        else{
            respuesta[0]="false"; respuesta[1]="El nombre ingresado es muy corto.";
            return respuesta;
        }
    }

    //valida apellidos
    public String[] validarApellidos(String apellidos) {
        String[] respuesta = new  String[2];
        String reg = "^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,2}[a-zA-ZñÑÁÉÍÓÚáéíóú]+)*)$";
        Log.e("Resp: ", "> reg:" + reg );
        if (apellidos.length() >= 2){ //apellidos mayor igual que 2 caracteres
            if(apellidos.length() <= 25){ //apellidos menor igual que 25 caracteres
                Pattern pattern = Pattern.compile(reg);
                Log.e("Resp: ", "> compila");
                Matcher matcher = pattern.matcher(apellidos);
                Log.e("Resp: ", "> matcher:"+matcher);
                boolean valid = matcher.find();
                Log.e("Resp: ", "> vali:"+valid);
                if (valid){
                    respuesta[0]="true";
                    return respuesta;
                }
                else{
                    respuesta[0]="false"; respuesta[1]="El apellido ingresado no es válido.";
                    return respuesta;
                }
            }
            else{
                respuesta[0]="false"; respuesta[1]="El apellido ingresado es demasiado largo, máximo 25 caracteres.";
                return respuesta;
            }
        }
        else{
            respuesta[0]="false"; respuesta[1]="El apellido ingresado es muy corto.";
            return respuesta;
        }

    }

    //valida descripción ingresada por el usuarios
    public String[] validarDescripción(String descripcion) {
        String[] respuesta = new  String[2];
        String reg = "^([a-zA-ZñÑÁÉÍÓÚáéíóú]+([[:space:]]{0,2}[a-zA-ZñÑÁÉÍÓÚáéíóú0-9\\.|\\'|\\\"|\\)|\\#|\\_|\\$|\\%|\\(|\\,|\\-|\\@|\\?|\\¿|\\:|\\/|\\+]+)*)$";
        Log.e("Resp: ", "> reg:" + reg );
        if(descripcion.equals("")) { //valida si descripción es vacia
            respuesta[0]="false"; respuesta[1]="La descripción esta vacía.";
            return respuesta;
        }else{
            if (descripcion.length() >= 15){ //descripción mayor igual a 15 caracteres
                if(descripcion.length() <= 250){ //descripción menor igual que 250 caracteres
                    Pattern pattern = Pattern.compile(reg);
                    Log.e("Resp: ", "> compila");
                    Matcher matcher = pattern.matcher(descripcion);
                    Log.e("Resp: ", "> matcher:"+matcher);
                    boolean valid = matcher.find();
                    Log.e("Resp: ", "> vali:"+valid);
                    if (valid){
                        respuesta[0]="true";
                        return respuesta;
                    }
                    else{
                        respuesta[0]="false"; respuesta[1]="La descripción no es válida.";
                        return respuesta;
                    }
                }
                else{
                    respuesta[0]="false"; respuesta[1]="La descripción ingresada es demasiado larga, máximo 250 caracteres";
                    return respuesta;
                }
            }
            else{
                respuesta[0]="false"; respuesta[1]="La descripción ingresada es muy corta.";
                return respuesta;
            }

        }

    }


}
