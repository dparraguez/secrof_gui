package com.example.nieves.appmovil;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.PolyUtil;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Reclamo extends AppCompatActivity implements View.OnClickListener {

    private Button btnCamara;
    private Button btnEnviar;
    private TextView textFoto;
    private Button btnReclamoEstandar;
    private TextView tipoReclamo;
    private EditText re_Descripcion;
    private String tipoBD; //tipo que se envia al web service
    private String descripcion; //descripcion del reclamo
    private String hora;//hora creación del reclamo
    private String fecha;//fecha creación del reclamo
    private String latitud;
    private String longitud;
    private String rutaFile;//ruta de la foto
    private AlertDialog.Builder builder;
    private Uri uri;
    private boolean respuesta;
    private int re_id_local;
    private String nombreFoto = null;
    private boolean hay_foto = false;
    private final String ruta_fotos = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/misfotos/";
    private File file = new File(ruta_fotos);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reclamo);
        init();
    }

    //Inicializa los componetes del layout
    public void init(){
        LinearLayout layout = (LinearLayout)findViewById(R.id.layoutReclamo);
        final String[] items;
        Bundle bundle = getIntent().getExtras();//recibir text de activity TipoReclamo
        tipoReclamo = (TextView)findViewById(R.id.textReclamo);
        tipoReclamo.setText(tipoReclamo.getText()+ bundle.getString("TipoReclamo"));//muesta tipo reclamo recibido de TipoReclamo
        tipoBD=bundle.getString("TipoBD");
        items = bundle.getStringArray("Seleccion");
        btnReclamoEstandar = (Button)findViewById(R.id.btnReclamoEstandar);

            btnReclamoEstandar.setOnClickListener(new View.OnClickListener() {
                String seleccion;
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Reclamo.this);
                    builder.setTitle("Seleccione un reclamo estandar:")
                            .setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    seleccion = (String) items[which];
                                }
                            })
                            .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    re_Descripcion.setText(seleccion);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });


        //Si no existe crea la carpeta donde se guardaran las fotos
        file.mkdirs();

        re_Descripcion =(EditText) findViewById (R.id.re_Descripcion);

        btnCamara = (Button)findViewById(R.id.btnCamara);
        btnCamara.setOnClickListener(this);
        btnEnviar = (Button)findViewById(R.id.btnEnviar);
        btnEnviar.setOnClickListener(this);
        textFoto = (TextView)findViewById(R.id.textFoto);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reclamo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    //metodo que guarda en una base de datos interna los datos ingresados por el usuario
    public boolean guardar(View v) {
        String[] desc = new String[2];
        descripcion = re_Descripcion.getText().toString();
            Validaciones val = new Validaciones();
            desc = val.validarDescripción(descripcion);
            if (desc[0].equals("false")) { //Consulta si la descripción es erronea, muy corta o muy grande
                re_Descripcion.setError(desc[1]);
                return false;
            } else {
                if (gps(latitud, longitud)) { //consulta por la lat y long que se almacenara
                    Sigeco admin = new Sigeco(this);
                    SQLiteDatabase bd = admin.getWritableDatabase();

                    //obtener fecha y hora
                    SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
                    Date fechaHoy = Calendar.getInstance().getTime();
                    hora = formatoHora.format(fechaHoy);
                    fecha = formato.format(fechaHoy);

                    ContentValues Reclamo = new ContentValues();

                    Reclamo.put("Descripcion", descripcion);
                    Reclamo.put("Tipo", tipoBD);
                    Reclamo.put("Fecha_C", fecha);
                    Reclamo.put("Hora_C", hora);
                    Reclamo.put("Latitud", latitud);
                    Reclamo.put("Longitud", longitud);
                    Reclamo.put("Estado", "Guardado");
                    Log.e("Resp: ", "> lat guardada" + latitud);
                    Log.e("Resp: ", "> long guardada" + longitud);
                    //guardar en bd ruta de la foto
                    if (hay_foto == true) Reclamo.put("Foto", rutaFile);
                    else Reclamo.put("Foto", "vacio");
                    re_id_local = (int) bd.insert("Reclamo", null, Reclamo);
                    bd.close();
                    return true;

                } else {
                    return false;
                }
            }
    }

    //Función para validar la lat y long
    public boolean gps(String lat, String lng){
        respuesta = false;
        PolygonOptions concepcion;
        Boolean en_conce;
        AlertDialog.Builder builder;
        if((lat) == null && (lng == null)) {//Si lat y long son nulos otiene la lat y long
            PosicionGps gps = new PosicionGps(Reclamo.this);
            latitud = Double.toString(gps.getLatitud());
            longitud = Double.toString(gps.getLongitud());
            Log.e("Resp: ", "> lat:"+latitud+" longitud:"+longitud);
        }
        LatLng point = new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud));
        if((latitud.equals("0.000000")) && (longitud.equals("0.000000"))){ //Consulta si la lat y long obtenida es 0.000000
            String mensaje = "No tiene activado el GPS en este momento, " +
                    "presione Ajustes para activarlo";
            mensajeAjuste(mensaje);
        }else {
            concepcion = PoligonoConcepcion.Concepcion();
            en_conce = PolyUtil.containsLocation(point, concepcion.getPoints(), false);
            if(!en_conce){ //Consulta que la lat y long obtenida del gps sean de concepción
                Log.e("Resp: ", "> no son de conce");
                    builder = new AlertDialog.Builder(Reclamo.this);
                    builder.setTitle("Importante");
                    builder.setMessage("Usted se encuentra fuera del área de Concepción, " +
                            "por favor seleccione un punto en el mapa.");
                    builder.setPositiveButton("MAPA", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent mapa = new Intent(Reclamo.this, Mapa.class);
                            startActivityForResult(mapa, 1);
                        }
                    });
                    builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    builder.create();
                    builder.show();
                    Log.e("Resp: ", "> ajuste");
            }else{
                return respuesta = true;
            }
        }
        return respuesta;
    }

    //Funcion que consulta si hay internet y si hay crea los json con la informacion a enviar al server
    public void enviarReclamoServer(){
        String mensaje;
        JsonEnvioServer jsonEnvio = new JsonEnvioServer();
        String datosEnvio;
        String[] datosUsuario;
        EnvioServer envio = new EnvioServer();
        ConexionInternet conn = new ConexionInternet();
        if (conn.estaConectado(this)){
            Sigeco db = new Sigeco(this);
            String idServer = db.idServerUsuario();
            if(idServer.equals("0")){ //si su id_server es 0 es primera vez que realiza un envio de información
                datosUsuario = db.datosUsuario();
                if (hay_foto==true){
                    datosEnvio = jsonEnvio.jsonReclamoDatosUsuario(tipoBD, descripcion, fecha, hora, nombreFoto,
                            latitud, longitud,datosUsuario[0],datosUsuario[1],datosUsuario[2],getNumeroTelefono());
                } else {
                    datosEnvio = jsonEnvio.jsonReclamoDatosUsuario(tipoBD, descripcion, fecha, hora, null,
                            latitud, longitud, datosUsuario[0], datosUsuario[1], datosUsuario[2], getNumeroTelefono());
                }
            }else {
                //Ya ha realizado un envio de informacion al server
                if (hay_foto==true){
                    datosEnvio = jsonEnvio.jsonReclamoDatosIdUsuario(tipoBD,descripcion,fecha,hora,nombreFoto,
                            latitud, longitud,idServer);
                }else {
                    datosEnvio = jsonEnvio.jsonReclamoDatosIdUsuario(tipoBD,descripcion,fecha,hora, null,
                            latitud, longitud,idServer);
                }
            }
            Log.e("Resp: ", "> datos envio:"+datosEnvio );
            envio.datosEnvio(datosEnvio,Reclamo.this,hay_foto,Integer.toString(re_id_local),rutaFile);
            envio.enviarServer();
        } else {
            mensaje = "No tiene conexión a internet en este momento, " +
                    "presione Ajustes para conectarlo o ingrese a Mis Fiscalizaciones" +
                    " cuando tenga conexión para enviar su Reclamo.  ";
            mensajeAjuste(mensaje);
        }
    }

    //muestra un mensaje de ajuste
    public void mensajeAjuste(String mensaje){
        builder = new AlertDialog.Builder(Reclamo.this);
        builder.setTitle("Importante");
        builder.setMessage(mensaje);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(getBaseContext(), MenuPrincipal.class);
                startActivity(intent);
            }
        });
        builder.setNeutralButton("AJUSTES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        });
        builder.create();
        builder.show();
    }

    //funcion para obtener el número de telefono
    public String getNumeroTelefono()
    {
        return ((TelephonyManager) getSystemService(TELEPHONY_SERVICE))
                .getLine1Number();
    }

    // Metodo privado que genera un codigo unico para la foto segun la hora y fecha del sistema
    @SuppressLint("SimpleDateFormat")
    private String getCode()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmddhhmmss");
        String date = dateFormat.format(new Date() );
        String photoCode = "pic_" + date;
        return photoCode;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnCamara:
                nombreFoto=  getCode() + ".png";
                rutaFile = ruta_fotos + nombreFoto;

                File mi_foto = new File(rutaFile);
                try {
                    mi_foto.createNewFile();
                } catch (IOException ex) {
                    Log.e("ERROR ", "Error:" + ex);
                }
                uri = Uri.fromFile( mi_foto );
                //Abre la camara para tomar la foto
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //Guarda imagen
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                //Retorna a la actividad
                hay_foto = true;
                startActivityForResult(cameraIntent, 2);
                break;
            case R.id.btnEnviar:
                boolean g = guardar(v);
                if(g) enviarReclamoServer();
                break;
        }
    }

    //funcion que obtiene los resultados al llamar a una segunda activity
    //obtiene la foto en caso de llamar a la camara o la lat y long en caso de llamar al mapa
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Resp: ", "> forresult " );
        if(resultCode == RESULT_OK) {
            if (requestCode == 1) {
                latitud = data.getStringExtra("latitud");
                longitud = data.getStringExtra("longitud");
                Log.e("Resp: ", "> lat:"+latitud+" long:"+longitud );
            }
            else if (requestCode == 2) {
                textFoto.setText("Foto adjunta");
            }
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getBaseContext(), TipoReclamo.class));
    }
}

