package com.example.nieves.appmovil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

//Clase para saber si el dispositivo tiene internet mediente wi-fi o datos moviles
public class ConexionInternet {

    //metodo que comprueba si el dispositivo tiene conexión a internet
    public Boolean estaConectado(Context context){
        if(conectadoWifi(context)){
            return true;
        }else{
            if(conectadoRedMovil(context)){
                return true;
            }else{
                return false;
            }
        }
    }

    //comprueba conexión a wifi
    public Boolean conectadoWifi(Context context){
        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    //comprueba conexión a datos moviles
    public Boolean conectadoRedMovil(Context context){
        ConnectivityManager connectivity = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }
}
