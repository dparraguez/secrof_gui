package com.example.nieves.appmovil;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;

//para la seleccion de un tipo de reclamo
public class TipoReclamo extends AppCompatActivity {

    private RadioButton TipoReclamo;
    private String mensaje;
    private String[] seleccion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo_reclamo);
        //pasar text de radiobutton  a activity reclamo
        findViewById(R.id.rbtnVehiculo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] items = {"Vehículo abandonado","Vehículo con filtración","Otro"};
                AlertDialog.Builder builder = new AlertDialog.Builder(TipoReclamo.this);
                builder.setTitle("Seleccione subtipo vehículo")
                        .setItems(items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int item) {
                                Intent i = new Intent(TipoReclamo.this, Reclamo.class);
                                Log.i("Dialogos", "Opción elegida: " + items[item]);
                                if (item == 0) {
                                    //abandonado
                                    seleccion = new String[]{"Abandono del vehículo cargado en la vía pública."};
                                    i.putExtra("TipoReclamo", items[item]);
                                    i.putExtra("TipoBD", "Vehículo abandonado"); //Tipo que se almacena en la bd del web service
                                    i.putExtra("Seleccion", seleccion);//Array para boton "Reclamo estandar"
                                    startActivity(i);
                                }
                                if (item == 1) {
                                    //filtración
                                    seleccion = new String[]{"Filtración de líquidos en los vehículos."};
                                    i.putExtra("TipoReclamo", items[item]);
                                    i.putExtra("TipoBD", "Vehículo filtración"); //Tipo que se almacena en la bd del web service
                                    i.putExtra("Seleccion", seleccion);//Array para boton "Reclamo estandar"
                                    startActivity(i);
                                }
                                if (item == 2) {
                                    //otro
                                    seleccion = new String[]{"Falta de accesorios,  higiene, etc. en los vehículos.",
                                            "Identificación inadecuada en los  vehículos o falta de ésta."};

                                    i.putExtra("TipoReclamo", "Vehículo otro");
                                    i.putExtra("TipoBD", "Vehículo otros"); //Tipo que se almacena en la bd del web service
                                    i.putExtra("Seleccion", seleccion);//Array para boton "Reclamo estandar"
                                    startActivity(i);
                                }
                            }
                        });
                builder.create();
                builder.show();
            }
        });
        findViewById(R.id.rbtnEscombros).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TipoReclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnEscombros);

                seleccion = new String[]{"Hay escombros en la vía pública."};

                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                i.putExtra("TipoBD","Escombros"); //Tipo que se almacena en la bd del web service
                i.putExtra("Seleccion",seleccion);//Array para boton "Reclamo estandar"
                startActivity(i);
            }
        });
        findViewById(R.id.rbtnMicrobasural).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TipoReclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnMicrobasural);

                seleccion = new String[]{"No eliminan microbasurales."};

                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                i.putExtra("TipoBD","Microbasural"); //Tipo que se almacena en la bd del web service
                i.putExtra("Seleccion",seleccion);//Array para boton "Reclamo estandar"
                startActivity(i);
            }
        });
        findViewById(R.id.rbtnTrabajadores).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TipoReclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnTrabajadores);

                seleccion = new String[]{"Falta de personal, menos de tres personas.",
                        "Personal sin uniforme completo.",
                        "Personal sin ropa de lluvia, cuando corresponde.",
                        "Falta de higiene en el vestuario del personal, comportamientos impropios."};

                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                i.putExtra("TipoBD","Trabajadores"); //Tipo que se almacena en la bd del web service
                i.putExtra("Seleccion",seleccion);//Array para boton "Reclamo estandar"
                startActivity(i);
            }
        });
        findViewById(R.id.rbtnCalle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TipoReclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnCalle);

                seleccion = new String[]{"No recolectar el todo o parte de los residuos, tanto en  calles como espacios públicos."};

                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                i.putExtra("TipoBD","Calles"); //Tipo que se almacena en la bd del web service
                i.putExtra("Seleccion",seleccion);//Array para boton "Reclamo estandar"
                startActivity(i);
            }
        });
        findViewById(R.id.rbtnResiduos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TipoReclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnResiduos);

                seleccion = new String[]{"No recolectar el todo o parte de la basura domiciliaria."};

                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                i.putExtra("TipoBD","Residuos"); //Tipo que se almacena en la bd del web service
                i.putExtra("Seleccion",seleccion);//Array para boton "Reclamo estandar"
                startActivity(i);
            }
        });
        findViewById(R.id.rbtnSumideros).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(TipoReclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnSumideros);

                seleccion = new String[]{"No limpian los sumideros de aguas lluvia (alcantarillas)."};

                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                i.putExtra("TipoBD","Sumideros"); //Tipo que se almacena en la bd del web service
                i.putExtra("Seleccion",seleccion);//Array para boton "Reclamo estandar"
                startActivity(i);
            }
        });
        //Ayudas sobre a que se refiere cada tipo
        findViewById(R.id.btnVehiculo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje = "Si usted presencia alguna de estas irregularidades, presione esta opción:\n" +
                        "1.- Abandono del vehículo cargado en la vía pública.\n" +
                        "2.- Falta de accesorios,  higiene, etc. en los vehículos.\n" +
                        "3.- Filtración de líquidos en los vehículos.\n" +
                        "4.- Identificación inadecuada en los  vehículos o falta de ésta.  ";
                mensajeAyuda(mensaje);
            }
        });
        findViewById(R.id.btnEscombros).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje = "Presione esta opción si ve escombros en la vía pública.";
                mensajeAyuda(mensaje);
            }
        });
        findViewById(R.id.btnMicrobasural).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje = "Si usted presencia alguna de estas irregularidades, presione esta opción:\n" +
                        "1.- No eliminan microbasurales.";
                mensajeAyuda(mensaje);
            }
        });
        findViewById(R.id.btnTrabajadores).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje = "Si usted presencia alguna de estas irregularidades, presione esta opción:\n" +
                        "1.- Falta de personal, menos de tres personas.\n" +
                        "2.- Personal sin uniforme completo.\n" +
                        "3.- Personal sin ropa de lluvia, cuando corresponde.\n" +
                        "4.- Falta de higiene en el vestuario del personal, comportamientos impropios.  ";
                mensajeAyuda(mensaje);
            }
        });
        findViewById(R.id.btnCalles).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje = "Si usted presencia alguna de estas irregularidades, presione esta opción:\n" +
                        "1.- No recolectar el todo o parte de los residuos, tanto en  calles como espacios públicos.";
                mensajeAyuda(mensaje);
            }
        });
        findViewById(R.id.btnResiduos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje = "Si usted presencia alguna de estas irregularidades, presione esta opción:\n" +
                        "1.- No recolectar el todo o parte de la basura domiciliaria.";
                mensajeAyuda(mensaje);
            }
        });
        findViewById(R.id.btnSumideros).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mensaje = "Si usted presencia alguna de estas irregularidades, presione esta opción:\n" +
                        "1.- No limpiar los sumideros de aguas lluvia (alcantarillas).";
                mensajeAyuda(mensaje);
            }
        });
    }

    //mensaje que muestra a que se refiere cada tipo
    public void mensajeAyuda(String mensaje){
        AlertDialog.Builder builder = new AlertDialog.Builder(TipoReclamo.this);
        builder.setTitle("Importante");
        builder.setMessage(mensaje);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reclamo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getBaseContext(), MenuPrincipal.class));
    }
}
