package com.example.nieves.appmovil;

import org.json.JSONException;
import org.json.JSONObject;

//Para la creación de los json donde se envia la información al server
public class JsonEnvioServer {

    //crear json para envio de datos Sugerencia  + datos Usuario
    public String jsonSugerenciaDatosUsuario(String tipo, String descripcion, String fecha, String hora,
                                          String lat, String lng, String nombres, String apellidos, String rut, String fono) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("control", 1);
            jsonObj.put("tipo", tipo);
            jsonObj.put("descripcion", descripcion);
            jsonObj.put("fecha", fecha);
            jsonObj.put("hora", hora);
            jsonObj.put("latitud", lat);
            jsonObj.put("longitud", lng);
            jsonObj.put("nombres", nombres);
            jsonObj.put("apellidos", apellidos);
            jsonObj.put("rut", rut);
            jsonObj.put("fono", fono);
            return jsonObj.toString();
        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    //crear json para envio de datos Sugerencia + Id Usuario
    public String jsonSugerenciaDatosIdUsuario( String tipo, String descripcion, String fecha, String hora,
                                               String lat, String lng, String id_usuario) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("control", 2);
            jsonObj.put("tipo", tipo);
            jsonObj.put("descripcion", descripcion);
            jsonObj.put("fecha", fecha);
            jsonObj.put("hora", hora);
            jsonObj.put("latitud", lat);
            jsonObj.put("longitud", lng);
            jsonObj.put("usuario", id_usuario);
            return jsonObj.toString();
        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    //crear json para envio de datos Reclamo  + datos Usuario
    public String jsonReclamoDatosUsuario( String tipo, String descripcion, String fecha, String hora, String foto,
                                   String lat, String lng, String nombres, String apellidos, String rut, String fono) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("control", 3);
            jsonObj.put("tipo", tipo);
            jsonObj.put("descripcion", descripcion);
            jsonObj.put("fecha", fecha);
            jsonObj.put("hora", hora);
            jsonObj.put("foto", foto);
            jsonObj.put("latitud", lat);
            jsonObj.put("longitud", lng);
            jsonObj.put("nombres", nombres);
            jsonObj.put("apellidos", apellidos);
            jsonObj.put("rut", rut);
            jsonObj.put("fono", fono);
            return jsonObj.toString();
        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    //crear json para envio de datos Reclamo  + Id Usuario
    public String jsonReclamoDatosIdUsuario( String tipo, String descripcion, String fecha, String hora, String foto,
                                          String lat, String lng, String id_usuario) {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("control", 4);
            jsonObj.put("tipo", tipo);
            jsonObj.put("descripcion", descripcion);
            jsonObj.put("fecha", fecha);
            jsonObj.put("hora", hora);
            jsonObj.put("foto", foto);
            jsonObj.put("latitud", lat);
            jsonObj.put("longitud", lng);
            jsonObj.put("usuario", id_usuario);
            return jsonObj.toString();
        } catch (JSONException e) {
            //TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }



}


