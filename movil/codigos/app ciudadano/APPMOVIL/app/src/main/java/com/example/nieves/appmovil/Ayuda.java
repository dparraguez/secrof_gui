package com.example.nieves.appmovil;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

//muestra un menú de ayuda para el ciudadano
public class Ayuda extends AppCompatActivity {

    private AlertDialog.Builder builder;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ayuda);

        findViewById(R.id.btnQuees).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulo, mensaje;
                titulo = "¿Qué es Conce más limpio?";
                mensaje = "Conce más limpio es una aplicación móvil disponible para " +
                        "teléfonos con sistema operativo Android, que te permite enviar " +
                        "reclamos, sugerencias o felicitaciones sobre el servicio de " +
                        " de basura de Concepción.";
                mensaje(titulo, mensaje);
            }
        });
        findViewById(R.id.btnParaque).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String titulo, mensaje;
                titulo = "¿Para qué sirve?";
                mensaje = "Conce más limpio sirve para fiscalizar el servicio de recolección " +
                        "de basura en Concepción, incorporando a los ciudadanos en esta labor, " +
                        "permitiéndoles enviar sus reclamos, sugerencias y felicitaciones desde " +
                        "una plataforma mucho más moderna, mejorando así la comunicación existente.";
                mensaje(titulo, mensaje);
            }
        });
        findViewById(R.id.btnComo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Ayuda.this, Ayuda2.class));
            }
        });

    }

    public void mensaje(String  titulo, String mensaje){
        builder = new AlertDialog.Builder(Ayuda.this);
        TextView myMsg = new TextView(this);
        myMsg.setText(mensaje);
        myMsg.setTextSize(20);
        myMsg.setGravity(Gravity.CENTER);
        builder.setTitle(titulo);
        builder.setView(myMsg);
      //  builder.setMessage(mensaje);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        builder.create();
        builder.show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getBaseContext(), MenuPrincipal.class));
    }
}
