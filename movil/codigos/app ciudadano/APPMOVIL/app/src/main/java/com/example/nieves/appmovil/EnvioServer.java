package com.example.nieves.appmovil;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class EnvioServer {
    private String datos;
    private Context context;
    private boolean hay_foto = false;
    private String rutaFoto;
    private String id_local; //id local de lo que se esta enviado (Sugerencia o Reclamo)
    private int status;

    //Funcion que recibe los datos necesarios para realizar el envio de la información
    public void datosEnvio(String datos, Context context, boolean hay_foto, String id_local, String rutaFoto){
        this.datos = datos; //Json con los datos a enviar
        this.context = context;
        this.hay_foto = hay_foto; //variable para saber si el reclamo contiene foto
        this.id_local = id_local; //el id local con que se almaceno el reclamo para modificar id_server una vez almacenado en el server
        this.rutaFoto = rutaFoto; //ruta donde se almaceno la foto en el dispositivo
    }

    //ejecuta la función AsyncTask que envia los datos al server
    public void enviarServer(){
      new enviar().execute();
    }

    //Crea un Bitmap para en envio de la foto (si existe) al server
    public Bitmap createBitMap(String ruta){
        Bitmap foto = null;
        File imgFile = new  File(ruta);
        if(imgFile.exists()){
            foto = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(foto,300, 300, false);
        return  resizedBitmap;
    }

    //función para mostrar mensajes una vez realizado el envio
    public void mensajeEnvio(String mensaje){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Importante");
        builder.setMessage(mensaje);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            //función onClick para volver al Menu_Principal luego de apretar 'OK' en el AlertDialog
            @Override
            public void onClick(DialogInterface dialog, int which){
                String nombreClass = context.getClass().getSimpleName();
                if(nombreClass.equals("MisFiscalizaciones")){
                    Intent intent = new Intent(context, MisFiscalizaciones.class);
                    context.startActivity(intent);
                }else{
                    Intent intent = new Intent(context, MenuPrincipal.class);
                    context.startActivity(intent);
                }
            }
        });
        builder.create();
        builder.show();
    }

    //Actualiza estado y id_server de los datos enviado
    public void datosRespServer(int control, String us_id, String id_server, String id_local ){
        Sigeco db = new Sigeco(context);
        switch (control){
            case 1: //Id_Server Usuario +Id_Server Sugerencia +  Estado Sugerencia 'Enviado'
                db.actualizaIdServer(1, us_id, null); // 1.- Usuario
                db.actualizaIdServer(2, id_server, id_local); // 2.- Sugerencia
                db.actualizaEstadoSugerencia(2, id_local); // 2.- Estado Sugerencia 'Enviado'
                break;
            case 2: //Id_Server Sugerencia + Estado Sugerencia 'Enviado'
                db.actualizaIdServer(2, id_server, id_local); //2.- Sugerencia
                db.actualizaEstadoSugerencia(2, id_local); // 2.- Estado Sugerencia 'Enviado'
                break;
            case 3: //Id_Server Usuario + Id_Server Reclamo + Estado Reclamo 'Enviado'
                db.actualizaIdServer(1, us_id, null); //1.- Usuario
                db.actualizaIdServer(3, id_server, id_local); //3.- Reclamo
                db.actualizaEstadoReclamo(2, id_local); // 2.- Estado Reclamo 'Enviado'
                break;
            case 4: //Id_Server Reclamo + Estado Reclamo 'Enviado'
                db.actualizaIdServer(3, id_server, id_local); //3.- Reclamo
                db.actualizaEstadoReclamo(2, id_local); // 2.- Estado Reclamo 'Enviado'
                break;
        }
        db.close();
    }

    //AsyncTask para el envio de los datos al server
    private class enviar extends AsyncTask<String, Void, String> {
        ProgressDialog ringProgressDialog;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            ringProgressDialog = ProgressDialog.show(context, "Por favor espere ...", "Enviando datos...", false);
        }

        @Override
        protected String doInBackground(String... params) {
            HttpURLConnection conn = null;
            URL url;
            String data = "";
            StringBuilder total = new StringBuilder("");
            String lnEnd = "\r\n";
            String dbHyphen = "--";
            String boundary =  "*****";
            try
            {
                url = new URL("http://cuchodechile.no-ip.org/ubbb/webservice/app-datos.php");
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("imageToUpload", "ups.jpg");
                OutputStream os = conn.getOutputStream();

                //datos envio
                data = datos;

                Log.e("Resp: ", "> data:" + data);
                os.write((dbHyphen + boundary + lnEnd).getBytes());
                os.write(("Content-Disposition: form-data; name=\"data\"" + lnEnd).getBytes());
                os.write(("Content-Type: text/plain; charset=UTF-8"+ lnEnd).getBytes());
                os.write(lnEnd.getBytes());
                os.write(data.getBytes());os.write(lnEnd.getBytes());
                os.write(lnEnd.getBytes());
                Log.e("Resp: ", "> cons:" + hay_foto);
                if (hay_foto==true){
                    os.write((dbHyphen + boundary + lnEnd).getBytes());
                    os.write(("Content-Disposition: form-data; name=\"imageToUpload\";filename="+ "ups.jpg" + lnEnd).getBytes());
                    os.write(("Content-Type: image/jpeg; charset=UTF-8" + lnEnd).getBytes());
                    os.write((lnEnd).getBytes());
                    Log.e("Resp: ", "> if hay foto");
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    Bitmap foto = createBitMap(rutaFoto);
                    foto.compress(Bitmap.CompressFormat.PNG, 100, bos);
                    byte[] bitmapdata = bos.toByteArray();
                    os.write(bitmapdata);
                    os.write((lnEnd).getBytes());
                    os.write((dbHyphen + boundary + dbHyphen + lnEnd).getBytes());
                }
                os.close();
                conn.connect();

                status = conn.getResponseCode();

                InputStream in = conn.getInputStream();
                BufferedReader r = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = r.readLine()) != null) {
                    total.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally{
                if(conn!=null)
                    conn.disconnect();
            }
            return total.toString();
        }

        @Override
        protected void onPostExecute(String result){
            try {
                String mensaje;
                //consulta si se realizo la conexión con el server
                if(status == 200){
                    JSONObject json_data = new JSONObject(result);
                    String control = json_data.getString("control");
                    String id, us_id;
                    //dependiendo la variable control sabe si fue enviado un reclamo una sugerencia y con que datos fue enviado
                    switch(control) {
                        case "1":
                            us_id = json_data.getString("us_id");
                            id = json_data.getString("su_id"); //Id con que se almaceno la Sugerencia en el Server
                            datosRespServer(1, us_id, id, id_local);
                            mensaje = "Su sugerencia o felicitación ha sido enviada exitosamente,registrado con el número:  #" + id +
                                    " ,su número de usuario es #" + us_id+".";
                            mensajeEnvio(mensaje);
                            break;
                        case "2":
                            id = json_data.getString("su_id");
                            datosRespServer(2, null, id, id_local);
                            mensaje = "Su sugerencia o felicitación ha sido enviada exitosamente,registrado con el número: #" + id+".";
                            mensajeEnvio(mensaje);
                            break;
                        case "3":
                            us_id = json_data.getString("us_id");
                            id = json_data.getString("re_id");
                            datosRespServer(3, us_id, id, id_local);
                            mensaje = "Su reclamo ha sido enviado exitosamente,registrado con el número: #" + id +
                                    " ,su número de usuario es #" + us_id+".";
                            mensajeEnvio(mensaje);
                            break;
                        case "4":
                            id = json_data.getString("re_id");
                            datosRespServer(4, null, id, id_local);
                            mensaje = "Su reclamo ha sido enviado exitosamente,registrado con el número: #" + id+".";
                            mensajeEnvio(mensaje);
                            break;
                    }
                }
                else{
                    mensaje = "En este momento no se puede realizar su petición, intente más tarde.";
                    mensajeEnvio(mensaje);
                }
            } catch (Exception e) {
                Log.d("appp", e.getMessage());
            }
            ringProgressDialog.dismiss();

        }

    }

}
