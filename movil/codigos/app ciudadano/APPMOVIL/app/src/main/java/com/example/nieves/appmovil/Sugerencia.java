package com.example.nieves.appmovil;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.PolyUtil;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Sugerencia extends AppCompatActivity  {

    private EditText su_Descripcion;
    private RadioButton sugerencia,felicitacion;
    private Button btnSugEstandar;
    private String tipo; //tipo sugerencia o reclamo
    private String descripcion; //descripcion de la sugerencia o felicitación
    private String hora;//hora creación de la  sugerencia o felicitación
    private String fecha;//fecha creación de la sugerencia o felicitación
    private String latitud;
    private String longitud;
    private int su_id_local;
    private boolean respuesta;
    private String[] selecBtnEstandar ;//opciones en el botón sug o fel estandar
    private AlertDialog.Builder builder;
    PosicionGps gps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugerencia);
        su_Descripcion =(EditText) findViewById (R.id.su_descripcion);
        sugerencia=(RadioButton)findViewById(R.id.rbtnSugerencia);
        felicitacion=(RadioButton)findViewById(R.id.rbtnFelicitacion);
        tipo = "Sugerencia";
        selecBtnEstandar = new String[]{"Avisen con anticipación cuando no pase el camión."};
        btnSugEstandar = (Button)findViewById(R.id.btnSugEstandar); //boton con sugerencias estadar
        findViewById(R.id.btnEnviar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean g = guardar(v);
                if(g) enviarSugerenciaServer();
            }
        });

        btnSugEstandar = (Button)findViewById(R.id.btnSugEstandar);
        btnSugEstandar.setOnClickListener(new View.OnClickListener() {
            String seleccion;
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Sugerencia.this);
                builder.setTitle("Seleccione un mensaje estandar:")
                        .setSingleChoiceItems(selecBtnEstandar, -1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                seleccion = (String) selecBtnEstandar[which];
                            }
                        })
                        .setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                su_Descripcion.setText(seleccion);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

    }

    //guarda los datos ingresados por el ciudadano en la base de datos del dispositivo.
    public boolean guardar(View v) {
        String[] desc = new String[2];
        descripcion = su_Descripcion.getText().toString();
        if (descripcion.equals("")) {
            Toast.makeText(this, "El campo descripción se encuentra vacio.", Toast.LENGTH_LONG).show();
            return false;
        } else {
            Validaciones val = new Validaciones();
            desc = val.validarDescripción(descripcion);
            if (desc[0].equals("false")) {
                su_Descripcion.setError(desc[1]);
                return false;
            } else {
                if (gps(latitud, longitud)) { //consulta si la lat y long son correctos
                    Sigeco admin = new Sigeco(this);
                    SQLiteDatabase bd = admin.getWritableDatabase();

                    SimpleDateFormat formato = new SimpleDateFormat("dd-MM-yyyy");
                    SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
                    Date fechaHoy = Calendar.getInstance().getTime();
                    hora = formatoHora.format(fechaHoy);
                    fecha = formato.format(fechaHoy);

                    ContentValues Sugerencia = new ContentValues();

                    Sugerencia.put("Tipo", tipo);
                    Sugerencia.put("Descripcion", descripcion);
                    Sugerencia.put("Fecha_C", fecha);
                    Sugerencia.put("Hora_C", hora);
                    Sugerencia.put("Latitud", latitud);
                    Sugerencia.put("Longitud", longitud);
                    Sugerencia.put("Estado", "Guardado");
                    Log.e("Resp: ", "> lat guardada" + latitud);
                    Log.e("Resp: ", "> long guardada" + longitud);
                    su_id_local = (int) bd.insert("Sugerencia", null, Sugerencia);
                    bd.close();
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    //valida la latitud y longitud
    public boolean gps(String lat, String lng){
        respuesta = false;
        PolygonOptions concepcion;
        Boolean en_conce;
        AlertDialog.Builder builder;
        if((lat) == null && (lng == null)) { // si lat y long son nulos los obtiene del gps
            PosicionGps gps = new PosicionGps(Sugerencia.this);
            latitud = Double.toString(gps.getLatitud());
            longitud = Double.toString(gps.getLongitud());
        }
        LatLng point = new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud));
        if((latitud.equals("0.000000")) && (longitud.equals("0.000000"))){ //si los valores obtenidos son 0.000000 pide activar gps
            String mensaje = "No tiene activado el GPS en este momento, " +
                    "presione Ajustes para activarlo.";
            mensajeAjuste(mensaje);
        }else {
            concepcion = PoligonoConcepcion.Concepcion();
            en_conce = PolyUtil.containsLocation(point, concepcion.getPoints(), false);
            if(!en_conce){ //consulta si los valores obtenidos son de conce sino pide selecionar en el mapa
                    builder = new AlertDialog.Builder(Sugerencia.this);
                    builder.setTitle("Importante");
                    builder.setMessage("Usted se encuentra fuera del área de Concepción, " +
                            "por favor seleccione un punto en el mapa.");
                    builder.setPositiveButton("MAPA", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent mapa = new Intent(Sugerencia.this, Mapa.class);
                            startActivityForResult(mapa, 1);
                        }
                    });
                    builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                    builder.create();
                    builder.show();
                    Log.e("Resp: ", "> ajuste");
            }else{
                return respuesta = true;
            }
        }
        Log.e("Resp: ", "> resp:"+respuesta);
        return respuesta;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sugerencia, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //radiobutton para seleccionar si lo que se esta realizado es sugerecia o felicitacion
    public void onRadioButtonClicked(View view){
        boolean checked=((RadioButton) view).isChecked();
        switch(view.getId()){
            case R.id.rbtnSugerencia:
                if(checked)
                    tipo ="Sugerencia";
                btnSugEstandar.setText("SUGERENCIA ESTANDAR");
                selecBtnEstandar = new String[]{"Avisen con anticipación cuando no pase el camión."}; //opciones btn sugerencia estandar
                break;
            case R.id.rbtnFelicitacion:
                if(checked)
                    tipo ="Felicitación";
                btnSugEstandar.setText("FELICITACIÓN ESTANDAR");
                selecBtnEstandar = new String[]{"Felicitaciones al personal de los camiones, son muy amables"}; //opciones btn felicitación estandar
                break;
        }
    }

    //metodo obtienen número de telefono
    public String getNumeroTelefono()
    {
        return ((TelephonyManager) getSystemService(TELEPHONY_SERVICE))
                .getLine1Number();
    }

    //metodo que consulta si hay internet y si lo hay crea un json para el envio de datos al server.
    public void enviarSugerenciaServer(){
        String mensaje;
        JsonEnvioServer jsonEnvio = new JsonEnvioServer();
        String datosEnvio;
        String[] datosUsuario;
        EnvioServer envio = new EnvioServer();
        ConexionInternet conn = new ConexionInternet();
            if (conn.estaConectado(this)){
                Sigeco db = new Sigeco(this);
                String idServer = db.idServerUsuario();
                if(idServer.equals("0")){
                    datosUsuario = db.datosUsuario();
                    datosEnvio = jsonEnvio.jsonSugerenciaDatosUsuario(tipo,descripcion,fecha,hora,latitud,latitud,
                            datosUsuario[0],datosUsuario[1],datosUsuario[2],getNumeroTelefono());

                }else{
                    datosEnvio = jsonEnvio.jsonSugerenciaDatosIdUsuario(tipo,descripcion, fecha, hora, latitud,
                            longitud, idServer);
                }
                envio.datosEnvio(datosEnvio,Sugerencia.this, false,Integer.toString(su_id_local), null);
                envio.enviarServer();
            } else {
                mensaje = "No tiene conexión a internet en este momento, " +
                        "presione Ajustes para conectarlo o ingrese a Mis Fiscalizaciones" +
                        " cuando tenga conexión para enviar su sugerencia o felicitación  ";
                mensajeAjuste(mensaje);
            }
        }

    //mensaje de ajuste
    public void mensajeAjuste(String mensaje){
        AlertDialog.Builder builder = new AlertDialog.Builder(Sugerencia.this);
        builder.setTitle("Importante");
        builder.setMessage(mensaje);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(getBaseContext(), MenuPrincipal.class);
                startActivity(intent);
            }
        });
        builder.setNeutralButton("AJUSTES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    //metodo que obtine lat y long del mapa
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Resp: ", "> forresult ");
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                latitud = data.getStringExtra("latitud");
                longitud = data.getStringExtra("longitud");
                Log.e("Resp: ", "> lat:"+latitud+" long:"+longitud );
            }
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getBaseContext(), MenuPrincipal.class));
    }
}
