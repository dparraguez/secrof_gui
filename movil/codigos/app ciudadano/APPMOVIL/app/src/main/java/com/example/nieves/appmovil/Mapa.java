package com.example.nieves.appmovil;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.PolyUtil;

//Para mostrar mapa en caso que la persona se encuentre fuera de concepción
//debe seleccionar el punto vio la irregularidad
public class Mapa extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private PolygonOptions concepcion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(final LatLng point) {
                boolean contains = false;
                AlertDialog.Builder builder = new AlertDialog.Builder(Mapa.this);
                builder.setTitle("Punto seleccionado");
                mMap.addMarker(new MarkerOptions()
                        .position(point));
                //para saber si el punto seleccionado esta dentro de concepción
                contains = PolyUtil.containsLocation(point, concepcion.getPoints(), false);
                if (contains) {
                    Log.d("appp", "Dentro de zona");
                    builder.setMessage("El punto seleccionado ha sido almacenado.");
                    builder.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            double lat = point.latitude;
                            double lng = point.longitude;
                            Intent i = getIntent();
                            i.putExtra("latitud", Double.toString(lat));
                            i.putExtra("longitud", Double.toString(lng));
                            setResult(RESULT_OK, i);
                            finish();
                        }
                    });
                    builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                } else {
                    Log.d("appp", "Fuera de zona");
                    builder.setMessage("El punto que ha seleccionado esta fuera de Concepción.\nPor favor seleccione otro.");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
                }
                builder.create();
                builder.show();

            }
        });
        //marca en el mapa la zona de concepción
        concepcion = PoligonoConcepcion.Concepcion();
        mMap.addPolygon(concepcion);
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(-36.8201352, -73.0443904));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder atras;
        atras = new AlertDialog.Builder(Mapa.this);
        atras.setTitle("Importante");
        atras.setMessage("Por favor seleccione un punto en el mapa.");
        atras.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        atras.create();
        atras.show();
    }
}
