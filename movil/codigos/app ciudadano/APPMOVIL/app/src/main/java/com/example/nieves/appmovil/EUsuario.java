package com.example.nieves.appmovil;

//Tabla Usuario de la base de datos del dispositivo
public class EUsuario {
    public static  final  String TABLE_NAME = "Usuario";
    public static final String FIELD_ID ="Id";
    public static final String FIELD_Nombre ="Nombres";
    public static final String FIELD_Apellidos ="Apellidos";
    public static final String FIELD_Rut="Rut";
    public static final String FIELD_Id_Server="Id_Server";
    public static final String CREATE_DB_TABLE=" CREATE TABLE "+TABLE_NAME + " (" +
            FIELD_ID + " integer primary key autoincrement,"+
            FIELD_Nombre + " text,"+
            FIELD_Apellidos + " text,"+
            FIELD_Rut + " text,"+
            FIELD_Id_Server + " Integer Default 0); ";

    private int _id;
    private String Nombre;
    private String Apellidos;
    private String Rut;
    private int Id_Server;

    public EUsuario(String nombre, String apellidos, String rut) {
        this.Nombre = nombre;
        this.Apellidos = apellidos;
        this.Rut = rut;
    }

    public int getID() {
        return _id;
    }

    public void setID(int ID) {
        this._id = _id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        this.Nombre = nombre;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        this.Apellidos = apellidos;
    }

    public String getRut() {
        return Rut;
    }

    public void setRut(String rut) {
        this.Rut = rut;
    }

    public int getId_Server() {
        return Id_Server;
    }

    public void setId_Server(int idServer) {
        this.Id_Server = idServer;
    }
}
