package com.example.nieves.appmovil;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.maps.android.PolyUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

//Muestra los reclamos, sugerencias o felicitaciones realizadas por el ciudadano.
public class MisFiscalizaciones extends AppCompatActivity {

    private TabHost th;
    private TextView textoR;
    private TextView textoS;
    private ListView listReclamo;
    private ListView listSugerencia;
    private String nombreFoto;
    private String latitud;
    private String longitud;
    private String seleccion;
    private String[] datos;
    private Sigeco db;
    private EnvioServer envio = new EnvioServer();
    private JsonEnvioServer jsonEnvio = new JsonEnvioServer();
    private String datosEnvio;
    private Boolean hay_foto = false;
    private String[] datosUsuario;
    private String idServer;
    private AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_fiscalizaciones);

        //Inicialización TabHost
        th = (TabHost)findViewById(R.id.tabHost);
        //Tab1 Pestaña Reclamo
        th.setup();
        TabHost.TabSpec ts1= th.newTabSpec("Tab1");
        ts1.setIndicator("Reclamos");
        ts1.setContent(R.id.tab1);
        th.addTab(ts1);

        //Tab2 Pestaña Sugerencias o Felicitaciones
        th.setup();
        TabHost.TabSpec ts2= th.newTabSpec("Tab1");
        ts2.setIndicator("Sugerencias o Felicitaciones");
        ts2.setContent(R.id.tab2);
        th.addTab(ts2);

        textoR = (TextView)findViewById(R.id.tvReclamo);
        textoS = (TextView)findViewById(R.id.tvSugerencia);

        //Consulta datos a la base de datos
        db = new Sigeco(this);
        Cursor reclamos =  db.reclamosTodos(); //Cursor que obtiene todos los reclamos realizados por el ciudadano
        Cursor sug_fel =  db.sugerenciasTodas(); //Cursor que obtiene todas las sug o felicitaciones realizadas por el ciudadano

        idServer = db.idServerUsuario(); //Obtiene el id server del usuario
        datosUsuario = db.datosUsuario(); //Obtiene los datos del usuario

        //Muestra una lista con los reclamos realizados con su id, tipo reclamo, fecha de cración y estado.
        if(reclamos.moveToFirst()){
            listReclamo = (ListView) findViewById(R.id.listReclamo);
            ArrayList<HashMap<String, String>> feedList= new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;
            do{
                String id = reclamos.getString(0);
                String tipo = reclamos.getString(1);
                String fecha = reclamos.getString(2);
                String estado = reclamos.getString(3);
                map = new HashMap<String, String>();
                map.put("Id",id);
                map.put("Tipo", tipo);
                map.put("Fecha", fecha);
                map.put("Estado", estado);
                feedList.add(map);
            }while(reclamos.moveToNext());

            SimpleAdapter simpleAdapter = new SimpleAdapter(this, feedList, R.layout.view_item, new String[]{"Id","Tipo", "Fecha", "Estado"},
                    new int[]{R.id.textViewId,R.id.textViewTipo, R.id.textViewFecha, R.id.textViewEstado});
            listReclamo.setAdapter(simpleAdapter);
            listReclamo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //Acciones que se realizan si se selecciona un reclamo de la lista
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    seleccion =((TextView)view.findViewById(R.id.textViewId)).getText().toString(); //Obtiene el id local del reclamo
                    datos = db.datosReclamo(Integer.parseInt(seleccion)); //Obtiene los datos almacenados del reclamo seleccionado
                    builder = new AlertDialog.Builder(MisFiscalizaciones.this);
                    builder.setTitle("Tipo de Reclamo: " + datos[1]);
                    builder.setMessage("Mensaje: " + datos[2]);
                    if(!datos[5].equals("vacio")){ //para mostra la foto si es que existe
                        hay_foto = true;
                        ImageView imageView = new ImageView(getBaseContext());
                        imageView.setAdjustViewBounds(true);
                        imageView.setImageBitmap(createBitMap(datos[5]));
                        builder.setView(imageView);
                    }
                    if(datos[8].equals("Guardado")){ //si tiene estado guardado se muestra boton cancelar, eliminar y enviar sino solo Ok
                        ConexionInternet conn = new ConexionInternet();
                        if(conn.estaConectado(MisFiscalizaciones.this)){
                            builder.setPositiveButton("ENVIAR", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    if (esConce(datos[6],datos[7])){
                                        Log.e("Resp: ", "> dentro de conce lat:"+datos[6]+" longitud:"+datos[7] );
                                        if(idServer.equals("0")){
                                            if (hay_foto==true){
                                                datosEnvio = jsonEnvio.jsonReclamoDatosUsuario(datos[1],datos[2],datos[3],datos[4],nombreFoto,
                                                        datos[6], datos[7],datosUsuario[0],datosUsuario[1],datosUsuario[2],getNumeroTelefono());
                                            }else {
                                                datosEnvio = jsonEnvio.jsonReclamoDatosUsuario(datos[1], datos[2], datos[3], datos[4], null,
                                                        datos[6], datos[7], datosUsuario[0], datosUsuario[1], datosUsuario[2], getNumeroTelefono());
                                            }
                                        }else {
                                            if (hay_foto==true){
                                                datosEnvio = jsonEnvio.jsonReclamoDatosIdUsuario(datos[1],datos[2],datos[3],datos[4],nombreFoto,
                                                        datos[6], datos[7],idServer);
                                            }else {
                                                datosEnvio = jsonEnvio.jsonReclamoDatosIdUsuario( datos[1], datos[2], datos[3], datos[4], null,
                                                        datos[6], datos[7],idServer);
                                            }
                                        }
                                        envio.datosEnvio(datosEnvio, MisFiscalizaciones.this,hay_foto ,seleccion, datos[5]);
                                        envio.enviarServer();
                                    }
                                    else{
                                        mapa(2);
                                    }
                                }
                            });
                        }
                        builder.setNeutralButton("ELIMINAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                db.actualizaEstadoReclamo(1,seleccion);
                                startActivity(new Intent(MisFiscalizaciones.this, MisFiscalizaciones.class));
                            }
                        });
                        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
                    } else {
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                    }
                    builder.create();
                    builder.show();

                }
            });
        }
        else {
            textoR.setText("No ha realizado ningún Reclamo.");
        }

        //Llenar lista con sugerencia o felicitaciones realizadas con su id, tipo, fecha y estado
        if(sug_fel.moveToFirst()){
            listSugerencia = (ListView) findViewById(R.id.listSugerencia);
            ArrayList<HashMap<String, String>> feedList= new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map;
            do{
                String id = sug_fel.getString(0);
                String tipo = sug_fel.getString(1);
                String fecha = sug_fel.getString(2);
                String estado = sug_fel.getString(3);
                map = new HashMap<String, String>();
                map.put("Id",id);
                map.put("Tipo", tipo);
                map.put("Fecha", fecha);
                map.put("Estado", estado);
                feedList.add(map);
            }while(sug_fel.moveToNext());

            SimpleAdapter simpleAdapter = new SimpleAdapter(this, feedList, R.layout.view_item, new String[]{"Id","Tipo","Fecha", "Estado"},
                    new int[]{R.id.textViewId,R.id.textViewTipo, R.id.textViewFecha, R.id.textViewEstado});
            listSugerencia.setAdapter(simpleAdapter);
            listSugerencia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                //Acciones en caso de que se selecione una sugerencia o felicitación
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    seleccion = ((TextView)view.findViewById(R.id.textViewId)).getText().toString();
                    datos = db.datosSugerencia(Integer.parseInt(seleccion)); //Obtiene los datos almacenados de la sugerencia o felicitación seleccionada

                    builder = new AlertDialog.Builder(MisFiscalizaciones.this);
                    builder.setTitle("Tipo de Sugerencia: " + datos[1]);
                    builder.setMessage("Mensaje: " + datos[2]);
                    if (datos[7].equals("Guardado")){ //si tiene estado guardado se muestra botón cancelar, elimiar y enviar sino solo ok
                        ConexionInternet conn = new ConexionInternet();
                        if(conn.estaConectado(MisFiscalizaciones.this)){
                            builder.setPositiveButton("ENVIAR", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Log.e("Resp: ", "> act result lat:" + datos[5] + " long:" + datos[6]);
                                    if (esConce(datos[5],datos[6])){
                                        Log.e("Resp: ", "dentro if");
                                        if(idServer.equals("0")){
                                            datosEnvio = jsonEnvio.jsonSugerenciaDatosUsuario(datos[1],datos[2],datos[3],datos[4],datos[5],datos[6],
                                                    datosUsuario[0],datosUsuario[1],datosUsuario[2],getNumeroTelefono());
                                        }else {
                                            datosEnvio = jsonEnvio.jsonSugerenciaDatosIdUsuario(datos[1], datos[2], datos[3], datos[4], datos[5],
                                                    datos[6], idServer);
                                        }
                                        envio.datosEnvio(datosEnvio, MisFiscalizaciones.this, false, seleccion, null);
                                        envio.enviarServer();
                                    }
                                    else{
                                        Log.e("Resp: ", "> dentro if");
                                        mapa(1);
                                    }
                                }
                            });
                        }
                        builder.setNeutralButton("ELIMINAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                db.actualizaEstadoSugerencia(1, seleccion);
                                startActivity(new Intent(MisFiscalizaciones.this, MisFiscalizaciones.class));
                            }
                        });
                        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                            }
                        });
                    } else {
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                            }
                        });
                    }
                    builder.create();
                    builder.show();
                }
            });
        }
        else{
            textoS.setText("No ha realizado ninguna sugerencia o felicitación.");
        }

    }

    public String getNumeroTelefono()
    {
        return ((TelephonyManager) getSystemService(TELEPHONY_SERVICE))
                .getLine1Number();
    }

    //crea el bitmap para mostar la foto si existe en un mensaje cuando se seleciona el reclamo
    public Bitmap createBitMap(String ruta){
        Bitmap foto = null;
        File imgFile = new  File(ruta);
        nombreFoto = imgFile.getName();
        if(imgFile.exists()){
            foto = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        }
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(foto,200, 200, false);
        return  resizedBitmap;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mis_fiscalizaciones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //Consulta si la lat o long almacenados en la base de datos interna son de concepción
    public boolean esConce(String latitud, String longitud){
        PolygonOptions concepcion;
        Boolean en_conce;
        AlertDialog.Builder builder;
        LatLng point = new LatLng(Double.parseDouble(latitud),Double.parseDouble(longitud));
        concepcion = PoligonoConcepcion.Concepcion();
        en_conce = PolyUtil.containsLocation(point, concepcion.getPoints(), false);
        if(en_conce) return true;
        else return false;
    }

    //Pide que seleccione un punto en el mapa si la lat y long almacenados son fuera de concepción
    public void mapa(final int control){
        String mensaje = "";
        if(control == 1){
            mensaje = "La dirección de su sugerencia o felicitación esta fuera de Concepción," +
                    "por favor seleccione un punto en el mapa. ";
        }
        else if(control ==2){
            mensaje = "La dirección de su reclamo esta fuera de Concepción, por favor seleccione" +
                    "un punto en el mapa.";
        }
        Log.e("Resp: ", "> mapa");
        builder = new AlertDialog.Builder(MisFiscalizaciones.this);
        builder.setTitle("Importante");
        builder.setMessage(mensaje);
        builder.setPositiveButton("MAPA", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent mapa = new Intent(MisFiscalizaciones.this, Mapa.class);
                startActivityForResult(mapa,control);
            }
        });
        builder.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        builder.create();
        builder.show();
    }

    //Obtienen la lat y long seleccionadas en el mapa
    @Override
    protected void  onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            latitud = data.getStringExtra("latitud");
            longitud = data.getStringExtra("longitud");
            if(requestCode == 1){
                db.actualizaLatLng(1,latitud,longitud,seleccion);
                Toast.makeText(this, "Se ha actualizado la ubicación de su sugerencia o felicitación.", Toast.LENGTH_LONG).show();
            }else if (requestCode == 2){
                db.actualizaLatLng(2,latitud,longitud,seleccion);
                Toast.makeText(this, "Se ha actualizado la ubicación de su reclamo.", Toast.LENGTH_LONG).show();
            }

        }
        Log.e("Resp: ", "> act result lat:" + latitud + " long:" + longitud);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getBaseContext(), MenuPrincipal.class));
    }
}
