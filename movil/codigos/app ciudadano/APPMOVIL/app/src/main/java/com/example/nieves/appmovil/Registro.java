package com.example.nieves.appmovil;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//Regitro de nombres, apellidos y rut del ciudadano
public class Registro extends AppCompatActivity implements View.OnClickListener {

    Button us_Login;
    EditText us_Nombres,us_Apellidos,us_Rut;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        us_Nombres = (EditText) findViewById(R.id.us_Nombres);
        us_Apellidos = (EditText) findViewById(R.id.us_Apellidos);
        us_Rut = (EditText) findViewById(R.id.us_Rut);
        us_Login = (Button) findViewById(R.id.us_Login);
        us_Login.setOnClickListener(this);
        findViewById(R.id.us_Login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean g = guardar(v);
                if(g) startActivity(new Intent(Registro.this, MenuPrincipal.class));
            }
        });

    }

    //metodo que guarda los datos del ciudadano en la base de datos del dispositivo
    public boolean guardar(View v) {
        String[] nombre = new String[2];
        String[] apellido = new String[2];
        Validaciones val = new Validaciones();
        nombre = val.validarNombres(us_Nombres.getText().toString()); //metodo que valida los nombres
        apellido = val.validarApellidos(us_Apellidos.getText().toString()); //metodo que valida los apellidos
        if (nombre[0].equals("true")) {
            if ( apellido[0].equals("true")){
                if (validarRut(us_Rut.getText().toString())) {
                    Sigeco admin = new Sigeco(this);
                    SQLiteDatabase bd = admin.getWritableDatabase();
                    String nombres = us_Nombres.getText().toString();
                    String apellidos = us_Apellidos.getText().toString();
                    String rut = us_Rut.getText().toString();
                    rut = formatear(rut);
                    ContentValues registro = new ContentValues();
                    registro.put("Nombres", nombres);
                    registro.put("Apellidos", apellidos);
                    registro.put("Rut", rut);
                    bd.insert("Usuario", null, registro);
                    bd.close();
                    Toast.makeText(this, "Se han guardado sus datos exitosamente.", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    us_Rut.setError("Ingrese R.U.T. válido");
                    return false;
                }
            } else {
                us_Apellidos.setError(apellido[1]);
                return false;
            }
        } else {
            us_Nombres.setError(nombre[1]);
            return false;
        }
    }

    //metodo para validar el rut ingresado
    public static boolean validarRut(String rut) {

        boolean validacion = false;
        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            if(!rut.equals("11111111")){
                int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

                char dv = rut.charAt(rut.length() - 1);

                int m = 0, s = 1;
                for (; rutAux != 0; rutAux /= 10) {
                    s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
                }
                if (dv == (char) (s != 0 ? s + 47 : 75)) {
                    validacion = true;
                }
            }
            else validacion = false;

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }

    //metodo para darle un formato especifico al rut ingresado rut solo con guion
    static public String formatear(String rut){
        int cont=0;
        String format;
        if(rut.length() == 0){
            return "";
        }else{
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            format = "-"+rut.substring(rut.length()-1);
            for(int i = rut.length()-2;i>=0;i--){
                format = rut.substring(i, i+1)+format;
                cont++;
            }
            return format;
        }
    }

    public void onClick(View v) {
        Intent intent = new Intent(Registro.this, MenuPrincipal.class);
        startActivity(intent);
    }
}
