package com.example.steffyta.appmovil2;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;


public class Tipo_Reclamo extends AppCompatActivity {
    private RadioButton TipoReclamo;
    private String tipo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tipo__reclamo);
        //pasar text de radiobutton  a activity reclamo
        findViewById(R.id.rbtnVehiculo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Tipo_Reclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnVehiculo);
                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                startActivity(i);
                //startActivity(new Intent(Tipo_Reclamo.this, Reclamo.class));
            }
        });
        findViewById(R.id.rbtnEscombros).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Tipo_Reclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnEscombros);
                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                startActivity(i);
                //startActivity(new Intent(Tipo_Reclamo.this, Reclamo.class));
            }
        });
        findViewById(R.id.rbtnMicrobasural).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Tipo_Reclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnMicrobasural);
                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                startActivity(i);
                //startActivity(new Intent(Tipo_Reclamo.this, Reclamo.class));
            }
        });
        findViewById(R.id.rbtnTrabajadores).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Tipo_Reclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnTrabajadores);
                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                startActivity(i);
                //startActivity(new Intent(Tipo_Reclamo.this, Reclamo.class));
            }
        });
        findViewById(R.id.rbtnCalle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Tipo_Reclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnCalle);
                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                startActivity(i);
                //startActivity(new Intent(Tipo_Reclamo.this, Reclamo.class));
            }
        });
        findViewById(R.id.rbtnResiduos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Tipo_Reclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnResiduos);
                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                startActivity(i);
                //startActivity(new Intent(Tipo_Reclamo.this, Reclamo.class));
            }
        });
        findViewById(R.id.rbtnSumideros).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Tipo_Reclamo.this,Reclamo.class);
                TipoReclamo = (RadioButton)findViewById(R.id.rbtnSumideros);
                i.putExtra("TipoReclamo",TipoReclamo.getText().toString());
                startActivity(i);
                //startActivity(new Intent(Tipo_Reclamo.this, Reclamo.class));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_reclamo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    public void onRadioButtonClicked(View view){

        boolean checked=((RadioButton) view).isChecked();
        switch(view.getId()){

            case R.id.rbtnVehiculo:
                if(checked)
                    //tipo ="vehiculo";
                    break;
            case R.id.rbtnEscombros:
                if(checked)
                    //tipo ="Escombros";
                    break;
            case R.id.rbtnMicrobasural:
                if(checked)
                    //tipo ="Microbasural";
                    break;
            case R.id.rbtnTrabajadores:
                if(checked)
                    //tipo ="Trabajadores";
                    break;
            case R.id.rbtnCalle:
                if(checked)
                    //tipo ="Limpieza de calles";
                    break;
            case R.id.rbtnResiduos:
                if(checked)
                    //tipo ="Residuos Domiciliarios";
                    break;
            case R.id.rbtnSumideros:
                if(checked)
                    //tipo ="Sumideros de aguas lluvia";
                    break;
        }
    }
}
