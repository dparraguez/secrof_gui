package com.example.steffyta.appmovil2;

/**
 * Created by Steffyta on 09-07-2015.
 */
public class EReclamo{
        public static  final  String TABLE_NAME_RECLAMO = "Reclamo";
        public static final String FIELD_ID = " Id ";
        public static final String FIELD_Tipo = " Tipo ";
        public static final String FIELD_Descripcion = " Descripcion ";
        public static final String FIELD_Fecha_C = " Fecha_C ";
        public static final String FIELD_Hora_C = " Hora_C ";
        public static final String FIELD_Foto = " Foto ";
        public static final String FIELD_Latitud = " Latitud ";
        public static final String FIELD_Longitud = " Longitud ";
        public static final String FIELD_Estado = " Estado ";
        public static final String CREATE_DB_TABLE=" CREATE TABLE "+TABLE_NAME_RECLAMO + "(" +
                FIELD_ID + " integer primary key autoincrement,"+
                FIELD_Tipo + " text,"+
                FIELD_Descripcion + " text not null,"+
                FIELD_Fecha_C + " text,"+
                FIELD_Hora_C + " text,"+
                FIELD_Foto + " text,"+
                FIELD_Latitud + " text,"+
                FIELD_Longitud + " text,"+
                FIELD_Estado + " Integer Default 0 "
                +");";

        private int Id;
        private String Tipo;
        private String Descripcion;
        private String Fecha_C;
        private String Hora_C;
        private String Foto;
        private String Latitud;
        private String Longitud;
        private int Estado;

        public EReclamo(String tipo, String descripcion, String fecha_C, String hora_C, String foto, String latitud, String longitud) {
            this.Tipo = tipo;
            this. Descripcion = descripcion;
            this.Fecha_C = fecha_C;
            this.Hora_C = hora_C;
            this.Foto = foto;
            this.Latitud = latitud;
            this.Longitud = longitud;
        }

        private EReclamo(Reclamo reclamo){


        }
        public int getID() {
            return Id;
        }

        public void setID(int ID) {
            this.Id = Id;
        }

        public String getTipo() {
            return Tipo;
        }

        public void setTipo(String tipo) {
            this.Tipo = tipo;
        }

        public String getDescripcion() {
            return Descripcion;
        }

        public void setDescripcion(String descripcion) {
            this.Descripcion = descripcion;
        }

        public String getFecha_C() {
            return Fecha_C;
        }

        public void setFecha_C(String fecha_C) {
            this.Fecha_C = fecha_C;
        }

        public String getHora_C() {
            return Hora_C;
        }

        public void setHora_C(String hora_C) {
            this.Hora_C = hora_C;
        }

        public String getFoto() {
            return Foto;
        }

        public void setFoto(String foto) {
            this.Foto = foto;
        }

        public String getLongitud() {
            return Longitud;
        }

        public void setLongitud(String longitud) {
            this.Longitud = longitud;
        }

        public String getLatitud() {
            return Latitud;
        }

        public void setLatitud(String latitud) {
            this.Latitud = latitud;
        }

        public int getEstado() {
            return Estado;
        }

        public void setEstado(int estado) {
            this.Estado = estado;
        }
    }
