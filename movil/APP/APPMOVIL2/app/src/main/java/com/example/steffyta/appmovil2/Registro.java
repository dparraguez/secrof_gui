package com.example.steffyta.appmovil2;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Registro extends AppCompatActivity implements View.OnClickListener {

    Button us_Login;
    EditText us_Nombres,us_Apellidos,us_Rut;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        us_Nombres = (EditText) findViewById(R.id.us_Nombres);
        if (us_Nombres.getText().toString().length() == 0){
            us_Nombres.setError("Ingrese Nombres");
        }
        us_Apellidos = (EditText) findViewById(R.id.us_Apellidos);
        if (us_Apellidos.getText().toString().length() == 0){
            us_Apellidos.setError("Ingrese Apellidos");
        }
        us_Rut = (EditText) findViewById(R.id.us_Rut);
        if (us_Rut.getText().toString().length() == 0){
            us_Rut.setError("Ingrese Rut");
        }
        us_Login = (Button) findViewById(R.id.us_Login);

        us_Login.setOnClickListener(this);
        findViewById(R.id.us_Login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alta(v);
                startActivity(new Intent(Registro.this, Menu_Principal.class));
            }
        });

    }

    public void alta(View v) {
        Secrof admin = new Secrof(this);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String nombres = us_Nombres.getText().toString();
        String apellidos = us_Apellidos.getText().toString();
        String rut = us_Rut.getText().toString();
        ContentValues registro = new ContentValues();
        registro.put("Nombres", nombres);
        registro.put("Apellidos", apellidos);
        registro.put("Rut", rut);
        bd.insert("Usuario", null, registro);
        bd.close();
        //no se para q es
       /* et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");*/
        Toast.makeText(this, "Se han guardado sus datos exitosamente",
                Toast.LENGTH_SHORT).show();
    }

    public void onClick(View v) {
        Intent intent = new Intent(Registro.this, Menu.class);
        startActivity(intent);
    }
}