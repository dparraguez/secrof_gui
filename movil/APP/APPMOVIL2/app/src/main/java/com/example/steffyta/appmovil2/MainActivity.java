package com.example.steffyta.appmovil2;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.support.v7.app.AppCompatActivity;



public class MainActivity extends AppCompatActivity{

    //RUTA POR DEFECTO DE BD EN ANDROID
    private static String DB_PATH = "/data/data/com.example.steffyta.appmovil2/databases/";
    private static final String DB_NAME= "app";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        SQLiteDatabase checkBD = null;
        try{
            String myPath=DB_PATH + DB_NAME;
            checkBD=SQLiteDatabase.openDatabase(myPath,null,SQLiteDatabase.OPEN_READONLY);
        }catch (SQLiteException e){

            //si llegamos aqu� es porque la base de datos no existe todavia
            startActivity(new Intent(MainActivity.this, Registro.class));
        }
        if(checkBD!=null){
            checkBD.close();
            startActivity(new Intent(MainActivity.this,Menu_Principal.class));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
