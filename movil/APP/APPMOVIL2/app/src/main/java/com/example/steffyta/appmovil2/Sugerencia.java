package com.example.steffyta.appmovil2;


import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class Sugerencia extends AppCompatActivity implements View.OnClickListener {
    private Button btnEnviar;
    private String tipo;
    private EditText su_Descripcion;
    private RadioButton sugerencia,felicitacion;
    PosicionGps gps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugerencia);
        su_Descripcion =(EditText) findViewById (R.id.su_descripcion);
        if (su_Descripcion.getText().toString().length() == 0){
            su_Descripcion.setError("Ingrese Descripci�n");
        }
        sugerencia=(RadioButton)findViewById(R.id.rbtnSugerencia);
        felicitacion=(RadioButton)findViewById(R.id.rbtnFelicitacion);

        btnEnviar = (Button) findViewById(R.id.btnEnviar);
        btnEnviar.setOnClickListener(this);
        findViewById(R.id.btnEnviar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Sugerencia.this, Menu_Principal.class));
                Operar(v);
            }
        });

    }

    public void Operar(View v) {
        Secrof admin = new Secrof(this);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String Descripcion = su_Descripcion.getText().toString();

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
        Date fechaHoy = Calendar.getInstance().getTime();
        String hora = formatoHora.format(fechaHoy);
        String fecha = formato.format(fechaHoy);

        gps=new PosicionGps(Sugerencia.this);
        double latitude = gps.getLatitud();
        double longitude=gps.getLongitud();
        //String l = Double.toString(latitude);

        Toast.makeText(getApplicationContext(),"lat"+latitude+ "long"+longitude,Toast.LENGTH_LONG).show();

        // String longitude = gps.getLongitud();

        ContentValues Sugerencia = new ContentValues();

        Sugerencia.put("Descripcion", Descripcion);
        Sugerencia.put("Tipo",tipo);
        Sugerencia.put("Fecha_C",fecha);
        Sugerencia.put("Hora_C",hora);
        Sugerencia.put("Latitud", latitude);
        Sugerencia.put("Longuitud",longitude);
        bd.insert("Sugerencia", null,Sugerencia);
        startActivity(new Intent(Sugerencia.this, Menu_Principal.class));
        bd.close();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sugerencia, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Importante");
        builder.setMessage("Su mensaje a sido enviado exitosamente");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            //funci�n onClick para volver al Menu_Principal luego de apretar 'OK' en el AlertDialog
            @Override
            public void onClick(DialogInterface dialog, int which){
                Intent intent = new Intent(getBaseContext(), Menu_Principal.class);
                startActivity(intent);
            }
        });
        builder.create();
        builder.show();
    }

    public void onRadioButtonClicked(View view){

        boolean checked=((RadioButton) view).isChecked();
        switch(view.getId()){

            case R.id.rbtnSugerencia:
                if(checked)
                    tipo ="sugerencia";
                break;
            case R.id.rbtnFelicitacion:
                if(checked)
                    tipo ="felicitacion";
                break;
        }
    }
}