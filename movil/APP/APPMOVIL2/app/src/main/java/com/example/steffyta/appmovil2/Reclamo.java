package com.example.steffyta.appmovil2;

/**
 * Created by Steffyta on 16-07-2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
    import android.content.ContentValues;
    import android.content.DialogInterface;
    import android.content.Intent;
    import android.database.sqlite.SQLiteDatabase;
    import android.graphics.Bitmap;
    import android.provider.MediaStore;
    import android.support.v7.app.ActionBarActivity;
    import android.os.Bundle;
    import android.support.v7.app.AppCompatActivity;
    import android.view.Menu;
    import android.view.MenuItem;
    import android.view.View;
    import android.widget.Button;
    import android.widget.EditText;
    import android.widget.ImageView;
    import android.widget.TextView;
    import android.widget.Toast;

    import java.text.SimpleDateFormat;
    import java.util.Calendar;
    import java.util.Date;
    import android.location.Location;
    import android.location.LocationManager;
    import android.location.LocationListener;

public class Reclamo extends AppCompatActivity implements View.OnClickListener {

        private Button btnCamara;
        private Button btnEnviar;
        TextView tipoReclamo;
        private EditText re_Descripcion;
        private ImageView img;
        Intent i;
        private String tipo;
        private Bitmap bmp;
        final static int cons = 0;
        PosicionGps gps;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_reclamo);
            init();
        }

        public void init(){
            //recibir text de activity Tipo_Reclamo
            Bundle bundle = getIntent().getExtras();

            tipoReclamo = (TextView)findViewById(R.id.textReclamo);
            tipoReclamo.setText(tipoReclamo.getText() + bundle.getString("TipoReclamo"));

            re_Descripcion =(EditText) findViewById (R.id.re_Descripcion);

            tipo = bundle.getString("TipoReclamo");

            btnCamara = (Button)findViewById(R.id.btnCamara);
            btnCamara.setOnClickListener(this);
            btnEnviar = (Button)findViewById(R.id.btnEnviar);
            btnEnviar.setOnClickListener(this);
            img = (ImageView)findViewById(R.id.viewFoto);

        }



        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_reclamo, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
        public void Guardar(View v) {

            Secrof admin = new Secrof(this);
            SQLiteDatabase bd = admin.getWritableDatabase();
            String Descripcion = re_Descripcion.getText().toString();

            SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm");
            Date fechaHoy = Calendar.getInstance().getTime();
            String hora = formatoHora.format(fechaHoy);
            String fecha = formato.format(fechaHoy);

            gps=new PosicionGps(Reclamo.this);
            double latitude = gps.getLatitud();
            double longitude = gps.getLongitud();
            //String l = Double.toString(latitude);
            Toast.makeText(getApplicationContext(),"lat"+latitude+ "long"+longitude,Toast.LENGTH_LONG).show();
            // String longitude = gps.getLongitud();

            ContentValues Reclamo = new ContentValues();

            Reclamo.put("Descripcion", Descripcion);
            Reclamo.put("Tipo", tipo);
            Reclamo.put("Fecha_C",fecha);
            Reclamo.put("Hora_C",hora);
            Reclamo.put("Latitud",latitude);
            Reclamo.put("longitud", longitude);
            bd.insert("Reclamo", null, Reclamo);
            Guardar(v);
            startActivity(new Intent(Reclamo.this, Menu_Principal.class));
            bd.close();
        }

        @Override
        public void onClick(View v) {
            // int id;
            // id= v.getId();
            switch (v.getId())
            {
                case R.id.btnCamara:
                    i= new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(i,cons);
                    break;
                case R.id.btnEnviar:
                    Guardar(v);
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Importante");
                    builder.setMessage("Su mensaje a sido enviado exitosamente");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
                        //funci�n onClick para volver al Menu_Principal luego de apretar 'OK' en el AlertDialog
                        @Override
                        public void onClick(DialogInterface dialog, int which){
                            Intent intent = new Intent(getBaseContext(), Menu_Principal.class);
                            startActivity(intent);
                        }
                    });
                    builder.create();
                    builder.show();
            }
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode,Intent data)
        {
            super.onActivityResult(requestCode,resultCode, data);
            if(resultCode == Activity.RESULT_OK)
            {
                Bundle ext = data.getExtras();
                bmp =(Bitmap)ext.get("data");
                img.setImageBitmap(bmp);
            }
        }
    }
