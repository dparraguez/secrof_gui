package com.example.steffyta.appmovil2;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
/**
 * Created by Steffyta on 11-06-2015.
 */
public class Secrof extends SQLiteOpenHelper{

    //crea base de datos llamada app
    private static final String DB_NAME= "app";
    private static final int SCREME_VERSION =1;
    private SQLiteDatabase app;

    public Secrof(Context context){
        super(context,DB_NAME,null, SCREME_VERSION);
        app=this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(EUsuario.CREATE_DB_TABLE);
        db.execSQL(EReclamo.CREATE_DB_TABLE);
        db.execSQL(ESugerencia.CREATE_DB_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
