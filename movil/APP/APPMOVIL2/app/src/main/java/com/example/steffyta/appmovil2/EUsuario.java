package com.example.steffyta.appmovil2;

/**
 * Created by Steffyta on 09-07-2015.
 */
    public class EUsuario{
        public static  final  String TABLE_NAME = "Usuario";
        public static final String FIELD_ID ="_id";
        public static final String FIELD_Nombre ="Nombres";
        public static final String FIELD_Apellidos ="Apellidos";
        public static final String FIELD_Rut="Rut";
        public static final String CREATE_DB_TABLE=" CREATE TABLE "+TABLE_NAME + " (" +
                FIELD_ID + " integer primary key autoincrement,"+
                FIELD_Nombre + " text,"+
                FIELD_Apellidos + " text,"+
                FIELD_Rut + " text);";

        private int _id;
        private String Nombre;
        private String Apellidos;
        private String Rut;

        public EUsuario(String nombre, String apellidos, String rut) {
            this.Nombre = nombre;
            this.Apellidos = apellidos;
            this.Rut = rut;
        }


        public int getID() {
            return _id;
        }

        public void setID(int ID) {
            this._id = _id;
        }

        public String getNombre() {
            return Nombre;
        }

        public void setNombre(String nombre) {
            this.Nombre = nombre;
        }

        public String getApellidos() {
            return Apellidos;
        }

        public void setApellidos(String apellidos) {
            this.Apellidos = apellidos;
        }

        public String getRut() {
            return Rut;
        }

        public void setRut(String rut) {
            this.Rut = rut;
        }
    }
