--Función que calcula el consumo en litros para un periodo determinado de un vehiculo
CREATE OR REPLACE FUNCTION costo_total_vehiculo_diario(timestamp,timestamp,integer) RETURNS real AS $$
DECLARE
    f_inicio ALIAS FOR $1;
    f_termino ALIAS FOR $2;
    idvehiculo ALIAS FOR $3;
    r posicion%rowtype;
    suma real;
BEGIN
suma=0;
    --Ciclo que recorre todas las tuplas del periodo de fechas recibido como parametro
    FOR r IN SELECT * FROM posicion
    where to_timestamp(fecha) between f_inicio and f_termino and id_vehiculo=idvehiculo
    LOOP
         --Se realiza la suma de los costos llamando a la funcion de consumo 
         suma = suma + funcion_consumo(0.00104,0.000055,0.0000000684,(r.id));
         
    END LOOP;


   RETURN suma/1000;
END
$$
LANGUAGE 'plpgsql' ;



/*
update  vehiculo set litros=suma/1000 where id=idvehiculo;
    update  vehiculo set valor=(suma/1000)*dinero where id=idvehiculo;
 
INSERT INTO consumo_diario_vehiculo (fecha, consumo, patente)
VALUES ('Los Angeles', 900, '10-Jan-1999');*/