﻿/* recibe parametros desde el lenguaje cliente contempla el calculo para un punto en mililitros por segundo*/
CREATE OR REPLACE FUNCTION funcion_consumo (alfa0 float,alfa1 float,alfa2 float,id_gps integer) RETURNS real AS $$
DECLARE
--- datos de entrada alfa0 numeric, alfa1 numeric, alfa2 numeric,tvelocidad numeric, tpendiente numeric,taceleracion numeric, idcamion varchar(60) ---
consumo_combustible float;
potencia float;
velocidad_gps float;
resistencias float;
aceleracion float;
pendiente float;
pendiente_sin_signo float;

BEGIN
--aceleracion:=funcion_aceleracion(id_gps);
aceleracion:=0.5;
Select p.velocidad into velocidad_gps from posicion p where id=id_gps; 
--pendiente_sin_signo:=get_pendiente(id_pendientes,id_gps);-- retorna el id_pendiente de la calle mas cercana al punto del gps--
pendiente:= pendiente_final(id_gps);

 -- funcion usada para calcular las fuerzas resistentes en un camion --
resistencias :=(6.75 *velocidad_gps) + (9.81 *pendiente) + 942.31;

-- funcion usada para calcular la la potencia generada por el camion --
potencia :=(( (resistencias + (1400 * aceleracion) )/ 2700) ) * velocidad_gps;


-- Funcion usada para calcular el consumo de combustible

if (potencia >=0)
then 
consumo_combustible := alfa0 + ( alfa1 * potencia)+ (alfa2 * power(potencia,2)) ;

RETURN consumo_combustible;
 END IF;

 
if (potencia <0)
then
 consumo_combustible := alfa0;
 END IF;
 
RETURN consumo_combustible;

END;
$$ LANGUAGE plpgsql;