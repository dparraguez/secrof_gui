﻿/* recibe parametros desde el lenguaje cliente contempla el calculo para un punto en mililitros por segundo*/
CREATE OR REPLACE FUNCTION funcion_consumo (alfa0 float,alfa1 float,alfa2 float,id_gps integer) RETURNS real AS $$
DECLARE
--- datos de entrada alfa0 numeric, alfa1 numeric, alfa2 numeric,tvelocidad numeric, tpendiente numeric,taceleracion numeric, idcamion varchar(60) ---
consumo_combustible float;
potencia float;
velocidad_gps float;
resistencias float;
aceleracion float;
pendiente float;
m float;
Af float;
Ch float;
Cd float;
p float;
Cr float;
c1 float;
c2 float;
RA float;
RR float;
RP float;

BEGIN

-- CONSTANTES Resistencia Aerodinámica--
Af = 4 ; --Area Frontal-- 
Ch = 1 ; --Correción por altura--
Cd = 0.75  ; --Coeficiente de arrastre--
p = 1.2256 ; --Densidad del Aire--
m = 14000 ; -- Masa del camión--

-- Constantes Resistencia a la rodadura--
Cr = 1.5 ; --coeficiente de friccion a la rodadura--
c1 =  0.0328 ; --Neumático radial--
c2 = 4.575 ; --Neumático radial--


--aceleracion:=funcion_aceleracion(id_gps);
aceleracion:=aceleracion(id_gps);
Select p.velocidad into velocidad_gps from posicion p where id=id_gps; 
-- paso la velocidad de K/h a m/s

velocidad_gps:=(velocidad_gps/3.6);

--pendiente_sin_signo:=get_pendiente(id_pendientes,id_gps);-- retorna el id_pendiente de la calle mas cercana al punto del gps--
pendiente:= pendiente_final(id_gps);


-- Resistencia Areodinámica -- 

  RA :=  (p/25.92)*Cd*Ch*Af*power(velocidad_gps,2);

-- Resistencia Rodadura --

 RR :=  ((9.8066*m)*(Cr/1000))*(c1*velocidad_gps + c2);

-- Resistencia debido a la Pendiente --

 RP := 9.8066 * m * pendiente;

 -- Resistencia --
 
resistencias := RA + RR + RP;

-- funcion usada para calcular la la potencia generada por el camion --
potencia :=(( (resistencias + ((1+0.1)*(14000 * aceleracion)) )/ 2700) ) * velocidad_gps;


-- Funcion usada para calcular el consumo de combustible

if (potencia >=0)
then 
consumo_combustible := alfa0 + ( alfa1 * potencia)+ (alfa2 * power(potencia,2)) ;


 END IF;

 
if (potencia <0)
then
 consumo_combustible := alfa0;
 END IF;
 
RETURN consumo_combustible;

END;


$$ LANGUAGE plpgsql;

