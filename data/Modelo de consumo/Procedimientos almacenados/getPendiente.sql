﻿CREATE OR REPLACE FUNCTION get_pendiente(text) RETURNS real AS $$
    DECLARE
      point alias for $1;
      r real;                                                    
    BEGIN
 
       -- La  subconsulta obtiene las distancias de un punto a las rectas alojadas en la tabla 'pendientes_concepcion' de la cual
       -- solo muestra la menor, mientras que la otra consulta simplemente selecciona el campo pendiente de la tupla anteriormente elejida
       -- se castea a geography para que entregue un valor en metros del calculo de la distancia.
       
	SELECT p.pendiente into r FROM (SELECT p.pendiente,ST_Distance(ST_GeomFromText(point,4326)::geography,p.calle::geography) as dis
	FROM pendientes_concepcion p
	ORDER BY dis ASC LIMIT 1) as p; 

      RETURN r;                                                       
    END;
    $$ LANGUAGE plpgsql;
