﻿-- Se recibe el id de la posicion de un camion y se calcula la velocidad de media para este con su posicion inmediatamente anterior.
-- Retorna la aceleracion media en metros por segundo. 
--OBS:CORREJIR EL LIMITE DE ID ANTERIOR SEGUN SEA EL VALOR DEL MENOR ID EN LA TABLA FINAL
CREATE OR REPLACE FUNCTION aceleracion(bigint) RETURNS real AS $$
DECLARE

   --Variables a utilizar.
   
    id_actual ALIAS FOR $1;
    id_anterior int;
    v1 real; 
    v2 real;
    t1 real;
    t2 real;
    tf real;
    a real;
    flag int;
    id_camion_actual integer;
    id_camion_buscado integer; 
BEGIN

    --variables necesarias para el posterior ciclo
    flag:=0;
    SELECT p.id_vehiculo into id_camion_buscado FROM posicion p where p.id=id_actual;
    id_anterior := (id_actual-1);

    -- ciclo que se ejecuta hasta encontrar la tupla inmediatamente aterior del id del camion actual recibido
    -- sino hay posicion anterior registrada se asume v1 y t1 = 0;
        
    while flag=0 LOOP
    
    SELECT p.id_vehiculo into id_camion_actual FROM posicion p where p.id=id_anterior;

    if(id_camion_actual=id_camion_buscado) then flag:=1;
    ELSE
    id_anterior := (id_anterior-1);
    end if;

    if(id_anterior<=2)then flag:=1;
    end if;
    
    end LOOP;
    
   

    --Obtenemos las velocidades	
    SELECT p.velocidad into v2 FROM posicion p where p.id=id_actual;
    if(id_anterior<=2)then v1=0;
    ELSE
    SELECT p.velocidad into v1 FROM posicion p where p.id=id_anterior;
    end if;

    --Extraemos el tiempo en segundos de las posiciones
    SELECT extract(epoch from (to_timestamp(p.fecha)::timestamp without time zone)::time)::integer into t2 FROM posicion p where p.id=id_actual;
    if(id_anterior<=2)then t1=0;
    else
    SELECT extract(epoch from (to_timestamp(p.fecha)::timestamp without time zone)::time)::integer into t1 FROM posicion p where p.id=id_anterior;
    end if;
    --Convertimos la velocidad de km/h a m/s.
    v1:=(v1/3.6);
    v2:=(v2/3.6);


    tf=t2-t1;

    --Calculamos la aceleracion media.	
    a := (v2-v1)/tf;

    
    return a;
    
END;
$$ LANGUAGE plpgsql;

