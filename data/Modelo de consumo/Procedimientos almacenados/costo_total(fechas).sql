﻿--Función que calcula el costo total para un periodo determinado
CREATE OR REPLACE FUNCTION costo_total(timestamp,timestamp) RETURNS real AS
$BODY$
DECLARE
    f_inicio ALIAS FOR $1;
    f_termino ALIAS FOR $2;
    r posicion%rowtype;
    suma real;
BEGIN
suma=0;
    --Ciclo que recorre todas las tuplas del periodo de fechas recibido como parametro
    FOR r IN SELECT * FROM posicion
    where to_timestamp(fecha) between f_inicio and f_termino
    LOOP
         --Se realiza la suma de los costos llamando a la funcion de consumo 
         suma = suma + funcion_consumo(1,1,1,(r.id));
         
    END LOOP;
    RETURN suma;
END
$BODY$
LANGUAGE 'plpgsql' ;

  select costo_total('2015-05-11 17:00:00','2015-05-11 17:59:59');

  