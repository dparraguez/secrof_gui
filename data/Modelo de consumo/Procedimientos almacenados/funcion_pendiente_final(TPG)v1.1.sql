﻿/* funcion que de vuelve la pendiente, a partir del id(pendiente) y id(gps), calcula la direccion y establece el signo de la pendiente*/
CREATE OR REPLACE FUNCTION pendiente_final(id_gps integer) RETURNS double precision AS $$
  DECLARE
  
  pendiente_punto double precision;
  direccion_calle character varying(10);
  direccion_punto character varying(10);
  latitud_gps double precision;
  longitud_gps double precision;
  
  lat1 double precision;
  long1 double precision;
  lat2 double precision;
  long2 double precision;
 diferencia_latitud double precision;
 diferencia_longitud double precision;
 angulo_punto double precision;
 id_pendiente real;
 point POINT;
  BEGIN
  pendiente_punto:=0;
select latitud into latitud_gps from posicion where id=id_gps  ;
select longitud into longitud_gps from posicion where id=id_gps  ;
  /* obtengo el id la calle mas cercana al punto del gps */
  id_pendiente:= get_pendiente(longitud_gps, latitud_gps);
/* extraigo las latitudes y logitudes de la calle */
SELECT latitud1 into lat1 from pendientes  where id=id_pendiente ;
SELECT latitud2 into lat2 from pendientes  where id=id_pendiente ;
SELECT longitud1 into long1 from pendientes  where id=id_pendiente ;
SELECT longitud2 into long2 from pendientes   where id=id_pendiente ;

/* calculo de la diferencia entre las latitudes y longitudes, apartir de eso estimo la direccion  de la calle */



diferencia_latitud:=(lat2*(-1)) - (lat1*(-1)) ;
diferencia_longitud:=(long2*(-1)) -  (long1*(-1) );


/* caso cuando es norEste*/
   if(diferencia_latitud>0 and diferencia_longitud>0) then
direccion_calle:='NE'; END IF;
/* caso cuando es surOeste*/
 if(diferencia_latitud<0 and diferencia_longitud<0)then
direccion_calle:='SO'; END IF;
   /*caso cuando es norOeste*/
   if(diferencia_latitud>0 and diferencia_longitud<0)then
direccion_calle:='NO'; END IF;
/* caso cuando es surEste*/
  if(diferencia_latitud<0 and diferencia_longitud>0)then
direccion_calle:='SE'; END IF;
/* caso cuando es Norte*/
 if(diferencia_latitud>0 and diferencia_longitud=0)then
direccion_calle:='N'; END IF;
/* caso cuando es Sur*/
 if(diferencia_latitud<0 and diferencia_longitud=0)then
direccion_calle:='S'; END IF;
/* caso cuando es Oeste*/
 if(diferencia_latitud=0 and diferencia_longitud<0)then
direccion_calle:='O'; END IF;
/* caso cuando es Este*/
 if(diferencia_latitud=0 and diferencia_longitud>0)then
direccion_calle:='E'; END IF;

/* extraigo el angulo de punto */
SELECT orientacion into angulo_punto from posicion where id=id_gps ;


/* a partir del angulo del punto, estimo la direccion del punto del gps*/
   /* caso cuando es norEste*/
   if(angulo_punto>0 and angulo_punto<90) then
direccion_punto:='NE'; END IF;
/* caso cuando es surEste*/
 if(angulo_punto>90 and angulo_punto<180)then
direccion_punto:='SE'; END IF;
   /*caso cuando es SurOeste*/
   if(angulo_punto>180 and angulo_punto<270)then
direccion_punto:='SO'; END IF;
/* caso cuando es NorOeste*/
  if(angulo_punto>270 and angulo_punto<360)then
direccion_punto:='NO'; END IF;
/* caso cuando es Norte*/
 if(angulo_punto=0)then
direccion_punto:='N'; END IF;
/* caso cuando es Sur*/
 if(angulo_punto=180)then
direccion_punto:='S'; END IF;
/* caso cuando es Oeste*/
 if(angulo_punto=270)then
direccion_punto:='O'; END IF;
/* caso cuando es Este*/
 if(angulo_punto=90)then
direccion_punto:='E'; END IF;


/* comparo ambas direcciones de la calle y el punto, para determinar el signo de la pendiente*/
/* caso en el que el tramo tiene igual direccion que el punto*/
if(direccion_calle=direccion_punto)then
SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
end if;

/* tramo y camion van en direccion opuesta */

/* caso en que el tramo tiene direccion Norte y el camion Sur*/
 if(direccion_calle='N' and direccion_punto='S')then
 SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
pendiente_punto:=(-1)*(pendiente_punto);

END IF;

/* caso en que el tramo tiene direccion Sur y el camion Norte */
 if(direccion_calle='S' and direccion_punto='N' )then
SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
pendiente_punto:=(-1)*(pendiente_punto); 

END if;

/* caso en que el tramo tiene direccion Oeste y el camion este*/
 if(direccion_calle='O' and direccion_punto='E')then
SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
pendiente_punto:=(-1)*(pendiente_punto); 
END IF;

/* caso en que el tramo tiene direccion Este y el camion Oeste*/
 if(direccion_calle='E'and direccion_punto='O' )then
SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
pendiente_punto:=(-1)*(pendiente_punto); 
END IF;

/* caso en que el tramo tiene direccion NorEste y el camion SurOeste*/
if(direccion_calle='NE' and direccion_punto='SO')then
SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
pendiente_punto:=(-1)*(pendiente_punto); 
END IF;

/* caso en que el tramo tiene direccion SurOeste y el camion Noreste*/
if(direccion_calle='SO' and direccion_punto='NE')then
SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
pendiente_punto:=(-1)*(pendiente_punto); 
END IF;

/* caso en que el tramo tiene direccion NorOeste y el camion SurEste*/
if(direccion_calle='NO' and direccion_punto='SE')then
SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
pendiente_punto:=(-1)*(pendiente_punto); 
END IF;

/* caso en que el tramo tiene direccion SurEste y el camion NorOeste*/
if(direccion_calle='SE' and direccion_punto='NO')then
SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
pendiente_punto:=(-1)*(pendiente_punto);  
END IF;

/* caso en el cual la direccion de la calle y del punto no son ni iguales ni opuesta si no se cruzan*/
if(direccion_calle!=direccion_punto)then
/* si pendiente_punto=0 quiere decir que el valor de la pendiente no fue modificado por ende  las direcciones no son iguales , ni opuesta sino se cruzan*/
if(pendiente_punto=0)then
	SELECT pendiente into pendiente_punto from pendientes  where id=id_pendiente ;
end if;


END IF;




  RETURN pendiente_punto;


END;
$$ LANGUAGE plpgsql;

