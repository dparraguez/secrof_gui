
<?php
$dia_anterior= date("Y-m-d");
$dia_anterior=strtotime('-1 day',strtotime($dia_actual));
$dia_anterior= date("Y-m-d",$dia_anterior);

$datos=null;
//extraer datos de la tabla posicion_gui

 $sql="SELECT * from posicion where to_timestamp(fecha)::date='$dia_anterior' order by fecha asc";
    $conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

    if($result!=false){


        for($l=0;$l<pg_num_rows($result);$l++)
        {

            $row=pg_fetch_array($result,$l);
            $datos[$l]=array(
               
  'latitud'=> $row['latitud'],
  'longitud' =>$row['longitud'],
  'velocidad' =>$row['velocidad'],
  'fecha' =>$row['fecha'],
  'orientacion'=> $row['orientacion'],
  'id_vehiculo' =>intval($row['id_vehiculo']),
  'geo'=>$row['geo'],

                );

        
        }


    }
    else{ $datos=null;}
    pg_free_result($result);
    pg_close($conexion);

//crear tabla

 $sql="CREATE TABLE posicion
(
  id serial NOT NULL,
  latitud numeric(9,6),
  longitud numeric(9,6),
  velocidad integer,
  fecha integer,
  orientacion integer,
  id_vehiculo integer,
  geo geography,
  CONSTRAINT posicion_pk PRIMARY KEY (id),
  CONSTRAINT posicion_fk1 FOREIGN KEY (id_vehiculo)
      REFERENCES vehiculo (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE posicion
  OWNER TO postgres;";
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");


//almacenar informacion
foreach ($datos as $key ) {
    $latitud=$key['latitud'];
    $longitud=$key['longitud'];
    $velocidad=$key['velocidad'];
    $fecha=$key['fecha'];
    $orientacion=$key['orientacion'];
    $id_vehiculo=$key['id_vehiculo'];
    $geo=$key['geo'];
$sql="INSERT INTO posicion (latitud,longitud,velocidad,fecha,orientacion,id_vehiculo,geo) VALUES($latitud,$longitud,$velocidad,'$fecha',$orientacion,$id_vehiculo,'$geo')"; 
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

}



//calcular consumo diario
 $sql="SELECT consumo_total_diario('$dia_anterior')";
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
    $mililitros= floatval(pg_fetch_result ($result,0,0));

//insertar valores en tabla de consumo consumo_diario
$sql="INSERT INTO consumo_diario (fecha,consumo) VALUES('$dia_anterior',$mililitros)"; 
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

// borrar tabla posicion temporal
$sql="DROP TABLE posicion"; 
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");


?>