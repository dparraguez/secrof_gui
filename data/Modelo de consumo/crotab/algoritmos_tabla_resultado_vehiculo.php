<?php
$dia_actual= date("Y-m-d");
//$dia_actual= '2015-05-10';
$dia_anterior=strtotime('-1 day',strtotime($dia_actual));
$dia_anterior= date("Y-m-d",$dia_anterior);

$datos=null;
$ids_vehiculos=null;
$datos_vehiculos=null;
$valorCombustible=530;
//extraer datos de la tabla posicion_gui

 $sql="SELECT * from posicion where to_timestamp(fecha)::date='$dia_anterior' order by fecha asc";
    $conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

    if($result!=false){


        for($l=0;$l<pg_num_rows($result);$l++)
        {

            $row=pg_fetch_array($result,$l);
            $datos[$l]=array(
               
  'latitud'=> $row['latitud'],
  'longitud' =>$row['longitud'],
  'velocidad' =>$row['velocidad'],
  'fecha' =>$row['fecha'],
  'orientacion'=> $row['orientacion'],
  'id_vehiculo' =>intval($row['id_vehiculo']),
  'geo'=>$row['geo'],

                );

        
        }


    }
    else{ $datos=null;}
    pg_free_result($result);
    pg_close($conexion);

//crear tabla

 $sql="CREATE TABLE posicion
(
  id serial NOT NULL,
  latitud numeric(9,6),
  longitud numeric(9,6),
  velocidad integer,
  fecha integer,
  orientacion integer,
  id_vehiculo integer,
  geo geography,
  CONSTRAINT posicion_pk PRIMARY KEY (id),
  CONSTRAINT posicion_fk1 FOREIGN KEY (id_vehiculo)
      REFERENCES vehiculo (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE posicion
  OWNER TO postgres;";
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

$l=0;
//almacenar informacion
foreach ($datos as $key ) {
    $latitud=$key['latitud'];
    $longitud=$key['longitud'];
    $velocidad=$key['velocidad'];
    $fecha=$key['fecha'];
    $orientacion=$key['orientacion'];
    $id_vehiculo=$key['id_vehiculo'];
    $geo=$key['geo'];
$sql="INSERT INTO posicion (latitud,longitud,velocidad,fecha,orientacion,id_vehiculo,geo) VALUES($latitud,$longitud,$velocidad,'$fecha',$orientacion,$id_vehiculo,'$geo')"; 
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

}


//calcular el consumo por vehiculo
//1- obtener los id_vehiculo
 $sql="SELECT DISTINCT id_vehiculo from posicion where to_timestamp(fecha)::date='$dia_anterior' ";
    $conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
 

        for($l=0;$l<pg_num_rows($result);$l++)
        {

            $row=pg_fetch_array($result,$l);
            $ids_vehiculos[$l]=array(             
  'id_vehiculo'=> $row['id_vehiculo'],
                );
        }
    pg_free_result($result);
    pg_close($conexion);


//extraer datos de los vehiculos
foreach ($ids_vehiculos as $key) {
    $idvehiculo=intval($key['id_vehiculo']);
$sql="SELECT patente,conductor,flota,tipo,id from vehiculo where id=$idvehiculo ";
    $conexion=pg_connect("host=localhost port=5432 dbname=secrof_gui user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
  if($result!=false){


        for($l=0;$l<pg_num_rows($result);$l++)
        {


            $row=pg_fetch_array($result,$l);
            $datos_vehiculos[$l]=array(             
  'id_vehiculo'=> $row['id'],
  'patente'=>$row['patente'],
'conductor'=>$row['conductor'],
'flota'=>$row['flota'],
'tipo'=>$row['tipo'],
                );

        
        }


    }
    else{ $datos_vehiculos[$l]=null;$l++;}
    pg_free_result($result);
    pg_close($conexion);


}

    foreach ($datos_vehiculos as $key ) {
      $idvehiculo=intval($key['id_vehiculo']);
//calcular consumo diario 
 $sql="SELECT consumo_total_diario_vehiculo( $idvehiculo)";
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
    $litros= floatval(pg_fetch_result ($result,0,0));
$valor=floatval($litros*$valorCombustible);
  $patente =$key['patente'];
  $conductor= $key['conductor'];
  $flota =$key['flota'];
  $tipo =$key['tipo'];

//insertar valores en tabla de consumo consumo_diario_vehiculo
$sql="INSERT INTO consumo_diario_vehiculo (fecha,consumo,patente,conductor,flota,tipo,valor) VALUES('$dia_anterior',$litros,'$patente','$conductor','$flota',$tipo,$valor )"; 
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
    }



    // borrar tabla posicion temporal
$sql="DROP TABLE posicion"; 
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

?>