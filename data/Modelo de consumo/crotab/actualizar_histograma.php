
<?
$histograma=null;
    $id_cuadras=null;
    $k=0;
    $dias=array("Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo");

    $hora_inicio='07:00:00';
    $hora_fin='07:30:00';
    $msg="no se ejecuto la consulta";
//1.extraer todos los id_cuadra distintos en promedio_cuadra
    $sql="SELECT DISTINCT(id_cuadra) from promedio_diario ORDER BY id_cuadra ";
    $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
    $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

    if($result!=false){


        for($l=0;$l<pg_num_rows($result);$l++)
        {

            $row=pg_fetch_array($result,$l);
            $id_cuadras[$l]=array(
                'id_cuadra'=>intval($row['id_cuadra']),


                );

        }


    }
    else{ $id_cuadras=null;}
    pg_free_result($result);
    pg_close($conexion);

    $i=0;
// entre los dias lunes y domingo
    while($i<7){
        $dia=$dias[$i];
// 1 el rango de hora es entra las 7:00:00  a 23:30:00 
        $hora_inicio='07:00:00';
        $hora_fin='07:30:00';
        while($hora_fin!='23:30:00'){

//2 recorre las cuadras, y calcular su histograma
            foreach ($id_cuadras as $row) {
                $idcuadra=$row['id_cuadra'];

  //$sql="SELECT COUNT(promedio_hora_pasada) as frecuencia from promedio_cuadra WHERE promedio_hora_pasada BETWEEN '$hora_inicio' and '$hora_fin' and dia='$dia' and id_cuadra=$idcuadra";
                $sql="SELECT COUNT(*) as frecuencia from promedio_diario WHERE promedio_hora_pasada>'$hora_inicio' and promedio_hora_pasada<'$hora_fin' and dia='$dia' and id_cuadra=$idcuadra";
                $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
                $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

                $var=pg_fetch_array($result,0);
                $histograma[$k]=array(
                    'id_cuadra'=>$idcuadra,
                    'dia'=>$dia,
                    'hora_inicio'=>$hora_inicio,
                    'hora_fin'=>$hora_fin,
                    'frecuencia'=>intval($var['frecuencia']),
                    );

                $k++;
            }
            pg_free_result($result);
            pg_close($conexion);

            $hora_inicio=strtotime('+1 second',strtotime($hora_fin));
            $hora_inicio= date('H:i:s',$hora_inicio);
            $hora_fin=strtotime('+30 minute',strtotime($hora_fin));
            $hora_fin= date('H:i:s',$hora_fin);
        }





        $i++;
//$hora_inicio='07:00:00';
//$hora_fin='07:30:00';

    }

// se borra la informacion de histograma
 $sql=" DELETE FROM histograma";
$conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

//reinicio el dato tipo serial de la tabla
$sql=" ALTER SEQUENCE histograma_id_histograma_seq restart WITH 1;";
$conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
$result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");

//se almacena la informacion obtenida en la tabla histograma (rango de horas, dia, frecuencia, id_calle (cuadra))
    foreach ($histograma as $fila) {
      $idcuadra=$fila['id_cuadra'];
      $dia=$fila['dia'];
      $hora_inicio=$fila['hora_inicio'];
      $hora_fin=$fila['hora_fin'];
      $frecuencia=$fila['frecuencia'];

      $sql="INSERT INTO histograma(id_cuadra,dia,hora_inicio,hora_fin,frecuencia) VALUES($idcuadra,'$dia','$hora_inicio','$hora_fin',$frecuencia)";
      $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
      $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
  }
  pg_free_result($result);
  pg_close($conexion);


//Establecer valor de la probabilidad en el histograma
    $k=0;
    $i=0;
    $prueba=null;
    $dia=null;

    while($i<7)
    {
        $dia=$dias[$i];

        foreach ($id_cuadras as $row ) {
         $idcuadra=$row['id_cuadra'];

         $sql="SELECT SUM(frecuencia) as casosposibles from histograma WHERE dia='$dia' and id_cuadra=$idcuadra";
        $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
         $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
         $var=pg_fetch_array($result,0);

         $casosPosibles=floatval($var['casosposibles']);
         $prueba[$k]=$casosPosibles;
         $k++;
         pg_free_result($result);
         pg_close($conexion);
         if($casosPosibles!=0){

            $sql="UPDATE histograma set probabilidad=(((frecuencia::double precision)/$casosPosibles)*100)::double precision where dia='$dia' and id_cuadra=$idcuadra";
            $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
            $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
            pg_free_result($result);
            pg_close($conexion);
        }
        else{
            $sql="UPDATE histograma set probabilidad=0 where dia='$dia' and id_cuadra=$idcuadra";
            $conexion=pg_connect("host=localhost port=5432 dbname=consumo user=postgres password=postgres...");
            $result = pg_query($conexion,$sql ) or die("Error en la consulta SQL");
            pg_free_result($result);
            pg_close($conexion);
        }


    }

    $i++;
}




  ?>