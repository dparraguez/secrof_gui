--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: prioridad; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE prioridad AS ENUM (
    'Alta',
    'Media',
    'Baja',
    'Derivar'
);


ALTER TYPE public.prioridad OWNER TO postgres;

--
-- Name: tipo; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE tipo AS ENUM (
    'Sugerencia',
    'Felicitación'
);


ALTER TYPE public.tipo OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_inspector; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_inspector (
    in_id integer NOT NULL,
    in_nombres character varying(25),
    in_apellidos character varying(25),
    in_rut character varying(15),
    in_fono character varying(15),
    in_clave character varying(25)
);


ALTER TABLE public.app_inspector OWNER TO postgres;

--
-- Name: app_inspector_in_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_inspector_in_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_inspector_in_id_seq OWNER TO postgres;

--
-- Name: app_inspector_in_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_inspector_in_id_seq OWNED BY app_inspector.in_id;


--
-- Name: app_reclamo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_reclamo (
    re_id integer NOT NULL,
    re_descripcion character varying(250),
    re_fecha_app date,
    re_estadoactual character varying(15) DEFAULT 'Recepcionado'::character varying,
    re_hora_app character varying(5),
    re_foto character varying(100),
    re_latitud numeric(9,6),
    re_longitud numeric(9,6),
    re_prioridad prioridad,
    us_id integer,
    ti_id integer
);


ALTER TABLE public.app_reclamo OWNER TO postgres;

--
-- Name: app_reclamo_re_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_reclamo_re_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_reclamo_re_id_seq OWNER TO postgres;

--
-- Name: app_reclamo_re_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_reclamo_re_id_seq OWNED BY app_reclamo.re_id;


--
-- Name: app_reporte; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_reporte (
    rp_id integer NOT NULL,
    rp_descripcion character varying(250),
    rp_fecha_app date,
    rp_hora_app character varying(5),
    rp_foto character varying(100),
    rp_latitud numeric(9,6),
    rp_longitud numeric(9,6),
    rp_estado character varying(15),
    rp_fecha_hora_web timestamp without time zone,
    in_id integer
);


ALTER TABLE public.app_reporte OWNER TO postgres;

--
-- Name: app_reporte_rp_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_reporte_rp_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_reporte_rp_id_seq OWNER TO postgres;

--
-- Name: app_reporte_rp_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_reporte_rp_id_seq OWNED BY app_reporte.rp_id;


--
-- Name: app_sugerencia; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_sugerencia (
    su_id integer NOT NULL,
    su_tipo tipo,
    su_descripcion character varying(250),
    su_fecha_app date,
    su_hora_app character varying(5),
    su_latitud numeric(9,6),
    su_longitud numeric(9,6),
    su_estado character varying(15),
    su_fecha_hora_web timestamp without time zone,
    us_id integer
);


ALTER TABLE public.app_sugerencia OWNER TO postgres;

--
-- Name: app_sugerencia_su_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_sugerencia_su_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_sugerencia_su_id_seq OWNER TO postgres;

--
-- Name: app_sugerencia_su_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_sugerencia_su_id_seq OWNED BY app_sugerencia.su_id;


--
-- Name: app_usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE app_usuario (
    us_id integer NOT NULL,
    us_nombres character varying(25),
    us_apellidos character varying(25),
    us_rut character varying(15),
    us_fono character varying(15)
);


ALTER TABLE public.app_usuario OWNER TO postgres;

--
-- Name: app_usuario_us_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE app_usuario_us_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.app_usuario_us_id_seq OWNER TO postgres;

--
-- Name: app_usuario_us_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE app_usuario_us_id_seq OWNED BY app_usuario.us_id;


--
-- Name: reclamo_estado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE reclamo_estado (
    re_id integer NOT NULL,
    es_id integer NOT NULL,
    re_es_fecha timestamp without time zone
);


ALTER TABLE public.reclamo_estado OWNER TO postgres;

--
-- Name: tiene_sucesor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tiene_sucesor (
    es_id integer NOT NULL,
    web_es_id integer NOT NULL
);


ALTER TABLE public.tiene_sucesor OWNER TO postgres;

--
-- Name: web_estado; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE web_estado (
    es_id integer NOT NULL,
    es_nombre character varying(25),
    es_descripcion character varying(150)
);


ALTER TABLE public.web_estado OWNER TO postgres;

--
-- Name: web_estado_es_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE web_estado_es_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.web_estado_es_id_seq OWNER TO postgres;

--
-- Name: web_estado_es_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE web_estado_es_id_seq OWNED BY web_estado.es_id;


--
-- Name: web_tipo_reclamo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE web_tipo_reclamo (
    ti_id integer NOT NULL,
    ti_nombre character varying(20),
    ti_prioridad integer
);


ALTER TABLE public.web_tipo_reclamo OWNER TO postgres;

--
-- Name: web_tipo_reclamo_ti_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE web_tipo_reclamo_ti_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.web_tipo_reclamo_ti_id_seq OWNER TO postgres;

--
-- Name: web_tipo_reclamo_ti_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE web_tipo_reclamo_ti_id_seq OWNED BY web_tipo_reclamo.ti_id;


--
-- Name: in_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_inspector ALTER COLUMN in_id SET DEFAULT nextval('app_inspector_in_id_seq'::regclass);


--
-- Name: re_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_reclamo ALTER COLUMN re_id SET DEFAULT nextval('app_reclamo_re_id_seq'::regclass);


--
-- Name: rp_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_reporte ALTER COLUMN rp_id SET DEFAULT nextval('app_reporte_rp_id_seq'::regclass);


--
-- Name: su_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_sugerencia ALTER COLUMN su_id SET DEFAULT nextval('app_sugerencia_su_id_seq'::regclass);


--
-- Name: us_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_usuario ALTER COLUMN us_id SET DEFAULT nextval('app_usuario_us_id_seq'::regclass);


--
-- Name: es_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_estado ALTER COLUMN es_id SET DEFAULT nextval('web_estado_es_id_seq'::regclass);


--
-- Name: ti_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY web_tipo_reclamo ALTER COLUMN ti_id SET DEFAULT nextval('web_tipo_reclamo_ti_id_seq'::regclass);


--
-- Data for Name: app_inspector; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_inspector (in_id, in_nombres, in_apellidos, in_rut, in_fono, in_clave) FROM stdin;
1	Nieves	Riffo	17872999-3		\N
\.


--
-- Name: app_inspector_in_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_inspector_in_id_seq', 1, true);


--
-- Data for Name: app_reclamo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_reclamo (re_id, re_descripcion, re_fecha_app, re_estadoactual, re_hora_app, re_foto, re_latitud, re_longitud, re_prioridad, us_id, ti_id) FROM stdin;
23	Hay muchisimos escombros en frente de mi casa	\N	Derivado	\N	\N	-36.816761	-73.041315	Derivar	1	8
1	Hay escombros en la vía pública.	2016-03-08	Derivado	07:34	../web/images/ciudadanos/2016/03/pic_20163208073243.png	-36.818120	-73.042352	Derivar	1	8
25	Hay mucha basura en la esquina de freire con anibal pinto	\N	Cerrado	\N	\N	-36.815057	-73.039770	Alta	1	7
24	hay un microbasural en la calle freire	2016-03-11	Cerrado	12:29		-36.819029	-73.038642	Alta	1	4
41	No recolectar el todo o parte de los residuos, tanto en  calles como espacios públicos.	2016-03-16	Recepcionado	00:49		-36.821721	-73.005059	Alta	10	6
21	Hay un camión abandonado en la via publica desde hace cinco horas	\N	Cerrado	\N	\N	-36.822807	-73.043032	Alta	1	1
26	los trabajadores no se encuentran con la ropa adecuada	\N	Cerrado	\N	\N	-36.822329	-73.021221	Media	1	5
34	Personal sin ropa de lluvia, cuando corresponde.	2016-03-15	Cerrado	22:58		-36.821795	-73.005107	Media	10	5
35	Hay escombros en la vía pública.	2016-03-15	Derivado	23:46		-36.821803	-73.005058	Derivar	10	8
39	Personal sin ropa de lluvia, cuando corresponde.	2016-03-16	Cerrado	00:48		-36.821751	-73.005055	Media	10	5
18	Falta de personal, menos de tres personas.	2016-03-08	Recepcionado	21:42	../web/images/ciudadanos/2016/03/pic_20164108094137.png	-36.806117	-72.956146	Media	3	5
42	vehículo abandonado fuera de la universidad de concepcion	\N	Recepcionado	\N	\N	-36.827856	-73.034782	Alta	12	1
31	Hay escombros en la vía pública.	2016-03-15	Derivado	12:51		-36.821762	-73.011741	Derivar	10	8
30	No eliminan microbasurales.	2016-03-15	Cerrado	12:48		-36.821740	-73.011787	Alta	10	4
29	No recolectar el todo o parte de los residuos, tanto en  calles como espacios públicos.	2016-03-15	Cerrado	12:21		-36.821736	-73.011782	Alta	10	6
36	Abandono del vehículo cargado en la vía pública.	2016-03-15	Cerrado	23:47		-36.821756	-73.005094	Alta	10	1
43	los sábados está lleno de basura por la feria sector collao	\N	Recepcionado	\N	\N	-36.815626	-73.021908	Derivar	6	8
32	Identificación inadecuada en los  vehículos o falta de ésta.	2016-03-15	Cerrado	12:51		-36.821768	-73.011739	Media	10	3
20	Hay un microbasural en la calle maipu con anibal pinto	\N	Cerrado	\N	\N	-36.824312	-73.051164	Alta	3	4
38	No limpian los sumideros de aguas lluvia (alcantarillas).	2016-03-16	Cerrado	00:23	../web/images/ciudadanos/2016/03/pic_20162316122331.png	-36.821812	-73.005154	Media	11	9
22	Hay muchisima basura en la calle	\N	Cerrado	\N	\N	-36.825143	-73.047838	Alta	1	7
44	Collao lleno de basura los dias sabados	\N	Recepcionado	\N	\N	-36.817412	-73.011093	Alta	7	4
19	No eliminan microbasurales.	2016-03-09	Leído	19:20	../web/images/ciudadanos/2016/03/pic_20161909071916.png	-36.817319	-73.042291	Alta	3	4
37	No recolectar el todo o parte de la basura domiciliaria.	2016-03-16	Leído	00:23		-36.821738	-73.005029	Alta	11	7
27	Personal sin ropa de lluvia, cuando corresponde.	2016-03-15	Cerrado	10:04		-36.821747	-73.005052	Media	9	5
40	Filtración de líquidos en los vehículos.	2016-03-16	Leído	00:48		-36.821734	-73.005059	Alta	10	2
28	Falta de higiene en el vestuario del personal, comportamientos impropios.	2016-03-15	Cerrado	12:21		-36.821771	-73.011737	Media	10	5
33	Falta de accesorios,  higiene, etc. en los vehículos.	2016-03-15	Cerrado	22:58		-36.821749	-73.005031	Media	10	3
45	Hay un camión detenido	\N	Recepcionado	\N	\N	-36.826517	-73.048182	Alta	1	1
46	Hay un camión abandonado en Freire	\N	Recepcionado	\N	\N	-36.835861	-73.057966	Alta	1	1
\.


--
-- Name: app_reclamo_re_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_reclamo_re_id_seq', 46, true);


--
-- Data for Name: app_reporte; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_reporte (rp_id, rp_descripcion, rp_fecha_app, rp_hora_app, rp_foto, rp_latitud, rp_longitud, rp_estado, rp_fecha_hora_web, in_id) FROM stdin;
1	El camión matricula FG1254 tiene una fuga de liquido	2016-03-09	20:01		-36.821994	-73.040756	Recepcionado	2016-03-09 20:02:55	1
2	hay un camion en freire 234	2016-03-11	00:35	../web/images/inspectores/2016/03/pic_20163511123537.png	-36.821712	-73.042626	Recepcionado	2016-03-11 00:37:38	1
3	en el parque ecuador se encuentra muchas latas,botellas plásticas, por lo que se llamó a la gente de la ordenanza municipal para que la retirara\n	2016-03-11	00:52	../web/images/logo_negro.png	-36.822708	-73.043021	Recepcionado	2016-03-11 00:54:10	1
4	prueba foto hola\n	2016-03-11	05:28	../web/images/inspectores/2016/03/pic_20162611052647.png	-36.821323	-73.042261	Recepcionado	2016-03-11 05:29:48	1
5	Hay un camión detenido por varias horas en freire con rengo	2016-03-11	06:35		-36.820003	-73.042383	Recepcionado	2016-03-11 06:37:16	1
6	el camión patente fg567 tiene un fuga de líquido	2016-03-11	12:35		-36.819418	-73.041105	Recepcionado	2016-03-11 12:37:35	1
7	hay un camion en freire	2016-03-16	09:43		-36.822643	-73.042869	Recepcionado	2016-03-16 09:45:27	1
8	hay una fuga en un camion	2016-03-16	09:44		-36.821612	-73.011536	Recepcionado	2016-03-16 09:45:49	1
9	ggghdhhddh hdhd\n	2016-03-16	09:53		-36.821631	-73.011471	Recepcionado	2016-03-16 09:54:43	1
\.


--
-- Name: app_reporte_rp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_reporte_rp_id_seq', 9, true);


--
-- Data for Name: app_sugerencia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_sugerencia (su_id, su_tipo, su_descripcion, su_fecha_app, su_hora_app, su_latitud, su_longitud, su_estado, su_fecha_hora_web, us_id) FROM stdin;
1	Felicitación	Felicitaciones al personal de los camiones, son muy amables	2016-03-08	07:32	-36.820544	-36.820544	Recepcionado	2016-03-08 07:34:03	1
2	Sugerencia	podría pasar el vehículo mas tarde por mi sector	\N	\N	-36.821241	-73.042173	Recepcionado	2016-03-08 08:21:15	2
4	Sugerencia	Avisen con anticipación cuando no pase el camión.	2016-03-08	09:11	-36.821545	-73.011676	Recepcionado	2016-03-08 09:12:46	3
6	Felicitación	Felicitaciones al personal de los camiones, son muy amables	2016-03-08	21:47	-36.821150	-73.043584	Recepcionado	2016-03-08 21:48:38	3
7	Sugerencia	Deberian limpiar con mayor regularidad los sumideros de agua lluvia	\N	\N	-36.815936	-73.048010	Recepcionado	2016-03-11 08:20:09	1
9	Felicitación	Trabajadores muy respetuosos	\N	\N	-36.817723	-73.045263	Recepcionado	2016-03-11 08:28:37	1
8	Felicitación	Tienen un maravilloso servicio	\N	\N	-36.823494	-73.052130	Leído	2016-03-11 08:25:10	1
11	Felicitación	Los chóferes manejan prudentemente	\N	\N	-36.816706	-73.038568	Recepcionado	2016-03-11 14:30:10	2
14	Felicitación	Felicitaciones al personal de los camiones, son muy amables	2016-03-15	12:21	-36.821780	-36.821780	Leído	2016-03-15 12:21:19	10
13	Felicitación	Felicitaciones al personal de los camiones, son muy amables	2016-03-15	10:03	-36.821774	-36.821774	Leído	2016-03-15 10:04:02	9
12	Felicitación	el servicio por el sector de nonguen es excelente	\N	\N	-36.827550	-73.004227	Leído	2016-03-15 02:03:07	8
10	Felicitación	los trabajadores son muy amables	2016-03-11	12:28	-36.817730	-36.817730	Leído	2016-03-11 12:29:58	1
15	Felicitación	Excelente servicio en Nonguen	\N	\N	-36.829779	-73.003025	Leído	2016-03-16 00:56:56	7
16	Felicitación	Excelente servicio en Nonguen	\N	\N	-36.828817	-73.010406	Recepcionado	2016-03-16 01:04:24	12
17	Felicitación	excelente servicio en Nonguen	\N	\N	-36.829779	-73.009377	Recepcionado	2016-03-16 01:06:59	13
18	Felicitación	excelente servicio en carrera	\N	\N	-36.825245	-73.050919	Leído	2016-03-16 01:08:29	14
19	Sugerencia	Mejorar servicio	\N	\N	-36.829678	-73.053846	Recepcionado	2016-03-23 23:32:43	1
20	Sugerencia	mejorar servicio	\N	\N	-36.825556	-73.052130	Recepcionado	2016-03-23 23:37:42	1
\.


--
-- Name: app_sugerencia_su_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_sugerencia_su_id_seq', 20, true);


--
-- Data for Name: app_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY app_usuario (us_id, us_nombres, us_apellidos, us_rut, us_fono) FROM stdin;
1	Nieves	Riffo	17872999-3	
2	Felipe	Riffo	19109189-2	90957288
3	tatiana	T G	22222222-2	
4	Patricio galvez	Galvez	6363560-k	
5	Estefania	Sandoval	17055963-0	4235267
6	Silvia	pineda	10930665-7	1234567
7	Silvia	Pineda	7603794-9	2345787
8	cecilia	escobar	10007292-0	53628284
9	esteban	sandoval	10456073-3	
10	carolina	zuñiga	13972485-2	
11	Jenyfer	Sandoval	16620769-k	
12	Carlos	pinto	19120807-2	73468763482
13	juana	Perez	10937331-1	67362627
14	maria	parra	17744065-5	263673788
\.


--
-- Name: app_usuario_us_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('app_usuario_us_id_seq', 14, true);


--
-- Data for Name: reclamo_estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY reclamo_estado (re_id, es_id, re_es_fecha) FROM stdin;
1	1	2016-03-08 07:35:47
38	3	2016-03-16 03:15:06
1	4	2016-03-08 10:40:17
33	2	2016-03-16 03:17:05
37	2	2016-03-16 03:17:09
27	3	2016-03-16 03:17:13
28	2	2016-03-16 03:17:17
28	3	2016-03-16 03:19:55
39	1	2016-03-16 00:49:05
40	1	2016-03-16 00:49:16
41	1	2016-03-16 00:49:25
34	2	2016-03-16 03:51:07
34	3	2016-03-16 03:51:11
39	2	2016-03-16 03:52:48
39	3	2016-03-16 03:52:53
42	1	2016-03-16 00:56:29
30	2	2016-03-16 04:00:55
30	3	2016-03-16 04:00:59
43	1	2016-03-16 01:06:20
44	1	2016-03-16 01:07:51
40	2	2016-03-16 04:08:50
33	3	2016-03-16 04:08:57
45	1	2016-03-23 23:09:11
46	1	2016-03-23 23:32:16
18	1	2016-03-08 21:44:23
19	1	2016-03-09 19:22:23
20	1	2016-03-11 08:08:24
21	1	2016-03-11 08:14:46
22	1	2016-03-11 08:24:17
23	1	2016-03-11 08:29:37
24	1	2016-03-11 12:30:55
20	2	2016-03-11 17:26:43
20	3	2016-03-11 17:26:47
22	2	2016-03-11 17:26:51
22	3	2016-03-11 17:26:55
25	1	2016-03-11 14:29:13
26	1	2016-03-15 02:02:07
27	1	2016-03-15 10:04:38
28	1	2016-03-15 12:21:30
29	1	2016-03-15 12:21:43
30	1	2016-03-15 12:49:13
31	1	2016-03-15 12:51:29
32	1	2016-03-15 12:52:03
23	4	2016-03-15 15:54:52
25	2	2016-03-15 15:58:48
24	2	2016-03-15 15:58:56
25	3	2016-03-15 15:59:08
24	3	2016-03-15 15:59:13
21	2	2016-03-16 01:57:54
21	3	2016-03-16 01:58:03
26	2	2016-03-16 01:58:19
33	1	2016-03-15 22:58:46
34	1	2016-03-15 22:59:09
26	3	2016-03-16 02:02:03
35	1	2016-03-15 23:47:06
36	1	2016-03-15 23:47:30
37	1	2016-03-16 00:05:50
38	1	2016-03-16 00:06:10
29	3	2016-03-16 03:13:21
36	3	2016-03-16 03:13:26
\.


--
-- Data for Name: tiene_sucesor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tiene_sucesor (es_id, web_es_id) FROM stdin;
\.


--
-- Data for Name: web_estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY web_estado (es_id, es_nombre, es_descripcion) FROM stdin;
1	Recepcionado	Estado inicial del reclamo recibido
2	Leído	El reclamo ya ha sido visto por el fiscalizador
3	Cerrado	Al reclamo ya se le dio una solución
4	Derivado	El reclamo es tipo Escombros por lo que se deriva
\.


--
-- Name: web_estado_es_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('web_estado_es_id_seq', 4, true);


--
-- Data for Name: web_tipo_reclamo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY web_tipo_reclamo (ti_id, ti_nombre, ti_prioridad) FROM stdin;
1	Vehículo abandonado	4
2	Vehículo filtración	3
3	Vehículo otros	1
4	Microbasural	4
5	Trabajadores	2
6	Calles	3
7	Residuos	5
8	Escombros	0
9	Sumideros	2
\.


--
-- Name: web_tipo_reclamo_ti_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('web_tipo_reclamo_ti_id_seq', 9, true);


--
-- Name: inspector_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_inspector
    ADD CONSTRAINT inspector_pk PRIMARY KEY (in_id);


--
-- Name: reclamo_estado_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY reclamo_estado
    ADD CONSTRAINT reclamo_estado_pk PRIMARY KEY (re_id, es_id);


--
-- Name: reclamo_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_reclamo
    ADD CONSTRAINT reclamo_pk PRIMARY KEY (re_id);


--
-- Name: reporte_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_reporte
    ADD CONSTRAINT reporte_pk PRIMARY KEY (rp_id);


--
-- Name: sugerencia_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_sugerencia
    ADD CONSTRAINT sugerencia_pk PRIMARY KEY (su_id);


--
-- Name: tiene_sucesor_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tiene_sucesor
    ADD CONSTRAINT tiene_sucesor_pk PRIMARY KEY (es_id, web_es_id);


--
-- Name: usuario_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY app_usuario
    ADD CONSTRAINT usuario_pk PRIMARY KEY (us_id);


--
-- Name: web_estado_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY web_estado
    ADD CONSTRAINT web_estado_pk PRIMARY KEY (es_id);


--
-- Name: web_tipo_reclamo_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY web_tipo_reclamo
    ADD CONSTRAINT web_tipo_reclamo_pk PRIMARY KEY (ti_id);


--
-- Name: reclamo_estado_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reclamo_estado
    ADD CONSTRAINT reclamo_estado_fk1 FOREIGN KEY (re_id) REFERENCES app_reclamo(re_id);


--
-- Name: reclamo_estado_fk2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY reclamo_estado
    ADD CONSTRAINT reclamo_estado_fk2 FOREIGN KEY (es_id) REFERENCES web_estado(es_id);


--
-- Name: reclamo_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_reclamo
    ADD CONSTRAINT reclamo_fk1 FOREIGN KEY (us_id) REFERENCES app_usuario(us_id);


--
-- Name: reclamo_fk2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_reclamo
    ADD CONSTRAINT reclamo_fk2 FOREIGN KEY (ti_id) REFERENCES web_tipo_reclamo(ti_id);


--
-- Name: reporte_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_reporte
    ADD CONSTRAINT reporte_fk1 FOREIGN KEY (in_id) REFERENCES app_inspector(in_id);


--
-- Name: sugerencia_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY app_sugerencia
    ADD CONSTRAINT sugerencia_fk1 FOREIGN KEY (us_id) REFERENCES app_usuario(us_id);


--
-- Name: tiene_sucesor_fk1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tiene_sucesor
    ADD CONSTRAINT tiene_sucesor_fk1 FOREIGN KEY (es_id) REFERENCES web_estado(es_id);


--
-- Name: tiene_sucesor_fk2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tiene_sucesor
    ADD CONSTRAINT tiene_sucesor_fk2 FOREIGN KEY (web_es_id) REFERENCES web_estado(es_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

